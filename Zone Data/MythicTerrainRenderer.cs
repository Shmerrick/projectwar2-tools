﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MypLib
{
    public class MythicTerrainRenderer
    {
        private static Dictionary<string, DDSImage> _images = new Dictionary<string, DDSImage>();
        private static Dictionary<int, Map> _maps = new Dictionary<int, Map>();
        private static Dictionary<string, int[][]> _scaledImg = new Dictionary<string, int[][]>();
        private static Dictionary<string, int[][]> _masks = new Dictionary<string, int[][]>();
        private static unsafe int[][] BitmapToArray(Bitmap bmp)
        {
            int[][] arr = new int[bmp.Height][];
            for (int i = 0; i < bmp.Height; i++)
                arr[i] = new int[bmp.Width];

            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);

            // Get the address of the first line.
            byte* ptr = (byte*)bmpData.Scan0;

            for (int y = 0; y < bmp.Height; y++)
            {
                var row = ptr + (y * bmpData.Stride);

                for (int x = 0; x < bmp.Width; x++)
                {
                    var pixel = row + x * 4; // ARGB has 4 bytes per pixel

                    arr[y][x] = pixel[2] << 24 | pixel[0] << 16 | pixel[1] << 8 | pixel[3];
                }
            }

            bmp.UnlockBits(bmpData);
            return arr;
        }

        private static unsafe void ArrayToBitmap(Bitmap bmp, int[][] source)
        {

            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);

            // Get the address of the first line.
            byte* ptr = (byte*)bmpData.Scan0;
            Random r = new Random();
            for (int y = 0; y < bmp.Height; y++)
            {

                for (int x = 0; x < bmp.Width; x++)
                {
                    int pos = 4 * x + bmpData.Stride * y;
                    ptr[pos] = (byte)(source[y][x] >> 16 & 0xFF); //blue
                    ptr[pos + 1] = (byte)(source[y][x] >> 8 & 0xFF); //green
                    ptr[pos + 2] = (byte)(source[y][x] >> 24 & 0xFF); ; //red
                    ptr[pos + 3] = (byte)(source[y][x] & 0xFF); ; //alpha
                }
            }

            bmp.UnlockBits(bmpData);
        }

        private static void Blit(int[][] dest, int destX, int destY, int destWidth, int destHeight, int[][] source, int sourceWidth, int sourceHeight)
        {
            Random r = new Random();
            for (int y = 0; y < sourceHeight && (destY + y < destHeight); y++)
            {
                int destOffset = destX * 4;
                int count = sourceWidth * 4;
                if (destX + sourceWidth >= destWidth)
                {
                    count = (destWidth * 4) - destOffset;
                }
                Buffer.BlockCopy(source[y], 0, dest[destY + y], destOffset, count);
            }
        }

        private static void BlitWithAlpha(int[][] dest, int destX, int destY, int destWidth, int destHeight, int[][] source, int sourceWidth, int sourceHeight, int[][] mask, int maskWidth, int maskHeight, 
            double maskTileSize, double maskOffX, double maskOffY, int maskChannel, LayerItem layer, float mult=1.0f)
        {
            //if (maskChannel != 0)
            //    return;

            int shift = 0;
            int shiftC = 0;

            if (maskChannel == 0)
                shift = 24;
            else if (maskChannel == 1)
                shift = 8;
            else if (maskChannel == 2)
                shift = 16;

            double maskScaleX = (double)maskTileSize / (double)sourceWidth;
            double maskScaleY = (double)maskTileSize / (double)sourceHeight;

            for (int x = 0; x < sourceWidth; x++)
                for (int y = 0; y < sourceHeight; y++)
                {

                    double maskX = ((((double)x * maskScaleX) + maskOffX));
                    double maskY = ((((double)y * maskScaleY) + maskOffY));

                    int dx = x + destX;
                    int dy = y + destY;

                    if (x + destX >= destWidth)
                        dx = destWidth - 1;
                    if (y + destY >= destHeight)
                        dy = destHeight - 1;

                    int mx = (int)maskX;
                    int my = (int)maskY;

                    if (mx >= maskWidth)
                        mx = maskWidth - 1;
                    if (my >= maskHeight)
                        my = maskHeight - 1;

                    byte destAlpha = (byte)(dest[dy][dx] & 0xFF);
                    byte srcAlpha = (byte)(mask[(int)my][(int)mx] >> shift & 0xFF);
                    byte srcAlpha1 = srcAlpha;
                    byte srcAlpha2 = srcAlpha;
                    byte srcAlpha3 = srcAlpha;
                    byte srcAlpha4 = srcAlpha;
                    byte srcAlpha5 = srcAlpha;
                    byte srcAlpha6 = srcAlpha;
                    byte srcAlpha7 = srcAlpha;
                    byte srcAlpha8 = srcAlpha;

                    if (mx - 1 >= 0 && my - 1 >= 0)
                        srcAlpha1 = (byte)(mask[(int)my - 1][(int)mx - 1] >> shift & 0xFF);

                    if (my - 1 >= 0)
                        srcAlpha2 = (byte)(mask[(int)my - 1][(int)mx] >> shift & 0xFF);

                    if (mx + 1 < maskWidth && my - 1 >= 0)
                        srcAlpha3 = (byte)(mask[(int)my - 1][(int)mx + 1] >> shift & 0xFF);

                    if (mx + 1 < maskWidth)
                        srcAlpha4 = (byte)(mask[(int)my][(int)mx + 1] >> shift & 0xFF);

                    if (mx + 1 < maskWidth && my + 1 < maskHeight)
                        srcAlpha5 = (byte)(mask[(int)my + 1][(int)mx + 1] >> shift & 0xFF);

                    if (my + 1 < maskHeight)
                        srcAlpha6 = (byte)(mask[(int)my + 1][(int)mx] >> shift & 0xFF);

                    if (mx - 1 >= 0 && my + 1 < maskHeight)
                        srcAlpha7 = (byte)(mask[(int)my + 1][(int)mx - 1] >> shift & 0xFF);

                    if (mx - 1 >= 0)
                        srcAlpha8 = (byte)(mask[(int)my][(int)mx - 1] >> shift & 0xFF);


                    srcAlpha = (byte)((srcAlpha + srcAlpha1 + srcAlpha2 + srcAlpha3 + srcAlpha4
                        + srcAlpha5 + srcAlpha6 + srcAlpha7 + srcAlpha8) / 9);

                    byte destR = (byte)(dest[dy][dx] >> 24 & 0xFF);
                    byte destG = (byte)(dest[dy][dx] >> 8 & 0xFF);
                    byte destB = (byte)(dest[dy][dx] >> 16 & 0xFF);

                    byte srcR = (byte)(source[y][x] >> 24 & 0xFF);
                    byte srcG = (byte)(source[y][x] >> 8 & 0xFF);
                    byte srcB = (byte)(source[y][x] >> 16 & 0xFF);

                    double amount = (srcAlpha);

                    float rv = (float)srcR * mult;
                    float gv = (float)srcG * mult;
                    float bv = (float)srcB * mult;

                    if (rv > 255)
                        rv = 255;
                    if (gv > 255)
                        gv = 255;
                    if (bv > 255)
                        bv = 255;

                    srcR = (byte)rv;
                    srcG = (byte)gv;
                    srcB = (byte)bv;

                    byte r = (byte)((srcR * amount / 255) + destR * (255 - amount) / 255);
                    byte g = (byte)((srcG * amount / 255) + destG * (255 - amount) / 255);
                    byte b = (byte)((srcB * amount / 255) + destB * (255 - amount) / 255);

                  
                    dest[dy][dx] = (int)(r << 24 | b << 16 | g << 8 | 255);

                }
        }

        private static void PaintChannel(int[][] dest, int scale, int maskWidth, int maskHeight, int[][] mask, int channel, LayerItem layer, float mult=1.0f)
        {
            if (!layer.Tilable)
                return;

            double tileCount = (int)(double)Math.Sqrt(layer.NumTiles);
            double maskSize = (double)maskWidth / tileCount;
            int tileSize = (int)Math.Ceiling((double)scale / tileCount);

            if (!_images.ContainsKey(layer.TextureName))
                return;

            var texture = _images[layer.TextureName].BitmapImage;

            for (int x = 0; x < tileCount; x++)
                for (int y = 0; y < tileCount; y++)
                {
                    string path = layer.TextureName + "_" + tileSize;
                    if (!_scaledImg.ContainsKey(path))
                    {
                        _scaledImg[path] = new int[tileSize][];
                        for (int i = 0; i < tileSize; i++)
                            _scaledImg[path][i] = new int[tileSize];

                        using (Bitmap b = new Bitmap(tileSize, tileSize))
                        {
                            using (Graphics gs = Graphics.FromImage(b))
                            {
                                gs.DrawImage(texture, new Rectangle(0, 0, tileSize, tileSize), new Rectangle(0, 0, texture.Width, texture.Height), GraphicsUnit.Pixel);
                                _scaledImg[path] = BitmapToArray(b);
                            }
                        }
                    }
                    BlitWithAlpha(dest, x * tileSize, y * tileSize, scale, scale, _scaledImg[path], tileSize, tileSize, mask, maskWidth, maskHeight, maskSize, (x * maskSize), (y * maskSize), layer.Channel, layer, mult);
                }


        }

        private class LayerItem
        {
            public string TextureName;
            public float Rotate;
            public float TranslateU;
            public float TranslateV;
            public float ScaleU;
            public float ScaleV;
            public int NumTiles;
            public bool Tilable;
            public int MaskType;
            public int WindowSize;
            public int Channel;
            public int X;
            public int Y;
            public int ImageIndex;
            public override string ToString()
            {
                return TextureName;
            }
        }

        private class Map
        {
            public List<LayerItem>[,] Layers = new List<LayerItem>[8, 8];
        }

        public static Bitmap RenderCell(MYPManager manager, int zoneID, int col, int row, int cellSize, float mult=1.0f)
        {
            int[][] _preview = new int[cellSize][];

            for (int i = 0; i < cellSize; i++)
                _preview[i] = new int[cellSize];

            if (!_maps.ContainsKey(zoneID))
                _maps[zoneID] = LoadMap(manager, zoneID);

            if (_maps.ContainsKey(zoneID))
            {
                var map = _maps[zoneID];
                Bitmap bmp = new Bitmap(cellSize, cellSize);

                using (Graphics gr = Graphics.FromImage(bmp))
                {
                    int x = col * cellSize;
                    int y = row * cellSize;

                    foreach (var layer in map.Layers[col, row])
                    {
                        string path1 = "zones/zone" + zoneID.ToString().PadLeft(3, '0') + "/textures/patch" + col.ToString().PadLeft(2, '0') + row.ToString().PadLeft(2, '0') + "-" + layer.ImageIndex.ToString().PadLeft(2, '0') + ".dds";
                        if (!_images.ContainsKey(path1))
                        {
                            var asset = manager.GetAsset(path1);
                            if (asset != null)
                            {
                                var ds = new DDSImage(manager.GetAsset(path1));
                                _images[path1] = ds;
                                _masks[path1] = BitmapToArray(_images[path1].BitmapImage);
                            }
                        }

                        if (_images.ContainsKey(path1))
                            PaintChannel(_preview, cellSize, _images[path1].BitmapImage.Width, _images[path1].BitmapImage.Height, _masks[path1], layer.Channel, layer, mult);
                    }

                    ArrayToBitmap(bmp, _preview);
                    return bmp;
                }
            }


            return null;
        }

        private static Map LoadMap(MYPManager manager, int zoneID)
        {
            var asset = manager.GetAssetStringASCII("zones/zone" + zoneID.ToString().PadLeft(3, '0') + "/textures/textures.csv");
            var map = new Map();

            if (asset != null)
            {
                var csv = new CSV(asset);

                int col = 0;
                int row = 0;
                int count = 0;
                int image = 0;

                while (!csv.EOF)
                {
                    csv.NextRow();
                    if (csv.Lines.Count == 0)
                        break;

                    if (csv.Row.Count == 1)
                    {
                        if (csv.EOF)
                            break;
                        csv.NextRow();
                    }
                    int val = csv.ReadInt32(0);
                    row = csv.ReadInt32(1);

                    if (col != val)
                    {
                        count = 0;
                        image = 0;
                    }

                    col = val;

                    if (col == 7 && row == 7)
                        col = col;

                    var texture = "art/textures/terrain/" + csv.ReadString(2).ToLower() + ".dds";
                    if (manager.HasAsset(texture) && !_images.ContainsKey(texture))
                    {
                        _images[texture] = new DDSImage(manager.GetAsset(texture));
                    }

                    if (map.Layers[col, row] == null)
                        map.Layers[col, row] = new List<LayerItem>();

                    map.Layers[col, row].Add(new LayerItem()
                    {
                        TextureName = texture,
                        Rotate = csv.ReadFloat(3),
                        TranslateU = csv.ReadFloat(4),
                        TranslateV = csv.ReadFloat(5),
                        ScaleU = csv.ReadFloat(6),
                        ScaleV = csv.ReadFloat(7),
                        NumTiles = csv.ReadInt32(9),
                        MaskType = csv.ReadInt32(14),
                        WindowSize = csv.ReadInt32(15),
                        Tilable = csv.ReadInt32(10) > 0,
                        Channel = count % 3,
                        X = col,
                        Y = row,
                        ImageIndex = image
                    });

                    count++;

                    if (count % 3 == 0)
                    {
                        image++;
                    }


                }
            }

            return map;

        }
    }
}
