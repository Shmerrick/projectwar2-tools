struct VertexBoneData
{ 
    uint IDs[NUM_BONES_PER_VEREX];
    float Weights[NUM_BONES_PER_VEREX];
}



int max_bones(const aiMesh *mesh)
{
    int *counts = calloc(mesh->mNumVertices, sizeof(int));
    for (int i = 0; i < mesh->mNumBones; i++) {
        const aiBone *bone = mesh->mBones[i];
        for (int j = 0; j < bone->mNumWeights; j++) {
            const aiVertexWeight *weight = &bone->mWeights[j];
            counts[weight->mVertexId]++;
        }
    }
    int max = 0;w
    for (int i = 0; i < mesh->mNumVertices; i++) {
        if (max < counts[i])
            max = counts[i];
    }
    return max;
}