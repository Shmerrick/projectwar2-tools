﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class XACTexture
    {
        public string mName;

        public XACTexture(string iName)
        {
            mName = string.Empty;
            if (!string.IsNullOrEmpty(iName))
            {
                mName = iName;
            }
        }

        public override string ToString()
        {
            string vName = string.Empty;
            if (!string.IsNullOrEmpty(mName))
            {
                vName = mName;
            }
            string vTheString = "XACTexture Name: " + vName;
            return vTheString;
        }
    }

}