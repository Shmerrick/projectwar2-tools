﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{
    public class Quaternion
    {
        public float mX;
        public float mY;
        public float mZ;
        public float mW;

        public Quaternion()
        {
            mX = 0.0f;
            mY = 0.0f;
            mZ = 0.0f;
            mW = 1.0f;
        }

        public Quaternion(float iX, float iY, float iZ, float iW)
        {
            mX = iX;
            mY = iY;
            mZ = iZ;
            mW = iW;
        }

        public void ReadIn(BinaryReader iStream)
        {
            mX = iStream.ReadSingle();
            mY = iStream.ReadSingle();
            mZ = iStream.ReadSingle();
            mW = iStream.ReadSingle();
        }

        public void WriteOut(BinaryWriter iStream)
        {
            iStream.Write(mX);
            iStream.Write(mY);
            iStream.Write(mZ);
            iStream.Write(mW);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += Marshal.SizeOf(mX);
            vSize += Marshal.SizeOf(mY);
            vSize += Marshal.SizeOf(mZ);
            vSize += Marshal.SizeOf(mW);
            return vSize;
        }

        public override string ToString()
        {
            return $"{Common.FormatFloat(mX)}, {Common.FormatFloat(mY)}, {Common.FormatFloat(mZ)}, {Common.FormatFloat(mW)}";
        }
    }
}
