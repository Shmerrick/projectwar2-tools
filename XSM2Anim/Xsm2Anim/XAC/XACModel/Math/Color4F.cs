﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{
    public class Color4F
    {
        public float mR;
        public float mG;
        public float mB;
        public float mA;

        public Color4F()
        {
            mR = 0.0f;
            mG = 0.0f;
            mB = 0.0f;
            mA = 1.0f;
        }
        public Color4F(float iR, float iG, float iB, float iA)
        {
            mR = iR;
            mG = iG;
            mB = iB;
            mA = iA;
        }

        public void ReadIn(BinaryReader iStream)
        {
            mR = iStream.ReadSingle();
            mG = iStream.ReadSingle();
            mB = iStream.ReadSingle();
            mA = iStream.ReadSingle();
        }

        public void WriteOut(BinaryWriter iStream)
        {
            iStream.Write(mR);
            iStream.Write(mG);
            iStream.Write(mB);
            iStream.Write(mA);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += Marshal.SizeOf(mR);
            vSize += Marshal.SizeOf(mG);
            vSize += Marshal.SizeOf(mB);
            vSize += Marshal.SizeOf(mA);
            return vSize;
        }

        public override string ToString()
        {
            return $"{Common.FormatFloat(mR)}, {Common.FormatFloat(mG)}, {Common.FormatFloat(mB)}, {Common.FormatFloat(mA)}";
        }
    }
}
