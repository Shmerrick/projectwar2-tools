﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{
  public class XAC
  {
      public XACFileHeader mXACFileHeader;
      public XACData mXACData;

      public XAC()
      {
          mXACFileHeader = new XACFileHeader();
          mXACData = new XACData();
      }

      public void clear()
      {
          mXACFileHeader = new XACFileHeader();
          mXACData = new XACData();
      }
  }
  
}
