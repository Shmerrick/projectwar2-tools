﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Header
    {
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 Magic { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 Unk1 { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 Unk2 { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 FileTableOffset { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 EntriesPerFile { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 FileCount { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 Unk5 { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 Unk6 { get; set; }
        [field: MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = 0x1DC)] public Byte[] Padding { get; set; }
    }
}