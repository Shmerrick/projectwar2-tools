﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
  public enum PackageType : Int32
    {
    None = 0,
    Mft = 1,
    Art = 2,
    Art2 = 3,
    Art3 = 4,
    Audio = 5,
    Data = 6,
    World = 7,
    Interface = 8,
    Video = 9,
    Bloodhunt = 10,
    Patch = 11,
    VO_English = 12,
    VO_French = 13,
    Video_French = 14,
    VO_German = 15,
    Video_German = 16,
    VO_Italian = 17,
    Video_Italian = 18,
    VO_Spanish = 19,
    Video_Spanish = 20,
    VO_Korean = 21,
    Video_Korean = 22,
    VO_Chinese = 23,
    Video_Chinese = 24,
    VO_Japanese = 25,
    Video_Japanese = 26,
    VO_Russian = 27,
    Video_Russian = 28,
    Wartest = 29,
    Nda = 30,
    Dev = 31
  }
}