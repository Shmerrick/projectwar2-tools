﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;




namespace GeomParser
{
    public class bos : ModelBaseClass
    {
        public const String a1 = "idLF";
        public const String b = "psLF";
        public const String c = "amLF";
        public const String d = "ntLF";
        public const String e = "lgLF";
        public Char[] f = new Char[4];
        public UInt32 g;
        public UInt32 h;
        public UInt32 i;
        public UInt16 j;
        public UInt16 k;
        public UInt32 l;

        public String a()
        {
            return new String(this.f);
        }
    }
    public class aca : ModelBaseClass
    {
        public bid[] a;
    }

    public class bid : ModelBaseClass
    {
        public UInt32 a;
        public UInt16 b;
        public UInt16 c;
        public UInt16 d;
        public UInt16 e;
    }
    public enum qx
    {
        a,
        b,
        c,
        d,
        e
    }
    //public class bef : a2t
    //{
    //    public override void a()
    //    {
    //        foreach (bid bid in base.b1.a)
    //        {
    //            if (bid.a != 0)
    //            {
    //                long num = (bid.a + 0x18) + (base.Images.Count * 12);
    //                base.Images.Add(this.a(num, bid.b, bid.c, qx.c));
    //            }
    //        }
    //    }

    //    public override Image a(long A_0, int A_1, int A_2, qx A_3)
    //    {
    //        long length = (A_1 * A_2) / 8;
    //        byte[] destinationArray = new byte[length];
    //        if (base.g1.LongLength < (length + A_0))
    //        {
    //            return null;
    //        }
    //        Array.Copy(base.g1, A_0, destinationArray, 0L, length);
    //        Bitmap bitmap = new Bitmap(A_1, A_2, PixelFormat.Format32bppArgb);
    //        int index = 0;
    //        int num3 = 0;
    //        for (int i = 0; i < A_2; i++)
    //        {
    //            for (int j = 0; j < A_1; j++)
    //            {
    //                byte num6 = destinationArray[index];
    //                byte red = ((num6 & (((int)1) << num3++)) == 0) ? ((byte)0) : ((byte)0xff);
    //                if (num3 == 8)
    //                {
    //                    num3 = 0;
    //                    index++;
    //                }
    //                bitmap.SetPixel(j, i, Color.FromArgb(red, red, red));
    //            }
    //        }
    //        return bitmap;
    //    }
    //}
    public class bef : a2t
    {
        public override void a()
        {
            foreach(bid bid in base.b1.a)
            {
                if(bid.a != 0)
                {
                    Int64 num = (bid.a + 0x18) + (base.Images.Count * 12);
                    base.Images.Add(this.a(num, bid.b, bid.c, qx.c));
                }
            }
        }

        public override Image a(Int64 A_0, Int32 A_1, Int32 A_2, qx A_3)
        {
            Int64 length = (A_1 * A_2) / 8;
            Byte[] destinationArray = new Byte[length];
            if(base.g1.LongLength < (length + A_0))
            {
                return null;
            }
            Array.Copy(base.g1, A_0, destinationArray, 0L, length);
            var bitmap = new Bitmap(A_1, A_2, PixelFormat.Format32bppRgb);
            Int32 index = 0;
            Int32 num3 = 0;
            for(Int32 i = 0; i < A_2; i++)
            {
                for(Int32 j = 0; j < A_1; j++)
                {
                    Byte num6 = destinationArray[index];
                    Byte red = ((num6 & (((Int32)1) << num3++)) == 0) ? ((Byte)0) : ((Byte)0xff);
                    if(num3 == 8)
                    {
                        num3 = 0;
                        index++;
                    }
                    bitmap.SetPixel(j, i, Color.FromArgb(red, red, red));
                }
            }
            return bitmap;
        }
    }





    public class gs : a2t
    {

        private qx a2;

        public override void a()
        {
            foreach(bid bid in base.b1.a)
            {
                if(bid.a != 0)
                {
                    Int64 num = (bid.a + 0x18) + (base.Images.Count * 12);
                    Console.WriteLine(num);
                    base.Images.Add(this.a(num, bid.b, bid.c, qx.c));
                }
            }
        }


        public void a(qx A_0)
        {
            this.a2 = A_0;
        }

        public override Image a(Int64 A_0, Int32 A_1, Int32 A_2, qx A_3)
        {
            Int32 num = 2;
            if((A_3 == qx.d) || (A_3 == qx.e))
            {
                num = 1;
            }
            Int64 length = (A_1 * A_2) / num;
            Byte[] buffer = null;
            Byte[] destinationArray = new Byte[length];
            if(base.g1.LongLength < (length + A_0))
            {
                return null;
            }
            Array.Copy(base.g1, A_0, destinationArray, 0L, length);
            switch(A_3)
            {
                case qx.c:
                    buffer = aab.a(destinationArray, A_1, A_2, 1);
                    break;

                case qx.d:
                    buffer = aab.a(destinationArray, A_1, A_2, 2);
                    break;

                case qx.e:
                    buffer = aab.a(destinationArray, A_1, A_2, 4);
                    break;
            }
            return w3.b(buffer, A_1, A_2);
            // return null;
        }


    }
    public abstract class bk4
    {
        private Boolean a1;
        private Int32 b1;

        public bk4(Boolean A_0)
        {
            this.a1 = A_0;
            this.b1 = Marshal.SizeOf(typeof(a_in2));
        }

        protected virtual void a(a_in2 A_0)
        {
        }

        public Bitmap a(Image A_0)
        {
            Int32 height = A_0.Height;
            Int32 width = A_0.Width;
            var rect = new Rectangle(0, 0, width, height);
            var image = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            var bitmap2 = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
            using(var graphics = Graphics.FromImage(image))
            {
                graphics.PageUnit = GraphicsUnit.Pixel;
                graphics.DrawImage(A_0, rect);
            }
            BitmapData data = null;
            try
            {
                data = image.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                if(!this.a1)
                {
                    this.a(data, width, height);
                }
                bitmap2.Palette = this.a(bitmap2.Palette);
                this.a(data, bitmap2, width, height, rect);
            } finally
            {
                image.UnlockBits(data);
            }
            return bitmap2;
        }

        protected abstract ColorPalette a(ColorPalette A_0);
        protected virtual void a(BitmapData A_0, Int32 A_1, Int32 A_2)
        {
            IntPtr ptr = A_0.Scan0;
            for(Int32 i = 0; i < A_2; i++)
            {
                IntPtr ptr2 = ptr;
                for(Int32 j = 0; j < A_1; j++)
                {
                    this.a(new a_in2(ptr2));
                    ptr2 = (IntPtr)(((Int32)ptr2) + this.b1);
                }
                ptr = (IntPtr)(((Int64)ptr) + A_0.Stride);
            }
        }

        protected virtual void a(BitmapData A_0, Bitmap A_1, Int32 A_2, Int32 A_3, Rectangle A_4)
        {
            BitmapData bitmapdata = null;
            try
            {
                bitmapdata = A_1.LockBits(A_4, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
                IntPtr ptr = A_0.Scan0;
                IntPtr ptr2 = ptr;
                IntPtr ptr3 = ptr2;
                IntPtr ptr4 = bitmapdata.Scan0;
                IntPtr ptr5 = ptr4;
                Byte val = this.b(new a_in2(ptr2));
                Marshal.WriteByte(ptr5, val);
                for(Int32 i = 0; i < A_3; i++)
                {
                    ptr2 = ptr;
                    ptr5 = ptr4;
                    for(Int32 j = 0; j < A_2; j++)
                    {
                        if(Marshal.ReadByte(ptr3) != Marshal.ReadByte(ptr2))
                        {
                            val = this.b(new a_in2(ptr2));
                            ptr3 = ptr2;
                        }
                        Marshal.WriteByte(ptr5, val);
                        ptr2 = (IntPtr)(((Int64)ptr2) + this.b1);
                        ptr5 = (IntPtr)(((Int64)ptr5) + 1L);
                    }
                    ptr = (IntPtr)(((Int64)ptr) + A_0.Stride);
                    ptr4 = (IntPtr)(((Int64)ptr4) + bitmapdata.Stride);
                }
            } finally
            {
                A_1.UnlockBits(bitmapdata);
            }
        }

        protected abstract Byte b(a_in2 A_0);

        [StructLayout(LayoutKind.Explicit)]
        public struct a_in2
        {
            [FieldOffset(0)]
            public Byte a1;
            [FieldOffset(1)]
            public Byte b1;
            [FieldOffset(2)]
            public Byte c1;
            [FieldOffset(3)]
            public Byte d1;
            [FieldOffset(0)]
            public Int32 e1;

            public a_in2(IntPtr A_0)
            {
                this = (a_in2)Marshal.PtrToStructure(A_0, typeof(a_in2));
            }

            public Color a()
            {
                return Color.FromArgb(this.d1, this.c1, this.b1, this.a1);
            }
        }
    }

    public class a6c : bk4
    {
        private a_in3 a1;
        private Int32 b1;

        public a6c(Int32 A_0, Int32 A_1)
            : base(false)
        {
            if(A_0 > 0xff)
            {
                throw new ArgumentOutOfRangeException("maxColors", A_0, "The number of colors should be less than 256");
            }
            if((A_1 < 1) | (A_1 > 8))
            {
                throw new ArgumentOutOfRangeException("maxColorBits", A_1, "This should be between 1 and 8");
            }
            this.a1 = new a_in3(A_1);
            this.b1 = A_0;
        }

        protected override void a(a_in2 A_0)
        {
            //this.a1.a(A_0);
        }

        protected override ColorPalette a(ColorPalette A_0)
        {
            ArrayList list = this.a1.a((Int32)(this.b1 - 1));
            for(Int32 i = 0; i < list.Count; i++)
            {
                A_0.Entries[i] = (Color)list[i];
            }
            A_0.Entries[this.b1] = Color.FromArgb(0, 0, 0, 0);
            return A_0;
        }

        protected override Byte b(a_in2 A_0)
        {
            Byte b = (Byte)this.b1;
            if(A_0.d1 > 0)
            {
                b = (Byte)this.a1.b(A_0);
            }
            return b;
        }

        private class a_in3
        {
            private static Int32[] a1 = new Int32[] { 0x80, 0x40, 0x20, 0x10, 8, 4, 2, 1 };
            private a_in b1;
            private Int32 c1;
            private a_in[] d;
            private Int32 e1;
            private a_in f1;
            private Int32 g1;

            public a_in3(Int32 A_0)
            {
                this.e1 = A_0;
                this.c1 = 0;
                this.d = new a_in[9];
                this.b1 = new a_in(0, this.e1, this);
                this.g1 = 0;
                this.f1 = null;
            }

            protected a_in[] a()
            {
                return this.d;
            }

            protected void a(a_in A_0)
            {
                this.f1 = A_0;
            }

            public void a(a_in2 A_0)
            {
                if(this.g1 == A_0.e1)
                {
                    if(this.f1 == null)
                    {
                        this.g1 = A_0.e1;
                        this.b1.a(A_0, this.e1, 0, this);
                    } else
                    {
                        this.f1.a(A_0);
                    }
                } else
                {
                    this.g1 = A_0.e1;
                    this.b1.a(A_0, this.e1, 0, this);
                }
            }

            public ArrayList a(Int32 A_0)
            {
                while(this.b() > A_0)
                {
                    this.c();
                }
                var list = new ArrayList(this.b());
                Int32 num = 0;
                this.b1.a(list, ref num);
                return list;
            }

            public Int32 b()
            {
                return this.c1;
            }

            public Int32 b(a_in2 A_0)
            {
                return this.b1.a(A_0, 0);
            }

            public void b(Int32 A_0)
            {
                this.c1 = A_0;
            }

            public void c()
            {
                Int32 index = this.e1 - 1;
                while((index > 0) && (this.d[index] == null))
                {
                    index--;
                }
                a_in a = this.d[index];
                this.d[index] = a.a();
                this.c1 -= a.c();
                this.f1 = null;
            }

            protected class a_in
            {
                private Boolean a1;
                private Int32 b1;
                private Int32 c1;
                private Int32 d1;
                private Int32 e1;
                private a_in[] f;
                private a_in g;
                private Int32 h;

                public a_in(Int32 A_0, Int32 A_1, a_in3 A_2)
                {
                    this.a1 = A_0 == A_1;
                    this.c1 = this.d1 = this.e1 = 0;
                    this.b1 = 0;
                    if(this.a1)
                    {
                        A_2.b((Int32)(A_2.b() + 1));
                        this.g = null;
                        this.f = null;
                    } else
                    {
                        this.g = A_2.a()[A_0];
                        A_2.a()[A_0] = this;
                        this.f = new a_in[8];
                    }
                }

                public a_in a()
                {
                    return this.g;
                }

                public void a(a_in A_0)
                {
                    this.g = A_0;
                }

                public void a(a_in2 A_0)
                {
                    this.b1++;
                    this.c1 += A_0.c1;
                    this.d1 += A_0.b1;
                    this.e1 += A_0.a1;
                }

                public Int32 a(a_in2 A_0, Int32 A_1)
                {
                    Int32 h = this.h;
                    if(this.a1)
                    {
                        return h;
                    }
                    Int32 num2 = 7 - A_1;
                    Int32 index = (((A_0.c1 & a6c.a_in3.a1[A_1]) >> (num2 - 2)) | ((A_0.b1 & a6c.a_in3.a1[A_1]) >> (num2 - 1))) | ((A_0.a1 & a6c.a_in3.a1[A_1]) >> num2);
                    if(this.f[index] == null)
                    {
                        throw new Exception("Didn't expect this!");
                    }
                    return this.f[index].a(A_0, A_1 + 1);
                }

                public void a(ArrayList A_0, ref Int32 A_1)
                {
                    if(this.a1)
                    {
                        this.h = A_1++;
                        A_0.Add(Color.FromArgb(this.c1 / this.b1, this.d1 / this.b1, this.e1 / this.b1));
                    } else
                    {
                        for(Int32 i = 0; i < 8; i++)
                        {
                            if(this.f[i] != null)
                            {
                                this.f[i].a(A_0, ref A_1);
                            }
                        }
                    }
                }

                public void a(a_in2 A_0, Int32 A_1, Int32 A_2, a_in3 A_3)
                {
                    if(this.a1)
                    {
                        this.a(A_0);
                        A_3.a(this);
                    } else
                    {
                        Int32 num = 7 - A_2;
                        Int32 index = (((A_0.c1 & a6c.a_in3.a1[A_2]) >> (num - 2)) | ((A_0.b1 & a6c.a_in3.a1[A_2]) >> (num - 1))) | ((A_0.a1 & a6c.a_in3.a1[A_2]) >> num);
                        a_in a = this.f[index];
                        if(a == null)
                        {
                            a = new a_in(A_2 + 1, A_1, A_3);
                            this.f[index] = a;
                        }
                        a.a(A_0, A_1, A_2 + 1, A_3);
                    }
                }

                public a_in[] b()
                {
                    return this.f;
                }

                public Int32 c()
                {
                    this.c1 = this.d1 = this.e1 = 0;
                    Int32 num = 0;
                    for(Int32 i = 0; i < 8; i++)
                    {
                        if(this.f[i] != null)
                        {
                            this.c1 += this.f[i].c1;
                            this.d1 += this.f[i].d1;
                            this.e1 += this.f[i].e1;
                            this.b1 += this.f[i].b1;
                            num++;
                            this.f[i] = null;
                        }
                    }
                    this.a1 = true;
                    return (num - 1);
                }
            }
        }
    }

    public static class w3
    {
        public static Single a1 = 0f;
        public static Single b1 = 0f;
        public static Single c1 = 1f;
        public static Single d1 = 1f;
        public static InterpolationMode e = InterpolationMode.Default;

        public static Byte[] a(Bitmap A_0)
        {
            return a(Rectangle.Empty, null, A_0);
        }

        public static Image a(Image A_0)
        {
            return b(a((Bitmap)A_0), A_0.Width, A_0.Height);
        }

        public static ImageCodecInfo a(String A_0)
        {
            foreach(ImageCodecInfo info in ImageCodecInfo.GetImageEncoders())
            {
                if(info.MimeType == A_0)
                {
                    return info;
                }
            }
            Logger.Warn(typeof(w3), "No Encoder for type '{0}' found.", new Object[] { A_0 });
            return null;
        }

        public static ColorPalette a(UInt32 A_0)
        {
            PixelFormat format = PixelFormat.Format1bppIndexed;
            if(A_0 > 2)
            {
                format = PixelFormat.Format4bppIndexed;
            }
            if(A_0 > 0x10)
            {
                format = PixelFormat.Format8bppIndexed;
            }
            var bitmap = new Bitmap(1, 1, format);
            ColorPalette palette = bitmap.Palette;
            bitmap.Dispose();
            return palette;
        }

        public static Bitmap a(Bitmap A_0, Byte A_1)
        {
            Byte[] buffer = a(A_0);
            Int32 width = A_0.Width;
            Int32 height = A_0.Height;
            Int64 num3 = 0L;
            for(Int32 i = 0; i < height; i++)
            {
                for(Int32 j = 0; j < width; j++)
                {
                    num3 = ((i * width) * 4) + (j * 4);
                    buffer[(Int32)((IntPtr)num3)] = buffer[(Int32)((IntPtr)num3)];
                    buffer[(Int32)((IntPtr)(num3 + 1L))] = buffer[(Int32)((IntPtr)(num3 + 1L))];
                    buffer[(Int32)((IntPtr)(num3 + 2L))] = buffer[(Int32)((IntPtr)(num3 + 2L))];
                    buffer[(Int32)((IntPtr)(num3 + 3L))] = A_1;
                }
            }
            return (Bitmap)c(buffer, width, height);
        }

        public static Bitmap a(Image A_0, Rectangle A_1)
        {
            return a(A_0, A_1, Color.Black);
        }

        public static Int32 a(ColorPalette A_0, Color A_1)
        {
            Int32 num = 0;
            Int32 num2 = 0x7fffffff;
            Int32 r = A_1.R;
            Int32 g = A_1.G;
            Int32 b = A_1.B;
            for(Int32 i = 0; i < A_0.Entries.Length; i++)
            {
                Color color = A_0.Entries[i];
                Int32 num7 = color.R - r;
                Int32 num8 = color.G - g;
                Int32 num9 = color.B - b;
                Int32 num10 = ((num7 * num7) + (num8 * num8)) + (num9 * num9);
                if(num10 < num2)
                {
                    num = (Byte)i;
                    num2 = num10;
                    if(num10 == 0)
                    {
                        return i;
                    }
                }
            }
            return num;
        }

        public static void a(String A_0, Image A_1)
        {
            var ac = new a6c(0xff, 4);
            using(Bitmap bitmap = ac.a(A_1))
            {
                bitmap.Save(A_0, ImageFormat.Gif);
            }
        }

        public static void a(Bitmap A_0, Bitmap A_1, Rectangle A_2)
        {
            a(A_0, A_1, A_2, InterpolationMode.Default);
        }

        public static Bitmap a(Bitmap A_0, Color A_1, Color A_2)
        {
            Byte[] buffer = a(A_0);
            Int32 width = A_0.Width;
            Int32 height = A_0.Height;
            Int64 num3 = 0L;
            for(Int32 i = 0; i < height; i++)
            {
                for(Int32 j = 0; j < width; j++)
                {
                    num3 = ((i * width) * 4) + (j * 4);
                    if(((buffer[(Int32)((IntPtr)num3)] == A_1.B) && (buffer[(Int32)((IntPtr)(num3 + 1L))] == A_1.G)) && ((buffer[(Int32)((IntPtr)(num3 + 2L))] == A_1.R) && (buffer[(Int32)((IntPtr)(num3 + 3L))] == A_1.A)))
                    {
                        buffer[(Int32)((IntPtr)num3)] = A_2.B;
                        buffer[(Int32)((IntPtr)(num3 + 1L))] = A_2.G;
                        buffer[(Int32)((IntPtr)(num3 + 2L))] = A_2.R;
                        buffer[(Int32)((IntPtr)(num3 + 3L))] = A_2.A;
                    }
                }
            }
            return (Bitmap)c(buffer, width, height);
        }

        public static BitmapData a(Bitmap A_0, out GCHandle A_1, PixelFormat A_2)
        {
            Int32 num = 4;
            if(A_2 == PixelFormat.Format8bppIndexed)
            {
                num = 1;
            }
            Byte[] buffer = new Byte[(A_0.Width * A_0.Height) * num];
            var handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            IntPtr ptr = handle.AddrOfPinnedObject();
            var data = new BitmapData
            {
                Width = A_0.Width,
                Height = A_0.Height,
                Stride = num * A_0.Width,
                PixelFormat = A_2,
                Reserved = 0,
                Scan0 = ptr
            };
            A_1 = handle;
            return data;
        }

        public static Bitmap a(Bitmap A_0, Int32 A_1, Int32 A_2)
        {
            return a(A_0, A_1, A_2, InterpolationMode.Default);
        }

        public static Bitmap a(Image A_0, Rectangle A_1, Color A_2)
        {
            if(A_0 == null)
            {
                return null;
            }
            var image = new Bitmap(A_1.Width, A_1.Height);
            var graphics = Graphics.FromImage(image);
            graphics.FillRectangle(new SolidBrush(A_2), A_1);
            graphics.DrawImage(A_0, 0, 0, A_1, GraphicsUnit.Pixel);
            graphics.Dispose();
            return image;
        }

        public static Icon a(Image A_0, Int32 A_1, Boolean A_2)
        {
            Int32 num;
            Int32 num2;
            Int32 num3;
            Int32 num4;
            var image = new Bitmap(A_1, A_1);
            var graphics = Graphics.FromImage(image);
            if(!A_2 || (A_0.Height == A_0.Width))
            {
                num = num2 = 0;
                num3 = num4 = A_1;
            } else
            {
                Single num5 = ((Single)A_0.Width) / ((Single)A_0.Height);
                if(num5 > 1f)
                {
                    num3 = A_1;
                    num4 = (Int32)(((Single)A_1) / num5);
                    num = 0;
                    num2 = (A_1 - num4) / 2;
                } else
                {
                    num3 = (Int32)(A_1 * num5);
                    num4 = A_1;
                    num2 = 0;
                    num = (A_1 - num3) / 2;
                }
            }
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(A_0, num, num2, num3, num4);
            graphics.Flush();
            return Icon.FromHandle(image.GetHicon());
        }

        public static ColorPalette a(ColorPalette A_0, Int32 A_1, Color A_2)
        {
            if(A_0.Entries.Length != 0)
            {
                for(Int32 i = 0; i < A_1; i++)
                {
                    Color color = A_0.Entries[i];
                    if(((A_2.R == color.R) && (A_2.G == color.G)) && (A_2.B == color.B))
                    {
                        A_0.Entries[i] = Color.FromArgb(0, color.R, color.G, color.B);
                    } else
                    {
                        A_0.Entries[i] = Color.FromArgb(0xff, color.R, color.G, color.B);
                    }
                }
            }
            return A_0;
        }

        public static Byte[] a(Rectangle A_0, BitmapData A_1, Bitmap A_2)
        {
            GCHandle handle;
            BitmapData bitmapData = null;
            if(A_1 != null)
            {
                bitmapData = A_1;
                var rectangle = new Rectangle(0, 0, A_2.Width, A_2.Height);
                if(A_0 != Rectangle.Empty)
                {
                    rectangle = A_0;
                }
                BitmapData data2 = A_2.LockBits(rectangle, ImageLockMode.UserInputBuffer | ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb, bitmapData);
                Int32 num = data2.Stride * data2.Height;
                Byte[] buffer = new Byte[num];
                Marshal.Copy(data2.Scan0, buffer, 0, num);
                A_2.UnlockBits(data2);
                return buffer;
            }
            bitmapData = a(A_2, out handle, PixelFormat.Format32bppArgb);
            var rect = new Rectangle(0, 0, A_2.Width, A_2.Height);
            if(A_0 != Rectangle.Empty)
            {
                rect = A_0;
            }
            BitmapData bitmapdata = A_2.LockBits(rect, ImageLockMode.UserInputBuffer | ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb, bitmapData);
            Int32 length = bitmapdata.Stride * bitmapdata.Height;
            Byte[] destination = new Byte[length];
            Marshal.Copy(bitmapdata.Scan0, destination, 0, length);
            A_2.UnlockBits(bitmapdata);
            if(handle.IsAllocated)
            {
                handle.Free();
            }
            return destination;
        }

        public static void a(String A_0, Image A_1, Int32 A_2)
        {
            if(A_1 != null)
            {
                Directory.CreateDirectory(Path.GetDirectoryName(A_0));
                ImageCodecInfo encoder = a("image/jpeg");
                var encoderParams = new EncoderParameters(1);
                encoderParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (Int64)A_2);
                A_1.Save(A_0, encoder, encoderParams);
            }
        }

        public static Image a(Byte[] A_0, Int32 A_1, Int32 A_2)
        {
            Byte[] buffer = new Byte[A_0.Length];
            Int64 num = 0L;
            for(Int32 i = 0; i < A_2; i++)
            {
                for(Int32 j = 0; j < A_1; j++)
                {
                    num = ((i * A_1) * 4) + (j * 4);
                    buffer[(Int32)((IntPtr)(num + 1L))] = A_0[(Int32)((IntPtr)num)];
                    buffer[(Int32)((IntPtr)(num + 2L))] = A_0[(Int32)((IntPtr)(num + 1L))];
                    buffer[(Int32)((IntPtr)(num + 3L))] = A_0[(Int32)((IntPtr)(num + 2L))];
                    buffer[(Int32)((IntPtr)num)] = A_0[(Int32)((IntPtr)(num + 3L))];
                }
            }
            return c(buffer, A_1, A_2);
        }

        public static void a(Bitmap A_0, Bitmap A_1, Rectangle A_2, InterpolationMode A_3)
        {
            var bitmap = new Bitmap(A_0);
            BitmapData bitmapdata = null;
            BitmapData data2 = null;
            try
            {
                GCHandle handle;
                if((A_2.Width != A_0.Width) || (A_2.Height != A_0.Height))
                {
                    Bitmap bitmap2 = a(bitmap, A_2.Width, A_2.Height, A_3);
                    bitmap.Dispose();
                    bitmap = bitmap2;
                }
                BitmapData bitmapData = a(bitmap, out handle, PixelFormat.Format32bppArgb);
                bitmapdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.UserInputBuffer | ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb, bitmapData);
                data2 = A_1.LockBits(A_2, ImageLockMode.UserInputBuffer | ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb, bitmapData);
                Int32 length = bitmapdata.Stride * bitmapdata.Height;
                Byte[] destination = new Byte[length];
                Marshal.Copy(bitmapdata.Scan0, destination, 0, length);
                Marshal.Copy(destination, 0, data2.Scan0, length);
                if(handle.IsAllocated)
                {
                    handle.Free();
                }
            } finally
            {
                if(data2 != null)
                {
                    A_1.UnlockBits(data2);
                }
                if(bitmapdata != null)
                {
                    bitmap.UnlockBits(bitmapdata);
                }
            }
        }

        public static Bitmap a(Bitmap A_0, Int32 A_1, Int32 A_2, InterpolationMode A_3)
        {
            if((A_0.Width == A_1) && (A_0.Height == A_2))
            {
                return A_0;
            }
            var image = new Bitmap(A_1, A_2);
            using(var graphics = Graphics.FromImage(image))
            {
                if(e == InterpolationMode.Default)
                {
                    graphics.InterpolationMode = A_3;
                } else
                {
                    graphics.InterpolationMode = e;
                }
                graphics.DrawImage(A_0, a1, b1, (Single)(A_1 + c1), (Single)(A_2 + d1));
                return image;
            }
        }

        public static void a(Int32 A_0, Int32 A_1, Bitmap A_2, out Bitmap A_3)
        {
            A_3 = null;
            if(A_2 != null)
            {
                if((A_2.Width < A_0) && (A_2.Height < A_1))
                {
                    A_3 = A_2;
                } else if((A_2.Width == A_0) && (A_2.Height == A_1))
                {
                    A_3 = A_2;
                } else
                {
                    Int32 num = 0;
                    Int32 num2 = A_0;
                    Int32 num3 = A_1;
                    if(A_2.Width > A_2.Height)
                    {
                        num = A_2.Width / A_0;
                        num3 = A_2.Height / num;
                    } else
                    {
                        num = A_2.Height / A_1;
                        num2 = A_2.Width / num;
                    }
                    if(num == 1)
                    {
                        A_3 = A_2;
                    } else
                    {
                        A_3 = a(A_2, num2, num3, InterpolationMode.HighQualityBicubic);
                        A_2.Dispose();
                    }
                }
            }
        }

        public static Image a(Byte[] A_0, Int32 A_1, Int32 A_2, PixelFormat A_3)
        {
            GCHandle handle;
            var bitmap = new Bitmap(A_1, A_2, A_3);
            BitmapData bitmapData = a(bitmap, out handle, A_3);
            BitmapData bitmapdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.UserInputBuffer | ImageLockMode.WriteOnly, A_3, bitmapData);
            Int32 length = bitmapdata.Stride * bitmapdata.Height;
            Marshal.Copy(A_0, 0, bitmapdata.Scan0, length);
            bitmap.UnlockBits(bitmapdata);
            if(handle.IsAllocated)
            {
                handle.Free();
            }
            return bitmap;
        }

        public static Bitmap a(List<Bitmap> A_0, Int32 A_1, Int32 A_2, Pen A_3)
        {
            if(A_0.Count == 0)
            {
                return null;
            }
            if((A_1 * A_2) != A_0.Count)
            {
                Logger.Error(typeof(w3), "Bitmap-Count {0} doesnt match desired tile-count {1}.", new Object[] { A_0.Count, A_1 * A_2 });
                return null;
            }
            Int32 width = 0;
            Int32 height = 0;
            Int32 num3 = 0;
            for(Int32 i = 0; i < A_2; i++)
            {
                for(Int32 k = 0; k < A_1; k++)
                {
                    Bitmap bitmap = A_0[(i * A_1) + k];
                    height += bitmap.Height;
                    width += bitmap.Width;
                }
            }
            var bitmap2 = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            Int32 y = 0;
            Int32 x = 0;
            num3 = 0;
            for(Int32 j = 0; j < A_2; j++)
            {
                for(Int32 m = 0; m < A_1; m++)
                {
                    if(m == 0)
                    {
                        x = 0;
                    }
                    Bitmap bitmap3 = A_0[num3++];
                    var rectangle = new Rectangle(x, y, bitmap3.Width, bitmap3.Height);
                    a(bitmap3, bitmap2, rectangle);
                    if(A_3 != null)
                    {
                        var graphics = Graphics.FromImage(bitmap2);
                        graphics.DrawRectangle(A_3, rectangle);
                        graphics.Dispose();
                    }
                    x += bitmap3.Width;
                }
                y += A_0[j * A_1].Height;
            }
            return bitmap2;
        }

        public static Bitmap b(Bitmap A_0)
        {
            Byte[] buffer = a(A_0);
            Int32 width = A_0.Width;
            Int32 height = A_0.Height;
            Int64 num3 = 0L;
            for(Int32 i = 0; i < height; i++)
            {
                for(Int32 j = 0; j < width; j++)
                {
                    num3 = ((i * width) * 4) + (j * 4);
                    Byte num6 = (Byte)(((buffer[(Int32)((IntPtr)num3)] * 0.11) + (buffer[(Int32)((IntPtr)(num3 + 1L))] * 0.59)) + (buffer[(Int32)((IntPtr)(num3 + 2L))] * 0.3));
                    buffer[(Int32)((IntPtr)num3)] = num6;
                    buffer[(Int32)((IntPtr)(num3 + 1L))] = num6;
                    buffer[(Int32)((IntPtr)(num3 + 2L))] = num6;
                    buffer[(Int32)((IntPtr)(num3 + 3L))] = buffer[(Int32)((IntPtr)(num3 + 3L))];
                }
            }
            return (Bitmap)c(buffer, width, height);
        }

        public static Image b(String A_0)
        {
            using(var stream = new MemoryStream(File.ReadAllBytes(A_0)))
            {
                return Image.FromStream(stream);
            }
        }

        public static void b(Bitmap A_0, Bitmap A_1, Rectangle A_2)
        {
            b(A_0, A_1, A_2, InterpolationMode.Default);
        }

        public static Image b(Byte[] A_0, Int32 A_1, Int32 A_2)
        {
            Byte[] buffer = new Byte[A_0.Length];
            Int64 num = 0L;
            var bmp = new Bitmap(A_1, A_2, PixelFormat.Format32bppArgb);

            for(Int32 i = 0; i < A_2; i++)
            {
                for(Int32 j = 0; j < A_1; j++)
                {
                    num = ((i * A_1) * 4) + (j * 4);

                    buffer[(Int32)((IntPtr)num)] = A_0[(Int32)((IntPtr)(num + 2L))];
                    buffer[(Int32)((IntPtr)(num + 1L))] = A_0[(Int32)((IntPtr)(num + 1L))];
                    buffer[(Int32)((IntPtr)(num + 2L))] = A_0[(Int32)((IntPtr)num)];
                    buffer[(Int32)((IntPtr)(num + 3L))] = A_0[(Int32)((IntPtr)(num + 3L))];

                }
            }



            return c(buffer, A_1, A_2);
        }

        public static Bitmap b(List<Bitmap> A_0, Int32 A_1, Int32 A_2, Pen A_3)
        {
            try
            {
                if(A_0.Count == 0)
                {
                    return null;
                }
                if((A_1 * A_2) != A_0.Count)
                {
                    Logger.Error(typeof(w3), "Bitmap-Count {0} doesnt match desired tile-count {1}.", new Object[] { A_0.Count, A_1 * A_2 });
                    return null;
                }
                Int32 width = 0;
                Int32 height = 0;
                Int32 num3 = 0;
                for(Int32 i = 0; i < A_2; i++)
                {
                    Int32 num5 = 1;
                    for(Int32 k = 0; k < A_1; k++)
                    {
                        Bitmap bitmap = A_0[(i * A_1) + k];
                        if(bitmap != null)
                        {
                            num5 = bitmap.Height;
                            break;
                        }
                    }
                    height += num5;
                    for(Int32 m = 0; m < A_1; m++)
                    {
                        Int32 num8 = 1;
                        for(Int32 n = 0; n < A_2; n++)
                        {
                            Bitmap bitmap2 = A_0[(n * A_1) + m];
                            if(bitmap2 != null)
                            {
                                num8 = bitmap2.Width;
                                break;
                            }
                        }
                        if(i == 0)
                        {
                            width += num8;
                        }
                        Bitmap bitmap3 = A_0[num3++];
                        if(bitmap3 == null)
                        {
                            bitmap3 = new Bitmap(num8, num5, PixelFormat.Format32bppArgb);
                            A_0[num3 - 1] = bitmap3;
                        }
                        if(num8 != bitmap3.Width)
                        {
                            Logger.Error(typeof(w3), "Image {0} in TileRow {1} has a different width ({2}) compared to the required width {3}.", new Object[] { m, i, bitmap3.Width, num8 });
                            return null;
                        }
                        if(num5 != bitmap3.Height)
                        {
                            Logger.Error(typeof(w3), "Image {0} in TileRow {1} has a different height ({2}) compared to the required height {3}.", new Object[] { m, i, bitmap3.Height, num5 });
                            return null;
                        }
                    }
                }
                var bitmap4 = new Bitmap(width, height, PixelFormat.Format32bppArgb);
                Int32 y = 0;
                Int32 x = 0;
                num3 = 0;
                for(Int32 j = 0; j < A_2; j++)
                {
                    for(Int32 num13 = 0; num13 < A_1; num13++)
                    {
                        if(num13 == 0)
                        {
                            x = 0;
                        }
                        Bitmap bitmap5 = A_0[num3++];
                        var rectangle = new Rectangle(x, y, bitmap5.Width, bitmap5.Height);
                        a(bitmap5, bitmap4, rectangle);
                        if(A_3 != null)
                        {
                            var graphics = Graphics.FromImage(bitmap4);
                            graphics.DrawRectangle(A_3, rectangle);
                            graphics.Dispose();
                        }
                        x += bitmap5.Width;
                    }
                    y += A_0[j * A_1].Height;
                }
                return bitmap4;
            } catch(Exception exception)
            {
                //Logger.Error(typeof(w3), exception, "Error combining images. Reason: {0}.", new object[] { exception.Message });
                return null;
            }
        }

        public static void b(Bitmap A_0, Bitmap A_1, Rectangle A_2, InterpolationMode A_3)
        {
            var bitmap = new Bitmap(A_0);
            BitmapData bitmapdata = null;
            BitmapData data2 = null;
            if((A_2.Width != 0) && (A_2.Height != 0))
            {
                try
                {
                    GCHandle handle;
                    if((A_2.Width != A_0.Width) || (A_2.Height != A_0.Height))
                    {
                        Bitmap bitmap2 = a(bitmap, A_2.Width, A_2.Height, A_3);
                        bitmap.Dispose();
                        bitmap = bitmap2;
                    }
                    BitmapData bitmapData = a(bitmap, out handle, PixelFormat.Format32bppArgb);
                    bitmapdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.UserInputBuffer | ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb, bitmapData);
                    Int32 length = bitmapdata.Stride * bitmapdata.Height;
                    Byte[] destination = new Byte[length];
                    Marshal.Copy(bitmapdata.Scan0, destination, 0, length);
                    Byte[] source = a(A_2, bitmapData, A_1);
                    data2 = A_1.LockBits(A_2, ImageLockMode.UserInputBuffer | ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb, bitmapData);
                    for(Int32 i = 0; i < bitmapdata.Height; i++)
                    {
                        for(Int32 j = 0; j < bitmapdata.Width; j++)
                        {
                            Int32 index = (i * bitmapdata.Stride) + (j * 4);
                            Int32 num5 = (i * data2.Stride) + (j * 4);
                            if(destination[index + 3] != 0)
                            {
                                Single num6 = ((Single)destination[index + 3]) / 255f;
                                if(num6 == 1f)
                                {
                                    source[num5] = destination[index];
                                    source[num5 + 1] = destination[index + 1];
                                    source[num5 + 2] = destination[index + 2];
                                } else
                                {
                                    Byte num7 = (Byte)((destination[index] * num6) + (source[num5] * (1f - num6)));
                                    Byte num8 = (Byte)((destination[index + 1] * num6) + (source[num5 + 1] * (1f - num6)));
                                    Byte num9 = (Byte)((destination[index + 2] * num6) + (source[num5 + 2] * (1f - num6)));
                                    source[num5] = num7;
                                    source[num5 + 1] = num8;
                                    source[num5 + 2] = num9;
                                }
                            }
                        }
                    }
                    Marshal.Copy(source, 0, data2.Scan0, length);
                    if(handle.IsAllocated)
                    {
                        handle.Free();
                    }
                } finally
                {
                    if(data2 != null)
                    {
                        A_1.UnlockBits(data2);
                    }
                    if(bitmapdata != null)
                    {
                        bitmap.UnlockBits(bitmapdata);
                    }
                    if(bitmap != null)
                    {
                        bitmap.Dispose();
                    }
                }
            }
        }

        public static Image c(Byte[] A_0, Int32 A_1, Int32 A_2)
        {
            return a(A_0, A_1, A_2, PixelFormat.Format32bppArgb);
        }
    }

    public static class aab
    {
        private const CallingConvention a1 = CallingConvention.Winapi;

        public static Boolean a()
        {
            return (Marshal.SizeOf(IntPtr.Zero) == 8);
        }

        public static Byte[] a(Image A_0, Int32 A_1)
        {
            return b(w3.a((Bitmap)A_0), A_0.Width, A_0.Height, A_1);
        }

        public static Byte[] a(Byte[] A_0, Int32 A_1, Int32 A_2, Int32 A_3)
        {
            Byte[] buffer = new Byte[(A_1 * A_2) * 4 + 100000];
            a(buffer, A_1, A_2, A_0, A_3);
            return buffer;
        }
        public sealed class aH
        {
            [SuppressUnmanagedCodeSecurity, DllImport("squishinterface_Win32.dll")]
            public static extern void SquishCompressImage(IntPtr A_0, Int32 A_1, Int32 A_2, IntPtr A_3, Int32 A_4);
            [SuppressUnmanagedCodeSecurity, DllImport("squishinterface_Win32.dll")]
            public static extern void SquishDecompressImage(IntPtr A_0, Int32 A_1, Int32 A_2, IntPtr A_3, Int32 A_4);
        }

        public sealed class bH
        {
            [SuppressUnmanagedCodeSecurity, DllImport("squishinterface_x64.dll")]
            public static extern void SquishCompressImage(IntPtr A_0, Int32 A_1, Int32 A_2, IntPtr A_3, Int32 A_4);
            [SuppressUnmanagedCodeSecurity, DllImport("squishinterface_x64.dll")]
            public static extern void SquishDecompressImage(IntPtr A_0, Int32 A_1, Int32 A_2, IntPtr A_3, Int32 A_4);
        }

        public enum SquishFlags
        {
            kColourClusterFit = 8,
            kColourIterativeClusterFit = 0x100,
            kColourMetricPerceptual = 0x20,
            kColourMetricUniform = 0x40,
            kColourRangeFit = 0x10,
            kDxt1 = 1,
            kDxt3 = 2,
            kDxt5 = 4,
            kWeightColourByAlpha = 0x80
        }


        [DllImport("squishinterface_Win32.dll", CallingConvention = CallingConvention.Cdecl)]
        internal static extern unsafe void SquishDecompressImage(IntPtr rgba, Int32 width, Int32 height, IntPtr blocks, Int32 flags);

        private static void a(Byte[] A_0, Int32 A_1, Int32 A_2, Byte[] A_3, Int32 A_4)
        {
            var handle = GCHandle.Alloc(A_0, GCHandleType.Pinned);
            IntPtr ptr = handle.AddrOfPinnedObject();
            var handle2 = GCHandle.Alloc(A_3, GCHandleType.Pinned);
            IntPtr ptr2 = handle2.AddrOfPinnedObject();
            //if (a())
            //{
            //   // SquishDecompressImage(ptr, A_1, A_2, ptr2, A_4);
            //    aab.aH.SquishCompressImage(ptr, A_1, A_2, ptr2, A_4);
            //}
            //else
            //{
            //   // ManagedSquish.SquishFlags flag = (ManagedSquish.SquishFlags)A_4;
            //    ManagedSquish.SquishFlags flag =  ManagedSquish.SquishFlags.Dxt1;
            //   // A_3 = ManagedSquish.Squish.DManagedSquish.SquishFlagsecompress(A_0, ManagedSquish.SquishFlags.ColourRangeFit);
            //    aab.aH.SquishCompressImage(ptr, A_1, A_2, ptr2, A_4);
            //}

            if(a())
            {
                // SquishDecompressImage(ptr, A_1, A_2, ptr2, A_4);
                A_0 = ManagedSquish.Squish.Decompress(A_3, ManagedSquish.SquishFlags.Dxt3);
            } else
            {
                // ManagedSquish.SquishFlags flag = (ManagedSquish.SquishFlags)A_4;
                ManagedSquish.SquishFlags flag = ManagedSquish.SquishFlags.Dxt1;
                // A_3 = ManagedSquish.Squish.DManagedSquish.SquishFlagsecompress(A_0, ManagedSquish.SquishFlags.ColourRangeFit);
                SquishDecompressImage(ptr, A_1, A_2, ptr2, (Int32)flag);
            }


            //ManagedSquish.Squish.DManagedSquish.SquishFlagsecompress(A_0, ManagedSquish.SquishFlags.ColourRangeFit);
            if(handle.IsAllocated)
            {
                handle.Free();
            }
            if(handle2.IsAllocated)
            {
                handle2.Free();
            }
        }

        public static Byte[] b(Byte[] A_0, Int32 A_1, Int32 A_2, Int32 A_3)
        {
            Int32 num = ((A_1 + 3) / 4) * ((A_2 + 3) / 4);
            Int32 num2 = ((A_3 & 1) != 0) ? 8 : 0x10;
            Byte[] buffer = new Byte[num * num2];
            b(A_0, A_1, A_2, buffer, A_3);
            return buffer;
        }

        private static void b(Byte[] A_0, Int32 A_1, Int32 A_2, Byte[] A_3, Int32 A_4)
        {
            //GCHandle handle = GCHandle.Alloc(A_0, GCHandleType.Pinned);
            //IntPtr ptr = handle.AddrOfPinnedObject();
            //GCHandle handle2 = GCHandle.Alloc(A_3, GCHandleType.Pinned);
            //IntPtr ptr2 = handle2.AddrOfPinnedObject();
            //if (a())
            //{
            //    aab.b.SquishCompressImage(ptr, A_1, A_2, ptr2, A_4);
            //}
            //else
            //{
            //    aab.a.SquishCompressImage(ptr, A_1, A_2, ptr2, A_4);
            //}
            //if (handle.IsAllocated)
            //{
            //    handle.Free();
            //}
            //if (handle2.IsAllocated)
            //{
            //    handle2.Free();
            //}
        }




    }

    public abstract class a2t : aee
    {
        protected bos a1;
        protected aca b1 = new aca();
        protected ot c1;
        protected Boolean d1;
        protected Boolean e1;
        public List<Image> Images = new List<Image>();
        protected Byte[] g1;

        public abstract void a();
        public void a(ot A_0)
        {
            this.e1 = true;
        }

        public Image a(Int32 A_0)
        {
            if(A_0 == -1)
            {
                return null;
            }
            if(this.Images.Count <= A_0)
            {
                return null;
            }
            return this.Images[A_0];
        }

        public void Load(Stream A_0)
        {
            var reader = new BinaryLogger(A_0);
            this.a1 = new bos();
            this.a1.Read(reader);
            this.b1.a = new bid[this.a1.l];
            this.b1.Read(reader);
            reader.BaseStream.Position = 0L;
            this.g1 = reader.ReadBytes((Int32)reader.BaseStream.Length);
            this.a();
            if(reader.BaseStream.Position != reader.BaseStream.Length)
            {
                Logger.Warn(base.GetType(), "No exact match at end of MythicTexture-File. Reader-Position: {0} File-End: {1}", new Object[] { reader.BaseStream.Position, reader.BaseStream.Length });
            }
            this.d1 = true;
        }

        public abstract Image a(Int64 A_0, Int32 A_1, Int32 A_2, qx A_3);
        public void b(ot A_0)
        {
            if(!File.Exists(A_0.j()))
            {
                throw new FileNotFoundException();
            }
            this.c1 = A_0;
            var stream = new FileStream(A_0.j(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            this.Load(stream);
            this.e1 = true;
        }

        public void b(Stream A_0)
        {
            var reader = new BinaryLogger(A_0);
            this.a1 = new bos();
            this.a1.Read(reader);
            this.b1.a = new bid[this.a1.l];
            this.b1.Read(reader);
        }

        public void c()
        {
            this.a(this.c1);
        }

        public Image d()
        {
            return this.a((Int32)(this.Images.Count - 1));
        }

        public ot e()
        {
            return this.c1;
        }

        public bos f()
        {
            return this.a1;
        }

        public aca g()
        {
            return this.b1;
        }

        public Boolean h()
        {
            return this.e1;
        }

        public Boolean i()
        {
            return this.d1;
        }

        ot aee.a()
        {
            throw new NotImplementedException();
        }

        public void b()
        {
            throw new NotImplementedException();
        }

        Boolean aee.c()
        {
            throw new NotImplementedException();
        }

        Boolean aee.d()
        {
            throw new NotImplementedException();
        }

        public void k(Stream A_0)
        {
            throw new NotImplementedException();
        }

        public void t(ot A_0)
        {
            throw new NotImplementedException();
        }
    }

    class TextureParser
    {
    }
}
