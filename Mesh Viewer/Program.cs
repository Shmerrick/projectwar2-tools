﻿using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;
using System;
using System.IO;
using System.Collections.Generic;

namespace GeomParser
{
    static class Program
    {

        [STAThread]
        static void Main(String[] args)
        {
            var device = IrrlichtDevice.CreateDevice(DriverType.Direct3D9, new Dimension2Di(1280, 720));
            SceneManager scene_manager = device.SceneManager;
            VideoDriver driver = device.VideoDriver;
            IrrlichtLime.GUI.GUIEnvironment gui = device.GUIEnvironment;

            LightSceneNode light = scene_manager.AddLightSceneNode(scene_manager.RootNode, new Vector3Df(200, 200, 200), new Colorf(1.0f, 1.0f, 1.0f), 2000);
            scene_manager.AmbientLight = new Colorf(0.3f, 0.3f, 0.3f);

            driver.SetTextureCreationFlag(TextureCreationFlag.Always32Bit, true);

            MeshSceneNode meshSceneNode = null;
            var m = new ModelParser();

            Byte[] data;

            if(args.Length > 0)
                data = File.ReadAllBytes(args[0]);
            else
                //data = File.ReadAllBytes(@"fg.0.0.emf_armor_wh_06.geom");
                data = File.ReadAllBytes(@"fg.0.0.emf_armor_wh_06.geom");

            m.Load(new MemoryStream(data));

            foreach(Mesh mesh in m.meshes)
            {
                var vlist = new Vertex3D[mesh.vertices.VertexList.Length];
                UInt16[] indices = new UInt16[mesh.indices.Count];
                var vWeights = new VertexWeight[mesh.weights.Count];

                for(Int32 i = 0; i < mesh.vertices.VertexList.Length; ++i)
                {
                    Vertex v = mesh.vertices.VertexList[i];
                    vlist[i] = new Vertex3D(v.position_y, v.position_z, v.position_x, v.normal_x, v.normal_y, v.normal_z, new Color(31, 31, 31, 63), v.texture_u, v.texture_v);
                }

                for(Int32 i = 0; i < mesh.indices.Count; ++i)
                {
                    indices[i] = mesh.indices[i].index;
                }

                for(Int32 i = 0; i < mesh.weights.Count; ++i)
                {
                    vWeights[i] = new VertexWeight(mesh.weights[i].index, mesh.weights[i].weight);
                }

                var mb = MeshBuffer.Create(VertexType.Standard, IndexType._16Bit);


                mb.Append(vlist, indices);
                mb.RecalculateBoundingBox();

                IrrlichtLime.Scene.Mesh newMesh = SkinnedMesh.Create();

                newMesh.AddMeshBuffer(mb);
                newMesh.AddMeshBuffer(mb);
                newMesh.GetMeshBuffer(0).RecalculateBoundingBox();
                newMesh.RecalculateBoundingBox();

                meshSceneNode = device.SceneManager.AddMeshSceneNode(newMesh);

                meshSceneNode.GetMaterial(0).SetFlag(MaterialFlag.TrilinearFilter, true);
                meshSceneNode.GetMaterial(0).SetFlag(MaterialFlag.AnisotropicFilter, true);
                meshSceneNode.GetMaterial(0).SetFlag(MaterialFlag.Fog, true);
                meshSceneNode.GetMaterial(0).SetFlag(MaterialFlag.Lighting, true);
                meshSceneNode.GetMaterial(0).MaterialTypeParam = 1.0f / 64.0f;
                meshSceneNode.GetMaterial(0).AmbientColor = new Color(127, 127, 127, 255) ;
                meshSceneNode.GetMaterial(0).AntiAliasing = AntiAliasingMode.PointSmooth;
                meshSceneNode.GetMaterial(0).Fog = true;
                meshSceneNode.GetMaterial(0).Lighting = true;
                meshSceneNode.GetMaterial(0).Shininess = 0;
                meshSceneNode.GetMaterial(0).SpecularColor = new Color(63, 127, 63, 255);
                meshSceneNode.GetMaterial(0).DiffuseColor = new Color(31, 63, 31, 255);

                meshSceneNode.DebugDataVisible = DebugSceneType.BBoxAll;
                meshSceneNode.DebugDataVisible = DebugSceneType.BBoxAll|  DebugSceneType.Skeleton | DebugSceneType.BBox;
                //meshSceneNode.SetMaterialFlag(MaterialFlag.BackFaceCulling, true);
                meshSceneNode.AutomaticCulling = CullingType.FrustumBox;
            }

            CameraSceneNode camera = device.SceneManager.AddCameraSceneNodeMaya(scene_manager.RootNode);
            camera.DebugDataVisible = DebugSceneType.Normals;

            camera.NearValue = 0.001f;
            camera.FarValue = 30000f;
            camera.AutomaticCulling = CullingType.OcclusionQuery;
            camera.Target = meshSceneNode.AbsolutePosition;
            //camera.Position = meshSceneNode.AbsolutePosition + new Vector3Df(100,200,300);
            //camera.AbsolutePosition = meshSceneNode.AbsolutePosition

            while(device.Run())
            {
                device.VideoDriver.BeginScene(true, true, IrrlichtLime.Video.Color.OpaqueBlack);
                device.SceneManager.DrawAll();
                device.VideoDriver.EndScene();
            }

        }
    }
}
