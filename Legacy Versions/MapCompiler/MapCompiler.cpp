
#include "MapCompiler.h"

//
// TASK LIST
//
//   KNOWN ISSUES:
//
//     - Need to find a proper solution for handling keep doors, right now the export file contains
//       static geometry for them in the closed state, which is not good.  Even if they were in the
//       open state, a solution for toggling occluders needs to be developed.
//     - some things are blocking LOS that should not (e.g. shrubs)
//
//   TASKS:
//
//     - [COMPILER] save BSP creation parameters (max depth and leaf triangle 'limit') in BSP chunk
//     - [SERVER]   make BSP tests non-recursive
//     - [GENERAL]  test DK and Fangbreaka
//     - [GENERAL]  move shared data structures into WarZoneLib
//     - [COMPILER] add some way to only include/exclude certain fixtures (i.e. only include STK and Mandred's at first)
//     - [COMPILER] terrain triangles should get compiled in to BSP
//     - [GENERAL]  handle zone seams properly
//

//
// TODO: dynamic optimization: runtime sort triangles which often cause LOS check fail ahead of other triangles
// TODO: might want a safeguard that says when your hitbox overlaps the hitbox of opponent, then LOS is established.
//       this would mainly be to prevent 'safe spots' where pathological LOS behavior could render a character unattackable
//       (think healer getting in one of these spots and spamming gheal)
//       this would (unfortunately) also allow AOE through occcluders when both the attacker and the opponent are
//       next to one another but on either side of the obstacle ...
//
