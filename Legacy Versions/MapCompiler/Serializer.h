
#pragma once

#include "NIF.h"
#include "Fixture.h"
#include "Terrain.h"
#include "Collision.h"
#include "Writer.h"
#include "Config.h"
#include "Zone.h"

#include "niflib.h"
#include "obj/NiAVObject.h"

ref class Serializer
{
public:
	Config ^Config;
	System::IO::MemoryStream ^Serialize(NIF ^);
	System::IO::MemoryStream ^SerializeEmptyNIF(NIF ^);
	System::IO::MemoryStream ^Serialize(Fixture ^);
	System::IO::MemoryStream ^Serialize(Zone^ zone, Terrain ^);
	System::IO::MemoryStream ^Serialize(int zoneID, Collision ^);
	System::IO::MemoryStream ^Serialize(int zoneID, BSPNode ^);
private:
	void WriteEmptyNIFNode(Writer ^writer);
	void WriteNIFNode(Writer ^writer, Niflib::NiAVObjectRef object);
	bool WriteTriShapeData(Writer ^writer, Niflib::NiAVObjectRef object);
	void WriteBSPNode(Writer ^writer, BSPNode ^node);
};