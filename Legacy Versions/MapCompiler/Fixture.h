
#pragma once

#include "Utility.h"

#include <crtdbg.h>

using namespace System;
using namespace WarZoneLib;

ref class NIF;
ref class Zone;

ref class Fixture
{
public:

	Zone ^Zone;
	int ID, NIF;
	String ^Name;
	double X, Y, Z, A, Scale;
	int UniqueID;
	int Collide;
	double Radius;
	Vector3 Axis3D;
	double Angle3D;

	property Matrix TransformationMatrix
	{
		Matrix get()
		{
			return ScalingMatrix * RotationMatrix * TranslationMatrix;
		}
	}

	property Matrix ScalingMatrix
	{
		Matrix get()
		{
			float s = (float)(Scale / 100.0f);
			return Matrix::Scaling(s, s, s);
		}
	}

	property Matrix RotationMatrix
	{
		Matrix get();
	}

	property Matrix TranslationMatrix
	{
		Matrix get();
	}
};