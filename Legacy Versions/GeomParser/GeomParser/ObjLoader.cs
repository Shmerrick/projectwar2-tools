﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml;
using IrrlichtLime;
using IrrlichtLime.Scene;
using IrrlichtLime.Core;
using IrrlichtLime.Video;

namespace GeomParser
{
    class OBJ
    {
        public List<OBJGroup> Groups = new List<OBJGroup>();
        public Dictionary<String, OBJGroup> NamedGroups = new Dictionary<String, OBJGroup>();
        public Dictionary<String, Material> Materials = new Dictionary<String, Material>();

        public String MTLLib = "";

        public class Material
        {
            public String Name = "";
            public Colorf AmbientColor = new Colorf(); //Ka
            public Colorf DiffuseColor = new Colorf(); //Kd
            public Colorf SpecularColor = new Colorf(); //Ks
            public Single SpecularWeight = 0; //Ns
            public Single TransmissionFilter = 0; //Tr
            /// <summary>
            /// NI
            /// </summary>
            public Single IndexOfRefraction = 0;
            public Single Alpha = 0f; //d
            public Int32 Illum = 0; //illum

            public String AmbientTexture = ""; //map_Ka
            public String DiffuseTexture = ""; //map_Kd
            public String SpecularTexture = ""; //map_Ks
            public String BumpTexture = ""; //bump map_bump
            public String SpecularWeightTexture = ""; //map_Ns
            public String AlphaTexture = ""; //map_d
            public String ReflectionTexture = ""; //map_refl
        }

        public class OBJFace
        {
            public Int32 SmoothingGroup;
            public List<Int32> VertexIndex = new List<Int32>();
            public List<Int32> TextureIndex = new List<Int32>();
            public List<Int32> NormalIndex = new List<Int32>();

        }
        public class OBJGroup
        {
            public Int32 Index = 0;
            public String MaterialName = null;
            public String Name = "";
            public List<Vector3Df> Verticies = new List<Vector3Df>();
            public List<Vector3Df> Normals = new List<Vector3Df>();
            public List<Vector3Df> TextureUV = new List<Vector3Df>();
            public List<OBJFace> Faces = new List<OBJFace>();
            public override String ToString()
            {
                if(Name.Length > 0)
                    return Name;
                return base.ToString();
            }
        }

        public static Boolean Compare(List<OBJGroup> obj1, List<OBJGroup> obj2)
        {
            if(obj1.Count != obj2.Count)
                return false;

            for(Int32 i = 0; i < obj1.Count; i++)
            {
                OBJGroup groupA = obj1[i];
                OBJGroup groupB = obj2[i];

                if(groupA.Faces.Count != groupB.Faces.Count)
                    return false;

                //if (groupA.MTLLib != groupB.MTLLib)
                //    return false;

                if(groupA.Name != groupB.Name)
                    return false;

                if(groupA.Normals.Count != groupB.Normals.Count)
                    return false;

                if(groupA.TextureUV.Count != groupB.TextureUV.Count)
                    return false;

                if(groupA.Verticies.Count != groupB.Verticies.Count)
                    return false;

                for(Int32 f = 0; f < groupA.Faces.Count; i++)
                {
                    OBJFace faceA = groupA.Faces[f];
                    OBJFace faceB = groupB.Faces[f];

                    //if(faceA.MTL != faceB.MTL)
                    //    return false;

                    if(faceA.NormalIndex.Count != faceB.NormalIndex.Count)
                        return false;

                    if(faceA.SmoothingGroup != faceB.SmoothingGroup)
                        return false;

                    if(faceA.TextureIndex.Count != faceB.TextureIndex.Count)
                        return false;

                    if(faceA.VertexIndex.Count != faceB.VertexIndex.Count)
                        return false;

                }
            }

            return true;
        }


        public void Write(Stream stream)
        {
            var writer = new StreamWriter(stream);

            //write mtl lib
            if(MTLLib.Length > 0)
                writer.WriteLine("mtllib " + MTLLib);


            foreach(OBJGroup group in Groups)
            {


                writer.WriteLine();
                //write vertices
                foreach(Vector3Df v in group.Verticies)
                    writer.WriteLine("v " + v.X.ToString() + " " + v.Y.ToString() + " " + v.Z.ToString());
                writer.WriteLine("# " + group.Verticies.Count.ToString() + " vertices");

                writer.WriteLine();
                //write vertex normals
                foreach(Vector3Df n in group.Normals)
                    writer.WriteLine("vn " + n.X.ToString() + " " + n.Y.ToString() + " " + n.Z.ToString());
                writer.WriteLine("# " + group.Normals.Count.ToString() + " vertex normals");

                writer.WriteLine();
                //write vertex UV coords
                foreach(Vector3Df t in group.TextureUV)
                    writer.WriteLine("vt " + t.X.ToString() + " " + t.Y.ToString() + " " + t.Z.ToString());
                writer.WriteLine("# " + group.TextureUV.Count.ToString() + " texture coords");

                writer.WriteLine();
                writer.WriteLine("g " + group.Name);
                if(group.MaterialName != null)
                    writer.WriteLine("usemtl " + group.MaterialName);

                foreach(OBJFace face in group.Faces)
                {

                    writer.Write("f ");
                    for(Int32 i = 0; i < face.VertexIndex.Count; i++)
                    {

                        if(face.TextureIndex.Count == 0 && face.NormalIndex.Count == 0)
                            writer.Write(face.VertexIndex[i].ToString() + " ");
                        else if(face.TextureIndex.Count != 0 && face.NormalIndex.Count == 0)
                            writer.Write(face.VertexIndex[i].ToString() + "/" + face.TextureIndex[i] + " ");
                        else if(face.TextureIndex.Count == 0)
                            writer.Write(face.VertexIndex[i].ToString() + "//" + face.NormalIndex[i] + " ");
                        else
                            writer.Write(face.VertexIndex[i].ToString() + "/" + face.TextureIndex[i] + "/" + face.NormalIndex[i] + " ");
                    }
                    writer.WriteLine();
                }

            }
            writer.Flush();
        }

        public void Load(Stream stream)
        {
            var reader = new StreamReader(stream);
            Groups.Clear();
            var currentGroup = new OBJGroup();
            String mtllib = "";
            String currentMTL = "";
            Int32 currentSmoothing = 0;
            Int32 vCount = 0;
            Boolean groupFound = false;
            while(!reader.EndOfStream)
            {
                String[] tokens = null;
                String line = reader.ReadLine().Trim();

                if(!line.StartsWith("#") && line.Length > 0)
                {
                    if(line.StartsWith("mtllib"))
                    {
                        //take name of the last group
                        tokens = line.Split(new Char[] { ' ' });
                        if(tokens.Length > 1)
                            mtllib = tokens[tokens.Length - 1];
                        MTLLib = mtllib;
                    }

                    if(line.StartsWith("v "))
                    {
                        if(groupFound)
                        {
                            Groups.Add(currentGroup);
                            currentGroup = new OBJGroup();
                            groupFound = false;
                        }
                        vCount++;
                        if(line.StartsWith("v  "))
                            line = line.Replace("v  ", "v ");

                        tokens = line.Split(new Char[] { ' ' });
                        if(tokens.Length > 3)
                            currentGroup.Verticies.Add(new Vector3Df(Single.Parse(tokens[1]), Single.Parse(tokens[2]), Single.Parse(tokens[3])));
                    }


                    if(line.StartsWith("f "))
                    {
                        if(line.StartsWith("f  "))
                            line = line.Replace("f  ", "f ");

                        tokens = line.Split(new Char[] { ' ' });
                        if(tokens.Length > 2)
                        {
                            var face = new OBJFace();
                            face.SmoothingGroup = currentSmoothing;
                            for(Int32 i = 1; i < tokens.Length; i++)
                            {
                                String[] subTokens = null;
                                Boolean vertex_normal = false;
                                if(tokens[i].Contains("//"))
                                {
                                    vertex_normal = true;
                                    subTokens = tokens[i].Split(new String[] { "//" }, StringSplitOptions.RemoveEmptyEntries);
                                } else
                                {
                                    subTokens = tokens[i].Split(new Char[] { '/' });
                                }

                                if(subTokens.Length == 1) //v v1 v2 v3 ...
                                {
                                    face.VertexIndex.Add(Int32.Parse(subTokens[0]));
                                }
                                if(subTokens.Length == 2 && vertex_normal) //v v1/vt1 v2/vt2 v3/vt3 ...
                                {
                                    face.VertexIndex.Add(Int32.Parse(subTokens[0]));
                                    face.NormalIndex.Add(Int32.Parse(subTokens[1]));
                                } else if(subTokens.Length == 2) //v v1/vt1 v2/vt2 v3/vt3 ...
                                {
                                    face.VertexIndex.Add(Int32.Parse(subTokens[0]));
                                    face.TextureIndex.Add(Int32.Parse(subTokens[1]));
                                } else if(subTokens.Length == 3) //v v1/vt1/vn1
                                {
                                    face.VertexIndex.Add(Int32.Parse(subTokens[0]));
                                    face.TextureIndex.Add(Int32.Parse(subTokens[1]));
                                    face.NormalIndex.Add(Int32.Parse(subTokens[2]));
                                }
                            }

                            currentGroup.Faces.Add(face);
                        }
                    }

                    if(line.StartsWith("vn"))
                    {
                        if(line.StartsWith("vn  "))
                            line = line.Replace("vn  ", "vn ");

                        tokens = line.Split(new Char[] { ' ' });
                        if(tokens.Length > 3)
                            currentGroup.Normals.Add(new Vector3Df(Single.Parse(tokens[1]), Single.Parse(tokens[2]), Single.Parse(tokens[3])));
                    }


                    if(line.StartsWith("vt"))
                    {
                        if(line.StartsWith("vt  "))
                            line = line.Replace("vt  ", "vt ");

                        tokens = line.Split(new Char[] { ' ' });
                        if(tokens.Length > 2)
                        {
                            Single w = 0;
                            if(tokens.Length > 3)
                                w = Single.Parse(tokens[3]);
                            currentGroup.TextureUV.Add(new Vector3Df(Single.Parse(tokens[1]), Single.Parse(tokens[2]), w));
                        }
                    }

                    if(line.StartsWith("g"))
                    {
                        groupFound = true;
                        tokens = line.Split(new Char[] { ' ' });
                        if(tokens.Length > 1)
                        {
                            if(currentGroup.Name != "")
                            {
                                Groups.Add(currentGroup);
                                currentGroup = new OBJGroup();
                                MTLLib = mtllib;
                            }
                            currentGroup.Name = tokens[tokens.Length - 1];
                        } else
                        {
                            if(currentGroup.Name != "")
                                Groups.Add(currentGroup);
                            currentGroup = new OBJGroup();
                            MTLLib = mtllib;
                        }
                    }

                    if(line.StartsWith("usemtl"))
                    {
                        tokens = line.Split(new Char[] { ' ' });
                        if(tokens.Length > 1)
                        {
                            currentMTL = tokens[1];
                            currentGroup.MaterialName = currentMTL;
                        }
                    }

                    if(line.StartsWith("s ") && !line.StartsWith("s off"))
                    {

                        tokens = line.Split(new Char[] { ' ' });
                        if(tokens.Length > 1)
                        {
                            currentSmoothing = Int32.Parse(tokens[1]);
                        }
                    }
                }
            }
            if(currentGroup.Name != "" && !Groups.Contains(currentGroup))
                Groups.Add(currentGroup);

            foreach(OBJGroup group in Groups)
            {
                NamedGroups[group.Name] = group;
            }

        }
        public void LoadMaterials(Stream stream)
        {
            var reader = new StreamReader(stream);

            Materials = new Dictionary<String, Material>();

            Material mat = null;

            while(!reader.EndOfStream)
            {
                String[] tokens = null;
                String line = reader.ReadLine().Trim();

                if(!line.StartsWith("#") && line.Length > 0)
                {
                    tokens = line.Split(new Char[] { ' ' });

                    if(line.StartsWith("newmtl"))
                    {
                        if(tokens.Length > 1)
                            mat = new Material() { Name = tokens[tokens.Length - 1] };
                        Materials[mat.Name] = mat;
                    }

                    if(line.StartsWith("Ns"))
                        mat.SpecularWeight = Single.Parse(tokens[1]);

                    else if(line.StartsWith("Ni"))
                        mat.IndexOfRefraction = Single.Parse(tokens[1]);
                    else if(line.StartsWith("d "))
                        mat.Alpha = Single.Parse(tokens[1]);
                    else if(line.StartsWith("Tf"))
                        mat.TransmissionFilter = Single.Parse(tokens[1]);
                    else if(line.StartsWith("illum"))
                        mat.Illum = Int32.Parse(tokens[1]);
                    else if(line.StartsWith("Ka"))
                        mat.AmbientColor = new Colorf(Single.Parse(tokens[1]), Single.Parse(tokens[2]), Single.Parse(tokens[3]));
                    else if(line.StartsWith("Kd"))
                        mat.DiffuseColor = new Colorf(Single.Parse(tokens[1]), Single.Parse(tokens[2]), Single.Parse(tokens[3]));
                    else if(line.StartsWith("Ks"))
                        mat.SpecularColor = new Colorf(Single.Parse(tokens[1]), Single.Parse(tokens[2]), Single.Parse(tokens[3]));
                    else if(line.StartsWith("map_Ka"))
                        mat.AmbientTexture = tokens[1];
                    else if(line.StartsWith("map_Kd"))
                        mat.DiffuseTexture = tokens[1];
                    else if(line.StartsWith("map_Ks"))
                        mat.SpecularTexture = tokens[1];
                    else if(line.StartsWith("map_d"))
                        mat.AlphaTexture = tokens[1];
                    else if(line.StartsWith("map_bump"))
                        mat.BumpTexture = tokens[1];
                    else if(line.StartsWith("map_refl"))
                        mat.ReflectionTexture = tokens[1];
                }
            }

        }


        public void RemvoeAllExcept(String groupName)
        {
            RemoveAllExcept(new List<String>() { groupName });
        }


        public void RemoveAllExcept(List<String> groupNames)
        {
            String[] currentGroupNames = NamedGroups.Keys.ToArray();

            Boolean found = false;
            foreach(String name in currentGroupNames)
            {
                if(groupNames.Contains(name))
                {
                    found = true;
                    break;
                }
            }
            if(found)
            {
                foreach(String name in currentGroupNames)
                {
                    if(!groupNames.Contains(name))
                        RemoveGroup(name);
                }
            } else
            {
                Int32 i = 0;
            }
        }

        public void RemoveGroup(List<String> groupNames)
        {
            String[] currentGroupNames = NamedGroups.Keys.ToArray();

            foreach(String name in currentGroupNames)
            {
                if(groupNames.Contains(name))
                    RemoveGroup(name);
            }
        }

        public void RemoveGroup(String groupName)
        {
            if(NamedGroups.ContainsKey(groupName))
            {
                OBJGroup groupToRemove = NamedGroups[groupName];
                Int32 groupIndex = Groups.IndexOf(groupToRemove);

                //update face indexes of all groups following it
                for(Int32 i = groupIndex + 1; i < Groups.Count; i++)
                {
                    OBJGroup group = Groups[i];
                    foreach(OBJFace face in group.Faces)
                    {
                        for(Int32 c = 0; c < face.VertexIndex.Count; c++)
                        {
                            face.VertexIndex[c] -= groupToRemove.Verticies.Count;
                        }
                        for(Int32 c = 0; c < face.NormalIndex.Count; c++)
                        {
                            face.NormalIndex[c] -= groupToRemove.Normals.Count;
                        }
                        for(Int32 c = 0; c < face.TextureIndex.Count; c++)
                        {
                            face.TextureIndex[c] -= groupToRemove.TextureUV.Count;
                        }

                    }
                }
                Groups.Remove(groupToRemove);
                if(NamedGroups.ContainsKey(groupToRemove.Name))
                    NamedGroups.Remove(groupToRemove.Name);
            }
        }

    }
}
