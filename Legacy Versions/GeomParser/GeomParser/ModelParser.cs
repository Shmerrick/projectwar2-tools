﻿#undef _LOG_FILE_READS 
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Sharp3D.Math.Core;

public class BinaryLogger : BinaryReader
{

    public BinaryLogger(Stream input) : base(input)
    {
    }

#if _LOG_FILE_READS
    public override bool ReadBoolean()
    {
        bool x = base.ReadBoolean();
        Console.WriteLine($"Boolean: {x}");
        return x;
    }

    public override byte ReadByte()
    {
        byte x = base.ReadByte();
        Console.WriteLine($"Byte: {x}");
        return x;
    }

    public override sbyte ReadSByte()
    {
        sbyte x = base.ReadSByte();
        Console.WriteLine($"Byte: {x}");
        return x;
    }

    public override char ReadChar()
    {
        char x = base.ReadChar();
        Console.WriteLine($"Char: {x}");
        return x;
    }

    public override short ReadInt16()
    {
        short x = base.ReadInt16();
        Console.WriteLine($"Int16: {x}");
        return x;
    }

    public override ushort ReadUInt16()
    {
        ushort x = base.ReadUInt16();
        Console.WriteLine($"UInt16: {x}");
        return x;
    }

    public override int ReadInt32()
    {
        int x = base.ReadInt32();
        Console.WriteLine($"Int32: {x}");
        return x;
    }

    public override uint ReadUInt32()
    {
        uint x = base.ReadUInt32();
        Console.WriteLine($"UInt32: {x}");
        return x;
    }

    public override long ReadInt64()
    {
        long x = base.ReadInt64();
        Console.WriteLine($"Int64: {x}");
        return x;
    }

    public override ulong ReadUInt64()
    {
        ulong x = base.ReadUInt64();
        Console.WriteLine($"UInt64: {x}");
        return x;
    }

    public override float ReadSingle()
    {
        float x = base.ReadSingle();
        Console.WriteLine($"Single: {x}");
        return x;
    }

    public override double ReadDouble()
    {
        double x = base.ReadDouble();
        Console.WriteLine($"Double: {x}");
        return x;
    }

    public override decimal ReadDecimal()
    {
        decimal x = base.ReadDecimal();
        Console.WriteLine($"Decimal: {x}");
        return x;
    }
    
    public override string ReadString()
    {
        string x = base.ReadString();
        Console.WriteLine($"String: {x}");
        return x;
    }
#endif
}

namespace GeomParser
{
    public static class w8
    {
        private static Dictionary<Type, FieldInfo[]> a1 = new Dictionary<Type, FieldInfo[]>();

        public static FieldInfo[] a(Type A_0)
        {
            if(a1.ContainsKey(A_0))
            {
                return a1[A_0];
            }
            FieldInfo[] fields = A_0.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            a1.Add(A_0, fields);
            return fields;
        }
    }

    public class bsd : Attribute
    {
        [CompilerGenerated]
        private Boolean a1;

        public bsd(Boolean A_0)
        {
            this.a(A_0);
        }

        [CompilerGenerated]
        public Boolean a()
        {
            return this.a1;
        }

        [CompilerGenerated]
        public void a(Boolean A_0)
        {
            this.a1 = A_0;
        }
    }

    [Serializable]
    public abstract class ModelBaseClass
    {
        public const Int32 a1 = 0x499602d2;

        protected ModelBaseClass()
        {
        }

        public virtual Boolean a(BinaryLogger A_0)
        {
            return false;
        }

        public virtual Boolean a(BinaryWriter A_0)
        {
            return false;
        }

        public virtual void Read(Stream A_0)
        {
            var reader = new BinaryLogger(A_0);
            this.Read(reader);
            reader.Close();
        }

        public virtual void a(StringBuilder A_0)
        {
            foreach(FieldInfo info in w8.a(base.GetType()))
            {
                if(typeof(ModelBaseClass).IsAssignableFrom(info.FieldType))
                {
                    ((ModelBaseClass)info.GetValue(this)).a(A_0);
                } else if(info.FieldType.IsArray && typeof(ModelBaseClass).IsAssignableFrom(info.FieldType.GetElementType()))
                {
                    var tgArray = (ModelBaseClass[])info.GetValue(this);
                    foreach(ModelBaseClass tg2 in tgArray)
                    {
                        tg2.a(A_0);
                    }
                } else if(info.FieldType.IsArray)
                {
                    var array = (Array)info.GetValue(this);
                    foreach(Object obj2 in array)
                    {
                        Object obj3 = obj2;
                        A_0.AppendLine(String.Format("{0} ({1}): {2}", info.Name, obj3.GetType().Name, obj3));
                    }
                } else
                {
                    Object obj4 = info.GetValue(this);
                    A_0.AppendLine(String.Format("{0} ({1}): {2}", info.Name, obj4.GetType().Name, obj4));
                }
            }
        }

        public List<String> aa()
        {
            var list = new List<String>();
            foreach(FieldInfo info in w8.a(base.GetType()))
            {
                Object[] customAttributes = info.GetCustomAttributes(typeof(bsd), true);
                if((customAttributes != null) && (customAttributes.Length > 0))
                {
                    var bsd = customAttributes[0] as bsd;
                    if(bsd.a())
                    {
                        continue;
                    }
                }
                list.Add(info.Name);
            }
            return list;
        }

        public Dictionary<String, Object> ab()
        {
            var dictionary = new Dictionary<String, Object>();
            foreach(FieldInfo info in w8.a(base.GetType()))
            {
                Object[] customAttributes = info.GetCustomAttributes(typeof(bsd), true);
                if((customAttributes != null) && (customAttributes.Length > 0))
                {
                    var bsd = customAttributes[0] as bsd;
                    if(bsd.a())
                    {
                        continue;
                    }
                }
                dictionary.Add(info.Name, info.GetValue(this));
            }
            return dictionary;
        }

        public virtual Byte[] ac()
        {
            //byte[] buffer = null;
            //MemoryStream output = new MemoryStream();
            //BinaryWriter writer = new BinaryWriter(output);
            //this.b(writer);
            //buffer = output.ToArray();
            //writer.Close();
            //output.Close();
            //return buffer;
            return null;
        }

        public virtual void Read(BinaryLogger A_0)
        {

            if(!this.a(A_0))
            {
                // Console.WriteLine("Structure {");
                foreach(FieldInfo info in w8.a(base.GetType()))
                {
                    Object[] customAttributes = info.GetCustomAttributes(typeof(bsd), true);


                    if((customAttributes != null) && (customAttributes.Length > 0))
                    {
                        var bsd = customAttributes[0] as bsd;
                        if(bsd.a())
                        {
                            continue;
                        }
                    }
                    if(typeof(ModelBaseClass).IsAssignableFrom(info.FieldType))
                    {
                        if(A_0.ReadInt32() == 0x499602d2)
                        {
                            info.SetValue(this, null);

                        } else
                        {
                            Stream baseStream = A_0.BaseStream;
                            baseStream.Position -= 4L;
                            var tg = (ModelBaseClass)Activator.CreateInstance(info.FieldType);
                            tg.Read(A_0);
                            info.SetValue(this, tg);


                        }
                    } else if(info.FieldType.IsArray && typeof(ModelBaseClass).IsAssignableFrom(info.FieldType.GetElementType()))
                    {
                        var tgArray = (ModelBaseClass[])info.GetValue(this);
                        for(Int32 i = 0; i < tgArray.Length; i++)
                        {
                            var tg2 = (ModelBaseClass)Activator.CreateInstance(info.FieldType.GetElementType());
                            tg2.Read(A_0);
                            tgArray[i] = tg2;
                        }
                        info.SetValue(this, tgArray);

                    } else if(info.FieldType.IsGenericType)
                    {
                        Type[] genericArguments = info.FieldType.GetGenericArguments();
                        if((genericArguments.Length == 1) && typeof(ModelBaseClass).IsAssignableFrom(genericArguments[0]))
                        {
                            Type type = genericArguments[0];
                            var typeArguments = new Type[] { type };
                            var list = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeArguments));
                            UInt64 num3 = A_0.ReadUInt64();
                            for(UInt64 j = 0L; j < num3; j += (UInt64)1L)
                            {
                                var tg3 = (ModelBaseClass)Activator.CreateInstance(type);
                                tg3.Read(A_0);
                                list.Add(tg3);
                            }
                            info.SetValue(this, list);

                        } else if((genericArguments.Length == 1) && (genericArguments[0] == typeof(String)))
                        {
                            Type type3 = genericArguments[0];
                            var typeArray3 = new Type[] { type3 };
                            var list2 = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeArray3));
                            UInt64 num5 = A_0.ReadUInt64();
                            for(UInt64 k = 0L; k < num5; k += (UInt64)1L)
                            {
                                list2.Add(this.e(A_0));
                            }
                            info.SetValue(this, list2);

                        } else if((genericArguments.Length == 1) && (genericArguments[0] == typeof(Int32)))
                        {
                            Type type5 = genericArguments[0];
                            var typeArray4 = new Type[] { type5 };
                            var list3 = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeArray4));
                            UInt64 num7 = A_0.ReadUInt64();
                            for(UInt64 m = 0L; m < num7; m += (UInt64)1L)
                            {
                                list3.Add(A_0.ReadInt32());
                            }
                            info.SetValue(this, list3);

                        } else if((genericArguments.Length == 1) && (genericArguments[0] == typeof(Vector2F)))
                        {
                            Type type7 = genericArguments[0];
                            var typeArray5 = new Type[] { type7 };
                            var list4 = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeArray5));
                            UInt64 num9 = A_0.ReadUInt64();
                            for(UInt64 n = 0L; n < num9; n += (UInt64)1L)
                            {
                                var vectorf = new Vector2F(A_0.ReadSingle(), A_0.ReadSingle());

                                list4.Add(vectorf);
                            }
                            info.SetValue(this, list4);

                        } else if((genericArguments.Length == 1) && (genericArguments[0] == typeof(Vector3F)))
                        {
                            Type type9 = genericArguments[0];
                            var typeArray6 = new Type[] { type9 };
                            var list5 = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeArray6));
                            UInt64 num11 = A_0.ReadUInt64();
                            for(UInt64 num12 = 0L; num12 < num11; num12 += (UInt64)1L)
                            {
                                var vectorf2 = new Vector3F(A_0.ReadSingle(), A_0.ReadSingle(), A_0.ReadSingle());

                                list5.Add(vectorf2);
                            }
                            info.SetValue(this, list5);

                        } else if((genericArguments.Length == 1) && (genericArguments[0] == typeof(ColorF)))
                        {
                            Type type11 = genericArguments[0];
                            var typeArray7 = new Type[] { type11 };
                            var list6 = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeArray7));
                            UInt64 num13 = A_0.ReadUInt64();
                            for(UInt64 num14 = 0L; num14 < num13; num14 += (UInt64)1L)
                            {
                                var rf = new ColorF
                                {
                                    Red = A_0.ReadSingle(),
                                    Green = A_0.ReadSingle(),
                                    Blue = A_0.ReadSingle(),
                                    Alpha = A_0.ReadSingle()
                                };
                                list6.Add(rf);
                            }
                            info.SetValue(this, list6);

                        } else if(((genericArguments.Length == 2) && (genericArguments[0] == typeof(Int32))) && typeof(ModelBaseClass).IsAssignableFrom(genericArguments[1]))
                        {
                            Type type13 = genericArguments[1];
                            var typeArray8 = new Type[] { typeof(Int32), type13 };
                            var dictionary = (IDictionary)Activator.CreateInstance(typeof(Dictionary<,>).MakeGenericType(typeArray8));
                            UInt64 num15 = A_0.ReadUInt64();
                            for(UInt64 num16 = 0L; num16 < num15; num16 += (UInt64)1L)
                            {
                                var tg4 = (ModelBaseClass)Activator.CreateInstance(type13);
                                tg4.Read(A_0);
                                dictionary.Add(A_0.ReadInt32(), tg4);
                            }
                            info.SetValue(this, dictionary);

                        } else if(((genericArguments.Length == 2) && (genericArguments[0] == typeof(String))) && typeof(ModelBaseClass).IsAssignableFrom(genericArguments[1]))
                        {
                            Type type15 = genericArguments[1];
                            var typeArray9 = new Type[] { typeof(String), type15 };
                            var dictionary2 = (IDictionary)Activator.CreateInstance(typeof(Dictionary<,>).MakeGenericType(typeArray9));
                            UInt64 num17 = A_0.ReadUInt64();
                            for(UInt64 num18 = 0L; num18 < num17; num18 += (UInt64)1L)
                            {
                                var tg5 = (ModelBaseClass)Activator.CreateInstance(type15);
                                tg5.Read(A_0);
                                dictionary2.Add(this.e(A_0), tg5);
                            }
                            info.SetValue(this, dictionary2);

                        } else
                        {
                            if(((genericArguments.Length != 2) || (genericArguments[0] != typeof(String))) || (genericArguments[1] != typeof(Int32)))
                            {
                                throw new Exception(String.Format("Unknown generic FieldType '{0}'", info.FieldType.FullName));
                            }
                            Type type17 = genericArguments[1];
                            var typeArray10 = new Type[] { typeof(String), type17 };
                            var dictionary3 = (IDictionary)Activator.CreateInstance(typeof(Dictionary<,>).MakeGenericType(typeArray10));
                            UInt64 num19 = A_0.ReadUInt64();
                            for(UInt64 num20 = 0L; num20 < num19; num20 += (UInt64)1L)
                            {
                                Int32 num21 = A_0.ReadInt32();
                                dictionary3.Add(this.e(A_0), num21);
                            }
                            info.SetValue(this, dictionary3);

                        }
                    } else if(info.FieldType.IsArray && info.FieldType.GetElementType().IsEnum)
                    {
                        var array = (Array)info.GetValue(this);
                        Type underlyingType = Enum.GetUnderlyingType(info.FieldType.GetElementType());
                        for(Int32 num22 = 0; num22 < array.Length; num22++)
                        {
                            if(underlyingType == typeof(Int32))
                            {
                                array.SetValue(Enum.ToObject(info.FieldType.GetElementType(), A_0.ReadInt32()), num22);
                            } else if(underlyingType == typeof(Int16))
                            {
                                array.SetValue(Enum.ToObject(info.FieldType.GetElementType(), A_0.ReadInt16()), num22);
                            } else if(underlyingType == typeof(UInt16))
                            {
                                array.SetValue(Enum.ToObject(info.FieldType.GetElementType(), A_0.ReadUInt16()), num22);
                            } else if(underlyingType == typeof(UInt32))
                            {
                                array.SetValue(Enum.ToObject(info.FieldType.GetElementType(), A_0.ReadUInt32()), num22);
                            } else if(underlyingType == typeof(Byte))
                            {
                                array.SetValue(Enum.ToObject(info.FieldType.GetElementType(), A_0.ReadByte()), num22);
                            } else if(underlyingType == typeof(UInt64))
                            {
                                array.SetValue(Enum.ToObject(info.FieldType.GetElementType(), A_0.ReadUInt64()), num22);
                            } else if(underlyingType == typeof(Int64))
                            {
                                array.SetValue(Enum.ToObject(info.FieldType.GetElementType(), A_0.ReadInt64()), num22);
                            } else
                            {
                                if(underlyingType != typeof(SByte))
                                {
                                    throw new Exception(String.Format("Unknown Enum-FieldType '{0}'", underlyingType.FullName));
                                }
                                array.SetValue(Enum.ToObject(info.FieldType.GetElementType(), A_0.ReadSByte()), num22);
                            }
                        }
                        info.SetValue(this, array);

                    } else if(info.FieldType.IsEnum)
                    {
                        Type type20 = Enum.GetUnderlyingType(info.FieldType);
                        if(type20 != typeof(Int32))
                        {
                            if(type20 != typeof(Int16))
                            {
                                if(type20 != typeof(UInt16))
                                {
                                    if(type20 != typeof(UInt32))
                                    {
                                        if(type20 != typeof(Byte))
                                        {
                                            if(type20 != typeof(UInt64))
                                            {
                                                if(type20 != typeof(Int64))
                                                {
                                                    if(type20 != typeof(SByte))
                                                    {
                                                        throw new Exception(String.Format("Unknown Enum-FieldType '{0}'", type20.FullName));
                                                    }
                                                    info.SetValue(this, Enum.ToObject(info.FieldType, A_0.ReadSByte()));

                                                } else
                                                {
                                                    info.SetValue(this, Enum.ToObject(info.FieldType, A_0.ReadInt64()));

                                                }
                                            } else
                                            {
                                                info.SetValue(this, Enum.ToObject(info.FieldType, A_0.ReadUInt64()));

                                            }
                                        } else
                                        {

                                            info.SetValue(this, Enum.ToObject(info.FieldType, A_0.ReadByte()));
                                        }
                                    } else
                                    {
                                        info.SetValue(this, Enum.ToObject(info.FieldType, A_0.ReadUInt32()));

                                    }
                                } else
                                {
                                    info.SetValue(this, Enum.ToObject(info.FieldType, A_0.ReadUInt16()));

                                }
                            } else
                            {
                                info.SetValue(this, Enum.ToObject(info.FieldType, A_0.ReadInt16()));

                            }
                        } else
                        {
                            info.SetValue(this, Enum.ToObject(info.FieldType, A_0.ReadInt32()));

                        }
                    } else if(info.FieldType == typeof(Boolean))
                    {
                        info.SetValue(this, A_0.ReadBoolean());

                    } else if(info.FieldType == typeof(Byte))
                    {
                        info.SetValue(this, A_0.ReadByte());

                    } else if(info.FieldType == typeof(Char))
                    {
                        info.SetValue(this, A_0.ReadChar());

                    } else if(info.FieldType == typeof(Decimal))
                    {
                        info.SetValue(this, A_0.ReadDecimal());

                    } else if(info.FieldType == typeof(Double))
                    {
                        info.SetValue(this, A_0.ReadDouble());

                    } else if(info.FieldType == typeof(Int16))
                    {
                        info.SetValue(this, A_0.ReadInt16());

                    } else if(info.FieldType == typeof(Int32))
                    {
                        info.SetValue(this, A_0.ReadInt32());

                    } else if(info.FieldType == typeof(Int64))
                    {
                        info.SetValue(this, A_0.ReadInt64());

                    } else if(info.FieldType == typeof(SByte))
                    {
                        info.SetValue(this, A_0.ReadSByte());

                    } else if(info.FieldType == typeof(Vector2F))
                    {
                        var vectorf3 = new Vector2F(A_0.ReadSingle(), A_0.ReadSingle());
                        info.SetValue(this, vectorf3);

                    } else if(info.FieldType == typeof(Vector3F))
                    {
                        var vectorf4 = new Vector3F(A_0.ReadSingle(), A_0.ReadSingle(), A_0.ReadSingle());
                        info.SetValue(this, vectorf4);

                    } else if(info.FieldType == typeof(ColorF))
                    {
                        if(A_0.ReadInt32() == 0x499602d2)
                        {
                            info.SetValue(this, null);
                        } else
                        {
                            Stream stream2 = A_0.BaseStream;
                            stream2.Position -= 4L;
                            var rf2 = new ColorF
                            {
                                Red = A_0.ReadSingle(),
                                Green = A_0.ReadSingle(),
                                Blue = A_0.ReadSingle(),
                                Alpha = A_0.ReadSingle()
                            };
                            info.SetValue(this, rf2);
                        }


                    } else if(info.FieldType == typeof(Image))
                    {
                        if(A_0.ReadInt32() == 0x499602d2)
                        {
                            info.SetValue(this, null);
                        } else
                        {
                            Stream stream3 = A_0.BaseStream;
                            stream3.Position -= 4L;
                            Int64 num25 = A_0.ReadInt64();
                            var stream = new MemoryStream(A_0.ReadBytes((Int32)num25));
                            var image = Image.FromStream(stream);
                            stream.Close();
                            stream.Dispose();
                            info.SetValue(this, image);
                        }


                    } else if(info.FieldType == typeof(Single))
                    {
                        info.SetValue(this, A_0.ReadSingle());

                    } else if(info.FieldType == typeof(UInt16))
                    {
                        info.SetValue(this, A_0.ReadUInt16());

                    } else if(info.FieldType == typeof(UInt32))
                    {
                        info.SetValue(this, A_0.ReadUInt32());

                    } else if(info.FieldType == typeof(UInt64))
                    {
                        info.SetValue(this, A_0.ReadUInt64());

                    } else if(info.FieldType == typeof(DateTime))
                    {
                        info.SetValue(this, DateTime.FromBinary(A_0.ReadInt64()));

                    } else if(info.FieldType == typeof(String))
                    {
                        info.SetValue(this, this.e(A_0));
                    } else if(info.FieldType == typeof(Boolean[]))
                    {
                        Boolean[] flagArray = (Boolean[])info.GetValue(this);
                        for(Int32 num26 = 0; num26 < flagArray.Length; num26++)
                        {
                            flagArray[num26] = A_0.ReadBoolean();
                        }
                        info.SetValue(this, flagArray);

                    } else if(info.FieldType == typeof(Byte[]))
                    {
                        Byte[] buffer = (Byte[])info.GetValue(this);
                        info.SetValue(this, A_0.ReadBytes(buffer.Length));

                    } else if(info.FieldType == typeof(Char[]))
                    {
                        Char[] chArray = (Char[])info.GetValue(this);
                        info.SetValue(this, A_0.ReadChars(chArray.Length));
                    } else if(info.FieldType == typeof(Decimal[]))
                    {
                        Decimal[] numArray = (Decimal[])info.GetValue(this);
                        for(Int32 num27 = 0; num27 < numArray.Length; num27++)
                        {
                            numArray[num27] = A_0.ReadDecimal();
                        }
                        info.SetValue(this, numArray);

                    } else if(info.FieldType == typeof(Double[]))
                    {
                        Double[] numArray2 = (Double[])info.GetValue(this);
                        for(Int32 num28 = 0; num28 < numArray2.Length; num28++)
                        {
                            numArray2[num28] = A_0.ReadDouble();
                        }
                        info.SetValue(this, numArray2);

                    } else if(info.FieldType == typeof(Int16[]))
                    {
                        Int16[] numArray3 = (Int16[])info.GetValue(this);
                        for(Int32 num29 = 0; num29 < numArray3.Length; num29++)
                        {
                            numArray3[num29] = A_0.ReadInt16();
                        }
                        info.SetValue(this, numArray3);

                    } else if(info.FieldType == typeof(Int32[]))
                    {
                        Int32[] numArray4 = (Int32[])info.GetValue(this);
                        for(Int32 num30 = 0; num30 < numArray4.Length; num30++)
                        {
                            numArray4[num30] = A_0.ReadInt32();
                        }
                        info.SetValue(this, numArray4);
                    } else if(info.FieldType == typeof(Int64[]))
                    {
                        Int64[] numArray5 = (Int64[])info.GetValue(this);
                        for(Int32 num31 = 0; num31 < numArray5.Length; num31++)
                        {
                            numArray5[num31] = A_0.ReadInt64();
                        }
                        info.SetValue(this, numArray5);

                    } else if(info.FieldType == typeof(SByte[]))
                    {
                        SByte[] numArray6 = (SByte[])info.GetValue(this);
                        for(Int32 num32 = 0; num32 < numArray6.Length; num32++)
                        {
                            numArray6[num32] = A_0.ReadSByte();
                        }

                        info.SetValue(this, numArray6);
                    } else if(info.FieldType == typeof(Single[]))
                    {
                        Single[] numArray7 = (Single[])info.GetValue(this);
                        for(Int32 num33 = 0; num33 < numArray7.Length; num33++)
                        {
                            numArray7[num33] = A_0.ReadSingle();
                        }
                        info.SetValue(this, numArray7);

                    } else if(info.FieldType == typeof(UInt16[]))
                    {
                        UInt16[] numArray8 = (UInt16[])info.GetValue(this);
                        for(Int32 num34 = 0; num34 < numArray8.Length; num34++)
                        {
                            numArray8[num34] = A_0.ReadUInt16();
                        }
                        info.SetValue(this, numArray8);

                    } else if(info.FieldType == typeof(UInt32[]))
                    {
                        UInt32[] numArray9 = (UInt32[])info.GetValue(this);
                        for(Int32 num35 = 0; num35 < numArray9.Length; num35++)
                        {
                            numArray9[num35] = A_0.ReadUInt32();
                        }
                        info.SetValue(this, numArray9);

                    } else if(info.FieldType == typeof(UInt64[]))
                    {
                        UInt64[] numArray10 = (UInt64[])info.GetValue(this);
                        for(Int32 num36 = 0; num36 < numArray10.Length; num36++)
                        {
                            numArray10[num36] = A_0.ReadUInt64();
                        }
                        info.SetValue(this, numArray10);

                    } else if(info.FieldType == typeof(Vector2F[]))
                    {
                        var vectorfArray = (Vector2F[])info.GetValue(this);
                        for(Int32 num37 = 0; num37 < vectorfArray.Length; num37++)
                        {
                            vectorfArray[num37] = new Vector2F(A_0.ReadSingle(), A_0.ReadSingle());
                        }
                        info.SetValue(this, vectorfArray);

                    } else if(info.FieldType == typeof(Vector3F[]))
                    {
                        var vectorfArray2 = (Vector3F[])info.GetValue(this);
                        for(Int32 num38 = 0; num38 < vectorfArray2.Length; num38++)
                        {
                            vectorfArray2[num38] = new Vector3F(A_0.ReadSingle(), A_0.ReadSingle(), A_0.ReadSingle());
                        }
                        info.SetValue(this, vectorfArray2);

                    } else if(info.FieldType == typeof(ColorF[]))
                    {
                        var rfArray = (ColorF[])info.GetValue(this);
                        for(Int32 num39 = 0; num39 < rfArray.Length; num39++)
                        {
                            rfArray[num39] = new ColorF { Red = A_0.ReadSingle(), Green = A_0.ReadSingle(), Blue = A_0.ReadSingle(), Alpha = A_0.ReadSingle() };
                        }
                        info.SetValue(this, rfArray);

                    } else
                    {
                        if(info.FieldType != typeof(String[]))
                        {
                            throw new Exception(String.Format("Unknown FieldType '{0}'", info.FieldType.FullName));
                        }
                        String[] strArray = (String[])info.GetValue(this);
                        for(Int32 num40 = 0; num40 < strArray.Length; num40++)
                        {
                            strArray[num40] = this.e(A_0);
                        }
                        info.SetValue(this, strArray);

                    }
                }
            }
        }

        protected String e(BinaryLogger A_0)
        {
            Int32 num = A_0.PeekChar();
            var builder = new StringBuilder();
            while(num != -1)
            {
                if(Convert.ToChar(num) == '\0')
                {
                    A_0.ReadChar();
                    break;
                }
                builder.Append(A_0.ReadChar());
                num = A_0.PeekChar();
            }
            return builder.ToString();
        }

        public virtual void w(String A_0)
        {
            var stream = new FileStream(A_0, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            this.Read(stream);
            stream.Close();
        }

        //public virtual void x(string A_0)
        //{
        //    FileStream output = new FileStream(A_0, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
        //    BinaryWriter writer = new BinaryWriter(output);
        //    this.b(writer);
        //    writer.Close();
        //    output.Close();
        //}
    }

    public class Geom : ModelBaseClass
    {
        public const String _geom_magic = "gsLF";
        public Char[] magic = new Char[4];
        public UInt32 version;
        public UInt32 file_size;
        public UInt32 id;
        public UInt32 bone_count;
        public UInt32 bones_offset;
        public UInt32 mesh_count;
        public UInt32 meshes_offset;

        public virtual String GetMagic()
        {
            return new String(this.magic);
        }
    }

    public class Logger
    {
        public static void Info(Type type, String message, Object param)
        {
            Console.WriteLine(message);
        }
        public static void Error(Type type, String message, Object param)
        {
            Console.WriteLine(message);
        }
        public static void Warn(Type type, String message, Object param)
        {
            Console.WriteLine(message);
        }
    }

    public class Mesh
    {
        public MeshHeader header = new MeshHeader();
        public VertexArray vertices = new VertexArray();
        public List<VertexIndex> indices = new List<VertexIndex>();
        public UnkChunk1 _unk_chunk1 = new UnkChunk1();
        public List<VertexWeight> weights = new List<VertexWeight>();
    }

    public class VertexWeight : ModelBaseClass
    {
        public VertexWeight() { }
        public VertexWeight(UInt32 i, Single w) : this() { index = i; weight = w; }
        public UInt32 index;
        public Single weight;
    }

    public class UnkChunk1 : ModelBaseClass
    {
        public Int16 a;
        public UInt16 b;
        public UInt32 c;
    }

    public class Vertex : ModelBaseClass
    {
        public Single position_x;
        public Single position_y;
        public Single position_z;
        public Single normal_x;
        public Single normal_y;
        public Single normal_z;
        public Single texture_u;
        public Single texture_v;
    }

    public class VertexArray : ModelBaseClass
    {
        public Vertex[] VertexList = new Vertex[0];
    }

    public class VertexIndex : ModelBaseClass
    {
        public static Int32 a1 = 2;
        public UInt16 index;

        public VertexIndex()
        {
        }

        public VertexIndex(UInt16 A_0)
        {
            this.index = A_0;
        }

        public String a()
        {
            return this.index.ToString();
        }

        public Boolean a(BinaryLogger A_0)
        {
            this.index = A_0.ReadUInt16();
            return true;
        }
    }

    public class MeshHeader : ModelBaseClass
    {
        public Int16 unk1;
        public UInt16 vertex_count;
        public UInt32 vertices_offset;
        public UInt32 triangle_count;
        public UInt32 triangles_offset;
        public UInt32 weight_count;
        public UInt32 weight_offset;
        public UInt32 unk_count;
        public UInt32 unk_offset;
    }

    public class Bone : ModelBaseClass
    {
        public UInt32 _name_offset;
        public Single[] _bone_data = new Single[0x1a];
    }

    public class auv
    {
    }

    public delegate Image RequestTexture(String fileName);

    public class ModelParser
    {
        public ModelFile a;
        public Geom geom;
        public ot c;
        public Boolean d;
        public Boolean e;
        public List<String> _bone_names = new List<String>();
        public List<Mesh> meshes = new List<Mesh>();
        public List<Bone> _bones = new List<Bone>();
        public List<auv> i = new List<auv>();

        [CompilerGenerated]
        private static RequestTexture j;
        [CompilerGenerated]
        private String k;


        public void Load(Stream A_0)
        {
            var reader = new BinaryLogger(A_0);
            geom = new Geom();
            this.geom.Read(reader);


            Int64 file_size = this.geom.bones_offset;
            if(reader.BaseStream.Position != file_size)
            {
                Logger.Warn(base.GetType(), "Bone Defininitions: Reader Position '{0}' wrong. Supposed Position '{1}'.", new Object[] { reader.BaseStream.Position, file_size });
            }
            reader.BaseStream.Seek(file_size, SeekOrigin.Begin);
            for(Int32 i = 0; i < this.geom.bone_count; i++)
            {
                var bone = new Bone();
                bone.Read(reader);
                this._bones.Add(bone);
            }
            Int64 num3 = 0L;
            if(this.geom.bone_count != 0)
            {
                num3 = this._bones[0]._name_offset;
            }


            file_size = this.geom.meshes_offset;
            if(reader.BaseStream.Position != file_size)
            {
                Logger.Warn(base.GetType(), "Geosets: Reader Position '{0}' wrong. Supposed Position '{1}'.", new Object[] { reader.BaseStream.Position, file_size });
            }
            reader.BaseStream.Seek(file_size, SeekOrigin.Begin);
            for(Int32 j = 0; j < this.geom.mesh_count; j++)
            {
                var mesh = new Mesh();
                this.meshes.Add(mesh);
                mesh.header.Read(reader);
            }
            for(Int32 k = 0; k < this.geom.mesh_count; k++)
            {
                Mesh mesh = this.meshes[k];
                if(mesh.header.unk_count != 0)
                {
                    mesh._unk_chunk1.Read(reader);
                }
            }
            for(Int32 m = 0; m < this.geom.mesh_count; m++)
            {
                Mesh mesh = this.meshes[m];
                Int64 mesh_index_x_mesh_header_size = m * 0x20;

                file_size = (this.geom.meshes_offset + mesh.header.vertices_offset) + mesh_index_x_mesh_header_size;
                if(reader.BaseStream.Position != file_size)
                {
                    Logger.Warn(base.GetType(), "Geoset [{0}]: Reader Position '{1}' wrong. Supposed Position '{2}'.", new Object[] { m, reader.BaseStream.Position, file_size });
                }
                reader.BaseStream.Seek(file_size, SeekOrigin.Begin);


                mesh.vertices.VertexList = new Vertex[mesh.header.vertex_count];
                mesh.vertices.Read(reader);


                file_size = (this.geom.meshes_offset + mesh.header.triangles_offset) + mesh_index_x_mesh_header_size;
                if(reader.BaseStream.Position != file_size)
                {
                    Logger.Warn(base.GetType(), "Geoset [{0}] Indices: Reader Position '{1}' wrong. Supposed Position '{2}'.", new Object[] { m, reader.BaseStream.Position, file_size });
                }
                reader.BaseStream.Seek(file_size, SeekOrigin.Begin);


                for(Int32 n = 0; n < (mesh.header.triangle_count * 3); n++)
                {
                    var a = new VertexIndex();
                    a.Read(reader);
                    mesh.indices.Add(a);
                }
                for(Int32 num9 = 0; num9 < mesh._unk_chunk1.b; num9++)
                {
                    var aqn = new VertexWeight();
                    aqn.Read(reader);
                    mesh.weights.Add(aqn);
                }
            }
            if(this.geom.bone_count != 0)
            {
                file_size = num3 + 0x20L;
                if(reader.BaseStream.Position != file_size)
                {
                    Logger.Warn(base.GetType(), "Bone Names: Reader Position '{0}' wrong. Supposed Position '{1}'.", new Object[] { reader.BaseStream.Position, file_size });
                }
                reader.BaseStream.Seek(file_size, SeekOrigin.Begin);
                for(Int32 num10 = 0; num10 < this.geom.bone_count; num10++)
                {
                    var builder = new StringBuilder();
                    while(reader.BaseStream.Position < reader.BaseStream.Length)
                    {
                        Char ch = (Char)reader.ReadByte();
                        if(ch == '\0')
                        {
                            this._bone_names.Add(builder.ToString());
                            break;
                        }
                        builder.Append(ch);
                    }
                }
            }
            if(reader.BaseStream.Position != reader.BaseStream.Length)
            {
                Logger.Warn(base.GetType(), "No exact match at end of Geom-File. Reader-Position: {0} File-End: {1}", new Object[] { reader.BaseStream.Position, reader.BaseStream.Length });
            }
            this.d = true;
        }
    }

    public class ot
    {
        private String a1 = String.Empty;
        private String b1 = String.Empty;
        private String c1 = String.Empty;
        private String d1 = String.Empty;

        public ot(String A_0)
        {
            this.d(A_0);
        }

        private void a()
        {
            this.d1 = this.c1 + @"\" + this.b1 + this.a1;
        }

        public void a(String A_0)
        {
            this.a1 = A_0;
            this.a();
        }

        public static Boolean a(String A_0, out ot A_1)
        {
            A_1 = new ot(A_0);
            return true;
        }

        public String b()
        {
            return this.c1;
        }

        public void b(String A_0)
        {
            this.c1 = A_0;
            this.a();
        }

        public String c()
        {
            return this.b1;
        }

        public void c(String A_0)
        {
            this.b1 = A_0;
            this.a();
        }

        public void d()
        {
            if(!this.f())
            {
                Directory.CreateDirectory(this.d1);
            } else
            {
                Directory.CreateDirectory(this.e().DirectoryName);
            }
        }

        public void d(String A_0)
        {
            this.d1 = A_0;
            this.c1 = Path.GetDirectoryName(this.d1);
            this.a1 = Path.GetExtension(this.d1);
            this.b1 = Path.GetFileNameWithoutExtension(this.d1);
        }

        public FileInfo e()
        {
            return new FileInfo(this.d1);
        }

        public Boolean f()
        {
            return Path.HasExtension(this.d1);
        }

        public String g()
        {
            return this.a1;
        }

        public String h()
        {
            return (this.b1 + this.a1);
        }

        public String i()
        {
            return this.j();
        }

        public String j()
        {
            return this.d1;
        }

        public Boolean k()
        {
            if(this.f())
            {
                return File.Exists(this.d1);
            }
            return Directory.Exists(this.d1);
        }

        public DirectoryInfo l()
        {
            return new DirectoryInfo(this.d1);
        }
    }

    public interface aee
    {
        ot a();
        void a(ot A_0);
        void b();
        Boolean c();
        Boolean d();
        void k(Stream A_0);
        void t(ot A_0);
    }

    public abstract class ajk
    {
        protected ajk()
        {
        }

        public abstract String a { get; set; }
        public abstract Int64 b { get; set; }
    }

    public abstract class a6m
    {
        protected a6m()
        {
        }

        protected internal void a(BinaryLogger A_0)
        {
        }
        protected internal void a(BinaryWriter A_0)
        {
        }
        protected internal void a(BinaryLogger A_0, ajk A_1)
        {
        }
        protected virtual String a(BinaryLogger A_0, Int64 A_1)
        {
            Byte[] bytes = A_0.ReadBytes((Int32)A_1);
            return Encoding.UTF8.GetString(bytes);
        }

        protected virtual void a(BinaryLogger A_0, ModelBaseClass A_1)
        {
            A_1.Read(A_0);
        }

        protected internal void a(BinaryWriter A_0, Object A_1, Int64 A_2)
        {
        }
        protected virtual void a(BinaryWriter A_0, Object A_1, String A_2)
        {
            Byte[] bytes = Encoding.UTF8.GetBytes(A_2);
            this.a(A_0, A_1, (Int64)bytes.Length);
            A_0.Write(bytes);
        }

        protected virtual void a(BinaryWriter A_0, Object A_1, ModelBaseClass A_2)
        {
            Byte[] buffer = A_2.ac();
            this.a(A_0, A_1, (Int64)buffer.Length);
            A_0.Write(buffer);
        }

        protected internal ajk b(BinaryLogger A_0)
        {
            return null;
        }
        protected internal void b(BinaryWriter A_0)
        {
        }
    }

    public class b02 : aee
    {
        protected ot a1;
        protected Boolean b1;
        protected Boolean c1;

        private Boolean d1;

        private Boolean e1;

        private ModelChunkHandler f1;

        public b02(ModelChunkHandler A_0)
        {
            this.a(A_0);
            this.b(true);
        }

        public ot a()
        {
            return this.a1;
        }

        private void a(ModelChunkHandler A_0)
        {
            this.f1 = A_0;
        }

        public virtual void a(ot A_0)
        {
            var output = new FileStream(A_0.j(), FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            var writer = new BinaryWriter(output);
            this.g().b(writer);
            this.g().a(writer);
            writer.Close();
            this.c1 = true;
        }

        public void a(Boolean A_0)
        {
            this.d1 = A_0;
        }

        public virtual void a(Stream A_0)
        {
            var reader = new BinaryLogger(A_0);
            this.g().a(reader);
            while(reader.BaseStream.Position < reader.BaseStream.Length)
            {
                ajk ajk = this.g().b(reader);
                Int64 num = ajk.b;
                Int64 num2 = reader.BaseStream.Position + num;
                if(this.f())
                {
                    Logger.Info(base.GetType(), "ChunkType {0} (Length = {1})", new Object[] { ajk.a, num });
                }
                if(num == 0L)
                {
                    if(this.c())
                    {
                        reader.BaseStream.Position = num2;
                    }
                } else
                {
                    this.g().a(reader, ajk);
                    if(num2 != reader.BaseStream.Position)
                    {
                        Logger.Warn(base.GetType(), "Difference in data-positions after Chunk '{0}'. Reader-Position: {1} Next Chunk: {2} - {3} bytes", new Object[] { ajk.a, reader.BaseStream.Position, num2, num2 - reader.BaseStream.Position });
                    }
                    if(this.c())
                    {
                        reader.BaseStream.Position = num2;
                    }
                }
            }
            if(reader.BaseStream.Position != reader.BaseStream.Length)
            {
                Logger.Warn(base.GetType(), "No exact match at end of file. Reader-Position: {0} File-End: {1}", new Object[] { reader.BaseStream.Position, reader.BaseStream.Length });
            }
            reader.Close();
            this.b1 = true;
        }

        public Boolean b()
        {
            return this.c1;
        }



        public void b(Boolean A_0)
        {
            this.e1 = A_0;
        }


        public Boolean c()
        {
            return this.e1;
        }

        public Boolean d()
        {
            return this.b1;
        }

        public void e()
        {
            this.a(this.a1);
        }

        public Boolean f()
        {
            return this.d1;
        }

        public ModelChunkHandler g()
        {
            return this.f1;
        }


        void aee.b()
        {
            throw new NotImplementedException();
        }

        public void k(Stream A_0)
        {
            throw new NotImplementedException();
        }

        public void t(ot A_0)
        {
            throw new NotImplementedException();
        }

        ot aee.a()
        {
            throw new NotImplementedException();
        }

        void aee.a(ot A_0)
        {
            if(!File.Exists(A_0.j()))
            {
                throw new FileNotFoundException();
            }
            this.a1 = A_0;
            var stream = new FileStream(A_0.j(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            this.a(stream);
            this.c1 = true;
        }



        Boolean aee.c()
        {
            throw new NotImplementedException();
        }

        Boolean aee.d()
        {
            throw new NotImplementedException();
        }

        void aee.k(Stream A_0)
        {
            throw new NotImplementedException();
        }

        void aee.t(ot A_0)
        {
            throw new NotImplementedException();
        }
    }

    public enum ModelChunkType
    {
        Unknown,
        String,
        Geoset_v1,
        Vertex_v1,
        Index_v1,
        Bone_v1
    }

    public class ModelChunkDefinition : ajk
    {

        public ModelChunkDefinition(ModelChunkType type, Int32 length)
        {
            InternalChunkType = type;
            this.InternalChunkLength = length;
        }

        public override Int64 b
        {
            get {
                return (Int64)this.InternalChunkLength;
            }
            set {
                this.InternalChunkLength = (Int32)value;
            }
        }

        public override String a
        {
            get {
                return this.InternalChunkType.ToString();
            }
            set {
                ModelChunkType type;
                Enum.TryParse(value, out type);
                InternalChunkType = type;
            }
        }

        public Int32 InternalChunkLength { get; private set; }

        public ModelChunkType InternalChunkType { get; private set; }
    }

    public class ModelChunkHandler : a6m
    {
        public ModelChunkHandler(ModelFile modelFile)
        {
            this.ModelFile = modelFile;
        }

        protected void a(BinaryLogger reader)
        {
            this.ModelFile.Geosets = new List<ModelGeoset>();
            this.ModelFile.Vertices = new List<ModelVertex>();
            this.ModelFile.Indices = new List<UInt32>();
            this.ModelFile.Bones = new List<ModelBone>();
        }

        protected void a(BinaryWriter writer)
        {
        }

        protected void a(BinaryLogger reader, ajk definition)
        {
            Int32 num;
            Int32 num2;
            var definition2 = definition as ModelChunkDefinition;
            switch(definition2.InternalChunkType)
            {
                case ModelChunkType.String:
                    this.ModelFile.Name = this.a(reader, (Int64)definition2.InternalChunkLength);
                    return;

                case ModelChunkType.Geoset_v1:
                {
                    var geoset = new ModelGeoset(String.Empty);
                    this.a(reader, geoset);
                    this.ModelFile.Geosets.Add(geoset);
                    return;
                }
                case ModelChunkType.Vertex_v1:
                {
                    var vertex = new ModelVertex();
                    this.a(reader, vertex);
                    this.ModelFile.Vertices.Add(vertex);
                    return;
                }
                case ModelChunkType.Index_v1:
                    num = ((Int32)definition2.InternalChunkLength) / 4;
                    num2 = 0;
                    break;

                case ModelChunkType.Bone_v1:
                {
                    var bone = new ModelBone(String.Empty);
                    this.a(reader, bone);
                    this.ModelFile.Bones.Add(bone);
                    return;
                }
                default:
                    return;
            }
            while(num2 < num)
            {
                this.ModelFile.Indices.Add(reader.ReadUInt32());
                num2++;
            }
        }

        protected ajk b(BinaryLogger reader)
        {
            Int32 num = reader.ReadInt32();
            return new ModelChunkDefinition((ModelChunkType)num, reader.ReadInt32());
        }

        protected void a(BinaryWriter writer, Object type, Int64 length)
        {
            writer.Write((Int32)type);
            writer.Write((Int32)length);
        }

        protected void b(BinaryWriter writer)
        {
            this.a(writer, ModelChunkType.String, this.ModelFile.Name);
            foreach(ModelGeoset geoset in this.ModelFile.Geosets)
            {
                this.a(writer, ModelChunkType.Geoset_v1, geoset);
            }
            foreach(ModelVertex vertex in this.ModelFile.Vertices)
            {
                this.a(writer, ModelChunkType.Vertex_v1, vertex);
            }
            this.a(writer, ModelChunkType.Index_v1, (Int64)(this.ModelFile.Indices.Count * 4));
            foreach(UInt32 num in this.ModelFile.Indices)
            {
                writer.Write(num);
            }
            foreach(ModelBone bone in this.ModelFile.Bones)
            {
                this.a(writer, ModelChunkType.Bone_v1, bone);
            }
        }

        public ModelFile ModelFile { get; private set; }
    }

    public abstract class ModelFile : IDisposable, aee
    {
        protected b02 _ChunkFile;

        public ModelFile(String name, ModelProvider source)
        {
            this.Name = name;
            this.Source = source;
            this.Geosets = new List<ModelGeoset>();
            this.Vertices = new List<ModelVertex>();
            this.Indices = new List<UInt32>();
            this.Bones = new List<ModelBone>();
        }

        private void CreateChunkFile()
        {
            if(this._ChunkFile == null)
            {
                this._ChunkFile = new b02(new ModelChunkHandler(this));
            }
        }

        public virtual void Dispose()
        {
            foreach(ModelGeoset geoset in this.Geosets)
            {
                geoset.Dispose();
            }
        }

        public abstract Boolean HasVertexTextureCoordinates(Int32 layer);
        public void Load(ot filePath)
        {
            this.CreateChunkFile();
            // this._ChunkFile.b(filePath);
            this._ChunkFile.a(filePath);
        }

        public void Load(Stream dataStream)
        {
            this.CreateChunkFile();
            this._ChunkFile.a(dataStream);
        }

        public static void LogError(String message)
        {
            Logger.Error(typeof(ModelFile), message, new Object[0]);
        }

        public static void LogInfo(String message, params Object[] args)
        {
            Logger.Info(typeof(ModelFile), message, args);
        }

        public static void LogWarn(String message)
        {
            Logger.Warn(typeof(ModelFile), message, new Object[0]);
        }

        public void Save()
        {
            this.CreateChunkFile();
            this._ChunkFile.e();
        }

        public void Save(ot filePath)
        {
            this.CreateChunkFile();
            this._ChunkFile.a(filePath);
        }

        public List<ModelBone> Bones { get; set; }

        public virtual List<String> FileDependencies
        {
            get {
                var list = new List<String>();
                list.AddRange(this.TextureFileDependencies);
                return list;
            }
        }

        public ot FilePath
        {
            get {
                this.CreateChunkFile();
                return this._ChunkFile.a();
            }
        }

        public List<ModelGeoset> Geosets { get; set; }

        public virtual Boolean HasAnimations
        {
            get {
                return false;
            }
        }

        public virtual Boolean HasBones
        {
            get {
                return ((this.Bones != null) && (this.Bones.Count != 0));
            }
        }

        public virtual Boolean HasGeosets
        {
            get {
                return ((this.Geosets != null) && (this.Geosets.Count != 0));
            }
        }

        public virtual Boolean HasIndices
        {
            get {
                return ((this.Indices != null) && (this.Indices.Count != 0));
            }
        }

        public virtual Boolean HasMaterials
        {
            get {
                return ((this.Materials != null) && (this.Materials.Count != 0));
            }
        }

        public abstract Boolean HasVertexColor { get; }

        public abstract Boolean HasVertexNormal { get; }

        public abstract Boolean HasVertexWeights { get; }

        public virtual Boolean HasVertices
        {
            get {
                return ((this.Vertices != null) && (this.Vertices.Count != 0));
            }
        }

        public List<UInt32> Indices { get; set; }

        public Boolean IsLoaded
        {
            get {
                this.CreateChunkFile();
                return this._ChunkFile.d();
            }
        }

        public Boolean IsSaved
        {
            get {
                this.CreateChunkFile();
                return this._ChunkFile.b();
            }
        }

        public virtual List<ModelMaterial> Materials
        {
            get {
                var list = new List<ModelMaterial>();
                var list2 = new List<String>();
                foreach(ModelGeoset geoset in this.Geosets)
                {
                    if((geoset.Material != null) && !list2.Contains(geoset.Material.Name))
                    {
                        list.Add(geoset.Material);
                        list2.Add(geoset.Material.Name);
                    }
                }
                return list;
            }
        }

        public String Name { get; set; }

        public ModelProvider Source { get; private set; }

        public abstract class ModelProvider
        {
            protected ModelProvider()
            {
            }

            public abstract ModelFile Model { get; }
        }

        public List<String> TextureFileDependencies
        {
            get {
                var list = new List<String>();
                foreach(ModelMaterial material in this.Materials)
                {
                    if(((material.DiffuseMap != null) && !String.IsNullOrEmpty(material.DiffuseMap.Source)) && File.Exists(material.DiffuseMap.Source))
                    {
                        list.Add(material.DiffuseMap.Source);
                    }
                    if(((material.EmissiveMap != null) && !String.IsNullOrEmpty(material.EmissiveMap.Source)) && File.Exists(material.EmissiveMap.Source))
                    {
                        list.Add(material.EmissiveMap.Source);
                    }
                    if(((material.NormalMap != null) && !String.IsNullOrEmpty(material.NormalMap.Source)) && File.Exists(material.NormalMap.Source))
                    {
                        list.Add(material.NormalMap.Source);
                    }
                    if(((material.SpecularMap != null) && !String.IsNullOrEmpty(material.SpecularMap.Source)) && File.Exists(material.SpecularMap.Source))
                    {
                        list.Add(material.SpecularMap.Source);
                    }
                }
                return list;
            }
        }

        public List<ModelVertex> Vertices { get; set; }

        public ot a()
        {
            throw new NotImplementedException();
        }

        public void a(ot A_0)
        {
            throw new NotImplementedException();
        }

        public void b()
        {
            throw new NotImplementedException();
        }

        public Boolean c()
        {
            throw new NotImplementedException();
        }

        public Boolean d()
        {
            throw new NotImplementedException();
        }

        public void k(Stream A_0)
        {
            throw new NotImplementedException();
        }

        public void t(ot A_0)
        {
            throw new NotImplementedException();
        }
    }

    public class ModelBone : ModelBaseClass
    {
        public ModelBone()
        {
        }

        public ModelBone(String name)
        {
            this.Name = name;
            this.ParentBoneIndex = -1;
            this.Pivot = new Vector3F();
        }

        public Boolean IsRoot { get; set; }

        public String Name { get; set; }

        public Int32 ParentBoneIndex { get; set; }

        public Vector3F Pivot { get; set; }
    }

    public class ModelGeoset : ModelBaseClass, IDisposable
    {
        public ModelGeoset()
        {
        }

        public ModelGeoset(String name)
        {
            this.Name = name;
            this.Material = ModelMaterial.Default;
        }

        public void Dispose()
        {
            if(this.Material != null)
            {
                this.Material.Dispose();
                this.Material = null;
            }
        }

        public Int32 IndexCount { get; set; }

        public ModelMaterial Material { get; set; }

        public String Name { get; set; }

        public Int32 StartIndex { get; set; }

        public Int32 StartVertex { get; set; }

        public Int32 VertexCount { get; set; }
    }

    public class ModelVertexWeight : ModelBaseClass
    {
        public ModelVertexWeight()
        {
        }

        public ModelVertexWeight(Int32 boneIndex, Single weight)
        {
            this.BoneIndex = boneIndex;
            this.Weight = weight;
        }

        public Int32 BoneIndex { get; set; }

        public Single Weight { get; set; }
    }

    public class ModelVertex : ModelBaseClass
    {
        public ModelVertex()
        {
            this.TextureCoordinates = new List<Vector2F>();
            this.Weights = new List<ModelVertexWeight>();
        }

        public ColorF Color { get; set; }

        public Vector3F Normal { get; set; }

        public Vector3F Position { get; set; }

        public List<Vector2F> TextureCoordinates { get; set; }

        public List<ModelVertexWeight> Weights { get; set; }
    }
    public enum ModelTextureType
    {
        Unknown,
        FileNameTexture,
        ReplaceableSkin1,
        ReplaceableSkin2,
        ReplaceableSkin3,
        ReplaceableSkin4,
        ReplaceableSkin5,
        SolidSkin1
    }

    public enum ModelBlendMode
    {
        Unknown,
        Opaque,
        Transparent,
        AlphaBlend,
        Additive,
        AdditiveAlpha,
        Modulate1x,
        Modulate2x
    }

    public class ModelMaterialTexture : ModelBaseClass, IDisposable
    {
        public ModelMaterialTexture()
        {
        }

        public ModelMaterialTexture(String name, String source, Image texture, Boolean hasAlpha)
        {
            this.Name = name;
            this.Source = source;
            this.Texture = texture;
            this.HasAlpha = hasAlpha;
        }

        public void Dispose()
        {
            if(this.Texture != null)
            {
                this.Texture.Dispose();
                this.Texture = null;
            }
        }

        public Boolean HasAlpha { get; set; }

        public String Name { get; set; }

        public String Source { get; set; }

        public Image Texture { get; set; }
    }

    public class ModelMaterial : ModelBaseClass, IDisposable
    {
        public static ModelMaterial Default = new ModelMaterial("Default", ModelTextureType.ReplaceableSkin1);

        public ModelMaterial()
        {
            this.BlendMode = ModelBlendMode.Opaque;
        }

        public ModelMaterial(String name, ModelTextureType textureType)
        {
            this.Name = name;
            this.TextureType = textureType;
            this.BlendMode = ModelBlendMode.Opaque;
            this.DiffuseColor = new ColorF(1f, 1f, 1f, 1f);
            this.AmbientColor = new ColorF(1f, 1f, 1f, 1f);
            this.SpecularColor = new ColorF(1f, 1f, 1f, 1f);
            this.EmissiveColor = new ColorF(0f, 0f, 0f, 1f);
        }

        public void Dispose()
        {
            if(this.DiffuseMap != null)
            {
                this.DiffuseMap.Dispose();
                this.DiffuseMap = null;
            }
            if(this.EmissiveMap != null)
            {
                this.EmissiveMap.Dispose();
                this.EmissiveMap = null;
            }
            if(this.SpecularMap != null)
            {
                this.SpecularMap.Dispose();
                this.SpecularMap = null;
            }
            if(this.NormalMap != null)
            {
                this.NormalMap.Dispose();
                this.NormalMap = null;
            }
        }

        public ColorF AmbientColor { get; set; }

        public ModelBlendMode BlendMode { get; set; }

        public ColorF DiffuseColor { get; set; }

        public ModelMaterialTexture DiffuseMap { get; set; }

        public ColorF EmissiveColor { get; set; }

        public ModelMaterialTexture EmissiveMap { get; set; }

        public String Name { get; set; }

        public ModelMaterialTexture NormalMap { get; set; }

        public ColorF SpecularColor { get; set; }

        public ModelMaterialTexture SpecularMap { get; set; }

        public ModelTextureType TextureType { get; set; }
    }
}
