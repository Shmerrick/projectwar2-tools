#include <windows.h>
#include <stdio.h>


void PrintError(DWORD dwErr) {
  char szMsg[256];
  DWORD dwFlags = FORMAT_MESSAGE_IGNORE_INSERTS |
                  FORMAT_MESSAGE_MAX_WIDTH_MASK |
                  FORMAT_MESSAGE_FROM_SYSTEM;

  if (!::FormatMessage(dwFlags, NULL, dwErr, 0, szMsg, sizeof(szMsg), NULL)) strcpy(szMsg, "Unknown error.");
  printf(szMsg);
  printf("\n");
}


void main(int argc, char *argv[]) {
  int iRetCode = EXIT_SUCCESS;

  if (argc != 2) {
    printf("\nDelete stream program: www.flexhex.com\n\nUsage:\n  DS stream\n\nExample:\n  DS C:\\file.dat:text\n\n");
    exit(EXIT_SUCCESS);
  }

  try {
    if (!::DeleteFile(argv[1])) throw ::GetLastError();
  }
  catch (DWORD dwErrCode) {
    PrintError(dwErrCode);
    iRetCode = EXIT_FAILURE;
  }

  exit(iRetCode);
}
