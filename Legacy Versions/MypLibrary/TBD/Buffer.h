
#pragma once

#include "Platform.h"

#include <crtdbg.h>
#include <stdint.h>
#include <iostream>


#define _WS2_32_WINSOCK_SWAP_LONGLONG(l)            \
            ( ( ((l) >> 56) & 0x00000000000000FFLL ) |       \
              ( ((l) >> 40) & 0x000000000000FF00LL ) |       \
              ( ((l) >> 24) & 0x0000000000FF0000LL ) |       \
              ( ((l) >>  8) & 0x00000000FF000000LL ) |       \
              ( ((l) <<  8) & 0x000000FF00000000LL ) |       \
              ( ((l) << 24) & 0x0000FF0000000000LL ) |       \
              ( ((l) << 40) & 0x00FF000000000000LL ) |       \
              ( ((l) << 56) & 0xFF00000000000000LL ) )

__inline unsigned __int64 ntohll2(unsigned __int64 Value)
{
	const unsigned __int64 Retval = _WS2_32_WINSOCK_SWAP_LONGLONG(Value);
	return Retval;
}



class Buffer
{
private:

	unsigned int _capacity, _level, _offset;
	unsigned int _packetSize, _offsetStart;
	int32_t _currentOp;
public:
	unsigned char *_data;

	Buffer() noexcept;
	Buffer(unsigned int capacity, unsigned int align = sizeof(unsigned long)) noexcept;
	//Buffer(const Buffer &);
	~Buffer();
	void setCurrentOp(int32_t op) noexcept { _currentOp = op; }
	void clear();

	unsigned int level() const noexcept { return _level; }
	unsigned int offset() const noexcept { return _offset; }
	void seek(unsigned int pos) noexcept
	{
		_offset = pos;
		_level = _capacity - pos;
	}
	unsigned int capacity() const noexcept { return _capacity; }
	void setPacketSize(unsigned size) noexcept
	{
		_packetSize = size;
		_offsetStart = _offset;
	}
	void setLevel(unsigned int offset) noexcept
	{
		_level = offset;
	}
	bool write(const void *data, unsigned int size);

	bool read(void *data, unsigned int size);
	bool read(Buffer &buf, unsigned int size);
	bool read(int8_t *value) { return read(value, sizeof(*value)); }
	bool read(uint8_t *value) { return read(value, sizeof(*value)); }
	bool read(int16_t *value) { return read(value, sizeof(*value)); }
	bool read(uint16_t *value) { return read(value, sizeof(*value)); }
	bool read(int32_t *value) { return read(value, sizeof(*value)); }
	bool read(int64_t *value) { return read(value, sizeof(*value)); }
	bool readr(int16_t *value) { bool r = read(value, sizeof(*value)); *value = ntohs(*value);  return r; }
	bool readr(int32_t *value) { bool r = read(value, sizeof(*value)); *value = ntohl(*value);  return r; }
	bool readr(int64_t *value) { bool r = read(value, sizeof(*value)); *value = ntohll2(*value);  return r; }
	bool peek(void *data, unsigned int size, bool frameRead = true) const;
	bool peek(int8_t *value)  const { return peek(value, sizeof(*value)); }
	bool peek(int16_t *value) const { return peek(value, sizeof(*value)); }
	bool peek(int32_t *value) const { return peek(value, sizeof(*value)); }
	bool peekr(int32_t *value, bool frameRead = true) { bool r = peek(value, sizeof(*value), frameRead); *value = ntohl2(*value);  return r; }
	bool discard(unsigned int size) noexcept;

	std::string readString32()
	{
		int32_t filenameSize = 0;
		readr(&filenameSize);

		char* filename = new char[filenameSize + 1];
		memset(filename, 0, filenameSize + 1);
		read(filename, filenameSize);

		std::string str(filename);
		delete[] filename;
		return str;
	}

	bool readString32(std::string& str)
	{
		int32_t size = 0;
		if (readr(&size) && size > 0 && size < 0xFFFF)
		{
			char* buffer = new char[size];
			read(buffer, size);
			str = std::string(buffer, size);
			delete[] buffer;
			return true;
		}
		return false;
	}
	unsigned char &operator[](unsigned int index);
	unsigned char operator[](unsigned int index) const;

	Buffer &operator=(Buffer &&buf) noexcept;
};