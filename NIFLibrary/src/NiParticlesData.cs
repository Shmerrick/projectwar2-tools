/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

namespace NIFLibrary
{
    using System;
    using System.IO;
#if OpenTK
	using OpenTK;
	using OpenTK.Graphics;
	using Matrix = OpenTK.Matrix4;
	using Color3 = OpenTK.Graphics.Color4;
#elif SharpDX
	using SharpDX;
#elif MonoGame
	using Microsoft.Xna.Framework;
	using Color3 = Microsoft.Xna.Framework.Color;
	using Color4 = Microsoft.Xna.Framework.Color;
#else
    using System.Numerics;
    using Matrix = System.Numerics.Matrix4x4;
    using Color3 = System.Numerics.Vector3;
    using Color4 = System.Numerics.Vector4;
#endif

    /// <summary>
    /// Class NiParticlesData.
    /// </summary>
    public class NiParticlesData : NiGeometryData
    {
        /// <summary>
        /// The number particles
        /// </summary>
        public UInt16 NumParticles;

        /// <summary>
        /// The particle radius
        /// </summary>
        public Single ParticleRadius;

        /// <summary>
        /// The has radii
        /// </summary>
        public Boolean HasRadii;

        /// <summary>
        /// The radii
        /// </summary>
        public Single[] Radii;

        /// <summary>
        /// The number active
        /// </summary>
        public UInt16 NumActive;

        /// <summary>
        /// The has sizes
        /// </summary>
        public Boolean HasSizes;

        /// <summary>
        /// The sizes
        /// </summary>
        public Single[] Sizes;

        /// <summary>
        /// The has rotations
        /// </summary>
        public Boolean HasRotations;

        /// <summary>
        /// The rotations
        /// </summary>
        public Vector4[] Rotations;

        /// <summary>
        /// Initializes a new instance of the <see cref="NiParticlesData"/> class.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="reader">The reader.</param>
        public NiParticlesData(NiFile file, BinaryReader reader) : base(file, reader)
        {
            if (this.File.Header.Version <= eNifVersion.v4_0_0_2)
            {
                this.NumParticles = reader.ReadUInt16();
            }
            if (this.File.Header.Version <= eNifVersion.v10_0_1_0)
            {
                this.ParticleRadius = reader.ReadSingle();
            }
            if (this.File.Header.Version >= eNifVersion.v10_1_0_0)
            {
                this.HasRadii = reader.ReadBoolean(Version);
                if (this.HasRadii)
                {
                    this.Radii = reader.ReadFloatArray((Int32)this.NumVertices);
                }
            }
            this.NumActive = reader.ReadUInt16();
            this.HasSizes = reader.ReadBoolean(Version);
            if (this.HasSizes)
            {
                this.Sizes = reader.ReadFloatArray((Int32)this.NumVertices);
            }
            if (this.File.Header.Version >= eNifVersion.v10_0_1_0)
            {
                this.HasRotations = reader.ReadBoolean(Version);
                if (this.HasRotations)
                {
                    this.Rotations = new Vector4[this.NumVertices];
                    var num = 0;
                    while ((Int64)num < (Int64)(UInt64)this.NumVertices)
                    {
                        this.Rotations[num] = reader.ReadVector4();
                        num++;
                    }
                }
            }
        }
    }
}
