/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

namespace NIFLibrary
{
	using System;
	using System.IO;

    /// <summary>
    /// Class NiPixelData.
    /// </summary>
    public class NiPixelData : ATextureRenderData
	{
        /// <summary>
        /// The number pixels
        /// </summary>
        public UInt32 NumPixels;

        /// <summary>
        /// The number faces
        /// </summary>
        public UInt32 NumFaces;

        /// <summary>
        /// The pixel data
        /// </summary>
        public Byte[][] PixelData;

        /// <summary>
        /// Initializes a new instance of the <see cref="NiPixelData"/> class.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="reader">The reader.</param>
        public NiPixelData(NiFile file, BinaryReader reader) : base(file, reader)
		{
			this.NumPixels = reader.ReadUInt32();
			if (base.Version >= eNifVersion.v20_0_0_4)
			{
				this.NumFaces = reader.ReadUInt32();
				this.PixelData = new Byte[this.NumFaces][];
                var num = 0;
				while ((Int64)num < (Int64)(UInt64)this.NumFaces)
				{
					this.PixelData[num] = new Byte[this.NumPixels];
                    var num2 = 0;
					while ((Int64)num2 < (Int64)(UInt64)this.NumPixels)
					{
						this.PixelData[num][num2] = reader.ReadByte();
						num2++;
					}
					num++;
				}
			}
			if (base.Version <= eNifVersion.v10_2_0_0)
			{
				this.NumFaces = 1u;
				this.PixelData = new Byte[this.NumFaces][];
                var num3 = 0;
				while ((Int64)num3 < (Int64)(UInt64)this.NumFaces)
				{
					this.PixelData[num3] = new Byte[this.NumPixels];
                    var num4 = 0;
					while ((Int64)num4 < (Int64)(UInt64)this.NumPixels)
					{
						this.PixelData[num3][num4] = reader.ReadByte();
						num4++;
					}
					num3++;
				}
			}
		}
	}
}
