﻿using System.Numerics;

namespace NIFLibrary
{
    //public static class Extensions
    //{
    //    public static Matrix4x4 Mult(this ref Matrix4x4 lhs, ref Matrix4x4 rhs, ref Matrix4x4 output)
    //    {
    //        return output = Matrix4x4.Multiply(lhs, rhs);
    //    }

    //    public static float operator [,](this Matrix4x4 lhs)
    //    {
    //        return output = Matrix4x4.Multiply(lhs, rhs);
    //    }

    //    public static Vector3 Transform(this ref Vector3 lhs, ref Matrix4x4 rhs, ref Vector3 output)
    //    {
    //        return output = Vector3.Transform(lhs, rhs);
    //    }
    //}

    public static class Matrix4x4Extensions
    {
        public static Matrix4x4 Mult(this ref Matrix4x4 lhs, ref Matrix4x4 rhs, ref Matrix4x4 output)
        {
            return output = Matrix4x4.Multiply(lhs, rhs);
        }

        /// <summary>
        /// Sets the value in specific matrix position.
        /// [,] as an extension doesn't work here.
        /// </summary>
        /// <param name="m">Matrix4x4</param>
        /// <param name="i">int idx</param>
        /// <param name="j">int idx</param>
        /// <param name="val">float</param>
        /// <returns></returns>
        public static System.Boolean SetVal(this ref Matrix4x4 m, System.Int32 i, System.Int32 j, System.Single val)
        {
            // NAK on idx out of bounds
            if (i < 0 || j < 0 || i > 3 || j > 3)
                return false;

            switch (i)
            {
                case 0:
                    switch (j)
                    {
                        case 0: m.M11 = val; break;
                        case 1: m.M12 = val; break;
                        case 2: m.M13 = val; break;
                        case 3: m.M14 = val; break;
                    }
                    return true;

                case 1:
                    switch (j)
                    {
                        case 0: m.M21 = val; break;
                        case 1: m.M22 = val; break;
                        case 2: m.M23 = val; break;
                        case 3: m.M24 = val; break;
                    }
                    return true;

                case 2:
                    switch (j)
                    {
                        case 0: m.M31 = val; break;
                        case 1: m.M32 = val; break;
                        case 2: m.M33 = val; break;
                        case 3: m.M34 = val; break;
                    }
                    return true;

                case 3:
                    switch (j)
                    {
                        case 0: m.M41 = val; break;
                        case 1: m.M42 = val; break;
                        case 2: m.M43 = val; break;
                        case 3: m.M44 = val; break;
                    }
                    return true;

                default:
                    return false;
            }
        }

        public static Vector3 Transform(this ref Vector3 lhs, ref Matrix4x4 rhs, ref Vector3 output)
        {
            return output = Vector3.Transform(lhs, rhs);
        }
    }
}
