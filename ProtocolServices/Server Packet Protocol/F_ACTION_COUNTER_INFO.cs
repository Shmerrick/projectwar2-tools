using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_ACTION_COUNTER_INFO, FrameType.Game)]
    public class F_ACTION_COUNTER_INFO : Frame
    {
        public F_ACTION_COUNTER_INFO() : base((int)GameOp.F_ACTION_COUNTER_INFO)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
