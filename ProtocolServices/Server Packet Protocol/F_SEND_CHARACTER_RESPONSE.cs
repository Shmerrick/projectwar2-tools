using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SEND_CHARACTER_RESPONSE, FrameType.Game)]
    public class F_SEND_CHARACTER_RESPONSE : Frame
    {
        public string Username;

        public F_SEND_CHARACTER_RESPONSE() : base((int)GameOp.F_SEND_CHARACTER_RESPONSE)
        {
        }

        public static F_SEND_CHARACTER_RESPONSE Create(string username)
        {
            return new F_SEND_CHARACTER_RESPONSE()
            {
                Username = username
            };
        }

		protected override void SerializeInternal()
        {
            FillString(Username, 24);
            WriteByte(0);
        }
    }
}
