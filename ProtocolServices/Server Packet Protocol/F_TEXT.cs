using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_TEXT, FrameType.Game)]
    public class F_TEXT : Frame
    {
        public byte Unk1;
        public string Text;
        public byte Unk2;
        public F_TEXT() : base((int)GameOp.F_TEXT)
        {
        }

        protected override void DeserializeInternal()
        {
            Unk1 = ReadByte();
            Text = ReadCString();
            Unk2 = ReadByte();
        }
    }
}
