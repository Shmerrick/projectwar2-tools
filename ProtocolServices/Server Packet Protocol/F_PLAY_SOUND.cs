using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAY_SOUND, FrameType.Game)]
    public class F_PLAY_SOUND : Frame
    {
        public ushort SoundID;
        public static F_PLAY_SOUND Create(ushort soundID)
        {
            return new F_PLAY_SOUND()
            {
                SoundID = soundID
            };
        }
        public F_PLAY_SOUND() : base((int)GameOp.F_PLAY_SOUND)
        {
        }

        protected override void SerializeInternal()
        {
            WriteByte(0);
            WriteUInt16((ushort)SoundID);
            Fill(0, 10);
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
