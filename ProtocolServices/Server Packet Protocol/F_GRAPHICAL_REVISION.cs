using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_GRAPHICAL_REVISION, FrameType.Game)]
    public class F_GRAPHICAL_REVISION : Frame
    {
        public F_GRAPHICAL_REVISION() : base((int)GameOp.F_GRAPHICAL_REVISION)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
