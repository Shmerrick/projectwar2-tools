using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_ESTABLISH_DATAGRAM, FrameType.Game)]
    public class F_ESTABLISH_DATAGRAM : Frame
    {
        public F_ESTABLISH_DATAGRAM() : base((int)GameOp.F_ESTABLISH_DATAGRAM)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
