using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_ZONEJUMP_FAILED, FrameType.Game)]
    public class F_ZONEJUMP_FAILED : Frame
    {
        public F_ZONEJUMP_FAILED() : base((int)GameOp.F_ZONEJUMP_FAILED)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
