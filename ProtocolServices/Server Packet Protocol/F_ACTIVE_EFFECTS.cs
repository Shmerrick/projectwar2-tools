using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_ACTIVE_EFFECTS, FrameType.Game)]
    public class F_ACTIVE_EFFECTS : Frame
    {
        public ushort ObjectID;
        public List<ActiveEffect> Effects = new List<ActiveEffect>();
        public class ActiveEffect
        {
            public ushort AbilityID;
            public ushort CasterID;
            public List<ActiveEffectData> Data = new List<ActiveEffectData>();

        }

        public class ActiveEffectData
        {
            public byte Unk1;
            public byte Unk2;
            public byte Unk3;
            public int Unk4;
        }
        public F_ACTIVE_EFFECTS() : base((int)GameOp.F_ACTIVE_EFFECTS)
        {
        }
		
		protected override void SerializeInternal()
        {
            WriteUInt16(ObjectID);
            WriteByte((byte)Effects.Count);

            foreach (var e in Effects)
            {
                WriteUInt16R(e.AbilityID);
                WriteUInt16R(e.CasterID);
                WriteByte((byte)e.Data.Count);
                for (int i = 0; i < e.Data.Count; i++)
                {
                    WriteByte(e.Data[i].Unk1);
                    WriteByte(e.Data[i].Unk2);
                    WriteByte(e.Data[i].Unk3);
                    WriteZigZag(e.Data[i].Unk4);
                }
            }
        }
    }
}
