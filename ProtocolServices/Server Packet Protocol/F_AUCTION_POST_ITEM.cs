using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_AUCTION_POST_ITEM, FrameType.Game)]
    public class F_AUCTION_POST_ITEM : Frame
    {
        public F_AUCTION_POST_ITEM() : base((int)GameOp.F_AUCTION_POST_ITEM)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
