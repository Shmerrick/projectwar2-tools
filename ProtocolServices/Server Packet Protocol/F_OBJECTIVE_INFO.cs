using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Game.Entities;
using WarServer.Net;
using WarServer.Services;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;
using WarShared.Utility.Math;

namespace WarGameServer.Protocol
{
  
    [FrameRoute((int)GameOp.F_OBJECTIVE_INFO, FrameType.Game)]
    public class F_OBJECTIVE_INFO : Frame
    {
        public class Encounter
        {
            public uint EncounterId { get; set; }
            public string Description { get; set; }
            public List<EncounterStage> Stages { get; set; } = new List<EncounterStage>();
            public EncounterStage CurrentStage { get; set; }
            public byte Difficulty { get; set; }
        }

        public class EncounterObjective
        {
            public byte Index { get; set; }
            public ushort Current { get; set; }
            public ushort Total { get; set; }
            public string Description { get; set; }
        }

        public class EncounterStage
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public List<Vector4> SpawnPoint { get; set; } = new List<Vector4>();
            public Realm Realm { get; set; }
            public List<EncounterObjective> Objectives { get; set; } = new List<EncounterObjective>();
            public uint StageIndex { get; set; }
            public uint TotalTime { get; set; }
            public uint TimeLeft { get; set; }
        }

        public class EncounterStageRealmInfo
        {
       
        }

        public Encounter EncounterInfo;

        public F_OBJECTIVE_INFO() : base((int)GameOp.F_OBJECTIVE_INFO)
        {
        }
		
		protected override void SerializeInternal()
        {
            WriteUInt32(EncounterInfo.EncounterId);
            WriteByte(0); // Type
            WriteByte(0);
            WriteByte(1);  // 1 1 condition 2 or condition
            WriteUInt16(0);
            WritePascalString(EncounterInfo.Description);
            WriteByte(0);
            WriteUInt32(EncounterInfo.CurrentStage.StageIndex);
            WriteByte(0);

            WriteByte((byte)EncounterInfo.CurrentStage.Objectives.Count);

            foreach (var objective in EncounterInfo.CurrentStage.Objectives)
            {
                WriteByte(objective.Index);
                WriteUInt16((ushort)objective.Total);  // kill count
                WriteUInt16((ushort)objective.Current);
                WriteByte(0);
               // WritePascalString(objective.Description);
                WritePascalString(objective.Description);
            }

            WriteByte((byte)EncounterInfo.Difficulty);    // difficulty 0 - 2
            WriteByte(0);
            WritePascalString(EncounterInfo.CurrentStage.Name);
            WriteByte(0);
            WritePascalString(EncounterInfo.CurrentStage.Description);
            WriteUInt32(EncounterInfo.CurrentStage.TotalTime);       // time left
            WriteUInt32(EncounterInfo.CurrentStage.TimeLeft);       // time left
            WriteUInt32(0);
            WriteByte(0xD1);
            WriteUInt32(0);
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
