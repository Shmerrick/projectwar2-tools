using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REQUEST_CHAR_TEMPLATES, FrameType.Game)]
    public class F_REQUEST_CHAR_TEMPLATES : Frame
    {
        public F_REQUEST_CHAR_TEMPLATES() : base((int)GameOp.F_REQUEST_CHAR_TEMPLATES)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
