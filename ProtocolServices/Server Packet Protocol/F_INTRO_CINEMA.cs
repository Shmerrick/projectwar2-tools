using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_INTRO_CINEMA, FrameType.Game)]
    public class F_INTRO_CINEMA : Frame
    {
        public F_INTRO_CINEMA() : base((int)GameOp.F_INTRO_CINEMA)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
