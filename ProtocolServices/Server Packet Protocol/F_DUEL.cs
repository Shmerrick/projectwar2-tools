using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_DUEL, FrameType.Game)]
    public class F_DUEL : Frame
    {
        public F_DUEL() : base((int)GameOp.F_DUEL)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
