using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_MORALE_LIST, FrameType.Game)]
    public class F_MORALE_LIST : Frame
    {
        private List<AbilityBinData> Abilities = new List<AbilityBinData>();

        private F_MORALE_LIST() : base((int)GameOp.F_MORALE_LIST)
        {
        }

        public static F_MORALE_LIST Create(List<AbilityBinData> abilities)
        {
            return new F_MORALE_LIST()
            {
                Abilities = abilities
            };
        }

        protected override void SerializeInternal()
        {
            ushort slot1 = 0;
            ushort slot2 = 0;
            ushort slot3 = 0;
            ushort slot4 = 0;

            foreach (var ability in Abilities)
            {
                if (ability.MoraleLevel == 1)
                    slot1 = (ushort)ability.ID;
                if (ability.MoraleLevel == 2)
                    slot2 = (ushort)ability.ID;
                if (ability.MoraleLevel == 3)
                    slot3 = (ushort)ability.ID;
                if (ability.MoraleLevel == 4)
                    slot4 = (ushort)ability.ID;
            }

            WriteUInt16(slot1); //morale 1
            WriteUInt16(slot2); //morale 2
            WriteUInt16(slot3); //morale 3
            WriteUInt16(slot4); //morale 4

            Fill(0, 3);
        }
    }
}
