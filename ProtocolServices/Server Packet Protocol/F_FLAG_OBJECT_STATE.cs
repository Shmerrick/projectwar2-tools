using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_FLAG_OBJECT_STATE, FrameType.Game)]
    public class F_FLAG_OBJECT_STATE : Frame
    {
        public F_FLAG_OBJECT_STATE() : base((int)GameOp.F_FLAG_OBJECT_STATE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
