using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_DO_ABILITY_AT_POS, FrameType.Game)]
    public class F_DO_ABILITY_AT_POS : Frame
    {
        public ushort unk;
        public ushort objectID;
        public ushort casterX;
        public ushort casterY;
        public ushort casterZoneID;
        public ushort casterZ;
        public ushort abilityID;
        public ushort x;
        public ushort y;
        public ushort z;
        public ushort targetZoneID;
        public byte sequence;
        public byte unk7;
        public byte unk8;
        public byte unk9;

        public F_DO_ABILITY_AT_POS() : base((int)GameOp.F_DO_ABILITY_AT_POS)
        {
        }
		
        protected override void DeserializeInternal()
        {
            unk = ReadUInt16();
            objectID = ReadUInt16();
            casterX = ReadUInt16();
            casterY = ReadUInt16();
            casterZoneID = ReadUInt16();
            casterZ = ReadUInt16();
            abilityID = ReadUInt16();
            x = ReadUInt16();
            y = ReadUInt16();
            z = ReadUInt16();
            targetZoneID = ReadUInt16();
            sequence = ReadByte();
            unk7 = ReadByte();
            unk8 = ReadByte();
            unk9 = ReadByte();
        }
    }
}
