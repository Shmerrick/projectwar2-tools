using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_LOGINQUEUE, FrameType.Game)]
    public class F_LOGINQUEUE : Frame
    {
        public F_LOGINQUEUE() : base((int)GameOp.F_LOGINQUEUE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
