using WarServer.Game.Entities;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_RENOWN, FrameType.Game)]
    public class F_PLAYER_RENOWN : Frame
    {
        private PlayerData Player;
        public F_PLAYER_RENOWN() : base((int)GameOp.F_PLAYER_RENOWN)
        {
        }

        public static F_PLAYER_RENOWN Create(Player player)
        {
            return new F_PLAYER_RENOWN()
            {
                Player = player.Data
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt32((uint)0);
            WriteUInt32((uint)Player.RenownXP);
            WriteByte((byte)Player.Renown);
            WriteByte((byte)0);
            WriteByte((byte)0);
            WriteByte((byte)0);
        }

    }
}
