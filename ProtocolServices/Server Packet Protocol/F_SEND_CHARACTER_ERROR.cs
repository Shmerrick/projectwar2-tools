using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SEND_CHARACTER_ERROR, FrameType.Game)]
    public class F_SEND_CHARACTER_ERROR : Frame
    {
        private string Username;
        private string Message;

        public F_SEND_CHARACTER_ERROR() : base((int)GameOp.F_SEND_CHARACTER_ERROR)
        {
        }

        public static F_SEND_CHARACTER_ERROR Create(string username, string message)
        {
            return new F_SEND_CHARACTER_ERROR()
            {
                Username = username,
                Message = message
            };
        }

		protected override void SerializeInternal()
        {
            FillString(Username, 24);
            WriteString(Message);
            WriteByte(0);
        }
    }
}
