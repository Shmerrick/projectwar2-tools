﻿using WarServer.Game.Groups;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_GROUP_STATUS, FrameType.Game)]
    public class F_GROUP_STATUS : Frame
    {
        public enum GroupStatusType
        {
            GroupUpdate = 1,
            GroupSecurityUpdate = 2,
            GroupAssistantUpdate = 3,
        }

        private GroupStatusType Type;
        private PlayerGroup Group;
        private byte Hide;

        public F_GROUP_STATUS() : base((int)GameOp.F_GROUP_STATUS)
        {
        }

        public static F_GROUP_STATUS Create(GroupStatusType type, PlayerGroup group, byte hide)
        {
            return new F_GROUP_STATUS()
            {
                Type = type,
                Group = group,
                Hide = hide
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16((ushort)Group.ID);
            WriteByte((byte)Type);
            WriteByte((byte)Hide);

            if (Type == GroupStatusType.GroupUpdate)
            {
                WriteByte((byte)Group.LootMode);
                WriteByte((byte)Group.LootThreshold);
                WriteUInt32(Group?.MasterLooter?.ID ?? 0);
                WriteByte((byte)(Group.NoNeedOnGreed ? 1 : 0));
                WriteByte((byte)(Group.AutoLootInRvr ? 1 : 0));
                WriteUInt32(Group?.MainAssist?.ID ?? 0);
                WriteUInt32(Group?.Leader?.ID ?? 0);
            }
        }
    }
}
