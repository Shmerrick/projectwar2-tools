using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CATAPULT, FrameType.Game)]
    public class F_CATAPULT : Frame
    {
        public F_CATAPULT() : base((int)GameOp.F_CATAPULT)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
