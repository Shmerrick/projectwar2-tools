using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAY_EFFECT, FrameType.Game)]
    public class F_PLAY_EFFECT : Frame
    {
        public ushort EffectID { get; set; }
        public uint X { get; set; }
        public uint Y { get; set; }
        public uint Z { get; set; }

        public static F_PLAY_EFFECT Create(ushort effectID, uint x, uint y, uint z)
        {
            return new F_PLAY_EFFECT()
            {
                EffectID = effectID,
                X = x,
                Y = y,
                Z = z
            };
        }
        public F_PLAY_EFFECT() : base((int)GameOp.F_PLAY_EFFECT)
        {
        }

        protected override void SerializeInternal()
        {
            WriteUInt16(EffectID);
            WriteUInt16(0);

            WriteUInt32(X);
            WriteUInt32(Y);
            WriteUInt32(Z);

            WriteUInt16(100);
            WriteUInt16(100);
            WriteUInt16(100);
            WriteUInt16(100);
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
