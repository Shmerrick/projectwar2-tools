using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAY_VOICE_OVER, FrameType.Game)]
    public class F_PLAY_VOICE_OVER : Frame
    {
        public F_PLAY_VOICE_OVER() : base((int)GameOp.F_PLAY_VOICE_OVER)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
