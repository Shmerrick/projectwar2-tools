using System.Collections.Generic;
using WarServer.Game.Entities;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_OBJECT_STATE, FrameType.Game)]
    public class F_OBJECT_STATE : Frame
    {
        public const int StateMovement = 1;
        public const int StateLookAt = 2;
        public const int ExtZone = 2;
        public const int StateAnimations = 0x10;
        public const int StateFlying = 0x20;
        public const int StateFollowing = 0x40;

        private ushort ObjectID;
        private ushort TargetObjectID;
        private ushort X;
        private ushort Y;
        private ushort Z;
        private float Heading;
        private byte WoundsPct;
        private ushort ZoneID;
        private ushort Speed;
        private ushort DestX;
        private ushort DestY;
        private ushort DestZ;
        private ushort DestZoneID;
        private List<(byte, byte)> Animations = new List<(byte, byte)>();
        private int State;
        private bool Flying;

        public F_OBJECT_STATE() : base((int)GameOp.F_OBJECT_STATE)
        {
        }

        public static F_OBJECT_STATE Create(Monster monster, float woundsPct)
        {
            return new F_OBJECT_STATE()
            {
                ObjectID = monster.ObjectID,
                TargetObjectID = monster.Target?.ObjectID ?? 0,
                X = (ushort)monster.ZoneX,
                Y = (ushort)monster.ZoneY,
                Z = (ushort)monster.Location.Z,
                Heading = monster.Heading,
                WoundsPct = (byte)woundsPct,
                ZoneID = (ushort)monster.Zone.ID,
                Flying = monster.Data.Flying,
                Speed = monster.Speed,
                DestX = monster.DestX,
                DestY = monster.DestY,
                DestZ = monster.DestZ,
                DestZoneID = monster.DestZoneID,
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16(ObjectID);
            WriteUInt16((ushort)X);
            WriteUInt16((ushort)(Y));
            WriteUInt16((ushort)(Z));
            WriteByte(WoundsPct);

            if (ZoneID > 255)
                State |= ExtZone;

            if (Flying)
                State |= StateFlying;

            WriteByte((byte)State);
            WriteByte((byte)ZoneID);
            WriteByte(0);
            WriteUInt32(3);

            if ((State & StateMovement) == 0x0)
                WriteUInt16R(Util.ToHeading16(Heading));
            else
            {
                WriteUInt16R((ushort)Speed);
                WriteByte(0); //some float number that cannot be greater then 80h
                WriteUInt16R((ushort)(DestX));
                WriteUInt16R((ushort)(DestY));
                WriteUInt16R((ushort)(DestZ));
                WriteByte((byte)DestZoneID);
            }

            if ((State & StateLookAt) == StateLookAt)
                WriteUInt16R(TargetObjectID);

            if ((State & StateAnimations) == StateAnimations)
            {
                WriteByte((byte)Animations.Count);
                foreach (var anim in Animations)
                {
                    WriteByte(anim.Item1);
                    WriteByte(anim.Item2);

                }
            }
        }
    }
}
