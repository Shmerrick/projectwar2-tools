using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_BUY_CAREER_PACKAGE, FrameType.Game)]
    public class F_BUY_CAREER_PACKAGE : Frame
    {
        public byte ActionType;
        public CareerCategoryType CategoryID;
        public ushort PackageID;

        public F_BUY_CAREER_PACKAGE() : base((int)GameOp.F_BUY_CAREER_PACKAGE)
        {
        }
		
        protected override void DeserializeInternal()
        {
            ActionType = ReadByte();
            CategoryID = (CareerCategoryType)ReadByte();
            PackageID = ReadUInt16();
        }
    }
}
