using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_RECEIVE_ENCRYPTKEY, FrameType.Game)]
    public class F_RECEIVE_ENCRYPTKEY : Frame
    {
        private bool Encrypt;
        public F_RECEIVE_ENCRYPTKEY() : base((int)GameOp.F_RECEIVE_ENCRYPTKEY)
        {
        }

        public static F_RECEIVE_ENCRYPTKEY Create(bool encrypt)
        {
            return new F_RECEIVE_ENCRYPTKEY()
            {
                Encrypt = encrypt
            };
        }
		protected override void SerializeInternal()
        {
            WriteBool(Encrypt);
        }
    }
}
