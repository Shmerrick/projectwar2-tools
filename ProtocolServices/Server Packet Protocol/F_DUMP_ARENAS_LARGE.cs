using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_DUMP_ARENAS_LARGE, FrameType.Game)]
    public class F_DUMP_ARENAS_LARGE : Frame
    {
        public byte Slot;
        public F_DUMP_ARENAS_LARGE() : base((int)GameOp.F_DUMP_ARENAS_LARGE)
        {
        }

        public static F_DUMP_ARENAS_LARGE Create(byte slot)
        {
            return new F_DUMP_ARENAS_LARGE()
            {
                Slot = slot
            };
        }
        protected override void DeserializeInternal()
        {
            Slot = ReadByte();
        }
    }
}
