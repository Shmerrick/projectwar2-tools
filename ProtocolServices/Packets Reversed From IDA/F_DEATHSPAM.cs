﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{

    public class F_DEATHSPAM : MythicPacket
    {
        public int Player1_data { get; set; }
        public int Player1_data4 { get; set; }
        public int Player1_data8 { get; set; }
        public int Player1_data12 { get; set; }
        public int Player1_data16 { get; set; }
        public string Player1_Name { get; set; }

        public int Player2_data { get; set; }
        public int Player2_data4 { get; set; }
        public int Player2_data8 { get; set; }
        public int Player2_data12 { get; set; }
        public int Player2_data16 { get; set; }

        public int data0 { get; set; }
        public int data52 { get; set; }
        public int data56 { get; set; }
        public int data60 { get; set; }
        public int data64 { get; set; }
        public int data68 { get; set; }
        public int data70 { get; set; }
        public string dataLocation { get; set; }
        public string dataAbilityName { get; set; }

        public string Player2_Name { get; set; }

        public F_DEATHSPAM(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_DEATHSPAM, ms, true)
        {
        }




        public override void Load(MemoryStream ms)
        {
            byte[] data = ms.ToArray().Skip(3).ToArray();
            byte[] bdPad = new byte[ms.Length + 100];
            Buffer.BlockCopy(data, 0, bdPad, 0, data.Length);
            BitReader reader = new BitReader();
            reader.eax_data = bdPad;
            Player1_data = reader.ReadInt(8);
            Player1_data4 = reader.ReadInt(8);
            Player1_data8 = reader.ReadInt(8);
            Player1_data12 = reader.ReadInt(8);
            Player1_data16 = reader.ReadInt(8);
            Player1_Name = "";
            for (int i = 0; i < Player1_data; i++)
                Player1_Name += (char)reader.ReadInt(8);


            Player2_data = reader.ReadInt(8);
            Player2_data4 = reader.ReadInt(8);
            Player2_data8 = reader.ReadInt(8);
            Player2_data12 = reader.ReadInt(8);
            Player2_data16 = reader.ReadInt(8);
            Player2_Name = "";
            for (int i = 0; i < Player2_data; i++)
                Player2_Name += (char)reader.ReadInt(8);

            data0 = reader.ReadInt(8);
            data52 = reader.ReadInt(16);
            data56 = reader.ReadInt(16);
            data60 = reader.ReadInt(16);
            data64 = reader.ReadInt(8);
            data68 = reader.ReadInt(8);
            data70 = reader.ReadInt(8);

            for (int i = 0; i < data0; i++)
                dataLocation += (char)reader.ReadInt(8);

            for (int i = 0; i < data70; i++)
                dataAbilityName += (char)reader.ReadInt(8);



        }
    }
}
