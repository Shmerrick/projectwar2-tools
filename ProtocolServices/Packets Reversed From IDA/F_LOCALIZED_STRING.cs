﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_LOCALIZED_STRING:MythicPacket 
    {
        public ushort A03_Color { get; set; }

        public LOCALIZED Section { get; set; } = new LOCALIZED();

        public class LOCALIZED
        {
            public void GetSections(List<int> sections)
            {
                if (!sections.Contains(LocSection))
                    sections.Add(LocSection);
                if (SubSection != null)
                    SubSection.GetSections(sections);
            }
            public int LocSection { get; set; }
            public int ItemCount { get; set; }
            public List<string> Strings { get; set; } = new List<string>();
            public LOCALIZED SubSection { get; set; }

            public void Load(MemoryStream ms)
            {

                LocSection = (int)FrameUtil.GetUint32(ms);
                ItemCount = (int)FrameUtil.GetUint32(ms);

                for (int i = 0; i < ItemCount; i++)
                {
                    int current = ItemCount;

                    byte v3 = FrameUtil.GetUint8(ms);
                    if (v3 == 0)
                    {

                        SubSection = new LOCALIZED();
                        SubSection.Load(ms);
                    }
                    else
                    {
                        string str = FrameUtil.GetPascalString16(ms);
                        Strings.Add(str);
                    }
                }

            }
        }

        public F_LOCALIZED_STRING()
        {
        }
        public F_LOCALIZED_STRING(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_LOCALIZED_STRING, ms, false)
        {
        }

        
        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            A03_Color = FrameUtil.GetUint16(ms);

            Section.Load(ms);

        }
    }
    
}
