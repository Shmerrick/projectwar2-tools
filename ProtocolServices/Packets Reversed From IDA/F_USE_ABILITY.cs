﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarShared;
using WarShared.Utility;

namespace WarAdmin.Packets
{
    public class F_USE_ABILITY:MythicPacket
    {
        public ushort A03_Unk { get; set; }
        public uint A05_AbilityID { get; set; }
        [IgnoreField]
        public string A05_AbilityName { get; set; }

        public ushort A07_CasterID { get; set; }
        public ushort A09_EffectID { get; set; }
        public ushort A11_TargetID { get; set; }
        public AbilityActionInfo A12_CastType { get; set; }
        public byte A13_Unk { get; set; }
        public int A14_AbilityResult { get; set; }
        public ushort A16_Time { get; set; }
        public uint CastSequence { get; set; }

        public F_USE_ABILITY()
        {
        }
        public F_USE_ABILITY(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_USE_ABILITY, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            A03_Unk = FrameUtil.GetUint16(ms);
            A05_AbilityID = FrameUtil.GetUint16(ms);
            A07_CasterID = FrameUtil.GetUint16(ms);
            A09_EffectID = FrameUtil.GetUint16(ms);
            A11_TargetID = FrameUtil.GetUint16(ms);
            A12_CastType = (AbilityActionInfo)FrameUtil.GetUint8(ms);
            A13_Unk = FrameUtil.GetUint8(ms);
            A14_AbilityResult = FrameUtil.GetUint16(ms);
            A16_Time = FrameUtil.GetUint16(ms);
            CastSequence = FrameUtil.GetUint8(ms);

            if (frmMain.Abilities.ContainsKey((ushort)A05_AbilityID))
                A05_AbilityName = frmMain.Abilities[(ushort)A05_AbilityID].Name;
        }
    }
}
