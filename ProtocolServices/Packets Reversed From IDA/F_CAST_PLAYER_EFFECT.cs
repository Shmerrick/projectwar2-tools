﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_CAST_PLAYER_EFFECT : MythicPacket
    {
        [MField(MFieldType.CASTER, 3)]
        public ushort A03_CasterID { get; set; }
        [MField(MFieldType.TARGET, 5)]
        public ushort A05_TargetID { get; set; }
        [MField(MFieldType.ABILITY, 7)]
        public ushort A07_AbilityID { get; set; }
        [IgnoreField]
        public string A07_AbilityName { get; set; }
        public byte A09_VfxID { get; set; }
        public AbilityResult A10_AbilityResult { get; set; }
        public byte A11_Flag { get; set; }
        public bool[] A11_Flags { get; set; }
        public int a1;
        public int v84; 
        public int v93 ;
        public int v83 ;
        public int v102;
        public int v103;

        public List<int> Values { get; set; }
        public List<int> Values2 { get; set; }

        public F_CAST_PLAYER_EFFECT()
        {
        }
        public F_CAST_PLAYER_EFFECT(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CAST_PLAYER_EFFECT, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            Values = new List<int>();
            Values2 = new List<int>();
            ms.Position = 3;
            A03_CasterID = FrameUtil.GetUint16(ms);
            A05_TargetID = FrameUtil.GetUint16(ms);
            A07_AbilityID = FrameUtil.GetUint16(ms);
            A07_AbilityName = frmMain.Abilities[A07_AbilityID]?.Name??"";
            A09_VfxID = (byte)FrameUtil.GetUint8(ms);
            A10_AbilityResult = (AbilityResult)FrameUtil.GetUint8(ms);
            byte data = FrameUtil.GetUint8(ms);
            A11_Flag = data;
            A11_Flags = new bool[8];

            A11_Flags[0] = (data & 0x1) == 1;
            A11_Flags[1] = (data >> 1 & 0x1) == 1;
            A11_Flags[2] = (data >> 2 & 0x1) == 1;
            A11_Flags[3] = (data >> 3 & 0x1) == 1;
            A11_Flags[4] = (data >> 4 & 0x1) == 1;
            A11_Flags[5] = (data >> 5 & 0x1) == 1;
            A11_Flags[6] = (data >> 6 & 0x1) == 1;
            A11_Flags[7] = (data >> 7 & 0x1) == 1;

            var pos = ms.Position;
            while (ms.Position < ms.Length)
            {
                Values.Add(FrameUtil.ReadVarInt(ms));
            }


           a1 = data >> 1 & 1;
           v84 = data >> 2 & 1;
           v93 = data >> 3 & 1;
           v83 = data >> 4 & 1;
           v102 = data >> 5 & 1;
           v103 = data >> 6 & 1;
            ms.Position = pos;
            if (a1 > 0 || v102 > 0)
            {
                if (a1 > 0)
                {
                    Values2.Add(FrameUtil.ReadVarInt(ms)); //hit/healed
                    Values2.Add(FrameUtil.ReadVarInt(ms)); //mitigated/overhealed
                }
                if (v102 > 0)
                    Values2.Add(FrameUtil.ReadVarInt(ms)); //absrob

                if (v103 > 0)
                {
                    Values2.Add(FrameUtil.ReadVarInt(ms));
                    Values2.Add(FrameUtil.ReadVarInt(ms));
                    Values2.Add(FrameUtil.ReadVarInt(ms));
                    Values2.Add(FrameUtil.ReadVarInt(ms));

                    Values2.Add(FrameUtil.ReadVarInt(ms));

                    Values2.Add(FrameUtil.ReadVarInt(ms));
                    Values2.Add(FrameUtil.ReadVarInt(ms));
                    Values2.Add(FrameUtil.ReadVarInt(ms));
                    Values2.Add(FrameUtil.ReadVarInt(ms));
                    Values2.Add(FrameUtil.ReadVarInt(ms));
                }
            }
        }

       
    }
}
