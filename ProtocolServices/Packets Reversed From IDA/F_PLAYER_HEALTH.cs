﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_PLAYER_HEALTH : MythicPacket
    {
        public uint Health { get; set; }
        public uint MaxWounds { get; set; }
        public ushort ActionPoints { get; set; }
        public ushort MaxActionPoints { get; set; }
        public ushort Morale { get; set; }
        public ushort MaxMorale { get; set; }

        public F_PLAYER_HEALTH(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAYER_HEALTH, ms, true)
        {
        }
    }
}
