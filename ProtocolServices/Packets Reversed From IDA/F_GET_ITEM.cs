﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_GET_ITEM : MythicPacket
    {
        public class ItemStat
        {
            public BonusType StatType { get; set; }
            public short Value { get; set; }
            public byte IsPercent { get; set; }
            public uint Time { get; set; }
        }

        public class ItemBuff
        {
            public ushort AbilityID { get; set; }
            public uint Time { get; set; }
        }

        public class ItemAbility
        {
            public ushort Unk1 { get; set; }
            public ushort AbilityID { get; set; }
            public ushort Value { get; set; }
            public ushort Time { get; set; }
        }

        public class ItemCraft
        {
            public byte CraftID { get; set; }
            public ushort Value { get; set; }
        }

        public class Talisman
        {
            public uint ModelID { get; set; }
            public ushort Unk1 { get; set; }
            public string Name { get; set; }
            public List<ItemStat> Stats { get; set; }
            public List<ItemBuff> Buffs { get; set; }
            public List<ItemAbility> Abilities { get; set; }
            public List<ItemCraft> Crafting { get; set; }
            public UnkBlock2 UnkBlock2 { get; set; }
            public ushort Unk2 { get; set; }
            public ushort Unk3 { get; set; }
        }

        public class UnkBlock2
        {
            public ushort Unk1 { get; set; }
            public ushort Unk2 { get; set; }
        }

        public class ItemData
        {
            public ushort CurrentSlotIndex { get; set; }
            public ushort DefaultSlotIndex { get; set; }
            public uint ItemID { get; set; }
            public string ModelNameApperance{ get; set; }
            public byte Unk {get;set;}
            public byte ItemType { get; set; }
            public byte MinLevel { get; set; }
            public byte ObjectLevel { get; set; }
            public byte MinRenownlevel1 { get; set; }
            public byte MinRenownLevel2 { get; set; }
            public byte UniqueEquiped { get; set; }
            public byte RarityID { get; set; }
            public byte Bind { get; set; }
            public ushort RaceMask { get; set; }
            public uint TrophyMask { get; set; }
            public uint CareerMask { get; set; }
            public uint SellPrice { get; set; }
            public ushort Quantity1 { get; set; }
            public ushort Quantity2 { get; set; }
            public uint ItemSetID { get; set; }
            public uint SkillMask { get; set; }
            public ushort DPS { get; set; }
            public ushort Speed { get; set; }
            public string Name { get; set; }
            public byte TalismanCount { get; set; }
            public string RepairName { get; set; }
            public uint RepairedIconNum { get; set; }
            public uint RepairPrice { get; set; }
            public uint RepairSellPrice { get; set; }
            public UnkBlock2 UnkBlock2 { get; set; }
            public ushort ModelID { get; set; }
            public ushort ApperanceUnk { get; set; }
            public uint ApperanceItemID { get; set; }
            public string ApperanceName { get; set; }
            public string Description { get; set; }

            public List<ItemStat> Stats { get; set; } = new List<ItemStat>();
            public List<ItemBuff> Buffs { get; set; } = new List<ItemBuff>();
            public List<ItemAbility> Abilities { get; set; } = new List<ItemAbility>();
            public List<ItemCraft> Crafting { get; set; } = new List<ItemCraft>();
            public List<Talisman> Talismans { get; set; } = new List<Talisman>();

            public byte Unk1 { get; set; }
            public byte Unk2 { get; set; }
            public ushort TintA { get; set; }
            public ushort TintB { get; set; }
          
            public byte Unk7 { get; set; }
            public byte Unk8 { get; set; }
            public byte NoChargeLeftDontDelete { get; set; }
            public byte ChargesRemaining { get; set; }
            public List<Byte> Unk11 { get; set; }
            public byte IsBound { get; set; }
            public ushort DyeTintA { get; set; }
            public ushort DyeTintB { get; set; }
            public ushort CultivationLevel { get; set; }
            public byte CultivationEnabled { get; set; }
            public uint Unk17 { get; set; }
            public byte Unk18Set { get; set; }
            public ushort Unk18 { get; set; }
            public ushort Unk19 { get; set; }
            public byte Unk20 { get; set; }
            public byte Unk21 { get; set; }
            public byte Unk22 { get; set; }
            public byte Unk23 { get; set; }
            public byte Unk24 { get; set; }
            public uint TimeLeftBeforeDecay { get; set; }
            public byte DecayPaused { get; set; }
            public byte IsTwoHanded { get; set; }
            public ushort SlotIndex { get; set; }
            public byte IsRepair { get; set; }
            public string Hash { get; set; }
            public long PosStart { get; set; }
            public long PosEnd { get; set; }
            public override string ToString()
            {
                return Name??CurrentSlotIndex.ToString();
            }
            public void DeSerialize(MemoryStream ms, bool store=false)
            {
                PosStart = ms.Position;
                if (!store)
                {
                    SlotIndex = FrameUtil.GetUint16(ms);
                    IsRepair = FrameUtil.GetUint8(ms);

                    if (IsRepair != 0)
                    {
                        RepairName = FrameUtil.GetPascalString(ms);
                        RepairedIconNum = FrameUtil.GetUint32(ms);
                        RepairPrice = FrameUtil.GetUint32(ms);
                        RepairSellPrice = FrameUtil.GetUint32(ms);
                    }
                }
                else
                {
                    SlotIndex = FrameUtil.GetUint8(ms);

                    if (SlotIndex == 1)
                    {
                        RepairName = FrameUtil.GetPascalString(ms);
                        RepairedIconNum = FrameUtil.GetUint32(ms);
                        RepairPrice = FrameUtil.GetUint32(ms);
                        RepairSellPrice = FrameUtil.GetUint32(ms);
                    }
                }

                var startPos = ms.Position;

                ItemID = FrameUtil.GetUint32(ms);

                if (ItemID == 0)
                    return;

                ModelID = FrameUtil.GetUint16(ms);
                ApperanceUnk = FrameUtil.GetUint16(ms);
                ApperanceItemID = FrameUtil.GetUint32(ms);
                ApperanceName = FrameUtil.GetPascalString(ms);
                DefaultSlotIndex = FrameUtil.GetUint16(ms);
                ItemType = FrameUtil.GetUint8(ms);
                MinLevel = FrameUtil.GetUint8(ms);
                ObjectLevel = FrameUtil.GetUint8(ms);
                MinRenownlevel1 = FrameUtil.GetUint8(ms);
                MinRenownLevel2 = FrameUtil.GetUint8(ms);
                UniqueEquiped = FrameUtil.GetUint8(ms);
                RarityID = FrameUtil.GetUint8(ms);
                RaceMask = FrameUtil.GetUint16(ms);
                TrophyMask = FrameUtil.GetUint32(ms);

                if (ItemType == 0x17 || ItemType == 0x18)
                    CareerMask = FrameUtil.GetUint32(ms);

                if (ItemType == 0x18)
                {
                    Unk1 = FrameUtil.GetUint8(ms);
                    Unk2 = FrameUtil.GetUint8(ms);
                }

                TintA = FrameUtil.GetUint16(ms);
                TintB = FrameUtil.GetUint16(ms);
                SellPrice = FrameUtil.GetUint32(ms);
                Quantity1 = FrameUtil.GetUint16(ms);
                Quantity2 = FrameUtil.GetUint16(ms);
                ItemSetID = FrameUtil.GetUint32(ms);
                SkillMask = FrameUtil.GetUint32(ms);
                DPS = FrameUtil.GetUint16(ms);
                Speed = FrameUtil.GetUint16(ms);
                Name = FrameUtil.GetPascalString(ms);
           
                Stats = ReadStatsInfo(ms);
                Buffs = ReadBuffsInfo(ms);
                Abilities = ReadAbilityList(ms);
                Crafting = ReadCraftingInfo(ms);
                UnkBlock2 = ReadUnkBlock2(ms);

                TalismanCount = FrameUtil.GetUint8(ms);
                for (int i = 0; i < TalismanCount; i++)
                {
                    var talismanItemID = FrameUtil.GetUint32(ms);
                    if (talismanItemID != 0)
                    {
                        Talismans = new List<Talisman>();
                        var Unk1 = FrameUtil.GetUint16(ms);
                        var Name = FrameUtil.GetPascalString(ms);
                        var Stats = ReadStatsInfo(ms);
                        var Buffs = ReadBuffsInfo(ms);
                        var Abilities = ReadAbilityList(ms);
                        var Crafting = ReadCraftingInfo(ms);
                        var UnkBlock2 = ReadUnkBlock2(ms);
                        var Unk2 = FrameUtil.GetUint8(ms);
                        var Unk3 = FrameUtil.GetUint8(ms);
                      

                    //var talisman = new Talisman()
                    //    {
                    //        ModelID = talismanItemID,
                    //        Unk1 = FrameUtil.GetUint16(ms),
                    //        Name = FrameUtil.GetPascalString(ms),
                    //        Stats = ReadStatsInfo(ms),
                    //        Buffs = ReadBuffsInfo(ms),
                    //        Abilities = ReadAbilityList(ms),
                    //        Crafting = ReadCraftingInfo(ms),
                    //        UnkBlock2 = ReadUnkBlock2(ms),
                    //        Unk2 = FrameUtil.GetUint8(ms),
                    //        Unk3 = FrameUtil.GetUint8(ms)
                    //    };
                       // Talismans.Add(talisman);
                    }
                }

                Description = FrameUtil.GetPascalString(ms);

                Unk7 = FrameUtil.GetUint8(ms);
                Unk8 = FrameUtil.GetUint8(ms);
                NoChargeLeftDontDelete = FrameUtil.GetUint8(ms);
                ChargesRemaining = FrameUtil.GetUint8(ms);
                Unk11 = ReadUnkBlock3(ms);
                IsBound = FrameUtil.GetUint8(ms);
                DyeTintA = FrameUtil.GetUint16(ms);
                DyeTintB = FrameUtil.GetUint16(ms);
                CultivationLevel = FrameUtil.GetUint16(ms);
                CultivationEnabled = FrameUtil.GetUint8(ms);
                Unk17 = FrameUtil.GetUint32(ms);

                Unk18Set = FrameUtil.GetUint8(ms);
                if (Unk18Set != 0)
                {
                   Unk18 = FrameUtil.GetUint16(ms);
                   Unk19 = FrameUtil.GetUint16(ms);
                   Unk20 = FrameUtil.GetUint8(ms);
                   Unk21 = FrameUtil.GetUint8(ms);
                   Unk22 = FrameUtil.GetUint8(ms);
                   Unk23 = FrameUtil.GetUint8(ms);
                   Unk24 = FrameUtil.GetUint8(ms);
                }

                TimeLeftBeforeDecay = FrameUtil.GetUint32(ms);
                DecayPaused = FrameUtil.GetUint8(ms);
                IsTwoHanded = FrameUtil.GetUint8(ms);

                var endPos = ms.Position;
                var data = new byte[endPos - startPos];
                ms.Position = startPos;
                ms.Read(data,0,data.Length);
                ms.Position = endPos;


                Guid g = new Guid(md5.ComputeHash(data));
                Hash = g.ToString();

                PosEnd = ms.Position;
            }
            MD5 md5 = MD5.Create();
            private void ReadUnkBlock4(MemoryStream ms)
            {

                var flag = FrameUtil.GetUint8(ms);
                if (flag != 0)
                {
                    var unk1 = FrameUtil.GetUint16(ms);
                    var unk2 = FrameUtil.GetUint16(ms);
                    var unk3 = FrameUtil.GetUint8(ms);
                    var unk4 = FrameUtil.GetUint8(ms);
                    var unk5 = FrameUtil.GetUint8(ms);
                    var unk6 = FrameUtil.GetUint8(ms);
                    var unk7 = FrameUtil.GetUint8(ms);
                }
            }

            private List<byte> ReadUnkBlock3(MemoryStream ms)
            {
                var list = new List<byte>();
                var count = FrameUtil.GetUint8(ms);

                for (int i = 0; i < count; i++)
                {
                    var unk = FrameUtil.GetUint8(ms);
                    list.Add(unk);
                }
                return list;
            }

            private UnkBlock2 ReadUnkBlock2(MemoryStream ms)
            {

                var Unk7 = FrameUtil.GetUint8(ms);

                if (Unk7 != 0) //from dissasembly
                {
                    UnkBlock2 b = new F_GET_ITEM.UnkBlock2()
                    {
                        Unk1 = FrameUtil.GetUint16(ms),
                        Unk2 = FrameUtil.GetUint16(ms)
                    };
                    return b;
                }
                return null;
            }

            private List<ItemStat> ReadStatsInfo(MemoryStream ms)
            {
                List<ItemStat> stats = new List<ItemStat>();
                var statCount = FrameUtil.GetUint8(ms);
                for (int i = 0; i < statCount; i++)
                {
                    var stat = new ItemStat()
                    {
                        StatType = (BonusType)FrameUtil.GetUint8(ms),
                        Value = (short)FrameUtil.GetUint16(ms),
                        IsPercent = FrameUtil.GetUint8(ms),
                        Time = FrameUtil.GetUint32(ms)
                    };
                    stats.Add(stat);
                }

                return stats;
            }

            private List<ItemBuff> ReadBuffsInfo(MemoryStream ms)
            {
                List<ItemBuff> buffs = new List<ItemBuff>();
                var buffCount = FrameUtil.GetUint8(ms);
                for (int i = 0; i < buffCount; i++)
                {
                    var buff = new ItemBuff()
                    {
                        AbilityID = FrameUtil.GetUint16(ms),
                        Time = FrameUtil.GetUint32(ms)
                    };
                    buffs.Add(buff);
                }

                return buffs;
            }

            private List<ItemAbility> ReadAbilityList(MemoryStream ms)
            {
                List<ItemAbility> abilities = new List<ItemAbility>();
                var abilityCount = FrameUtil.GetUint8(ms);
                for (int i = 0; i < abilityCount; i++)
                {
                    var itemAbility = new ItemAbility()
                    {
                        Unk1 = FrameUtil.GetUint16(ms),
                        AbilityID = FrameUtil.GetUint16(ms),
                        Value = FrameUtil.GetUint16(ms),
                        Time = FrameUtil.GetUint16(ms)
                    };
                    abilities.Add(itemAbility);
                }

                return abilities;
            }

            private List<ItemCraft> ReadCraftingInfo(MemoryStream ms)
            {
                List<ItemCraft> crafting = new List<ItemCraft>();
                var craftingCount = FrameUtil.GetUint8(ms);
                for (int i = 0; i < craftingCount; i++)
                {
                    var craft = new ItemCraft()
                    {
                        CraftID = FrameUtil.GetUint8(ms),
                        Value = FrameUtil.GetUint16(ms)
                    };
                    crafting.Add(craft);
                }
                return crafting;
            }
        }
        public byte A03_Count { get; set; }
        public byte A04 { get; set; }
        public byte A05 { get; set; }
        public byte A06 { get; set; }
    
        public List<ItemData> Items { get; set; }

        public F_GET_ITEM(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_GET_ITEM, ms, true)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            Items = new List<ItemData>();
            A03_Count = FrameUtil.GetUint8(ms);
            A04 = FrameUtil.GetUint8(ms);
            A05 = FrameUtil.GetUint8(ms);
            A06 = FrameUtil.GetUint8(ms);

            for (int i = 0; i < A03_Count && ms.Position < ms.Length; i++)
            {
    
                var item2 = new ItemData();
                item2.DeSerialize(ms);
                Items.Add(item2);
        

            }
        }
    }
}
