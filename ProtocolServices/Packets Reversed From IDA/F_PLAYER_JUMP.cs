﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_PLAYER_JUMP : MythicPacket
    {
        public uint A03_X { get; set; }
        public uint A07_Y { get; set; }
        public ushort A11_ObjectID { get; set; }
        public ushort A13_ZoneID { get; set; }
        public ushort A15_Direction { get; set; }
        public uint A17_Unk { get; set; }
        public byte A21_Unk { get; set; }
        public byte A22_Unk { get; set; }


        public F_PLAYER_JUMP(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAYER_JUMP, ms, true)
        {
        }

    }
}
