﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_PLAYER_RENOWN : MythicPacket
    {
        public uint A03_TotalRenown { get; set; }
        public uint A07_CurrentRenown { get; set; }
        public byte A11_Level { get; set; }
        public byte A12 { get; set; }
        public byte A13 { get; set; }
        public byte A14 { get; set; }

        public F_PLAYER_RENOWN(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAYER_RENOWN, ms, true)
        {
        }
    }
}
