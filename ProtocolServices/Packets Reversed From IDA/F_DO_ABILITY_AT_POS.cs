﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_DO_ABILITY_AT_POS : MythicPacket 
    {

        public ushort A00_unk { get; set; }
        public ushort A02_Oid { get; set; }
        public ushort A04_CastPx { get; set; }
        public ushort A06_CastPy { get; set; }
        public ushort A08_CastZoneId { get; set; }
        public ushort A10_unk2 { get; set; }
        public ushort A12_AbilityId { get; set; }
        public ushort A14_Px { get; set; }
        public ushort A16_Py { get; set; }
        public ushort A18_ZoneId { get; set; }


        public F_DO_ABILITY_AT_POS(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_DO_ABILITY_AT_POS, ms, true)
        {
        }

    }
}
