﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_KNOCKBACK : MythicPacket
    {
        public uint A03_TargetX { get; set; }//    FrameUtil.WriteUInt32(stream,(uint)(target.X + target.XZone));
        public uint A07_TargetY { get; set; }//    FrameUtil.WriteUInt32(stream, (uint)(target.Y+ target.YZone)); 
        public uint A11_CasterX { get; set; }//    FrameUtil.WriteUInt32(stream, (uint)(caster.X + caster.XZone));
        public uint A15_CasterY { get; set; }//    FrameUtil.WriteUInt32(stream,(uint)(caster.Y + caster.YZone));
        public byte A19 { get; set; }       //1     
        public byte A20 { get; set; }       //2
        public byte A21 { get; set; }       //3
        public byte A22 { get; set; }       //4
        public byte A23 { get; set; }       //5
        public byte A24 { get; set; }       //6
        public byte A25 { get; set; }       //7
        public byte A26 { get; set; }       //8
        public byte A27 { get; set; }       //9
        public byte A28 { get; set; }       //10
        public ushort A29_Force1 { get; set; } //FrameUtil.WriteUInt16(stream, force);  // KickForce
        public byte A31 { get; set; }       //    FrameUtil.Fill(stream, 0, 3);
        public byte A32 { get; set; }
        public byte A33 { get; set; }
        public byte A34_Angle { get; set; } // FrameUtil.WriteByte(stream, angle);
        public byte A35 { get; set; }      //1     
        public byte A36 { get; set; }      //2
        public byte A37 { get; set; }      //3
        public byte A38 { get; set; }      //4
        public byte A39 { get; set; }      //5
        public byte A40 { get; set; }      //6
        public byte A41 { get; set; }      //7
        public byte A42 { get; set; }      //8
        public byte A43 { get; set; }      //9
        public byte A44 { get; set; }      //10
        public ushort A45_Force2 { get; set; }//FrameUtil.WriteUInt16(stream, 0);  // KickForce2
        public byte A47 { get; set; }
        public byte A48 { get; set; }
        public byte A49 { get; set; }
        public byte A50_Gravity { get; set; }       //1
        public ushort A51_CasterZ { get; set; }
        public ushort A53_TargetZ { get; set; }
        public byte A55 { get; set; }
        public byte A56 { get; set; }
        public ushort A57_ObjectID { get; set; }
        public byte A59 { get; set; }
        public byte A60 { get; set; }
        public byte A61 { get; set; }
        public byte A62 { get; set; }


        public F_KNOCKBACK()
        {
        }

        public F_KNOCKBACK(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_KNOCKBACK, ms, true)
        {
        }
    }
}
