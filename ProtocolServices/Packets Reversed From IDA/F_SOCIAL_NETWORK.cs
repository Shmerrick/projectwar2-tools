﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_SOCIAL_NETWORK: MythicPacket
    {
        public ushort A03_Unk { get; set; }
        public byte A05_Type { get; set; }
        public byte A06_Count { get; set; }

        public class Character
        {
            public byte A01_Unk { get; set; }
            public string A02_Name { get; set; }
            public byte A03_Level { get; set; }
            public byte A04_Unk { get; set; }
            public CareerLine A05_CareerLine { get; set; }
            public byte A06_Unk { get; set; }

            public override string ToString()
            {
                return A02_Name;
            }

            public void Load(MemoryStream ms)
            {
                A01_Unk = FrameUtil.GetUint8(ms);
                A02_Name = FrameUtil.GetPascalString(ms);
                A03_Level = FrameUtil.GetUint8(ms);
                A04_Unk = FrameUtil.GetUint8(ms);
                A05_CareerLine = (CareerLine)FrameUtil.GetUint16(ms);
                A06_Unk = FrameUtil.GetUint8(ms);
            }
        }

        public class Group
        {
            public ushort A03_Unk { get; set; }
            public string A05_LeaderName { get; set; }
            public byte A07_Unk { get; set; }
            public byte A08_GroupType { get; set; }
            public ushort A09_Unk { get; set; }
            public byte A11_Purpose { get; set; }
            public byte A12_LeaderLevel { get; set; }
            public ushort A13_Career { get; set; }
            public ushort A14_ZoneID { get; set; }
            public byte A15_Unk { get; set; }
            public string A16_Unk { get; set; }

            public byte A1A_Unk { get; set; }
            public byte A17_Unk { get; set; }
            public ushort A18_SecondsAway { get; set; }
         

            public byte A27_WarbandSize { get; set; }
            public byte A27_OpenSlots { get; set; }
            public List<Character> Characters { get; set; }

            public override string ToString()
            {
                return A05_LeaderName;
            }


            public void Load(MemoryStream ms, int index)
            {
                A03_Unk = FrameUtil.GetUint16(ms);
                A05_LeaderName = FrameUtil.GetPascalString(ms);
                A07_Unk = FrameUtil.GetUint8(ms);
                A08_GroupType = FrameUtil.GetUint8(ms);
                A09_Unk = FrameUtil.GetUint16(ms);
                A11_Purpose = FrameUtil.GetUint8(ms);
                A12_LeaderLevel = FrameUtil.GetUint8(ms);
                A13_Career = FrameUtil.GetUint16(ms);
                A14_ZoneID = FrameUtil.GetUint16(ms);
                A15_Unk = FrameUtil.GetUint8(ms);
                A16_Unk = FrameUtil.GetPascalString(ms); //note?

     
             
              //  A17_Unk = FrameUtil.GetUint8(ms);
                A18_SecondsAway = FrameUtil.GetUint16(ms); //seconds away

  
                var unk = FrameUtil.GetUint8(ms);

                if (unk == 0)
                {

                    var unk1 = FrameUtil.GetUint8(ms);
                    if (unk1 == 0)
                        A27_WarbandSize = FrameUtil.GetUint8(ms); //players in group/party
                    else
                        A27_WarbandSize = unk1;


                    if (A27_WarbandSize == 8 && unk > 0)
                    {
                        var data = FrameUtil.GetByteArray(ms, 9);
                    }
                }
                else
                {
                    A27_WarbandSize = unk;
                    if (A27_WarbandSize == 8)
                    {
                        var data = FrameUtil.GetByteArray(ms, 9);
                    }
                }
                A27_OpenSlots = FrameUtil.GetUint8(ms); //players sent 

                Characters = new List<Character>();

                for (int i = 0; i < A27_OpenSlots; i++)
                {
                 //   var a2 = FrameUtil.GetPascalString(ms);
                    var c = new Character();
                    c.Load(ms);
                    Characters.Add(c);
                }
            }
        }


        public List<Group> Groups { get; set; }
        public F_SOCIAL_NETWORK(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_SOCIAL_NETWORK, ms, false)
        {
          
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;

            A03_Unk = FrameUtil.GetUint16(ms);
            A05_Type = FrameUtil.GetUint8(ms);
            A06_Count = FrameUtil.GetUint8(ms);
             FrameUtil.GetUint8(ms);
            if (A05_Type == 0x0A)
            {
                Groups = new List<Group>();
                for (int i = 0; i < A06_Count; i++)
                {
                    Group group = new Group();
                    group.Load(ms, i);
                    Groups.Add(group);
                }
            }
        }

   
    }
}
