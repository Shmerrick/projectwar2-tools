﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;


namespace WarAdmin.Packets
{
    public class F_UPDATE_HOT_SPOT : MythicPacket
    {
        public byte Count;
        public byte Type { get; set; }
        public ushort ZoneID { get; set; }

        public class HotSpot
        {
            public byte ZoneID { get; set; }
            public byte Unk1 { get; set; }
            public byte Unk2 { get; set; }
            public byte Unk3 { get; set; }
            public byte Unk4 { get; set; }
            public void Load(MemoryStream ms)
            {
                ZoneID = FrameUtil.GetUint8(ms);
                Unk1 = FrameUtil.GetUint8(ms);
                Unk2 = FrameUtil.GetUint8(ms);
                Unk3 = FrameUtil.GetUint8(ms);
                Unk4 = FrameUtil.GetUint8(ms);
            }

            public override string ToString()
            {
                return ZoneID + " " + Unk1 + " " + Unk2 + " " + Unk3 + " " + Unk4;
            }
        }
        public List<HotSpot> HotSpots { get; set; }
        public F_UPDATE_HOT_SPOT(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_UPDATE_HOT_SPOT, ms, false)
        {
        }
        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            Count = FrameUtil.GetUint8(ms);
            Type = FrameUtil.GetUint8(ms);
            var Unk = FrameUtil.GetUint8(ms);
            HotSpots = new List<HotSpot>();
            if (Type == 2)
            {
                for (int i = 0; i < Count; i++)
                {
                    var hotspot = new HotSpot();
                    hotspot.Load(ms);
                    HotSpots.Add(hotspot);
                }
            }


          
        }
    }
}
