﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace WarAdmin.Packets
{
    public class F_SET_TARGET : MythicPacket
    {
        public ushort TargetID { get; set; }
        public ushort PlayerID { get; set; }
        public byte Type { get; set; }
        public byte Unk { get; set; }

        [IgnoreField]
        public string TargetName { get; set; }
        [IgnoreField]
        public int Seq { get; set; }
        public F_SET_TARGET(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_BAG_INFO, ms, true)
        {
        }
    }

}
