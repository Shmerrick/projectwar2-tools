﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarClientTool
{
    public partial class FrmCSVViewer : Form
    {
        private Myp _Myp;
        private String _path;
        public Boolean Changed => csvGrid1.Changed;
        public FrmCSVViewer()
        {

            InitializeComponent();


        }
        public void LoadAsset(Myp Myp, String path, Byte[] data)
        {
            csvGrid1.LoadCSV(data);
            _Myp = Myp;
            _path = path;
        }

        private void SaveToolStripMenuItem_Click(Object sender, EventArgs e) => _Myp.UpdateAsset(_path, System.Text.ASCIIEncoding.ASCII.GetBytes(csvGrid1.CSV.ToText()), true);
    }
}
