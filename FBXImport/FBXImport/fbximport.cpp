#include <fbxsdk.h>
#include "Common.h"
#define NIFLIB_STATIC_LINK
#include "niflib.h"

#include "obj/NiObject.h"
#include "obj/NiNode.h"
#include "obj/NiAVObject.h"
#include "obj/NiTriShape.h"
#include "obj/NiTriShapeData.h"
#include "obj/NiTextureProperty.h"
#include "obj/NiTexturingProperty.h"
#include "obj/NiSourceTexture.h"
#include "obj/NiLODNode.h"
#include "obj/NiCollisionObject.h"
#include "obj/NiCollisionData.h"
#include "obj/NiZBufferProperty.h"
#include "obj/NiMaterialProperty.h"
#include "obj/NiVertexColorProperty.h"
#include "obj/NiBinaryExtraData.h"
#include "obj/NiFloatsExtraData.h"
#include "obj/NiAlphaProperty.h"
#include "MatTexCollection.h"

#include <fstream>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <set>
#include <map>

using namespace std;
using namespace Niflib;

#define degToRad(angleInDegrees) ((angleInDegrees) * PI / 180.0)
#define radToDeg(angleInRadians) ((angleInRadians) * 180.0 / PI)

FILE* file;
std::vector<std::string> splitpath(const std::string& str, const std::set<char> delimiters)
{
	std::vector<std::string> result;

	char const* pch = str.c_str();
	char const* start = pch;
	for (; *pch; ++pch)
	{
		if (delimiters.find(*pch) != delimiters.end())
		{
			if (start != pch)
			{
				std::string str(start, pch);
				if (str.size() > 0)
					result.push_back(str);
			}
			//else
			//{
			//	result.push_back("");
			//}
			start = pch + 1;
		}
	}
	if (strlen(start) > 0)
		result.push_back(start);

	return result;
}
std::vector<std::string> splitpath(const std::string& str)
{
	std::set<char> delims;
	delims.insert('\\');
	delims.insert('/');
	return splitpath(str, delims);
}
bool StartsWith(std::string string, std::string with)
{
	return strncmp(string.c_str(), with.c_str(), with.size()) == 0;
}

std::string GetFilename(std::string& str)
{
	auto p = splitpath(str);
	if (p.size() > 0)
		return p[p.size() - 1];
	return "";
}

NiNode* CreateNode(NiNode* parent, const char* name)
{
	auto nifNode = new NiNode();
	if (StartsWith(name, "nifid"))
	{
		nifNode->SetName("Editable Poly");
	}
	else
		nifNode->SetName(name);
	nifNode->SetFlags(272);

	if (parent != nullptr)
	{
		parent->AddChild(nifNode);
	}

	return nifNode;
}
class NT : public NiSourceTexture
{
public:
    void DirectRender(bool render)
	{
		directRender = render;
	}
protected:
	NIFLIB_HIDDEN virtual void Write(ostream& out, const map<NiObjectRef, unsigned int> & link_map, list<NiObject *> & missing_link_stack, const NifInfo & info) const override
	{

		NiSourceTexture::Write(out, link_map, missing_link_stack, info);
	}
};

class TX : public NiTexturingProperty
{
public:
	void SetFlag(unsigned f)
	{
		flags = f;
	}
};


NiAVObjectRef trishape = nullptr;
NiAVObjectRef trishapeData = nullptr;
TexDesc* CreateTX(std::string ds)
{
	auto tex = new NT();
	tex->SetExternalTexture(ds);
	tex->SetAlphaFormat(AlphaFormat::ALPHA_BINARY);
	tex->SetMipMapFormat(MipMapFormat::MIP_FMT_YES);
	tex->SetPixelLayout(PixelLayout::PIX_LAY_DEFAULT);
	tex->DirectRender(false);
	TexDesc* texDesc = new TexDesc;
	memset(texDesc, 0, sizeof(texDesc));
	texDesc->source = tex;
	texDesc->uvSet = 0;
	texDesc->clampMode = TexClampMode::WRAP_S_WRAP_T;
	return texDesc;
}
FbxArray<FbxVector4>  normals;

FbxArray<FbxVector2> ConvertMapping(FbxLayerElementUV* pUVs, FbxMesh* pMesh)
{
	normals.Clear();
	if (pUVs->GetMappingMode() == FbxLayerElement::eByPolygonVertex)
	{
		if (pUVs->GetReferenceMode() == FbxLayerElement::eIndexToDirect)
		{
			FbxArray<int> lNewIndexToDirect;
			FbxArray<FbxVector2> lNewUVs;

			// keep track of the processed control points
			map<FbxHandle, bool> lProcessedCP;
			for (int i = 0; i < pMesh->GetControlPointsCount(); i++)
				lProcessedCP[(FbxHandle)i] = false;

			int points = 0;
			FbxArray<FbxVector4> n;
			pMesh->GetPolygonVertexNormals(n);

			// visit each polygon and polygon vertex
			for (int p = 0; p < pMesh->GetPolygonCount(); p++)
			{
				for (int pv = 0; pv < pMesh->GetPolygonSize(p); pv++)
				{
					int lCP = pMesh->GetPolygonVertex(p, pv);
				
					// check if we already processed this control point
					if (!lProcessedCP[(FbxHandle)lCP])
					{
						FbxVector2 uv = pUVs->GetDirectArray().GetAt(lCP);
						lNewUVs.Add(uv);
						lNewIndexToDirect.Add(lCP);
						lProcessedCP[(FbxHandle)lCP] = true;
						normals.Add(n[points]);
					}
					points++;
				}
			}

			// change the content of the index array and its mapping
			pUVs->SetMappingMode(FbxLayerElement::eByControlPoint);
			pUVs->GetIndexArray().Clear();
			pUVs->GetIndexArray().Resize(lNewIndexToDirect.GetCount());
			int* lIndexArray = (int*)pUVs->GetIndexArray().GetLocked();
			for (int i = 0; i < lNewIndexToDirect.GetCount(); i++)
				lIndexArray = (int*)lNewIndexToDirect.GetAt(i);
			pUVs->GetIndexArray().Release((void**)&lIndexArray);

			// and the content of the direct array
			pUVs->GetDirectArray().Clear();
			pUVs->GetDirectArray().Resize(lNewUVs.GetCount());
			FbxVector2* lDirectArray = (FbxVector2*)pUVs->GetDirectArray().GetLocked();
			for (int j = 0; j < lNewUVs.GetCount(); j++)
				lDirectArray = &(lNewUVs.GetAt(j));
			pUVs->GetDirectArray().Release((void**)&lDirectArray);
			return lNewUVs;
		}
	}
	return FbxArray<FbxVector2>();
}

void AddShaderTexture(TX* textures, TexDesc* texture, int index)
{
	textures->SetShaderTextureCount(index + 1);
	TexDesc sh0;
	memset(&sh0, 0, sizeof(TexDesc));
	sh0.source = texture->source;
	sh0.flags = 12800;

	sh0.ps2K = -75;
	sh0.filterMode = TexFilterMode::FILTER_TRILERP;
	sh0.tiling = TexCoord(1, 1);
	textures->SetShaderTexture(index, sh0);

}

TexDesc* AddMaterialTexture(TexType type, TX* textures, FbxProperty property, bool addTexture = true)
{
	auto txt = property.GetSrcObject<FbxFileTexture>();
	auto ds = GetFilename(string(txt->GetFileName()));

	auto base = CreateTX(ds);
	base->flags = 12800;
	if(addTexture)
		textures->SetTexture(type, *base);

	int shaderIndex = textures->GetShaderTextureCount();



	AddShaderTexture(textures, base, shaderIndex);
	return base;
}


struct SubMesh
{
	SubMesh() : IndexOffset(0), TriangleCount(0) {}

	int IndexOffset;
	int TriangleCount;
};

const int TRIANGLE_VERTEX_COUNT = 3;

// Four floats for every position.
const int VERTEX_STRIDE = 4;
// Three floats for every normal.
const int NORMAL_STRIDE = 3;
// Two floats for every UV.
const int UV_STRIDE = 2;

FbxArray<SubMesh*> mSubMeshes;
bool mHasNormal;
bool mHasUV;
bool mAllByControlPoint; // Save data in VBO by control point or by polygon vertex.


void BuildMesh(FbxMesh* pMesh, vector<Triangle>& tris,
	vector<Vector3>& verticies,
	vector<TexCoord>& uvs,
	vector<Vector3>& ns)
{
	mSubMeshes.Clear();
	const int lPolygonCount = pMesh->GetPolygonCount();

	// Count the polygon count of each material
	FbxLayerElementArrayTemplate<int>* lMaterialIndice = NULL;
	FbxGeometryElement::EMappingMode lMaterialMappingMode = FbxGeometryElement::eNone;

	if (pMesh->GetElementMaterial())
	{
		lMaterialIndice = &pMesh->GetElementMaterial()->GetIndexArray();
		lMaterialMappingMode = pMesh->GetElementMaterial()->GetMappingMode();
		if (lMaterialIndice && lMaterialMappingMode == FbxGeometryElement::eByPolygon)
		{
			// Count the faces of each material
			for (int lPolygonIndex = 0; lPolygonIndex < lPolygonCount; ++lPolygonIndex)
			{
				const int lMaterialIndex = lMaterialIndice->GetAt(lPolygonIndex);
				if (mSubMeshes.GetCount() < lMaterialIndex + 1)
				{
					mSubMeshes.Resize(lMaterialIndex + 1);
				}
				if (mSubMeshes[lMaterialIndex] == NULL)
				{
					mSubMeshes[lMaterialIndex] = new SubMesh;
				}
				mSubMeshes[lMaterialIndex]->TriangleCount += 1;
			}

			// Make sure we have no "holes" (NULL) in the mSubMeshes table. This can happen
			// if, in the loop above, we resized the mSubMeshes by more than one slot.
			for (int i = 0; i < mSubMeshes.GetCount(); i++)
			{
				if (mSubMeshes[i] == NULL)
					mSubMeshes[i] = new SubMesh;
			}

			// Record the offset (how many vertex)
			const int lMaterialCount = mSubMeshes.GetCount();
			int lOffset = 0;
			for (int lIndex = 0; lIndex < lMaterialCount; ++lIndex)
			{
				mSubMeshes[lIndex]->IndexOffset = lOffset;
				lOffset += mSubMeshes[lIndex]->TriangleCount * 3;
				// This will be used as counter in the following procedures, reset to zero
				mSubMeshes[lIndex]->TriangleCount = 0;
			}
		}
	}


	// All faces will use the same material.
	if (mSubMeshes.GetCount() == 0)
	{
		mSubMeshes.Resize(1);
		mSubMeshes[0] = new SubMesh();
	}

	// Congregate all the data of a mesh to be cached in VBOs.
	// If normal or UV is by polygon vertex, record all vertex attributes by polygon vertex.
	mHasNormal = pMesh->GetElementNormalCount() > 0;
	mHasUV = pMesh->GetElementUVCount() > 0;
	FbxGeometryElement::EMappingMode lNormalMappingMode = FbxGeometryElement::eNone;
	FbxGeometryElement::EMappingMode lUVMappingMode = FbxGeometryElement::eNone;
	if (mHasNormal)
	{
		lNormalMappingMode = pMesh->GetElementNormal(0)->GetMappingMode();
		if (lNormalMappingMode == FbxGeometryElement::eNone)
		{
			mHasNormal = false;
		}
		if (mHasNormal && lNormalMappingMode != FbxGeometryElement::eByControlPoint)
		{
			mAllByControlPoint = false;
		}
	}
	if (mHasUV)
	{
		lUVMappingMode = pMesh->GetElementUV(0)->GetMappingMode();
		if (lUVMappingMode == FbxGeometryElement::eNone)
		{
			mHasUV = false;
		}
		if (mHasUV && lUVMappingMode != FbxGeometryElement::eByControlPoint)
		{
			mAllByControlPoint = false;
		}
	}

	// Allocate the array memory, by control point or by polygon vertex.
	int lPolygonVertexCount = pMesh->GetControlPointsCount();
	if (!mAllByControlPoint)
	{
		lPolygonVertexCount = lPolygonCount * TRIANGLE_VERTEX_COUNT;
		printf("lPolygonVertexCount:%d\r\n", lPolygonVertexCount);
	}
	float * lVertices = new float[lPolygonVertexCount * VERTEX_STRIDE];
	unsigned int * lIndices = new unsigned int[lPolygonCount * TRIANGLE_VERTEX_COUNT];

	printf("lVertices:%d\r\n", lPolygonVertexCount * VERTEX_STRIDE);
	printf("lIndices:%d\r\n", lPolygonCount * TRIANGLE_VERTEX_COUNT);

	float * lNormals = NULL;
	if (mHasNormal)
	{
		lNormals = new float[lPolygonVertexCount * NORMAL_STRIDE];
		printf("lNormals:%d\r\n", lPolygonVertexCount * NORMAL_STRIDE);
	}
	float * lUVs = NULL;
	FbxStringList lUVNames;
	pMesh->GetUVSetNames(lUVNames);
	const char * lUVName = NULL;
	if (mHasUV && lUVNames.GetCount())
	{
		lUVs = new float[lPolygonVertexCount * UV_STRIDE];
		printf("lUVs:%d\r\n", lPolygonVertexCount * UV_STRIDE);
		lUVName = lUVNames[0];
	}

	// Populate the array with vertex attribute, if by control point.
	const FbxVector4 * lControlPoints = pMesh->GetControlPoints();
	printf("lControlPoints:%d\r\n", pMesh->GetControlPointsCount());

	FbxVector4 lCurrentVertex;
	FbxVector4 lCurrentNormal;
	FbxVector2 lCurrentUV;
	if (mAllByControlPoint)
	{
		const FbxGeometryElementNormal * lNormalElement = NULL;
		const FbxGeometryElementUV * lUVElement = NULL;
		if (mHasNormal)
		{
			lNormalElement = pMesh->GetElementNormal(0);
		}
		if (mHasUV)
		{
			lUVElement = pMesh->GetElementUV(0);
		}
		for (int lIndex = 0; lIndex < lPolygonVertexCount; ++lIndex)
		{
			// Save the vertex position.
			lCurrentVertex = lControlPoints[lIndex];
			lVertices[lIndex * VERTEX_STRIDE] = static_cast<float>(lCurrentVertex[0]);
			lVertices[lIndex * VERTEX_STRIDE + 1] = static_cast<float>(lCurrentVertex[1]);
			lVertices[lIndex * VERTEX_STRIDE + 2] = static_cast<float>(lCurrentVertex[2]);
			lVertices[lIndex * VERTEX_STRIDE + 3] = 1;

			// Save the normal.
			if (mHasNormal)
			{
				int lNormalIndex = lIndex;
				if (lNormalElement->GetReferenceMode() == FbxLayerElement::eIndexToDirect)
				{
					lNormalIndex = lNormalElement->GetIndexArray().GetAt(lIndex);
				}
				lCurrentNormal = lNormalElement->GetDirectArray().GetAt(lNormalIndex);
				lNormals[lIndex * NORMAL_STRIDE] = static_cast<float>(lCurrentNormal[0]);
				lNormals[lIndex * NORMAL_STRIDE + 1] = static_cast<float>(lCurrentNormal[1]);
				lNormals[lIndex * NORMAL_STRIDE + 2] = static_cast<float>(lCurrentNormal[2]);
			}

			// Save the UV.
			if (mHasUV)
			{
				int lUVIndex = lIndex;
				if (lUVElement->GetReferenceMode() == FbxLayerElement::eIndexToDirect)
				{
					lUVIndex = lUVElement->GetIndexArray().GetAt(lIndex);
				}
				lCurrentUV = lUVElement->GetDirectArray().GetAt(lUVIndex);
				lUVs[lIndex * UV_STRIDE] = static_cast<float>(lCurrentUV[0]);
				lUVs[lIndex * UV_STRIDE + 1] = static_cast<float>(lCurrentUV[1]);
			}
		}
	}

	int lVertexCount = 0;
	for (int lPolygonIndex = 0; lPolygonIndex < lPolygonCount; ++lPolygonIndex)
	{
		// The material for current face.
		int lMaterialIndex = 0;
		if (lMaterialIndice && lMaterialMappingMode == FbxGeometryElement::eByPolygon)
		{
			lMaterialIndex = lMaterialIndice->GetAt(lPolygonIndex);
		}

		// Where should I save the vertex attribute index, according to the material
		const int lIndexOffset = mSubMeshes[lMaterialIndex]->IndexOffset + mSubMeshes[lMaterialIndex]->TriangleCount * 3;

		auto tri = Triangle(lIndexOffset, lIndexOffset + 1, lIndexOffset + 2);
		//			//	triIndex += 3;
		tris.push_back(tri);

		for (int lVerticeIndex = 0; lVerticeIndex < TRIANGLE_VERTEX_COUNT; ++lVerticeIndex)
		{
			const int lControlPointIndex = pMesh->GetPolygonVertex(lPolygonIndex, lVerticeIndex);

			if (mAllByControlPoint)
			{
				lIndices[lIndexOffset + lVerticeIndex] = static_cast<unsigned int>(lControlPointIndex);
			}
			// Populate the array with vertex attribute, if by polygon vertex.
			else
			{
				lIndices[lIndexOffset + lVerticeIndex] = static_cast<unsigned int>(lVertexCount);

				lCurrentVertex = lControlPoints[lControlPointIndex];
				lVertices[lVertexCount * VERTEX_STRIDE] = static_cast<float>(lCurrentVertex[0]);
				lVertices[lVertexCount * VERTEX_STRIDE + 1] = static_cast<float>(lCurrentVertex[1]);
				lVertices[lVertexCount * VERTEX_STRIDE + 2] = static_cast<float>(lCurrentVertex[2]);
				lVertices[lVertexCount * VERTEX_STRIDE + 3] = 1;

				verticies.push_back(Vector3(static_cast<float>(lCurrentVertex[0]), static_cast<float>(lCurrentVertex[1]), static_cast<float>(lCurrentVertex[2])));

				if (mHasNormal)
				{
					pMesh->GetPolygonVertexNormal(lPolygonIndex, lVerticeIndex, lCurrentNormal);
					lNormals[lVertexCount * NORMAL_STRIDE] = static_cast<float>(lCurrentNormal[0]);
					lNormals[lVertexCount * NORMAL_STRIDE + 1] = static_cast<float>(lCurrentNormal[1]);
					lNormals[lVertexCount * NORMAL_STRIDE + 2] = static_cast<float>(lCurrentNormal[2]);

					ns.push_back(Vector3(static_cast<float>(lCurrentNormal[0]), static_cast<float>(lCurrentNormal[1]), static_cast<float>(lCurrentNormal[2])));
				}

				if (mHasUV)
				{
					bool lUnmappedUV;
					pMesh->GetPolygonVertexUV(lPolygonIndex, lVerticeIndex, lUVName, lCurrentUV, lUnmappedUV);
					lUVs[lVertexCount * UV_STRIDE] = static_cast<float>(lCurrentUV[0]);
					lUVs[lVertexCount * UV_STRIDE + 1] = static_cast<float>(lCurrentUV[1]);

					uvs.push_back(TexCoord(static_cast<float>(lCurrentUV[0]), 1.0 - static_cast<float>(lCurrentUV[1])));
				}
			}
			++lVertexCount;
		}


		mSubMeshes[lMaterialIndex]->TriangleCount += 1;
	}
}


NiNode* Import(FbxNode * pNode, FbxAnimLayer * pAnimLayer, NiNode* parent, int level = 0)
{
	cout << pNode->GetName() << " " << level << endl;

	auto nif = CreateNode(parent, pNode->GetName());
	float move = 0;

	FbxAMatrix matrixGeo;
	matrixGeo.SetIdentity();

		FbxNodeAttribute* lNodeAttribute = pNode->GetNodeAttribute();
		if (lNodeAttribute)
		{

			auto lT = pNode->GetGeometricTranslation(FbxNode::eSourcePivot);
			auto lR = pNode->GetGeometricRotation(FbxNode::eSourcePivot);
			auto lS = pNode->GetGeometricScaling(FbxNode::eSourcePivot);

			matrixGeo.SetT(lT);
			matrixGeo.SetR(lR);
			matrixGeo.SetS(lS);

			if (lNodeAttribute->GetAttributeType() == FbxNodeAttribute::eMesh)
			{
				nif->SetFlags(16);

				FbxMesh * pMesh = pNode->GetMesh();

				pMesh->ApplyPivot();

				int coud = pMesh->GetElementPolygonGroupCount();
				int matCount = pNode->GetMaterialCount();
				int polyCount = pMesh->GetPolygonCount();
				int vertexCount = pMesh->GetControlPointsCount();
				FbxVector4 * lControlPoints = pMesh->GetControlPoints();
				int controlPointCount = pMesh->GetControlPointsCount();
				FbxVector4 lCurrentVertex;
				FbxVector4 lCurrentNormal;
				FbxVector2 lCurrentUV;
				int vertexIndex = 0;
				FbxStringList lUVNames;

				pMesh->GetUVSetNames(lUVNames);

				vector<Triangle> tris;
				vector<Vector3> verticies;
				vector<TexCoord> uvs;
				vector<Vector3> ns;

				BuildMesh(pMesh, tris, verticies, uvs, ns);

				FbxArray<FbxVector2> pUV;
				FbxArray<FbxVector4> pNormals;


				NiTriBasedGeomRef shape = (NiTriBasedGeom*)new NiTriShape();
				NiTriBasedGeomDataRef data = (NiTriBasedGeomData*)new NiTriShapeData();

				if (parent != nullptr)
				{
					parent->RemoveChild(nif);
					nif = parent;
				}


				int count = pMesh->GetElementUVCount();
				pMesh->GetPolygonVertexNormals(pNormals);

				if (StartsWith(pNode->GetName(), "nifid"))
				{
					shape->SetName("Editable Poly");
					if (count > 0)
					{
						shape->SetFlags(17);
					}
					else
					{
						shape->SetFlags(16);
					}
				}
				else
					shape->SetName(pNode->GetName());


				if (count > 0)
				{
					cout << "UV:" << count << endl;

					auto fl = new NiFloatsExtraData();
					vector<float> floats;
					fl->SetName("xyTextureTilesPerWorldUnit");
					floats.push_back(0.0000);
					floats.push_back(0.0001);

					fl->SetData(floats);
					shape->AddExtraData(fl);
				}

				FbxVector4* cp = pMesh->GetControlPoints();
				int polySize = pMesh->GetPolygonSize(3);
				int* ptr = pMesh->GetPolygonVertices();

				char coord[100];
				map<string, int> controlHash;
				map<string, FbxVector4*> normalHash;
				map<string, FbxVector2*> uvHash;

				data->SetVertices(verticies);

				data->SetTriangles(tris);
				if (ns.size())
					data->SetNormals(ns);
				auto dd = DynamicCast<NiTriShapeData>(data);

				if (count > 0)
				{
					data->SetUVSetCount(count);
					data->SetUVSet(0, uvs);
					shape->SetFlags(16);
				}
				else
				{
					shape->SetFlags(17);
				}
				data->SetConsistencyFlags(CT_STATIC);


				shape->SetData(data);

				nif->AddChild(DynamicCast<NiAVObject>(shape));


				NiAVObjectRef av(DynamicCast<NiAVObject>(shape));

				auto textures = new TX();

				textures->SetTextureCount(9);
				shape->SetActiveMaterial(0);

				bool hasSpec = false;
				vector< TexDesc*> added;
				for (int i = 0; i < matCount; i++)
				{

					auto mat = pNode->GetMaterial(i);

					auto diffuse = mat->FindProperty(FbxSurfaceMaterial::sDiffuse);
					if (diffuse.GetSrcObjectCount<FbxFileTexture>())
					{
						auto d = AddMaterialTexture(TexType::BASE_MAP, textures, diffuse);
						std::string filename = d->source->GetTextureFileName();
						fprintf(file, "DIFFUSE:%s\n", filename.c_str());
						
						added.push_back(d);
					}

					auto glow = mat->FindProperty(FbxSurfaceMaterial::sEmissive);
					if (glow.GetSrcObjectCount<FbxFileTexture>())
					{
						auto d = AddMaterialTexture(TexType::GLOW_MAP, textures, glow);
						std::string filename = d->source->GetTextureFileName();
						fprintf(file, "GLOW:%s\n", filename.c_str());

						added.push_back(d);
					}

					auto spec = mat->FindProperty(FbxSurfaceMaterial::sSpecularFactor);
					if (spec.GetSrcObjectCount<FbxFileTexture>())
					{
						auto d = AddMaterialTexture(TexType::UNKNOWN2_MAP, textures, spec);
						std::string filename = d->source->GetTextureFileName();
						fprintf(file, "SPECULAR:%s\n", filename.c_str());

						added.push_back(d);
						hasSpec = true;
					}

				}

				if (count)
				{
					for (int i = 0; i < added.size(); i++)
					{
						AddShaderTexture(textures, added[i], textures->GetShaderTextureCount());
					}
					av->AddProperty(textures);
					if (hasSpec)
						textures->SetFlag(5);
					else
						textures->SetFlag(4);

				}
			}

		}

	FbxAMatrix local = pNode->EvaluateLocalTransform();
	FbxAMatrix global = pNode->EvaluateGlobalTransform();
	FbxAMatrix globalMatrix = pNode->EvaluateLocalTransform();
	FbxAMatrix matrix = globalMatrix * matrixGeo;
	auto tran = pNode->EvaluateLocalTranslation();
	Matrix44 m4(Matrix44::IDENTITY);
	
	m4.Set(
		matrix.GetRow(0)[0], matrix.GetRow(0)[1], matrix.GetRow(0)[2], matrix.GetRow(0)[3],
		matrix.GetRow(1)[0], matrix.GetRow(1)[1], matrix.GetRow(1)[2], matrix.GetRow(1)[3],
		matrix.GetRow(2)[0], matrix.GetRow(2)[1], matrix.GetRow(2)[2], matrix.GetRow(2)[3],
		matrix.GetRow(3)[0], matrix.GetRow(3)[1], matrix.GetRow(3)[2], matrix.GetRow(3)[3]);




	const int lChildCount = pNode->GetChildCount();


	for (int lChildIndex = 0; lChildIndex < lChildCount; ++lChildIndex)
	{
		Import(pNode->GetChild(lChildIndex), pAnimLayer, nif, level+1);
	}
	return nif;
}

enum MythicNodeTypes
{
	NIF = 1,
	HELD = 2,
	ITEM = 3,
	BACK = 4,
	VFX = 5,
	SPELL = 6,
	TRAILBASE = 7,

};

void Load(Niflib::NiAVObjectRef object)
{
	
	cout << object->GetName() << endl;
	Niflib::NiNodeRef n = Niflib::DynamicCast<Niflib::NiNode>(object);
	
	if (object->GetName() == "Editable Poly")
	{
		trishape = object.operator Niflib::NiAVObject *();

	}

	if (n)
	{
		std::vector<Niflib::NiAVObjectRef> children = n->GetChildren();

		for (size_t i = 0; i < children.size(); i++)
		{
			Load(children[i]);
		}
	}
}



int main(int argc, char** argv)
{
	if (argc < 4)
	{
		printf("Expected parameters: <input_fbx_path> <output_nif_path> <output_textlist_path>");
		return -1;
	}
	FbxString lFilePath("");
	lFilePath = argv[1];


	file = fopen(argv[3], "w+");

	FbxManager* lSdkManager;
	FbxScene* lScene;

	InitializeSdkObjects(lSdkManager, lScene);

	auto importer = FbxImporter::Create(lSdkManager, "");

	if (importer->Initialize(argv[1]) == true)
	{
		if (importer->Import(lScene) == true)
		{
			// Convert Axis System to what is used in this example, if needed
			FbxAxisSystem SceneAxisSystem = lScene->GetGlobalSettings().GetAxisSystem();
			FbxAxisSystem OurAxisSystem(FbxAxisSystem::eYAxis, FbxAxisSystem::eParityOdd, FbxAxisSystem::eRightHanded);
			if (SceneAxisSystem != OurAxisSystem)
			{
				OurAxisSystem.ConvertScene(lScene);
			}

			// Convert Unit System to what is used in this example, if needed
			FbxSystemUnit SceneSystemUnit = lScene->GetGlobalSettings().GetSystemUnit();
			auto factor = SceneSystemUnit.GetScaleFactor();
			if (SceneSystemUnit.GetScaleFactor() != 1.0)
			{
				//he unit in this example is centimeter.
				FbxSystemUnit::Foot.ConvertScene(lScene);
			}

			std::ofstream output(argv[2], std::ios::binary);
			Niflib::NifInfo info = Niflib::NifInfo();
			info.version = Niflib::VER_20_3_0_9;

			auto root = lScene->GetRootNode();

			auto nif = Import(root->GetChild(0), nullptr, nullptr, 0);
			auto zbuffer = new NiZBufferProperty();
			zbuffer->SetFlags(15);
			nif->AddProperty(zbuffer);

			auto vc = new NiVertexColorProperty();
			vc->SetFlags(8);
			vc->SetVertexMode(VertMode::VERT_MODE_SRC_IGNORE);
			vc->SetLightingMode(LightMode::LIGHT_MODE_EMISSIVE);
			nif->AddProperty(vc);

		

			auto collision = new Niflib::NiCollisionData();
			nif->SetCollisionObject(collision);
			nif->SetCollisionMode(Niflib::NiAVObject::CollisionType::CT_BOUNDINGBOX);

			for (int lChildIndex = 1; lChildIndex < root->GetChildCount(); ++lChildIndex)
			{
				auto val = Import(root->GetChild(lChildIndex), nullptr, nif, lChildIndex);
			}

			float angle = degToRad(90);

			nif->SetLocalScale(1);

			Niflib::WriteNifTree(output, nif, info);
			output.flush();
			output.close();
		}


		fclose(file);
	}


}