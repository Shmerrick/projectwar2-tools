﻿using BrotliSharpLib;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace AssetHashPacker
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            byte[] uncompressed = File.ReadAllBytes(@"asset_hashes.tab");
            byte[] compressed = Brotli.CompressBuffer(uncompressed, 0, uncompressed.Length, 11, 22/*, customDictionary */);
            File.WriteAllBytes("asset_hashes.dat", compressed);
        }
    }
}
