﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using HelixToolkit.Wpf.SharpDX.Assimp;
using Microsoft.Practices.Unity;

namespace ManagedFbx.Viewer
{
    public class FileViewModel
    {
        #region Constructors

        public FileViewModel(string fileName)
        {
            FileName = fileName;

            Importer importer = new Importer();
            Scene = importer.Load(FileName);
        }

        #endregion Constructors

        #region Members

        private FileView view;

        public ContentViewModel ContentViewModel { get; set; } = null;

        public string Header
        {
            get { return (Scene != null) ? Scene.ToString() : Path.GetFileNameWithoutExtension(FileName); }
        }

        public string FileName { get; private set; }

        public HelixToolkitScene Scene { get; private set; }

        public IEnumerable<HelixToolkit.Wpf.SharpDX.Model.Scene.SceneNode> Nodes
        {
            get { yield return Scene.Root; }
        }

        [Dependency]
        public FileView View
        {
            get { return view; }

            set
            {
                view = value;
                if (view != null)
                    view.DataContext = this;
            }
        }

        #endregion Members

        #region Functions

        #endregion Functions
    }
}
