﻿using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;

[ClassInterface(ClassInterfaceType.AutoDispatch)]
[ComVisible(true)]
public class TransparentEventCatcher : ContainerControl
{
    public TransparentEventCatcher() : base()
    {
        if (!base.DesignMode)
        {
            SetStyle(ControlStyles.Opaque, true);
            UpdateStyles();
            AutoScaleMode = AutoScaleMode.None;
            AutoScroll = false;
            TabStop = false;
        }
    }

    private const short WS_EX_TRANSPARENT = 0x20;

    protected override CreateParams CreateParams
    {
        [SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode = true)]
        get
        {
            CreateParams l_cp;
            l_cp = base.CreateParams;
            l_cp.ExStyle = (l_cp.ExStyle | WS_EX_TRANSPARENT);
            if (base.DesignMode)
            {
                return base.CreateParams;
            }

            return l_cp;
        }
    }
}
