﻿using System;

namespace Launcher
{
    public class SourceReadyEventArgs : EventArgs
    {
        public string Html { get; protected set; }

        public SourceReadyEventArgs(string source) : base()
        {
            Html = source;
        }

        public SourceReadyEventArgs() : base()
        {
            Html = null;
        }
    }
}