namespace Launcher
{
    public class SimpleSubClass
    {
        public string PropertyOne { get; set; }
        public int[] Numbers { get; set; }
    }
}
