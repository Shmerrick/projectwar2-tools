﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UnexpectedBytes.War.Terrain
{
    public class Patch
    {
        public ushort ZoneID;
        public byte PatchX;
        public byte PatchY;
        ushort LayerCount;

        public List<UV> UVs = new List<UV>();
        public Heightmap HeightmapData = new Heightmap();
        public List<object> UnkData = new List<object>();

        public Vertex[] Vertices;
        public int[] Indices;


        public float Unk0 { get; set; }
        public ushort Unk1 { get; set; }
        public ushort Unk2 { get; set; }
        public float Unk3 { get; set; }
        public float Unk4 { get; set; }
        public float Unk5 { get; set; }
        public ushort Unk6 { get; set; }
        public ushort Unk7 { get; set; }
        public float Unk8 { get; set; }
        public ushort Unk9 { get; set; }

        public void Load(string patches_path, int zone_index, int column, int row)
        {
            string patch = Path.Combine(patches_path, $"0.{zone_index}.{column}.{row}.patch");
            if (File.Exists(patch))
            {
                Load(File.ReadAllBytes(patch));
            }
        }

        public void Load(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            BinaryReader br = new BinaryReader(ms);

            uint size = br.ReadUInt32();
            ZoneID = br.ReadUInt16();
            PatchX = br.ReadByte();
            PatchY = br.ReadByte();

            Unk0 = br.ReadSingle();
            Unk1 = br.ReadUInt16();
            Unk2 = br.ReadUInt16();
            Unk3 = br.ReadSingle();
            Unk4 = br.ReadSingle();
            Unk5 = br.ReadSingle();
            Unk6 = br.ReadUInt16();
            Unk7 = br.ReadUInt16();
            Unk8 = br.ReadSingle();
            Unk9 = br.ReadUInt16();

            int count = 0;

            while (ms.Position < 0x55C8)
            {
                UV patch = new UV()
                {
                    Index = count
                };
                patch.Read(br);
                UVs.Add(patch);
                count++;
            }

            ms.Position += 2;

            HeightmapData.Read(br);

            LayerCount = br.ReadUInt16();

            //long pos = br.BaseStream.Position;
            //br.BaseStream.Position = pos;

            while (br.BaseStream.Position < 0xDCD4)
            {
                float a0 = br.ReadSingle();
                if (a0 < 0.00000001 || a0 > 2.36395336E+05)
                {
                    br.BaseStream.Position -= 4;
                    var s = br.ReadUInt16();
                    UnkData.Add("USHORT:" + s);
                }
                else
                {
                    UnkData.Add("FLOAT:" + a0);
                }
            }

        }
    }
}
