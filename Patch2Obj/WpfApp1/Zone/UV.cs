﻿using System.IO;

namespace UnexpectedBytes.War.Terrain
{
    public class UV
    {
        public int Index { get; set; }
        public byte Unk0a { get; set; }
        public byte Unk0b { get; set; }
        public ushort Unk0c { get; set; }
        public ushort Unk1 { get; set; }
        public ushort Unk2 { get; set; }
        public ushort Unk3 { get; set; }
        public ushort Unk4A { get; set; }
        public ushort Unk4B { get; set; }
        public byte Unk5A { get; set; }
        public byte Unk5B { get; set; }

        public void Read(BinaryReader reader)
        {
            Unk0a = reader.ReadByte();
            Unk0b = reader.ReadByte();
            Unk0c = reader.ReadUInt16();
            Unk1 = reader.ReadUInt16();
            Unk2 = reader.ReadUInt16();
            Unk3 = reader.ReadUInt16();
            Unk4A = reader.ReadUInt16();
            Unk4B = reader.ReadUInt16();
            Unk5A = reader.ReadByte();
            Unk5B = reader.ReadByte();
        }
    }
}
