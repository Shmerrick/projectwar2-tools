﻿using System.Collections.Generic;

namespace UnexpectedBytes.War
{
    using System;
    using System.Numerics;
    using Terrain;

    public class Zone
    {
        public Vertex[] Vertices;
        public Triangle[] Triangles;

        public Sector Sector;

        public PCX Terrain;
        public PCX Offset;

        public PCX Shadow;
        public PCX Hole;

        public List<Patch> Patches;

        public int ScaleFactor => Sector.ScaleFactor;

        public int OffsetFactor => Sector.OffsetFactor;

        public int Width => Terrain.Width;

        public int Height => Terrain.Height;

        public int VertexCount => Terrain.Width * Terrain.Height;

        public int TriangleCount => 2 * (Terrain.Width - 2) * (Terrain.Height - 2);

        public int IndexCount => 3 * TriangleCount;

        public void LoadZone(string zones_path, int zone_index)
        {
            Sector = new Sector();
            Sector.Load(zones_path, zone_index);

            Terrain = new PCX();
            Terrain.Load(zones_path, zone_index, "terrain");
            Offset = new PCX();
            Offset.Load(zones_path, zone_index, "offset");
            Shadow = new PCX();
            Shadow.Load(zones_path, zone_index, "shadow");
            Hole = new PCX();
            Hole.Load(zones_path, zone_index, "holemap");


            BuildVertices();

            BuildIndices();

            BuildNormals();


            Patches = new List<Patch>();
        }

        private void BuildVertices()
        {
            Vertices = new Vertex[VertexCount];

            for (int y = 0; y < Height - 1; ++y)
            {
                for (int x = 0; x < Width - 1; ++x)
                {
                    Vertices[(y * Width) + x] = new Vertex
                    {
                        P = GetVertexPosition(x, y),
                        N = new Vector3(0, 0, 0)
                    };
                }
            }
        }

        private void BuildIndices()
        {
            Triangles = new Triangle[TriangleCount];

            int i = 0;
            for (int y = 1; y < Height - 1; ++y)
            {
                for (int x = 1; x < Width - 1; ++x)
                {
                    int b = y * Width; // base vertex index for this row

                    Triangles[i] = new Triangle();
                    Triangles[i].A = b + x;
                    Triangles[i].B = b + x + Width;
                    Triangles[i].C = b + x + 1;
                    ++i;

                    Triangles[i] = new Triangle();
                    Triangles[i].A = b + x + 1;
                    Triangles[i].B = b + x + Width;
                    Triangles[i].C = b + x + Width + 1;
                    ++i;
                }
            }
        }

        private void BuildNormals()
        {
            for (int i = 0; i < Vertices.Length; ++i)
                Vertices[i].N = Vector3.Zero;

            for (int i = 0; i < Triangles.Length; ++i)
            {
                // Get triangle indices
                int i0 = Triangles[i].A;
                int i1 = Triangles[i].B;
                int i2 = Triangles[i].C;

                // Get vertex positions of triangle
                Vector3 v0 = Vertices[i0].P;
                Vector3 v1 = Vertices[i1].P;
                Vector3 v2 = Vertices[i2].P;

                // Compute face normal of triangle
                Vector3 n = GetFaceNormal(v0, v1, v2);

                // Add normal to all vertices referenced by this triangle
                Vertices[i0].N += n;
                Vertices[i1].N += n;
                Vertices[i2].N += n;

                // FIXME: use angle weighting
            }

            for (int i = 0; i < Vertices.Length; i++)
                Vertices[i].N = Vector3.Normalize(Vertices[i].N);
        }

        Vector3 GetFaceNormal(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            Vector3 e1 = v0 - v1;
            Vector3 e2 = v2 - v1;
            Vector3 no = Vector3.Cross(e1, e2);

            return no;
        }

        public Vector3 GetVertexPosition(int x, int y)
        {
            int xw = Terrain.Width - x - 1;
            int yh = Terrain.Height - y - 1;

            float fx = 65535.0f * x / (float)(Terrain.Width - 1);
            float fy = 65535.0f * y / (float)(Terrain.Height - 1);
            float fz = GetValue(xw, y);

            return new Vector3(fx, fy, fz);
        }

        public int GetValue(int x, int y)
        {
            return Sector.ScaleFactor * Terrain.GetValue(x, y) + Sector.OffsetFactor * Offset.GetValue(x, y);
        }

    }
}
