﻿using System.IO;

namespace UnexpectedBytes.War.Terrain
{
    public class UV
    {
        public System.Int32 Index { get; set; }
        public System.Byte Unk0a { get; set; }
        public System.Byte Unk0b { get; set; }
        public System.UInt16 Unk0c { get; set; }
        public System.UInt16 Unk1 { get; set; }
        public System.UInt16 Unk2 { get; set; }
        public System.UInt16 Unk3 { get; set; }
        public System.UInt16 Unk4A { get; set; }
        public System.UInt16 Unk4B { get; set; }
        public System.Byte Unk5A { get; set; }
        public System.Byte Unk5B { get; set; }

        public void Read(BinaryReader reader)
        {
            Unk0a = reader.ReadByte();
            Unk0b = reader.ReadByte();
            Unk0c = reader.ReadUInt16();
            Unk1 = reader.ReadUInt16();
            Unk2 = reader.ReadUInt16();
            Unk3 = reader.ReadUInt16();
            Unk4A = reader.ReadUInt16();
            Unk4B = reader.ReadUInt16();
            Unk5A = reader.ReadByte();
            Unk5B = reader.ReadByte();
        }
    }
}
