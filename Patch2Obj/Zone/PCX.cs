﻿using System;
using System.IO;

namespace UnexpectedBytes.War.Terrain
{
    public class PCX
    {
        public struct Header
        {
            public Byte Identifier { get; set; }                /* ZSoft = 0x0A */
            public Byte Version { get; set; }                   /* Software Version (0x00, 0x02, 0x03, 0x04, 0x05) */
            public Byte Encoding { get; set; }                  /* RLE = 0x01*/
            public Byte BitsPerPixel { get; set; }              /* 0x01, 0x02, 0x04, 0x08 */
            public UInt16 XStart { get; set; }                  /* Left viewable image offset */
            public UInt16 YStart { get; set; }                  /* Top viewable image offset */
            public UInt16 XEnd { get; set; }                    /* Right viewable image offset */
            public UInt16 YEnd { get; set; }                    /* Bottom  viewable image offset */
            public UInt16 HorizontalResolution { get; set; }    /* Horizontal DPI */
            public UInt16 VerticalResolution { get; set; }      /* Vertical DPI */
            public Byte[/*48*/] Palette { get; set; }           /* Color Palette for 16bit */
            public Byte Reserved1 { get; set; }                 /* Reserved (Always 0x00) */
            public Byte NumBitPlanes { get; set; }              /* Number of Bit Planes */
            public UInt16 BytesPerLine { get; set; }            /* Scan line length in bytes */
            public UInt16 PaletteType { get; set; }             /* Color = 0x01, Greyscale = 0x02 */
            public UInt16 HorzontalScreenSize { get; set; }     /* Horizontal Screen Size */
            public UInt16 VerticalScreenSize { get; set; }      /* Vertical Screen Size */
            public Byte[/*54*/] Reserved2 { get; set; }         /* Reserved (Always 0x00) */
        }

        public Header HeaderData { get; set; }                  /*  */
        public Byte[] PixelData { get; set; }

        public Int32 Width => HeaderData.XEnd + 1 - HeaderData.XStart;
        public Int32 Height => HeaderData.YEnd + 1 - HeaderData.YStart;
        public Int32 MaxColors => (Int32)(1L << (HeaderData.BitsPerPixel * HeaderData.NumBitPlanes));
        public Int32 ScanLineLength => HeaderData.BytesPerLine * HeaderData.NumBitPlanes;
        public Int32 TotalLength => Width * Height * HeaderData.NumBitPlanes;
        public String Name { get; set; }

        public PCX() { }

        public PCX(String zones_path, Int32 zone_id, String filename) => Load(zones_path, zone_id, filename);

        public void Load(String zones_path, Int32 zone_id, String filename)
        {
            Name = $"zone{zone_id:D3}/{filename}.pcx";
            var pcx = Path.Combine(zones_path, $"zone{zone_id:D3}", $"{filename}.pcx");

            if (File.Exists(pcx))
            {
                System.Console.WriteLine($"[!] Processing {pcx}.");
                Load(new MemoryStream(File.ReadAllBytes(pcx)));
            }
        }

        public PCX(Stream bs) => Load(bs);

        public void Load(Stream bs)
        {
            var br = new BinaryReader(bs);
            Load(ref br);
            br.Dispose();
        }

        public PCX(ref BinaryReader br) => Load(ref br);

        private void Load(ref BinaryReader br)
        {
            HeaderData = new Header
            {
                Identifier = br.ReadByte(),
                Version = br.ReadByte(),
                Encoding = br.ReadByte(),
                BitsPerPixel = br.ReadByte(),

                XStart = br.ReadUInt16(),
                YStart = br.ReadUInt16(),
                XEnd = br.ReadUInt16(),
                YEnd = br.ReadUInt16(),
                HorizontalResolution = br.ReadUInt16(),
                VerticalResolution = br.ReadUInt16(),

                Palette = br.ReadBytes(48),

                Reserved1 = br.ReadByte(),
                NumBitPlanes = br.ReadByte(),

                BytesPerLine = br.ReadUInt16(),
                PaletteType = br.ReadUInt16(),
                HorzontalScreenSize = br.ReadUInt16(),
                VerticalScreenSize = br.ReadUInt16(),

                Reserved2 = br.ReadBytes(54),
            };

            DecodeImage(ref br);
        }

        public void DecodeImage(ref BinaryReader br)
        {
            PixelData = new Byte[TotalLength];
            var RowData = new Byte[Width];

            for (var i = 0; i < Height; ++i)
            {
                DecodeRow(ref RowData, ref br);
                var RowOffset = i * HeaderData.NumBitPlanes * Width;
                RowData.CopyTo(PixelData, RowOffset);
            }
        }

        public void DecodeRow(ref Byte[] RowData, ref BinaryReader br)
        {
            Byte value = 0;
            var pos = 0;
            var runLength = 0;

            while (pos < HeaderData.BytesPerLine)
            {
                if (runLength > 0)
                {
                    RowData[pos++] = value;
                    runLength--;
                }
                else
                {
                    value = br.ReadByte();
                    if ((value & 0xC0) == 0xC0)
                    {
                        runLength = value & 0x3F;
                        value = br.ReadByte();
                    }
                    else
                    {
                        runLength = 0;
                        RowData[pos++] = value;
                    }
                }
            }
        }

        public Byte GetValue(Int32 x, Int32 y) => GetValue(x, y, 0);

        public Byte GetValue(Int32 x, Int32 y, Int32 plane)
        {
            if (plane >= HeaderData.NumBitPlanes)
            {
                throw new NotImplementedException();
            }

            var rowOffset = y * (HeaderData.NumBitPlanes + plane) * Width;
            return PixelData[rowOffset + (x * (HeaderData.NumBitPlanes + plane))];
        }
    }
}
