CREATE DATABASE  IF NOT EXISTS `War` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `War`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: War
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `PlayerItem`
--

DROP TABLE IF EXISTS `PlayerItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `PlayerItem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PlayerID` bigint(20) DEFAULT NULL,
  `ItemID` bigint(20) DEFAULT NULL,
  `SlotIndex1` int(11) DEFAULT NULL,
  `SlotIndex2` int(11) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `UsedTime` bigint(20) DEFAULT NULL,
  `ModelID` int(11) DEFAULT NULL,
  `ModelIDAlternate` int(11) DEFAULT NULL,
  `Dye1` int(11) DEFAULT NULL,
  `Dye2` int(11) DEFAULT NULL,
  `TrophySlot` tinyint(3) unsigned DEFAULT NULL,
  `TrophyData` tinyint(3) unsigned DEFAULT NULL,
  `ShowCloak` tinyint(1) DEFAULT NULL,
  `HeraldryEmblemID` int(11) DEFAULT NULL,
  `HeraldryPattternID` int(11) DEFAULT NULL,
  `HeraldryColorID1` tinyint(3) unsigned DEFAULT NULL,
  `HeraldryColorID2` tinyint(3) unsigned DEFAULT NULL,
  `HeraldryShapeIndex` tinyint(3) unsigned DEFAULT NULL,
  `HeraldryUnk1` int(11) DEFAULT NULL,
  `HeraldryUnk2` int(11) DEFAULT NULL,
  `Bound` tinyint(1) DEFAULT NULL,
  `ItemApperanceID` bigint(20) DEFAULT NULL,
  `ChargesRemaning` tinyint(3) unsigned DEFAULT NULL,
  `TimeLeftBeforeDecay` bigint(20) DEFAULT NULL,
  `DecayPaused` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PlayerItem_PlayerID` (`PlayerID`),
  KEY `IX_PlayerItem_ItemID` (`ItemID`),
  KEY `FK_PlayerItem_Item_ItemApperanceID` (`ItemApperanceID`),
  CONSTRAINT `FK_PlayerItem_Item_ItemApperanceID` FOREIGN KEY (`ItemApperanceID`) REFERENCES `item` (`id`),
  CONSTRAINT `FK_PlayerItem_Item_ItemID` FOREIGN KEY (`ItemID`) REFERENCES `item` (`id`),
  CONSTRAINT `FK_PlayerItem_Player_PlayerID` FOREIGN KEY (`PlayerID`) REFERENCES `player` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=446 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PlayerItem`
--

LOCK TABLES `PlayerItem` WRITE;
/*!40000 ALTER TABLE `PlayerItem` DISABLE KEYS */;
INSERT INTO `PlayerItem` VALUES (422,39,640223,906,40,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(423,39,640223,44,40,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(424,39,206027,115,40,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(425,39,5900215,54,40,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(426,39,435415,45,40,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(427,37,197905,40,40,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(428,37,435415,20,41,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(429,37,5850154,1014,42,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(431,37,436586,42,42,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(432,39,435396,41,41,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(434,39,88883,51,40,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(435,39,88883,40,41,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(436,39,435331,22,40,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,0),(437,39,435343,21,43,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,0),(438,39,435355,55,44,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,0),(439,39,435367,43,45,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,0),(440,39,435379,28,46,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,0),(441,39,435391,23,47,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,0),(442,39,435403,27,48,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,0),(443,39,435415,20,49,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,0),(444,39,5012541,52,52,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),(445,39,5758877,53,53,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0);
/*!40000 ALTER TABLE `PlayerItem` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-26 18:40:18
