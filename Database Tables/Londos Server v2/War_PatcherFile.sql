CREATE DATABASE  IF NOT EXISTS `War` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `War`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: War
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `PatcherFile`
--

DROP TABLE IF EXISTS `PatcherFile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `PatcherFile` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EnvironmentID` bigint(20) NOT NULL,
  `Filename` longtext,
  `Access` bigint(20) DEFAULT NULL,
  `Required` tinyint(1) DEFAULT NULL,
  `PatcherFileID` bigint(20) DEFAULT NULL,
  `Exists` tinyint(1) DEFAULT NULL,
  `Size` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PatcherFile_EnvironmentID` (`EnvironmentID`),
  KEY `IX_PatcherFile_PatcherFileID` (`PatcherFileID`),
  CONSTRAINT `FK_PatcherFile_Environment_EnvironmentID` FOREIGN KEY (`EnvironmentID`) REFERENCES `environment` (`id`),
  CONSTRAINT `FK_PatcherFile_PatcherFile_PatcherFileID` FOREIGN KEY (`PatcherFileID`) REFERENCES `patcherfile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PatcherFile`
--

LOCK TABLES `PatcherFile` WRITE;
/*!40000 ALTER TABLE `PatcherFile` DISABLE KEYS */;
INSERT INTO `PatcherFile` VALUES (95,5,'art.myp',0,0,NULL,1,3916309575),(96,5,'art2.myp',0,0,NULL,1,1507732764),(97,5,'art3.myp',0,0,NULL,1,415829977),(98,5,'audio.myp',0,0,NULL,1,1424613376),(99,5,'binkw32.dll',0,0,NULL,1,173056),(100,5,'data.myp',0,0,NULL,1,61204533),(101,5,'interface.myp',0,0,NULL,1,210249208),(102,5,'mss32.dll',0,0,NULL,1,449024),(103,5,'uninst2.exe',0,0,NULL,1,123575),(104,5,'vo_english.myp',0,0,NULL,1,847425511),(106,5,'world.myp',0,0,NULL,1,2783583629),(107,5,'miles',0,0,NULL,0,0),(108,5,'mssdolby.flt',0,0,107,1,7680),(109,5,'mssds3d.flt',0,0,107,1,13312),(110,5,'mssdsp.flt',0,0,107,1,57856),(111,5,'msseax.flt',0,0,107,1,60416),(112,5,'mssmp3.asi',0,0,107,1,96256),(113,5,'msssrs.flt',0,0,107,1,12800),(114,5,'mssvoice.asi',0,0,107,1,153600),(116,1,'dev.myp',0,0,NULL,1,59039468);
/*!40000 ALTER TABLE `PatcherFile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-26 18:40:10
