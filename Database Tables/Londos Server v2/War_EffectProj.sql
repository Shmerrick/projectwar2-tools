CREATE DATABASE  IF NOT EXISTS `War` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `War`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: War
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `EffectProj`
--

DROP TABLE IF EXISTS `EffectProj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `EffectProj` (
  `ID` bigint(20) NOT NULL,
  `Description` longtext,
  `AutoArc` int(11) DEFAULT NULL,
  `CtrlPt1HorzUnitsMin` int(11) DEFAULT NULL,
  `CtrlPt1HorzUnitsMax` int(11) DEFAULT NULL,
  `CtrlPt1HorzDir` int(11) DEFAULT NULL,
  `CtrlPt1VertUnitsMin` int(11) DEFAULT NULL,
  `CtrlPt1VertUnitsMax` int(11) DEFAULT NULL,
  `CtrlPt1VertDir` int(11) DEFAULT NULL,
  `CtrlPt1OffsetMin` int(11) DEFAULT NULL,
  `CtrlPt1OffsetMax` int(11) DEFAULT NULL,
  `CtrlPt2HorzUnitsMin` int(11) DEFAULT NULL,
  `CtrlPt2HorzUnitsMax` int(11) DEFAULT NULL,
  `CtrlPt2HorzDir` int(11) DEFAULT NULL,
  `CtrlPt2VertUnitsMin` int(11) DEFAULT NULL,
  `CtrlPt2VertUnitsMax` int(11) DEFAULT NULL,
  `CtrlPt2VertDir` int(11) DEFAULT NULL,
  `CtrlPt2OffsetMin` int(11) DEFAULT NULL,
  `CtrlPt2OffsetMax` int(11) DEFAULT NULL,
  `TargetNode` longtext,
  `ReversePath` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EffectProj`
--

LOCK TABLES `EffectProj` WRITE;
/*!40000 ALTER TABLE `EffectProj` DISABLE KEYS */;
INSERT INTO `EffectProj` VALUES (1,'Direct',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'fx_torso_NA',0),(2,'Lob UP',0,0,0,2,200,300,0,0,0,0,0,0,100,150,1,0,0,'fx_torso_NA',0),(3,'s bend L_R',0,200,400,2,0,0,0,0,0,200,400,0,0,0,0,0,0,'fx_torso_NA',0),(4,'Lob High ',0,0,0,2,500,600,1,0,0,0,0,0,100,200,1,0,0,'fx_torso_NA',0),(5,'[R] Tight',0,0,100,2,0,100,2,0,0,0,100,2,0,100,2,0,0,'fx_torso_NA',0),(6,'[R] Wide',0,150,200,2,0,200,2,0,0,0,200,2,0,200,2,0,0,'fx_torso_NA',0),(7,'[R] T to W',0,0,75,2,0,75,2,0,0,100,200,2,100,200,2,0,0,'fx_torso_NA',0),(8,'[R] W to T',0,100,200,2,100,200,2,0,0,0,75,2,0,75,2,0,0,'fx_torso_NA',0),(9,'RIGHT W to T',0,25,50,0,25,50,2,0,0,0,50,1,0,0,2,0,0,'fx_torso_NA',0),(10,'LEFT W to T',0,25,50,1,25,50,2,0,0,0,50,0,0,0,2,0,0,'fx_torso_NA',0),(11,'Straight',0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,'fx_torso_NA',0),(12,'s bend L_R',0,200,400,2,0,0,0,0,0,200,400,0,0,0,0,0,0,'fx_torso_NA',1),(13,'[R] T to W Reverse',0,300,300,0,0,20,1,0,0,300,300,1,0,20,1,0,0,'fx_torso_NA',1),(14,'[R] W to T Reverse',0,300,300,1,0,20,0,0,0,300,300,0,0,20,0,0,0,'fx_torso_NA',1),(15,'Lob UP Headshot',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'fx_head',0),(16,'Lob UP Neckshot',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'fx_neck',0),(17,'foot shot',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'root',0),(18,'Above Head lob test',0,0,0,2,500,600,1,0,0,0,0,0,100,200,1,0,0,'attach_name',0),(19,'flare',0,0,0,2,1000,1100,1,0,0,0,0,2,1000,1100,1,0,0,'attach_name',0),(20,'Lob Mid',0,0,0,2,350,450,1,0,0,0,0,0,100,150,1,0,0,'fx_torso_NA',0),(21,'Lob Mid early apex',0,0,0,2,200,300,1,0,0,0,0,0,100,150,1,0,0,'fx_torso_NA',0),(75,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0),(76,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0),(99,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0),(100,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0);
/*!40000 ALTER TABLE `EffectProj` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-26 18:40:17
