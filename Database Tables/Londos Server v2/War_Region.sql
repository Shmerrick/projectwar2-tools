CREATE DATABASE  IF NOT EXISTS `War` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `War`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: War
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Region`
--

DROP TABLE IF EXISTS `Region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Region` (
  `ID` bigint(20) NOT NULL,
  `Name` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Region`
--

LOCK TABLES `Region` WRITE;
/*!40000 ALTER TABLE `Region` DISABLE KEYS */;
INSERT INTO `Region` VALUES (0,'Region 0'),(1,'Bloodhorn North | Dangerous Territory | Ekrund | Mount Bloodhorn'),(2,'Black Crag | Butcher\'s Pass | Cinderfall | Dangerous Territory | Death Peak | Kadrin Valley | Stonewatch | Thunder Mountain'),(3,'Chrace | Dangerous Territory | The Blighted Isle'),(4,'Caledor | Dangerous Territory | Dragonwake | Eataine | Fell Landing | Isle of the Dead | Shining Way'),(5,'296 | 299 | Item Test | Mount Pigwank | Pigwank Hills | The Tomb of Pigwank'),(6,'Dangerous Territory | High Pass | Talabecland'),(7,'Altdorf | Bright Wizard College | Sigmar\'s Hammer'),(8,'Dangerous Territory | Nordland | Norsca'),(9,'301 | 302 | 305 | Necropolis of Zandri | TK Scenery | Tomb Kings North Scenery'),(10,'Black Fire Pass | Dangerous Territory | The Badlands'),(11,'186 | 187 | 188 | Chaos Wastes | Dangerous Territory | Outer Dark | Praag | Reikland | Reikwald | The Kraken Sea | The Maw | West Praag'),(12,'Barak Varr | Dangerous Territory | Marshes of Madness'),(14,'Dangerous Territory | Ostland | Troll Country'),(15,'Dangerous Territory | Ellyrion | The Shadowlands'),(16,'Avelorn | Dangerous Territory | Saphery'),(17,'The Inevitable City | The Viper Pit | Winds of Chaos'),(30,'Gates of Ekrund | Karaz-a-Karak'),(31,'Mourkain Temple'),(32,'The Ironclad'),(33,'Doomfist Crater'),(34,'Thunder Valley'),(36,'Kadrin Valley Pass'),(37,'The Mad WAAAGH! Pit'),(38,'Black Fire Basin'),(39,'Logrin\'s Forge'),(41,'Altdorf War Quarters'),(42,'The Undercroft'),(43,'Gromril Crossing'),(44,'Howling Gorge'),(45,'Gates of Ekrund T2'),(50,'Hunter\'s Vale'),(51,'51'),(52,'52'),(60,'Mount Gunbad'),(63,'Gunbad Nursery'),(64,'Gunbad Lab'),(65,'Squig Boss'),(66,'Gunbad Barracks'),(130,'Nordenwatch'),(131,'Stonetroll Crossing'),(132,'Talabec Dam'),(133,'Maw of Madness'),(134,'Reikland Hills'),(135,'Twisting Tower'),(136,'Battle for Praag'),(137,'Grovod Caverns'),(138,'Reikland Factory'),(139,'High Pass Cemetery'),(140,'Dangerous Territory'),(152,'The Sewers of Altdorf'),(153,'The Sewers of Altdorf'),(154,'Warpblade Tunnels'),(155,'Sacellum Dungeons'),(156,'Sacellum Dungeons'),(157,'Temple of Sigmar'),(158,'The Monolith'),(159,'The Sacellum'),(160,'Bastion Stair'),(163,'Thar\'Ignan'),(164,'Lord Slaurith'),(165,'Kaarn the Vanquisher'),(166,'Skull Lord Var\'Ithrok'),(167,'The Inevitable City'),(168,'Altdorf'),(169,'The Sewers of Altdorf'),(170,'Altdorf Palace'),(171,'The Screaming Cat'),(172,'The Eternal Citadel'),(173,'Sacellum Dungeons'),(174,'The Elysium'),(176,'Sigmar Crypts'),(177,'Warpblade Tunnels'),(179,'Tomb of the Vulture Lord'),(189,'Bright Wizard College'),(195,'Bloodwrought Enclave'),(196,'Bilerot Burrow'),(197,'The Bright Wizard College'),(230,'Khaine\'s Embrace'),(231,'Phoenix Gate'),(232,'Tor Anroc'),(234,'Serpent\'s Passage'),(235,'Dragon\'s Bane'),(236,'Lost Temple of Isha'),(237,'Caledor Woods'),(238,'Blood of the Black Cairn'),(241,'Tomb of the Stars'),(242,'Tomb of the Moon'),(243,'Tomb of the Sky'),(244,'Tomb of the Sun'),(260,'The Lost Vale'),(263,'263'),(264,'264'),(265,'265'),(266,'266'),(410,'ROR | Thanquol\'s Incursion'),(411,'College of Corruption'),(412,'412'),(450,'Unk');
/*!40000 ALTER TABLE `Region` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-26 18:40:09
