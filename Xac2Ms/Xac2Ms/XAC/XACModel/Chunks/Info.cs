﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{
    public enum UnitType : byte
    {
        Inches = 0,
        Feet = 1,
        Yards = 2,
        Miles = 3,
        Millimeters = 4,
        Centimeters = 5,
        Decimeters = 6,
        Meters = 7,
        Kilometers = 8
    };

    public class Info
    {
        public int mMotionExtractionNodeIndex;
        public int mTrajectoryNodeIndex;
        public float mRetargetRootOffset;
        public byte mExporterPluginMajorVersion;
        public byte mExporterPluginMinorVersion;
        public UnitType mUnitType;
        public byte mUnknown1;

        public string mExporterProgramName;
        public string mSourceFilename;
        public string mExporterCompileDate;
        public string mActorName;

        public Info()
        {
            mMotionExtractionNodeIndex = 0;
            mTrajectoryNodeIndex = 0;
            mRetargetRootOffset = 0;
            mExporterPluginMajorVersion = 0;
            mExporterPluginMinorVersion = 0;
            mUnitType = UnitType.Inches;

            mExporterProgramName = string.Empty;
            mSourceFilename = string.Empty;
            mExporterCompileDate = string.Empty;
            mActorName = string.Empty;
        }

        public void ReadIn(BinaryReader iStream, Chunk iChunk)
        {
            mMotionExtractionNodeIndex = iStream.ReadInt32();
            mTrajectoryNodeIndex = iStream.ReadInt32();
            if (iChunk.mVersion == 2)
                mRetargetRootOffset = iStream.ReadUInt32();
            mExporterPluginMajorVersion = iStream.ReadByte();
            mExporterPluginMinorVersion = iStream.ReadByte();
            mUnitType = (UnitType)iStream.ReadByte();
            mUnknown1 = iStream.ReadByte();

            mExporterProgramName = Common.ReadString(iStream);
            mSourceFilename = Common.ReadString(iStream);
            mExporterCompileDate = Common.ReadString(iStream);
            mActorName = Common.ReadString(iStream);
        }

        public void WriteOut(BinaryWriter iStream, Chunk iChunk)
        {
            iStream.Write(mMotionExtractionNodeIndex);
            iStream.Write(mTrajectoryNodeIndex);
            if (iChunk.mVersion == 2)
                iStream.Write(mRetargetRootOffset);
            iStream.Write(mExporterPluginMajorVersion);
            iStream.Write(mExporterPluginMinorVersion);
            iStream.Write((byte)mUnitType);
            iStream.Write(mUnknown1);

            Common.WriteString(iStream, mExporterProgramName);
            Common.WriteString(iStream, mSourceFilename);
            Common.WriteString(iStream, mExporterCompileDate);
            Common.WriteString(iStream, mActorName);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += Marshal.SizeOf(mMotionExtractionNodeIndex);
            vSize += Marshal.SizeOf(mTrajectoryNodeIndex);
            vSize += Marshal.SizeOf(mExporterPluginMajorVersion);
            vSize += Marshal.SizeOf(mExporterPluginMinorVersion);
            vSize += Marshal.SizeOf(mUnitType);
            vSize += Marshal.SizeOf(mUnknown1);

            vSize += sizeof(uint) + mExporterProgramName.Length;
            vSize += sizeof(uint) + mSourceFilename.Length;
            vSize += sizeof(uint) + mExporterCompileDate.Length;
            vSize += sizeof(uint) + mActorName.Length;
            return vSize;
        }
    }
}
