﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class XACFaceGroup
    {
        public List<XACFace> mFacesList;
        public Int64 mVerticesCount;
        public Int64 mBonesCount;

        public XACFaceGroup(List<XACFace> iFacesList, Int64 iFaceGroupVerticesCount, Int64 iBonesCount)
        {
            mFacesList = new List<XACFace>(iFacesList);
            mVerticesCount = iFaceGroupVerticesCount;
            mBonesCount = iBonesCount;
        }

        public override string ToString()
        {
            int vFaceCount = 0;
            if (null != mFacesList)
            {
                vFaceCount = mFacesList.Count;
            }
            string vTheString = "XACFaceGroup Faces:" + vFaceCount.ToString() + " Vertices:" + mVerticesCount.ToString() + " Bones:" + mBonesCount.ToString(); ;
            return vTheString;
        }

    }


}

