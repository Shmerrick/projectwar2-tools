﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{

    public class XACFileHeader
    {
        public uint mMagic;
        public byte mMajorVersion;
        public byte mMinorVersion;
        public byte mBigEndianFlag;
        public byte mMultiplyOrder; // MULORDER_ROT_SCALE_TRANS

        public readonly uint mSize = 0x08;

        public XACFileHeader()
        {
            mMagic = 0;
            mMajorVersion = 0;
            mMinorVersion = 0;
            mBigEndianFlag = 0;
            mMultiplyOrder = 0;
        }

        public void ReadIn(BinaryReader iStream)
        {
            mMagic = iStream.ReadUInt32();
            mMajorVersion = iStream.ReadByte();
            mMinorVersion = iStream.ReadByte();
            mBigEndianFlag = iStream.ReadByte();
            mMultiplyOrder = iStream.ReadByte();
        }

        public void WriteOut(BinaryWriter iStream)
        {
            iStream.Write(mMagic);
            iStream.Write(mMajorVersion);
            iStream.Write(mMinorVersion);
            iStream.Write(mBigEndianFlag);
            iStream.Write(mMultiplyOrder);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += Marshal.SizeOf(mMagic);
            vSize += Marshal.SizeOf(mMajorVersion);
            vSize += Marshal.SizeOf(mMinorVersion);
            vSize += Marshal.SizeOf(mBigEndianFlag);
            vSize += Marshal.SizeOf(mMultiplyOrder);
            return vSize;
        }
    }
}
