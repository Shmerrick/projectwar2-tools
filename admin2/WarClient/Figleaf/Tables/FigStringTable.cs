﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WarClient.Figleaf.Tables
{
    public class FigString : FigRecord
    {
        public Int32 Index { get; }
        public UInt32 Unk1 { get; set; }
        public Byte Type { get; set; }
        public String Value { get; set; }
        public Byte Unk3 { get; set; }

        public override String ToString() => Value;

        public FigString(FigleafDB db, Int32 index) : base(db) => Index = index;
    }

    public class FigStringRef
    {
        private readonly FigleafDB _db;

        public Int32 SourceIndex { get; set; }

        private readonly FigString _ref;
        public FigString Ref {
            get {
                if (_ref == null)
                {
                    FigString _ref = _db.TableStrings1.GetString(SourceIndex);
                    if (_ref == null)
                        return null;
                    return _ref;
                }
                else
                {
                    return _ref;
                }
            }
        }
        public String Value {
            get {
                if (_ref == null)
                {
                    FigString _ref = _db.TableStrings1.GetString(SourceIndex);
                    if (_ref == null)
                        return $"[{SourceIndex}]";
                    return _ref.Value;
                }
                else
                {
                    return _ref.Value;
                }
            }
        }

        public static implicit operator Int32(FigStringRef r) => r.SourceIndex;

        public static implicit operator UInt32(FigStringRef r) => (UInt32)r.SourceIndex;

        public String FigString() => Value;

        public override String ToString() => Value + " [" + SourceIndex + "]";
        public FigStringRef(FigleafDB db, Int32 sourceIndex)
        {
            _db = db;
            SourceIndex = sourceIndex;
        }

        public FigStringRef(FigleafDB db, UInt32 sourceIndex)
        {
            _db = db;
            SourceIndex = (Int32)sourceIndex;

        }
    }
    public class FigStringTable : FigTable<FigString>
    {
        private const Int32 HeaderPosition = 0xC;
        private readonly Dictionary<String, FigString> _hash = new Dictionary<String, FigString>();
        private readonly FigString _invalidString;
        public FigStringTable(FigleafDB db) : base(db) => _invalidString = new FigString(_db, -1);

        public FigString GetString(Int32 index)
        {
            if (index >= 0 && index < Records.Count)
                return Records[index];
            return null;
        }

        public Int32 GetStringIndex(String str)
        {
            if (_hash.ContainsKey(str))
                return _hash[str].Index;
            return -1;
        }

        public Boolean HasString(String value)
        {
            if (_hash.ContainsKey(value))
                return true;
            return false;
        }

        public FigString AddString(String value)
        {
            if (HasString(value))
            {
                _hash[value].Value = value;
                return _hash[value];
            }
            else
            {
                var str = new FigString(_db, Records.Count)
                {
                    Value = value
                };
                _hash[value] = str;
                Records.Add(str);
                return str;
            }
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;
            var str = new Byte[0xFFFF];


            for (var i = 0; i < EntryCount; i++)
            {
                var Unk1 = reader.ReadUInt32();
                var type = reader.ReadByte();
                var offset = reader.BaseStream.Position;

                var size = 0;
                for (size = 0; size < 0xFFFF; size++)
                {
                    var c = reader.ReadByte();
                    if (c == '\0')
                        break;
                    str[size] = c;
                }

                var fs = new FigString(_db, i)
                {
                    Unk1 = Unk1,
                    Type = type,
                    Value = Encoding.GetEncoding(437).GetString(str, 0, size),
                    Unk3 = reader.ReadByte(),
                };
                _hash[fs.Value] = fs;

                Records.Add(fs);
            }

            var b = reader.ReadByte();

        }

        public override void Save(BinaryWriter writer)
        {
            writer.BaseStream.Position = HeaderPosition;
            writer.Write((UInt32)0);
            writer.Write((UInt32)0);
            writer.Write((UInt32)0);

            var pos = writer.BaseStream.Position;
            writer.BaseStream.Position = 0x33c;

            for (var i = 0; i < Records.Count; i++)
            {
                var starPos = writer.BaseStream.Position;
                FigString entry = Records[i];

                writer.Write(entry.Unk1);
                writer.Write(entry.Type);

                if (!String.IsNullOrEmpty(entry.Value))
                    writer.Write(Encoding.GetEncoding(437).GetBytes(entry.Value), 0, entry.Value.Length);

                writer.Write((Byte)0);
                writer.Write(entry.Unk3);
            }

            writer.Write((Byte)0);
            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((UInt32)(Records.Count));
            writer.Write((UInt32)(0x33c));
            writer.Write((UInt32)(endPos - 0x33c));

            writer.BaseStream.Position = endPos;
        }
    }
}
