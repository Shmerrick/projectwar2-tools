﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AssetHashHunter
{
    internal abstract class UnifiedDataAsyncBase : UnifiedDataBase, IUnifiedDataAsync
    {
        public abstract Task ForEachReadAsync(Action<byte[], int, int> action, CancellationToken cancellationToken);

        public abstract Task ForEachGroupAsync(int groupSize, Action<byte[], int, int> action, Action<byte[], int, int> remainderAction, CancellationToken cancellationToken);

        public abstract Task<byte[]> ToArrayAsync(CancellationToken cancellationToken);
    }
}