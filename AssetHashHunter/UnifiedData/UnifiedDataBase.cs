﻿using System;
using System.Threading;

namespace AssetHashHunter
{
    internal abstract class UnifiedDataBase : IUnifiedData
    {
        public abstract long Length { get; }

        public virtual int BufferSize
        {
            get { return _BufferSize; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException(nameof(value), $"{nameof(value)} must be greater than 0");

                _BufferSize = value;
            }
        }

        private int _BufferSize = 4096;

        public abstract void ForEachRead(Action<byte[], int, int> action, CancellationToken cancellationToken);

        public abstract void ForEachGroup(int groupSize, Action<byte[], int, int> action, Action<byte[], int, int> remainderAction, CancellationToken cancellationToken);

        public abstract byte[] ToArray(CancellationToken cancellationToken);
    }
}