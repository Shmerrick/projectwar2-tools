﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AssetHashHunter
{
    public interface IUnifiedDataAsync : IUnifiedData
    {
        Task ForEachReadAsync(Action<byte[], int, int> action, CancellationToken cancellationToken);

        Task ForEachGroupAsync(int groupSize, Action<byte[], int, int> action, Action<byte[], int, int> remainderAction, CancellationToken cancellationToken);

        Task<byte[]> ToArrayAsync(CancellationToken cancellationToken);
    }
}