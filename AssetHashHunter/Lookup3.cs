﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace AssetHashHunter
{
    public class JenkinsLookup3
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public long ComputeInt64HashInternal(ref byte[] data)
        {
            uint a = 0xdeadbeef + (uint)data.Length;
            uint b = a;
            uint c = a;

            var totalBytes = data.Length;
            var remainderBytes = totalBytes % 12;
            var blockBytes = totalBytes - remainderBytes;

            if (blockBytes > 0)
                ProcessGroup(ref a, ref b, ref c, ref data, 0, blockBytes);

            if (remainderBytes > 0)
                ProcessRemainder(ref a, ref b, ref c, ref data, blockBytes, remainderBytes);

            Final(ref a, ref b, ref c);

            return (long)((((ulong)b) << 32) | c);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public HashValue ComputeHashInternal(ref byte[] data)
        {
            uint a = 0xdeadbeef + (uint)data.Length;
            uint b = a;
            uint c = a;

            var totalBytes = data.Length;
            var remainderBytes = totalBytes % 12;
            var blockBytes = totalBytes - remainderBytes;

            if (blockBytes > 0)
                ProcessGroup(ref a, ref b, ref c, ref data, 0, blockBytes);

            if (remainderBytes > 0)
                ProcessRemainder(ref a, ref b, ref c, ref data, blockBytes, remainderBytes);

            Final(ref a, ref b, ref c);

            return new HashValue(b, c);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public async Task<HashValue> ComputeHashInternalAsync(IUnifiedDataAsync data, CancellationToken cancellationToken)
        {
            uint a = 0xdeadbeef + (uint)data.Length;
            uint b = a;
            uint c = a;

            int dataCount = 0;

            await data.ForEachGroupAsync(12, (dataGroup, position, length) =>
            {
                ProcessGroup(ref a, ref b, ref c, ref dataGroup, position, length);
            },
            (remainder, position, length) =>
            {
                ProcessRemainder(ref a, ref b, ref c, ref remainder, position, length);
            },
            cancellationToken).ConfigureAwait(false);

            if (dataCount > 0)
                Final(ref a, ref b, ref c);

            return new HashValue(b, c);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void ProcessGroup(ref uint a, ref uint b, ref uint c, ref byte[] data, int position, int length)
        {
            for (int x = position; x < position + length; x += 12)
            {
                if (x > position)
                    Mix(ref a, ref b, ref c);

                fixed (byte* pData = &data[0])
                {
                    a += *(uint*)(pData + x);
                    b += *(uint*)(pData + x + 4);
                    c += *(uint*)(pData + x + 8);
                }
            }
            Mix(ref a, ref b, ref c);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ProcessRemainder(ref uint a, ref uint b, ref uint c, ref byte[] data, int position, int length)
        {
            switch (length)
            {
                case 11: c += (uint)data[position + 10] << 16; goto case 10;
                case 10: c += (uint)data[position + 9] << 8; goto case 9;
                case 9: c += (uint)data[position + 8]; goto case 8;
                case 8: b += BitConverter.ToUInt32(data, position + 4); goto case 4;
                case 7: b += (uint)data[position + 6] << 16; goto case 6;
                case 6: b += (uint)data[position + 5] << 8; goto case 5;
                case 5: b += (uint)data[position + 4]; goto case 4;
                case 4: a += BitConverter.ToUInt32(data, position); break;
                case 3: a += (uint)data[position + 2] << 16; goto case 2;
                case 2: a += (uint)data[position + 1] << 8; goto case 1;
                case 1: a += (uint)data[position]; break;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Mix(ref uint a, ref uint b, ref uint c)
        {
            a -= c; a ^= RotateLeft(c, 4); c += b;
            b -= a; b ^= RotateLeft(a, 6); a += c;
            c -= b; c ^= RotateLeft(b, 8); b += a;
            a -= c; a ^= RotateLeft(c, 16); c += b;
            b -= a; b ^= RotateLeft(a, 19); a += c;
            c -= b; c ^= RotateLeft(b, 4); b += a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Final(ref uint a, ref uint b, ref uint c)
        {
            c ^= b; c -= RotateLeft(b, 14);
            a ^= c; a -= RotateLeft(c, 11);
            b ^= a; b -= RotateLeft(a, 25);
            c ^= b; c -= RotateLeft(b, 16);
            a ^= c; a -= RotateLeft(c, 4);
            b ^= a; b -= RotateLeft(a, 14);
            c ^= b; c -= RotateLeft(b, 24);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static uint RotateLeft(uint operand, int shiftCount)
        {
            shiftCount &= 0x1f;
            return (operand << shiftCount) | (operand >> (32 - shiftCount));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public long ComputeInt64Hash(byte[] data)
        {
            return ComputeInt64HashInternal(ref data);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public long ComputeInt64Hash(string data)
        {
            byte[] bytes = new byte[data.Length];

            for (int i = 0; i < data.Length; ++i)
                bytes[i] = (byte)data[i];

            return ComputeInt64HashInternal(ref bytes);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public HashValue ComputeHash(byte[] data)
        {
            return ComputeHashInternal(ref data);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public HashValue ComputeHash(string data)
        {
            byte[] bytes = new byte[data.Length];

            for (int i = 0; i < data.Length; ++i)
                bytes[i] = (byte)data[i];

            return ComputeHashInternal(ref bytes);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public async Task<HashValue> ComputeHashAsync(Stream data) => await ComputeHashAsync(data, CancellationToken.None);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public async Task<HashValue> ComputeHashAsync(Stream data, CancellationToken cancellationToken)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            cancellationToken.ThrowIfCancellationRequested();

            return await ComputeHashInternalAsync(new StreamData(data), cancellationToken).ConfigureAwait(false);
        }
    }
}