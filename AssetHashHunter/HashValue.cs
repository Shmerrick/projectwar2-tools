﻿using System;
using System.Collections;
using System.Text;

namespace AssetHashHunter
{
    public sealed class HashValue
    {
        public bool toggle = false;

        public bool UseHash32 { get { return toggle; } set { toggle = true; } }
        public bool UseHash64 { get { return !toggle; } set { toggle = false; } }

        public uint UInt32 { get; set; }
        public ulong UInt64 { get; set; }

        public int Int32 { get { return (int)UInt32; } }
        public long Int64 { get { return (long)UInt64; } }

        private bool _bytes = false;
        private byte[] _hash32;
        private byte[] _hash64;

        public byte[] Hash32 { get { return _bytes ? _hash32 : GetHash32(); } }
        public byte[] Hash64 { get { return _bytes ? _hash64 : GetHash64(); } }

        public byte[] Hash { get { return toggle ? Hash32 : Hash64; } }

        public HashValue(uint b, uint c)
        {
            //UInt32 = c;
            //UInt32 = (UInt32 >> 16) | (UInt32 << 16); // swap adjacent 16-bit blocks
            //UInt32 = ((UInt32 & 0xFF00FF00) >> 8) | ((UInt32 & 0x00FF00FF) << 8); // swap adjacent 8-bit blocks

            UInt64 = (((ulong)b) << 32) | c;
            //UInt64 = (UInt64 >> 32) | (UInt64 << 32); // swap adjacent 32-bit blocks
            //UInt64 = ((UInt64 & 0xFFFF0000FFFF0000) >> 16) | ((UInt64 & 0x0000FFFF0000FFFF) << 16); // swap adjacent 16-bit blocks
            //UInt64 = ((UInt64 & 0xFF00FF00FF00FF00) >> 8) | ((UInt64 & 0x00FF00FF00FF00FF) << 8); // swap adjacent 8-bit blocks
        }

        private byte[] GetHash32()
        {
            _hash32 = BitConverter.GetBytes(UInt32);
            _bytes = true;
            return _hash32;
        }

        private byte[] GetHash64()
        {
            _hash64 = BitConverter.GetBytes(UInt64);
            _bytes = true;
            return _hash64;
        }

        public string ToHexString() => ToHexString(false);

        public string ToHexString(bool uppercase)
        {
            var stringBuilder = new StringBuilder(Hash.Length);
            var formatString = uppercase ? "X2" : "x2";

            foreach (var byteValue in Hash)
                stringBuilder.Append(byteValue.ToString(formatString));

            return stringBuilder.ToString();
        }

        public string ToString(bool uppercase)
        {
            var stringBuilder = new StringBuilder(Hash.Length);
            stringBuilder.Append("0x");
            stringBuilder.Append(ToHexString(uppercase));
            return stringBuilder.ToString();
        }

        public int ToInt32()
        {
            return BitConverter.ToInt32(Hash);
        }

        public uint ToUInt32()
        {
            return BitConverter.ToUInt32(Hash);
        }

        public long ToInt64()
        {
            return BitConverter.ToInt64(Hash);
        }

        public ulong ToUInt64()
        {
            return BitConverter.ToUInt64(Hash);
        }

        public float ToSingle()
        {
            return BitConverter.ToSingle(Hash);
        }

        public double ToDouble()
        {
            return BitConverter.ToDouble(Hash);
        }

        public string ToBase64String()
        {
            return Convert.ToBase64String(Hash);
        }

        public char[] ToBase64CharArray()
        {
            char[] chars = new char[Hash.Length];
            Convert.ToBase64CharArray(Hash, 0, Hash.Length, chars, chars.Length, Base64FormattingOptions.None);
            return chars;
        }

        public BitArray ToBitArray()
        {
            return new BitArray(Hash);
        }

        public char[] ToCharArray()
        {
            char[] newHashArray = new char[Hash.Length];
            Array.Copy(Hash, newHashArray, Hash.Length);
            return newHashArray;
        }
    }
}