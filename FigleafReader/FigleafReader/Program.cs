﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf;
using WarFigleaf.Structs;
using WarFigleaf.Tables;

namespace FigleafReader
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Fig Leaf
            */

            WarFigleaf.FigleafDB db = new WarFigleaf.FigleafDB();
            db.Load(File.ReadAllBytes(@"C:\Users\Admin\Desktop\newzone\dev\assetdb\figleaf.db"));

            foreach (var table in db.Tables.Values)
            {
                Console.WriteLine($"[{table.Type}] records:{table.Records.Count}");
            }

            /*
                MySQL Handler
            */

            MySQLHandler myHandler = new MySQLHandler("localhost", "root", "password", "figleaf");
            myHandler.Connect();

            /*
                Strings1
            */
            myHandler.CreateTable(MySQLHandler.SelectedTable.Strings1);
            myHandler.BeginTransaction();
            foreach (var row in db.TableStrings1.Strings)
                myHandler.InsertRow(row);
            myHandler.CommitTransaction();
            Console.WriteLine($"Strings1 rows inserted:{db.TableStrings1.Strings.Count}");

            /*
                Strings2
            */
            myHandler.CreateTable(MySQLHandler.SelectedTable.Strings2);
            myHandler.BeginTransaction();
            foreach (var row in db.TableStrings2.Strings)
                myHandler.InsertRow(row);
            myHandler.CommitTransaction();
            Console.WriteLine($"Strings2 rows inserted:{db.TableStrings2.Strings.Count}");

            /*
                CharacterArt
            */
            myHandler.CreateTable(MySQLHandler.SelectedTable.CharacterArt);
            myHandler.BeginTransaction();
            foreach (var row in db.TableCharacterArt.CharacterArt)
                myHandler.InsertRow(row);
            myHandler.CommitTransaction();
            Console.WriteLine($"CharacterArt rows inserted:{db.TableCharacterArt.CharacterArt.Count}");

            /*
                CharacterArtData
            */
            myHandler.CreateTable(MySQLHandler.SelectedTable.CharacterArtData);
            myHandler.BeginTransaction();
            uint CharacterArtDataIndex = 0;
            foreach (var row in db.TableCharacterArt.Parts)
                foreach (var data in row.Value)
                    myHandler.InsertRow(CharacterArtDataIndex++, row.Key, data);
            myHandler.CommitTransaction();
            Console.WriteLine($"CharacterArtData rows inserted:{db.TableCharacterArt.Parts.Count}");

            /*
                FigureParts
            */
            myHandler.CreateTable(MySQLHandler.SelectedTable.FigureParts);
            myHandler.BeginTransaction();
            foreach (var row in db.TableFigureParts.Figures)
                myHandler.InsertRow(row);
            myHandler.CommitTransaction();
            Console.WriteLine($"FigureParts rows inserted:{db.TableFigureParts.Figures.Count}");

            /*
                ArtSwitches
            */
            myHandler.CreateTable(MySQLHandler.SelectedTable.ArtSwitches);
            myHandler.BeginTransaction();
            foreach (var row in db.TableArtSwitches.Switches)
                myHandler.InsertRow(row);
            myHandler.CommitTransaction();
            Console.WriteLine($"ArtSwitches rows inserted:{db.TableArtSwitches.Switches.Count()}");

            /*
                CharacterDecals
            */

            /*
                CharacterMeshes
            */

            /*
                Textures
            */

            /*
                Unk8
            */



        }
    }
}
