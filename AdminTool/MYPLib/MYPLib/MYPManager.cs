﻿// Decompiled with JetBrains decompiler
// Type: MypLib.MYPManager
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using MYPLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MypLib
{
    public class MYPManager
    {
        private Dictionary<Language, Dictionary<string, StringTable>> _strings = new Dictionary<Language, Dictionary<string, StringTable>>();
        public Dictionary<string, MYPManager.Folder> Root = new Dictionary<string, MYPManager.Folder>();
        public Dictionary<string, MYPManager.Folder> Folders = new Dictionary<string, MYPManager.Folder>();
        public Dictionary<MythicPackage, MYP> Packages = new Dictionary<MythicPackage, MYP>();
        public Dictionary<MythicPackage, MYPManager.Folder> PackageFolders = new Dictionary<MythicPackage, MYPManager.Folder>();
        private string _mypFolder;
        public static MYPManager Region;
        private ILogger Log;
        public static Dictionary<string, ulong> DeHashes;
        public static Dictionary<ulong, string> DeHashes3;

        public string MYPFolder
        {
            get
            {
                return this._mypFolder;
            }
        }

        public event MYPManager.MYPChangeDelegate OnMYPUpdated;

        public event MYPManager.MYPChangeDelegate OnMYPSaved;

        public void RaiseUpdate(MYP myp)
        {
            if (this.OnMYPUpdated == null)
                return;
            this.OnMYPUpdated(this, myp);
        }

        public void Close()
        {
            foreach (MYP myp in this.Packages.Values)
                myp.Dispose();
            List<string> list = MYPManager.DeHashes.Keys.ToList<string>().OrderBy<string, string>((Func<string, string>)(e => e)).ToList<string>();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string str in list)
                stringBuilder.AppendLine(str);
            File.WriteAllText("dehash.txt", stringBuilder.ToString());
        }

        public static void LoadDehashes(string file)
        {
            MYPManager.DeHashes = new Dictionary<string, ulong>();
            MYPManager.DeHashes3 = new Dictionary<ulong, string>();
            foreach (string readAllLine in File.ReadAllLines(file))
            {
                ulong index = MYP.HashWAR(readAllLine);
                MYPManager.DeHashes[readAllLine] = index;
                MYPManager.DeHashes3[index] = readAllLine;
            }
        }

        public string GetStringByKey(
          Language language,
          string tableName,
          string key,
          MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string key1 = tableName.Replace(string.Format("data/strings/{0}/", (object)language), "");
            string str = "";
            if (!this._strings.ContainsKey(language))
                this._strings[language] = new Dictionary<string, StringTable>();
            if (!this._strings[language].ContainsKey(key1))
            {
                MYP.AssetInfo asset = this.FindAsset("data/strings/" + (object)language + "/" + key1, defaulPackage, true);
                if (asset != null)
                {
                    this._strings[language][key1] = new StringTable();
                    this._strings[language][key1].Load(this.GetAsset(asset));
                }
            }
            if (this._strings[language].ContainsKey(key1))
                return this._strings[language][key1].GetKeyedValue(key);
            return str;
        }

        public string GetString(
          Language language,
          string tableName,
          int row,
          MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string key = tableName.Replace(string.Format("data/strings/{0}/", (object)language), "");
            string str = "";
            if (!this._strings.ContainsKey(language))
                this._strings[language] = new Dictionary<string, StringTable>();
            if (!this._strings[language].ContainsKey(key))
            {
                MYP.AssetInfo asset = this.FindAsset("data/strings/" + (object)language + "/" + key, defaulPackage, true);
                if (asset != null)
                {
                    this._strings[language][key] = new StringTable();
                    this._strings[language][key].Load(this.GetAsset(asset));
                }
            }
            if (this._strings[language].ContainsKey(key))
                return this._strings[language][key].GetValue(row);
            return str;
        }

        public string GetString(Language language, string tableName)
        {
            string index = tableName.Replace(string.Format("data/strings/{0}/", (object)language), "");
            if (!this._strings.ContainsKey(language))
                this._strings[language] = new Dictionary<string, StringTable>();
            if (!this._strings[language].ContainsKey(index))
                this.GetStringTable(language, index, MythicPackage.DEV);
            if (this._strings[language].ContainsKey(index))
                return this._strings[language][index].ToDocument();
            return "";
        }

        public byte[] GetStringUnicode(Language language, string tableName)
        {
            string index = tableName.Replace(string.Format("data/strings/{0}/", (object)language), "");
            if (!this._strings.ContainsKey(language))
                this._strings[language] = new Dictionary<string, StringTable>();
            if (!this._strings[language].ContainsKey(index))
                this.GetStringTable(language, index, MythicPackage.DEV);
            if (this._strings[language].ContainsKey(index))
                return Encoding.Unicode.GetBytes(this._strings[language][index].ToDocument());
            return Encoding.Unicode.GetBytes("");
        }

        public List<string> GetStringTable(
          Language language,
          string tableName,
          MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string key = tableName.Replace(string.Format("data/strings/{0}/", (object)language), "");
            if (!this._strings.ContainsKey(language))
                this._strings[language] = new Dictionary<string, StringTable>();
            if (!this._strings[language].ContainsKey(key))
            {
                MYP.AssetInfo asset = this.FindAsset(string.Format("data/strings/{0}/{1}", (object)language, (object)key), defaulPackage, true);
                if (asset != null)
                {
                    this._strings[language][key] = new StringTable();
                    this._strings[language][key].Load(this.GetAsset(asset));
                }
            }
            if (this._strings[language].ContainsKey(key))
                return this._strings[language][key].List;
            return new List<string>();
        }

        public void SetString(
          Language language,
          string tableName,
          int label,
          string value,
          MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string index = tableName.Replace(string.Format("data/strings/{0}/", (object)language), "");
            if (!this._strings.ContainsKey(language))
                this._strings[language] = new Dictionary<string, StringTable>();
            if (!this._strings[language].ContainsKey(index))
            {
                MYP.AssetInfo asset = this.FindAsset(index, defaulPackage, true);
                if (asset != null)
                {
                    this._strings[language][index] = new StringTable();
                    this._strings[language][index].Load(this.GetAsset(asset));
                }
            }
            if (!this._strings[language].ContainsKey(index))
                return;
            this._strings[language][index].SetValue(label, value);
        }

        public string GetStringFormat(Language language, string label, params object[] vals)
        {
            string str = this.GetStringByKey(language, "/default.txt", label, MythicPackage.DEV);
            for (int index = 0; index < vals.Length; ++index)
                str = str.Replace("<<" + (index + 1).ToString() + ">>", vals[index].ToString());
            return str.Replace("<BR>", "\r\n");
        }

        public bool HasDehash(string path)
        {
            ulong key1 = MYP.HashWAR(path.ToLower());
            foreach (string key2 in MYPManager.DeHashes.Keys)
            {
                if (MYPManager.DeHashes3.ContainsKey(key1))
                    return true;
            }
            return false;
        }

        public static ulong Hash(string path)
        {
            MYPManager.DeHashes[path] = MYP.HashWAR(path.ToLower());
            MYPManager.DeHashes3[MYP.HashWAR(path.ToLower())] = path;
            return MYPManager.DeHashes[path];
        }

        public string Name { get; set; }

        public MYPManager(ILogger logger, string mypFolder, string db = null, params MythicPackage[] include)
        {
            this.Log = logger;
            this.Log.SetContext(nameof(MYPManager));
            this._mypFolder = mypFolder;
            foreach (MythicPackage package in Enum.GetValues(typeof(MythicPackage)))
            {
                if (include != null && (uint)include.Length > 0U)
                {
                    if (((IEnumerable<MythicPackage>)include).Contains<MythicPackage>(package))
                    {
                        string str = Path.Combine(mypFolder, package.ToString() + ".myp");
                        if (File.Exists(str))
                        {
                            try
                            {
                                this.Log.Append(string.Format("Loading {0}", (object)str));
                                this.Packages[package] = new MYP(logger, package, str, false)
                                {
                                    Manager = this
                                };
                            }
                            catch (Exception ex)
                            {
                                this.Log.Error(ex.ToString());
                            }
                        }
                    }
                }
                else
                {
                    string str = Path.Combine(mypFolder, package.ToString() + ".myp");
                    if (File.Exists(str))
                    {
                        try
                        {
                            this.Log.Append(string.Format("Loading {0}", (object)str));
                            this.Packages[package] = new MYP(logger, package, str, false)
                            {
                                Manager = this
                            };
                        }
                        catch (Exception ex)
                        {
                            this.Log.Error(ex.ToString());
                        }
                    }
                }
            }
            if (db == null)
                return;
            if (MYPManager.DeHashes == null)
                MYPManager.LoadDehashes(db);
            foreach (MYP myp in this.Packages.Values)
            {
                foreach (MYP.AssetInfo entry in myp.Assets.Values)
                {
                    if (MYPManager.DeHashes3.ContainsKey(entry.Hash))
                        entry.Name = MYPManager.DeHashes3[entry.Hash];
                    this.AddToFolder(entry.Package, entry);
                }
            }
        }

        public bool HasAsset(string assetName)
        {
            ulong key = MYP.HashWAR(assetName);
            foreach (MYP myp in this.Packages.Values)
            {
                if (myp.Assets.ContainsKey(key))
                    return true;
            }
            return false;
        }

        public string GetAssetStringASCII(string assetName, MythicPackage? archive = null)
        {
            ulong key = MYP.HashWAR(assetName);
            if (!archive.HasValue)
            {
                foreach (MYP myp in this.Packages.Values)
                {
                    if (myp.Assets.ContainsKey(key))
                        return Encoding.ASCII.GetString(this.GetAsset(myp.Assets[key]));
                }
            }
            else if (this.Packages.ContainsKey(archive.Value) && this.Packages[archive.Value].Assets.ContainsKey(key))
                return Encoding.ASCII.GetString(this.GetAsset(this.Packages[archive.Value].Assets[key]));
            return "";
        }

        public string GetAssetStringUnicode(string assetName)
        {
            ulong key = MYP.HashWAR(assetName);
            foreach (MYP myp in this.Packages.Values)
            {
                if (myp.Assets.ContainsKey(key))
                    return Encoding.Unicode.GetString(this.GetAsset(myp.Assets[key]));
            }
            return "";
        }

        public byte[] GetAsset(string assetName)
        {
            return this.GetAsset(assetName, new MythicPackage?());
        }

        public byte[] GetAsset(MYP.AssetInfo asset)
        {
            return this.Packages[asset.Package].GetAssetData(asset.Hash);
        }

        public byte[] GetAsset(string assetName, MythicPackage? package)
        {
            ulong key = MYP.HashWAR(assetName);
            foreach (MYP myp in this.Packages.Values)
            {
                if ((!package.HasValue || package.Value == myp.Package) && myp.Assets.ContainsKey(key))
                    return this.GetAsset(myp.Assets[key]);
            }
            return (byte[])null;
        }

        public byte[] GetAsset2(string assetName, MythicPackage? package = null)
        {
            ulong key = MYP.HashWAR(assetName);
            foreach (MYP myp in this.Packages.Values)
            {
                if ((!package.HasValue || package.Value == myp.Package) && myp.Assets.ContainsKey(key))
                    return this.GetAsset(myp.Assets[key]);
            }
            return new byte[0];
        }

        public byte[] GetAsset(ulong hash, MythicPackage? package = null)
        {
            foreach (MYP myp in this.Packages.Values)
            {
                if ((!package.HasValue || package.Value == myp.Package) && myp.Assets.ContainsKey(hash))
                    return this.GetAsset(myp.Assets[hash]);
            }
            return (byte[])null;
        }

        public Stream GetAssetStream(string assetName)
        {
            ulong key = MYP.HashWAR(assetName);
            foreach (MYP myp in this.Packages.Values)
            {
                if (myp.Assets.ContainsKey(key))
                    return (Stream)new MemoryStream(this.GetAsset(myp.Assets[key]));
            }
            return (Stream)null;
        }

        private void AddToFolder(MythicPackage package, MYP.AssetInfo entry)
        {
            string lower = package.ToString().ToLower();
            int num = entry.Name == "shaders/blobshadowshader.psh" ? 1 : 0;
            if (entry.Name != null)
            {
                MYPManager.Folder folder = this.GetFolder(package, Path.GetDirectoryName(entry.Name));
                folder.Archive = this.Packages[entry.Package];
                if (!folder.FilesByPackage.ContainsKey(entry.Package))
                    folder.FilesByPackage[entry.Package] = new Dictionary<ulong, List<MYP.AssetInfo>>();
                if (!folder.Files.ContainsKey(entry.Hash))
                    folder.Files[entry.Hash] = new List<MYP.AssetInfo>();
                if (!folder.FilesByPackage[entry.Package].ContainsKey(entry.Hash))
                    folder.FilesByPackage[entry.Package][entry.Hash] = new List<MYP.AssetInfo>();
                folder.Files[entry.Hash].Add(entry);
                folder.FilesByPackage[entry.Package][entry.Hash].Add(entry);
            }
            else
            {
                if (this.Folders.ContainsKey(lower))
                    return;
                this.Folders[lower] = new MYPManager.Folder(this)
                {
                    Package = package,
                    Archive = this.Packages[entry.Package]
                };
            }
        }

        private MYPManager.Folder GetFolder(MythicPackage package, string path)
        {
            if (this.Folders.ContainsKey(path))
                return this.Folders[path];
            string[] strArray = path.Split(new char[1] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
            string str = "";
            MYPManager.Folder folder1 = (MYPManager.Folder)null;
            int num = 0;
            foreach (string key1 in strArray)
            {
                string key2 = str + key1;
                MYPManager.Folder folder2;
                if (!this.Folders.ContainsKey(key2))
                {
                    folder2 = new MYPManager.Folder(this)
                    {
                        Package = package,
                        Path = key2
                    };
                    this.Folders[key2] = folder2;
                }
                else
                    folder2 = this.Folders[key2];
                if (folder1 != null)
                {
                    folder1.Folders[folder2.Path] = folder2;
                    folder2.Parent = folder1;
                }
                str = key2 + "\\";
                folder1 = folder2;
                if (num == 0 && !this.Root.ContainsKey(key1))
                {
                    this.Root[key1] = folder2;
                    folder2.Path = key1;
                }
                ++num;
            }
            return this.Folders[path];
        }

        public void Save()
        {
            foreach (MYP myp1 in this.Packages.Values.ToList<MYP>())
            {
                if (myp1.Changed)
                {
                    this.Log.Append(string.Format("Saving archive '{0}'", (object)myp1.Filename));
                    myp1.Save();
                    if (this.OnMYPSaved != null)
                        this.OnMYPSaved(this, myp1);
                    myp1.Dispose();
                    MYP myp2 = new MYP(this.Log, myp1.Package, myp1.Filename, false);
                    this.Packages[myp2.Package] = myp2;
                    foreach (MYP.AssetInfo assetInfo in myp2.Assets.Values)
                    {
                        if (myp1.Assets.ContainsKey(assetInfo.Hash))
                            assetInfo.Name = myp1.Assets[assetInfo.Hash].Name;
                    }
                }
            }
        }

        public bool Changed
        {
            get
            {
                foreach (MYP myp in this.Packages.Values)
                {
                    if (myp.Changed)
                        return true;
                }
                return false;
            }
        }

        public MYP ValidateCreateMyp(MythicPackage package)
        {
            if (!this.Packages.ContainsKey(package))
            {
                MYP myp = new MYP(this.Log, package, Path.Combine(this._mypFolder, package.ToString().ToLower() + ".myp"), true);
                this.Packages[package] = myp;
                myp.Manager = this;
            }
            return this.Packages[package];
        }

        public void UpdateAsset(MythicPackage package, ulong hash, byte[] data, bool compressed)
        {
            this.ValidateCreateMyp(package);
            this.Packages[package].UpdateAsset(hash, data, compressed, true);
            if (this.OnMYPUpdated == null)
                return;
            this.OnMYPUpdated(this, this.Packages[package]);
        }

        public void UpdateAssets(
          MythicPackage package,
          List<Tuple<MYPManager, MYP.AssetInfo>> assets,
          MYPManager.AssetUpdated callback = null)
        {
            this.ValidateCreateMyp(package);
            int index = 0;
            foreach (Tuple<MYPManager, MYP.AssetInfo> asset in assets)
            {
                this.Packages[package].UpdateAsset(asset.Item2.Hash, asset.Item1.GetAsset(asset.Item2.Hash, new MythicPackage?(asset.Item2.Package)), asset.Item2.Compressed, false);
                if (callback != null)
                    callback(this, asset.Item2, index, assets.Count);
                ++index;
            }
        }

        public void UpdateAsset(MythicPackage package, string path, byte[] data, bool compress)
        {
            ulong hash = MYP.HashWAR(path);
            this.UpdateAsset(package, hash, data, compress);
        }

        public void UpdateAsset(MYP.AssetInfo asset, byte[] data)
        {
            this.UpdateAsset(asset.Package, asset.Hash, data, asset.Compressed);
        }

        public MYP.AssetInfo FindAsset(ulong hash, MythicPackage defaulPackage = MythicPackage.NONE)
        {
            if (defaulPackage != MythicPackage.NONE && this.Packages.ContainsKey(defaulPackage) && this.Packages[defaulPackage].Assets.ContainsKey(hash))
                return this.Packages[defaulPackage].Assets[hash];
            foreach (MythicPackage key in this.Packages.Keys)
            {
                if (key != MythicPackage.WARTEST && this.Packages[key].Assets.ContainsKey(hash))
                    return this.Packages[key].Assets[hash];
            }
            return (MYP.AssetInfo)null;
        }

        public MYP.AssetInfo FindAsset(string path, MythicPackage defaulPackage = MythicPackage.NONE, bool allMyps = true)
        {
            ulong key1 = MYP.HashWAR(path);
            if (defaulPackage != MythicPackage.NONE && this.Packages.ContainsKey(defaulPackage) && this.Packages[defaulPackage].Assets.ContainsKey(key1))
                return this.Packages[defaulPackage].Assets[key1];
            if (!allMyps)
                return (MYP.AssetInfo)null;
            foreach (MythicPackage key2 in this.Packages.Keys)
            {
                if (key2 != MythicPackage.WARTEST && this.Packages[key2].Assets.ContainsKey(key1))
                    return this.Packages[key2].Assets[key1];
            }
            return (MYP.AssetInfo)null;
        }

        public class Folder
        {
            public Dictionary<string, MYPManager.Folder> Folders = new Dictionary<string, MYPManager.Folder>();
            public Dictionary<ulong, List<MYP.AssetInfo>> Files = new Dictionary<ulong, List<MYP.AssetInfo>>();
            public Dictionary<MythicPackage, Dictionary<ulong, List<MYP.AssetInfo>>> FilesByPackage = new Dictionary<MythicPackage, Dictionary<ulong, List<MYP.AssetInfo>>>();

            public MythicPackage Package { get; internal set; }

            public string Name
            {
                get
                {
                    if (this.Path == null)
                        return this.Package.ToString().ToLower();
                    return this.Path;
                }
            }

            public MYP Archive { get; internal set; }

            public MYPManager.Folder Parent { get; internal set; }

            public MYPManager Manager { get; internal set; }

            public string Path { get; internal set; }

            public override string ToString()
            {
                return this.Name;
            }

            public Folder(MYPManager manager)
            {
                this.Manager = manager;
            }

            public List<MYP.AssetInfo> GetAssets(MythicPackage packageFilter = MythicPackage.NONE)
            {
                List<MYP.AssetInfo> assets = new List<MYP.AssetInfo>();
                this.GetAssetsRecursive(assets, packageFilter);
                return assets;
            }

            public List<MythicPackage> GetPackages()
            {
                List<MYP.AssetInfo> assetInfoList = new List<MYP.AssetInfo>();
                this.GetAssetsRecursive(assetInfoList, MythicPackage.NONE);
                return assetInfoList.GroupBy<MYP.AssetInfo, MythicPackage>((Func<MYP.AssetInfo, MythicPackage>)(e => e.Package)).Select<IGrouping<MythicPackage, MYP.AssetInfo>, MythicPackage>((Func<IGrouping<MythicPackage, MYP.AssetInfo>, MythicPackage>)(e => e.Key)).ToList<MythicPackage>();
            }

            private void GetAssetsRecursive(List<MYP.AssetInfo> assets, MythicPackage packageFilter = MythicPackage.NONE)
            {
                assets.AddRange(this.Files.Values.SelectMany<List<MYP.AssetInfo>, MYP.AssetInfo>((Func<List<MYP.AssetInfo>, IEnumerable<MYP.AssetInfo>>)(e => (IEnumerable<MYP.AssetInfo>)e)).Where<MYP.AssetInfo>((Func<MYP.AssetInfo, bool>)(e =>
             {
                 if ((uint)packageFilter > 0U)
                     return e.Package == packageFilter;
                 return true;
             })));
                foreach (MYPManager.Folder folder in this.Folders.Values)
                    folder.GetAssetsRecursive(assets, MythicPackage.NONE);
            }
        }

        public delegate void MYPChangeDelegate(MYPManager manager, MYP myp);

        public delegate void AssetUpdated(
          MYPManager manager,
          MYP.AssetInfo asset,
          int index,
          int total);
    }
}