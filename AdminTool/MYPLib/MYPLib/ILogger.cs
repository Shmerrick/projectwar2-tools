﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.ILogger
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;

namespace MYPLib
{
    public interface ILogger
    {
        void SetContext(string name);

        void Append(string msg);

        void Error(string msg);

        void Exception(string msg, Exception e);
    }
}