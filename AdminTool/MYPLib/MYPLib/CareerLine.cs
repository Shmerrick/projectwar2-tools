﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.CareerLine
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MYPLib
{
    public enum CareerLine
    {
        IRON_BREAKER = 1,
        SLAYER = 2,
        RUNE_PRIEST = 3,
        ENGINEER = 4,
        BLACK_ORC = 5,
        CHOPPA = 6,
        SHAMAN = 7,
        SQUIG_HERDER = 8,
        WITCH_HUNTER = 9,
        KNIGHT_OF_THE_BLAZING_SUN = 10, // 0x0000000A
        BRIGHT_WIZARD = 11, // 0x0000000B
        WARRIOR_PRIEST = 12, // 0x0000000C
        CHOSEN = 13, // 0x0000000D
        MARAUDER = 14, // 0x0000000E
        ZEALOT = 15, // 0x0000000F
        MAGUS = 16, // 0x00000010
        SWORD_MASTER = 17, // 0x00000011
        SHADOW_WARRIOR = 18, // 0x00000012
        WHITE_LION = 19, // 0x00000013
        ARCHMAGE = 20, // 0x00000014
        BLACK_GUARD = 21, // 0x00000015
        WITCH_ELF = 22, // 0x00000016
        DISCIPLE_OF_KHAINE = 23, // 0x00000017
        SORCERESS = 24, // 0x00000018
    }
}