﻿// Decompiled with JetBrains decompiler
// Type: MypLib.OcclusionResult
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MypLib
{
    public enum OcclusionResult
    {
        NotLoaded = -1,
        NotOccluded = 0,
        OccludedByGeometry = 1,
        OccludedByTerrain = 2,
        OccludedByWater = 3,
        OccludedByLava = 4,
        OccludedByDynamicObject = 5,
    }
}