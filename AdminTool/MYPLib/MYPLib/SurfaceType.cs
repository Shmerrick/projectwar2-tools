﻿// Decompiled with JetBrains decompiler
// Type: MypLib.SurfaceType
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MypLib
{
    public enum SurfaceType
    {
        SOLID,
        DOOR1,
        DOOR2,
        DOOR3,
        DOOR4,
        DOOR5,
        DOOR6,
        DOOR7,
        DOOR8,
        DOOR9,
        WATER_GENERIC,
        WATER_RIVER,
        WATER_HOTSPRING,
        WATER_OCEAN,
        WATER_DIRTY,
        WATER_STREAM,
        WATER_TAINTED,
        WATER_BOG,
        WATER_ICY,
        WATER_POISON,
        WATER_LAKE,
        WATER_MARSH,
        WATER_MUCK,
        LAVA,
        LAVA_MAGMA,
        TAR,
        INSTANT_DEATH,
        FIXTURE,
        TERRAIN,
        JUMP1,
        JUMP2,
        JUMP3,
        JUMP4,
        JUMP5,
        JUMP6,
        JUMP7,
    }
}