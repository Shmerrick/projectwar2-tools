﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.PacketUtil
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MYPLib
{
    public class PacketUtil
    {
        public static uint ConvertToUInt32(byte v1, byte v2, byte v3, byte v4)
        {
            return (uint)((int)v1 << 24 | (int)v2 << 16 | (int)v3 << 8) | (uint)v4;
        }

        public static int IndexOfSequence(byte[] buff, int offset, params byte[] sequence)
        {
            int num = -1;
            for (int index1 = offset; index1 < buff.Length - sequence.Length; ++index1)
            {
                bool flag = true;
                num = index1;
                for (int index2 = 0; index2 < sequence.Length; ++index2)
                {
                    if ((int)buff[index1 + index2] == (int)sequence[index2])
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    num = index1;
                    break;
                }
            }
            return num;
        }

        public static uint GetUint32(Stream stream)
        {
            int num1 = (int)(byte)stream.ReadByte();
            byte num2 = (byte)stream.ReadByte();
            byte num3 = (byte)stream.ReadByte();
            byte num4 = (byte)stream.ReadByte();
            int num5 = (int)num2;
            int num6 = (int)num3;
            int num7 = (int)num4;
            return PacketUtil.ConvertToUInt32((byte)num1, (byte)num5, (byte)num6, (byte)num7);
        }

        public static float GetFloat(Stream stream)
        {
            byte[] buffer = new byte[4];
            stream.Read(buffer, 0, 4);
            return BitConverter.ToSingle(buffer, 0);
        }

        public static ulong GetUint64(Stream stream)
        {
            return (ulong)((PacketUtil.GetUint32(stream) << 24) + PacketUtil.GetUint32(stream));
        }

        public static byte GetUint8(Stream stream)
        {
            return (byte)stream.ReadByte();
        }

        public static ushort GetUint16R(Stream stream)
        {
            byte uint8 = PacketUtil.GetUint8(stream);
            return PacketUtil.ConvertToUInt16(PacketUtil.GetUint8(stream), uint8);
        }

        public static short GetInt16R(Stream stream)
        {
            return PacketUtil.ConvertToInt16(PacketUtil.GetUint8(stream), PacketUtil.GetUint8(stream));
        }

        public static short ConvertToInt16(byte v1, byte v2)
        {
            return (short)((int)v1 | (int)v2 << 8);
        }

        public static void WriteUInt16R(Stream stream, ushort val)
        {
            PacketUtil.WriteByte(stream, (byte)((uint)val & (uint)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)((uint)val >> 8));
        }

        public static uint GetUint32R(Stream stream)
        {
            byte uint8_1 = PacketUtil.GetUint8(stream);
            byte uint8_2 = PacketUtil.GetUint8(stream);
            byte uint8_3 = PacketUtil.GetUint8(stream);
            return PacketUtil.ConvertToUInt32(PacketUtil.GetUint8(stream), uint8_3, uint8_2, uint8_1);
        }

        private static string ConvertToString(byte[] cstyle)
        {
            if (cstyle == null)
                return (string)null;
            for (int count = 0; count < cstyle.Length; ++count)
            {
                if (cstyle[count] == (byte)0)
                    return Encoding.GetEncoding("iso-8859-1").GetString(cstyle, 0, count);
            }
            return Encoding.GetEncoding("iso-8859-1").GetString(cstyle);
        }

        public static string GetString(Stream stream)
        {
            int uint32 = (int)PacketUtil.GetUint32(stream);
            byte[] numArray = new byte[uint32];
            stream.Read(numArray, 0, uint32);
            return PacketUtil.ConvertToString(numArray);
        }

        public static string GetPascalString(Stream stream)
        {
            byte uint8 = PacketUtil.GetUint8(stream);
            byte[] numArray = new byte[(int)uint8];
            stream.Read(numArray, 0, (int)uint8);
            return PacketUtil.ConvertToString(numArray);
        }

        public static string GetCString(Stream stream)
        {
            string str = "";
            while (stream.Position < stream.Length)
            {
                byte num = (byte)stream.ReadByte();
                if (num > (byte)0)
                    str += ((char)num).ToString();
                else
                    break;
            }
            return str;
        }

        public static void WritePascalString(Stream stream, string str)
        {
            if (str == null || str.Length <= 0)
            {
                PacketUtil.WriteByte(stream, (byte)0);
            }
            else
            {
                byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(str);
                PacketUtil.WriteByte(stream, (byte)bytes.Length);
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        public static void WritePascalString32(Stream stream, string str)
        {
            if (str == null || str.Length <= 0)
            {
                PacketUtil.WriteUInt32R(stream, 0U);
            }
            else
            {
                byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(str);
                PacketUtil.WriteUInt32R(stream, (uint)bytes.Length);
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        public static void WriteCString(Stream stream, string str)
        {
            if (str == null || str.Length <= 0)
            {
                PacketUtil.WriteByte(stream, (byte)0);
            }
            else
            {
                byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(str);
                stream.Write(bytes, 0, bytes.Length);
                PacketUtil.WriteByte(stream, (byte)0);
            }
        }

        public static string GetString(Stream stream, int maxlen)
        {
            byte[] numArray = new byte[maxlen];
            stream.Read(numArray, 0, maxlen);
            return PacketUtil.ConvertToString(numArray);
        }

        public static byte[] GetByteArray(Stream stream, int size)
        {
            byte[] buffer = new byte[size];
            stream.Read(buffer, 0, size);
            return buffer;
        }

        public static int DecodeMythicSize(MemoryStream stream)
        {
            int num1 = 0;
            int num2 = 0;
            int num3 = stream.ReadByte();
            while ((num3 & 128) == 128)
            {
                int num4 = num3 ^ 128;
                num1 |= num4 << 7 * num2;
                if (stream.Length == (long)stream.Capacity)
                    return 0;
                num3 = stream.ReadByte();
                ++num2;
            }
            return num1 | num3 << 7 * num2;
        }

        public static ushort ConvertToUInt16(byte v1, byte v2)
        {
            return (ushort)((uint)v2 | (uint)v1 << 8);
        }

        public static ushort GetUint16(Stream stream)
        {
            return PacketUtil.ConvertToUInt16(PacketUtil.GetUint8(stream), PacketUtil.GetUint8(stream));
        }

        public static void WriteHexStringBytes(Stream stream, string hexString)
        {
            int num = hexString.Length / 2;
            if (hexString.Length % 2 == 0)
            {
                for (int index = 0; index < num; ++index)
                    PacketUtil.WriteByte(stream, Convert.ToByte(hexString.Substring(index * 2, 2), 16));
            }
            else
                PacketUtil.WriteByte(stream, (byte)0);
        }

        public static void WritePacketString(Stream stream, string packet)
        {
            packet = packet.Replace(" ", string.Empty);
            using (StringReader stringReader = new StringReader(packet))
            {
                string str;
                while ((str = stringReader.ReadLine()) != null)
                    PacketUtil.WriteHexStringBytes(stream, str.Substring(1, str.IndexOf("|", 2) - 1));
            }
        }

        public static string Bin(byte[] dump, int start, int len)
        {
            string str1 = "";
            for (int index = start; index < dump.Length; ++index)
            {
                string str2 = Convert.ToString(dump[index], 2).PadLeft(8, '0');
                str1 += str2;
            }
            return str1;
        }

        public static string Hex(byte[] dump, int start, int len, int? current = null)
        {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.AppendLine("|------------------------------------------------|----------------|");
            stringBuilder1.AppendLine("|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|");
            stringBuilder1.AppendLine("|------------------------------------------------|----------------|");
            try
            {
                int num1 = start + len;
                for (int index1 = start; index1 < num1; index1 += 16)
                {
                    StringBuilder stringBuilder2 = new StringBuilder();
                    StringBuilder stringBuilder3 = new StringBuilder();
                    for (int index2 = 0; index2 < 16; ++index2)
                    {
                        if (index2 + index1 < num1)
                        {
                            byte num2 = dump[index2 + index1];
                            if (current.HasValue && current.Value == index2 + index1)
                                stringBuilder3.Append("[" + dump[index2 + index1].ToString("X2") + "]");
                            else
                                stringBuilder3.Append(dump[index2 + index1].ToString("X2"));
                            stringBuilder3.Append(" ");
                            if (num2 >= (byte)32 && num2 < (byte)127)
                            {
                                if (current.HasValue && current.Value == index2 + index1)
                                    stringBuilder2.Append("[" + ((char)num2).ToString() + "]");
                                else
                                    stringBuilder2.Append((char)num2);
                            }
                            else if (current.HasValue && current.Value == index2 + index1)
                                stringBuilder2.Append("[.]");
                            else
                                stringBuilder2.Append(".");
                        }
                    }
                    stringBuilder1.AppendLine("|" + stringBuilder3.ToString().PadRight(48) + "|" + stringBuilder2.ToString().PadRight(16) + "|");
                }
            }
            catch (Exception ex)
            {
            }
            stringBuilder1.Append("-------------------------------------------------------------------");
            return stringBuilder1.ToString();
        }

        public static void Skip(Stream stream, long num)
        {
            stream.Seek(num, SeekOrigin.Current);
        }

        public static void WriteBool(Stream stream, bool data)
        {
            stream.WriteByte(data ? (byte)1 : (byte)0);
        }

        public static void WriteInt16R(Stream stream, short val)
        {
            PacketUtil.WriteByte(stream, (byte)((uint)val & (uint)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)((uint)val >> 8));
        }

        public static void WriteByte(Stream stream, byte data)
        {
            stream.WriteByte(data);
        }

        public static void WriteParam(Stream stream, byte paramIndex, int paramValue)
        {
            PacketUtil.WriteByte(stream, paramIndex);
            foreach (byte data in PacketUtil.Pack(new List<int>()
      {
        paramValue
      }))
                PacketUtil.WriteByte(stream, data);
        }

        public static void WriteBytes(Stream stream, byte[] data)
        {
            if (data == null)
                return;
            stream.Write(data, 0, data.Length);
        }

        public static void WriteString(Stream stream, string str)
        {
            PacketUtil.WriteUInt32(stream, (uint)str.Length);
            PacketUtil.WriteStringBytes(stream, str);
        }

        public static int GetTimeStamp()
        {
            return (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static void WriteString(Stream stream, string str, int maxlen)
        {
            if (str.Length <= 0)
                return;
            byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(str);
            stream.Write(bytes, 0, bytes.Length < maxlen ? bytes.Length : maxlen);
        }

        public static void WriteStringBytes(Stream stream, string str)
        {
            if (str.Length <= 0)
                return;
            byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(str);
            stream.Write(bytes, 0, bytes.Length);
        }

        public static void WriteUInt32(Stream stream, uint val)
        {
            PacketUtil.WriteByte(stream, (byte)(val >> 24));
            PacketUtil.WriteByte(stream, (byte)(val >> 16 & (uint)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)((val & (uint)ushort.MaxValue) >> 8));
            PacketUtil.WriteByte(stream, (byte)((int)val & (int)ushort.MaxValue & (int)byte.MaxValue));
        }

        public static void WriteUInt64(Stream stream, ulong val)
        {
            PacketUtil.WriteByte(stream, (byte)(val >> 56));
            PacketUtil.WriteByte(stream, (byte)(val >> 48 & (ulong)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)(val >> 40 & (ulong)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)(val >> 32 & (ulong)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)(val >> 24 & (ulong)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)(val >> 16 & (ulong)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)(val >> 8 & (ulong)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)(val & (ulong)byte.MaxValue));
        }

        public static void WriteUInt32R(Stream stream, uint val)
        {
            PacketUtil.WriteByte(stream, (byte)((int)val & (int)ushort.MaxValue & (int)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)((val & (uint)ushort.MaxValue) >> 8));
            PacketUtil.WriteByte(stream, (byte)(val >> 16 & (uint)byte.MaxValue));
            PacketUtil.WriteByte(stream, (byte)(val >> 24));
        }

        public static void WriteUInt16(Stream stream, ushort val)
        {
            PacketUtil.WriteByte(stream, (byte)((uint)val >> 8));
            PacketUtil.WriteByte(stream, (byte)((uint)val & (uint)byte.MaxValue));
        }

        public static void Fill(Stream stream, byte val, int num)
        {
            for (int index = 0; index < num; ++index)
                PacketUtil.WriteByte(stream, val);
        }

        public static List<int> Unpack(Stream stream, int count)
        {
            if (stream.Position == stream.Length)
                return new List<int>() { 0 };
            List<int> intList = new List<int>();
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            bool flag1 = false;
            bool flag2 = true;
            while (intList.Count < count && stream.Position < stream.Length)
            {
                byte uint8 = PacketUtil.GetUint8(stream);
                int num4 = ((int)uint8 & 128) >> 7 == 1 ? 1 : 0;
                int num5 = (!flag2 ? (int)uint8 & (int)sbyte.MaxValue : ((int)uint8 & (int)sbyte.MaxValue) >> 1) << num3;
                if (flag2)
                {
                    num3 += 6;
                    flag1 = ((int)uint8 & 1) == 1;
                    flag2 = false;
                }
                else
                    num3 += 7;
                int num6 = num2;
                num2 = num5 | num6;
                if (num4 == 0)
                {
                    if (flag1)
                    {
                        if (num2 == 0)
                            num2 = -1;
                        else
                            ++num2;
                    }
                    else if (num2 > 1)
                        num2 = -num2;
                    intList.Add(num2);
                    num2 = 0;
                    flag1 = false;
                    num3 = 0;
                    flag2 = true;
                }
                ++num1;
            }
            if ((uint)intList.Count > 0U)
                return intList;
            return new List<int>() { 0 };
        }

        public static string GetPascalString32(Stream stream)
        {
            int uint32R = (int)PacketUtil.GetUint32R(stream);
            if (uint32R <= 0 || uint32R >= 100)
                return "";
            byte[] numArray = new byte[uint32R];
            stream.Read(numArray, 0, uint32R);
            return PacketUtil.ConvertToString(numArray);
        }

        public static int ReadVarInt(MemoryStream stream)
        {
            uint num = PacketUtil.ReadVarUInt((Stream)stream);
            if ((num & 1U) > 0U)
                return ~(int)(num >> 1);
            return (int)(num >> 1);
        }

        public static uint ReadVarUInt(Stream stream)
        {
            int num1 = 0;
            for (int index = 0; index < 5 && stream.Position < stream.Length; ++index)
            {
                int num2 = stream.ReadByte();
                num1 |= (num2 & (int)sbyte.MaxValue) << index * 7;
                if ((num2 & 128) == 0)
                    break;
            }
            return (uint)num1;
        }

        public static void WriteVarInt32(MemoryStream stream, int value)
        {
            if (value < 0)
                value = -value << 1 | 1;
            else
                value <<= 1;
            PacketUtil.WriteVarUInt32(stream, (uint)value);
        }

        public static void WriteVarUInt32(MemoryStream stream, uint val)
        {
            if (val == 0U)
            {
                stream.WriteByte((byte)0);
            }
            else
            {
                for (; val > 0U; val >>= 7)
                    stream.WriteByte((byte)((long)(val & (uint)sbyte.MaxValue) ^ (val > (uint)sbyte.MaxValue ? 128L : 0L)));
            }
        }

        public static byte[] ToVarUInt32(uint val)
        {
            if (val == 0U)
                return new byte[0];
            int length = 0;
            int index = 0;
            while (val > 0U)
            {
                val >>= 7;
                ++length;
            }
            byte[] numArray = new byte[length];
            while (val > 0U)
            {
                numArray[index] = (byte)((long)(val & (uint)sbyte.MaxValue) ^ (val > (uint)sbyte.MaxValue ? 128L : 0L));
                val >>= 7;
                ++index;
            }
            return numArray;
        }

        public static void WriteZigZag(MemoryStream stream, int val)
        {
            byte num = val < 0 ? (byte)1 : (byte)0;
            if (num == (byte)1)
                ++val;
            val = Math.Abs(val);
            stream.WriteByte((byte)(val << 1 & (int)sbyte.MaxValue ^ (val > 63 ? 128 : 0) + (int)num));
            for (val >>= 6; val > 0; val >>= 7)
                stream.WriteByte((byte)(val & (int)sbyte.MaxValue ^ (val > (int)sbyte.MaxValue ? 128 : 0)));
        }

        public static List<uint> UnpackUnsigned(Stream stream, int count)
        {
            if (stream.Position == stream.Length)
                return new List<uint>() { 0U };
            List<uint> uintList = new List<uint>();
            uint num1 = 0;
            uint num2 = 0;
            uint num3 = 0;
            bool flag1 = false;
            bool flag2 = true;
            while (uintList.Count < count && stream.Position < stream.Length)
            {
                byte uint8 = PacketUtil.GetUint8(stream);
                int num4 = ((int)uint8 & 128) >> 7 == 1 ? 1 : 0;
                int num5 = (!flag2 ? (int)uint8 & (int)sbyte.MaxValue : ((int)uint8 & (int)sbyte.MaxValue) >> 1) << (int)num3;
                if (flag2)
                {
                    num3 += 6U;
                    flag1 = ((int)uint8 & 1) == 1;
                    flag2 = false;
                }
                else
                    num3 += 7U;
                int num6 = (int)num2;
                num2 = (uint)(num5 | num6);
                if (num4 == 0)
                {
                    if (flag1)
                    {
                        if (num2 == 0U)
                            num2 = 0U;
                        else
                            ++num2;
                    }
                    uintList.Add(num2);
                    num2 = 0U;
                    flag1 = false;
                    num3 = 0U;
                    flag2 = true;
                }
                ++num1;
            }
            return uintList;
        }

        public static List<byte> Pack(List<int> values)
        {
            List<byte> byteList = new List<byte>();
            foreach (int num1 in values)
            {
                switch (num1)
                {
                    case -1:
                        byteList.Add((byte)1);
                        continue;
                    case 0:
                        byteList.Add((byte)0);
                        continue;
                    case 1:
                        byteList.Add((byte)2);
                        continue;
                    default:
                        int num2 = num1;
                        if (num1 != 1)
                        {
                            if (num1 > 0)
                                --num2;
                            else
                                num2 = -num2;
                        }
                        int num3 = PacketUtil.BitCount(num2);
                        int num4 = 0;
                        int num5 = 0;
                        while (num3 > 0)
                        {
                            if (num5 == 0)
                                num3 -= 6;
                            else
                                num3 -= 7;
                            ++num4;
                        }
                        int a = 0;
                        int num6 = 6;
                        for (int index = 0; index < num4; ++index)
                        {
                            int num7 = (PacketUtil.CreateMask(a, a + num6) & num2) >> a;
                            if (index == 0)
                            {
                                int num8 = num7 << 1;
                                num7 = num1 <= 0 ? num8 & -2 : num8 | 1;
                            }
                            if (index != num4 - 1)
                                num7 |= 128;
                            byteList.Add((byte)num7);
                            a += num6;
                            if (index == 0)
                                num6 = 7;
                        }
                        continue;
                }
            }
            return byteList;
        }

        private static int BitCount(int value)
        {
            int num1 = 0;
            int num2 = value;
            while (num2 > 0)
            {
                num2 /= 2;
                ++num1;
            }
            return num1;
        }

        private static int CreateMask(int a, int b)
        {
            int num = 0;
            for (int index = a; index <= b; ++index)
                num |= 1 << index;
            return num;
        }

        public static void FillString(Stream stream, string str, int len)
        {
            long position = stream.Position;
            PacketUtil.Fill(stream, (byte)0, len);
            if (str == null)
                return;
            stream.Position = position;
            if (str.Length <= 0)
            {
                stream.Position = position + (long)len;
            }
            else
            {
                byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(str);
                stream.Write(bytes, 0, len > bytes.Length ? bytes.Length : len);
                stream.Position = position + (long)len;
            }
        }
    }
}