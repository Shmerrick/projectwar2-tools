﻿// Decompiled with JetBrains decompiler
// Type: MypLib.MythicPackage
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MypLib
{
    public enum MythicPackage
    {
        NONE,
        MFT,
        ART,
        ART2,
        ART3,
        AUDIO,
        DATA,
        WORLD,
        INTERFACE,
        VIDEO,
        BLOODHUNT,
        PATCH,
        VO_ENGLISH,
        VO_FRENCH,
        VIDEO_FRENCH,
        VO_GERMAN,
        VIDEO_GERMAN,
        VO_ITALIAN,
        VIDEO_ITALIAN,
        VO_SPANISH,
        VIDEO_SPANISH,
        VO_KOREAN,
        VIDEO_KOREAN,
        VO_CHINESE,
        VIDEO_CHINESE,
        VO_JAPANESE,
        VIDEO_JAPANESE,
        VO_RUSSIAN,
        VIDEO_RUSSIAN,
        WARTEST,
        DEV,
    }
}