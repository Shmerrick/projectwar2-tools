﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.AbilityComponentExport
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using MypLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MYPLib
{
    public class AbilityComponentExport
    {
        public List<AbilityComponent> Components = new List<AbilityComponent>();
        public Dictionary<ushort, AbilityComponent> CompHash = new Dictionary<ushort, AbilityComponent>();
        public uint Header;
        public uint Size;

        public void Load(MYPManager manager, Stream stream)
        {
            this.Header = PacketUtil.GetUint32R(stream);
            this.Size = PacketUtil.GetUint32R(stream);
            while (stream.Position < stream.Length)
            {
                AbilityComponent abilityComponent = new AbilityComponent(stream);
                this.Components.Add(abilityComponent);
                abilityComponent.Description = manager.GetString(Language.english, "componenteffects.txt", (int)abilityComponent.A11_ComponentID, MythicPackage.DEV);
                abilityComponent.BonusType = abilityComponent.A09_Operation != ComponentOP.BONUS_TYPE ? (abilityComponent.A09_Operation != ComponentOP.STAT_CHANGE ? BonusType.NONE : (BonusType)abilityComponent.A07) : (BonusType)abilityComponent.Values[0];
                if (abilityComponent.A09_Operation == ComponentOP.LINKED_ABILITY)
                    abilityComponent.LinkedAbility = manager.GetString(Language.english, "abilitynames.txt", (int)(ushort)abilityComponent.Values[0], MythicPackage.DEV).Replace("^n", "") + " (" + (object)abilityComponent.Values[0] + ")";
            }
            foreach (AbilityComponent component in this.Components)
                this.CompHash[component.A11_ComponentID] = component;
        }

        public AbilityComponent NewComponent(
          MYPManager manager,
          MythicPackage package,
          AbilityBin ability,
          AbilityComponent cloneFrom = null)
        {
            if (ability.Components.Count >= 10)
                return (AbilityComponent)null;
            int num = 1;
            if (this.Components.Count > 0)
                num = (int)this.Components.Select<AbilityComponent, ushort>((Func<AbilityComponent, ushort>)(e => e.A11_ComponentID)).Max<ushort>() + 1;
            AbilityComponent abilityComponent = new AbilityComponent((ushort)num, cloneFrom);
            abilityComponent.Index = (byte)this.Components.Count;
            this.Components.Add(abilityComponent);
            ability.Components.Add(abilityComponent);
            if (cloneFrom != null)
            {
                foreach (Language language in Enum.GetValues(typeof(Language)))
                {
                    string str1 = "data/strings/" + (object)language + "/componenteffects.txt";
                    string str2 = manager.GetString(language, str1, (int)cloneFrom.A11_ComponentID, package);
                    if (str2 != null && str2.Length > 0)
                    {
                        manager.SetString(language, str1, (int)abilityComponent.A11_ComponentID, str2, package);
                        string s = manager.GetString(language, str1);
                        manager.UpdateAsset(package, str1, Encoding.Unicode.GetBytes(s), false);
                    }
                }
                abilityComponent.Description = manager.GetString(Language.english, "data/strings/english/componenteffects.txt", (int)abilityComponent.A11_ComponentID, package);
            }
            for (int index = 0; index < 10; ++index)
            {
                if (ability.ComponentIDs[index] == (ushort)0)
                {
                    ability.ComponentIDs[index] = abilityComponent.A11_ComponentID;
                    break;
                }
            }
            this.CompHash[abilityComponent.A11_ComponentID] = abilityComponent;
            return abilityComponent;
        }

        public void Save(Stream stream)
        {
            PacketUtil.WriteUInt32R(stream, 12U);
            PacketUtil.WriteUInt32R(stream, (uint)this.Components.Count);
            for (int index = 0; index < this.Components.Count; ++index)
                this.Components[index].Save(stream);
        }
    }
}