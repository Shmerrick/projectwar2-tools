﻿// Decompiled with JetBrains decompiler
// Type: MypLib.Fixture
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;
using System.Reflection;

namespace MypLib
{
    public class Fixture
    {
        public string TextualName = "";
        public string Variation = "";
        public int ID;
        public int NifID;
        public float X;
        public float Y;
        public float Z;
        public int A;
        public float Scale;
        public int Collide;
        public float CollideRadius;
        public int Animate;
        public int OnGround;
        public int Flip;
        public int Cave;
        public int UniqueID;
        public float Angle;
        public float AxisX;
        public float AxisY;
        public float AxisZ;
        public int SecondaryTint;
        public int FarDraw;
        public int Interior;
        public int KeepStatus;
        public int Mount;
        public int TooSteep;

        public string FileName { get; set; }

        public ZoneNif Nif { get; set; }

        public Fixture()
        {
        }

        public Fixture(string[] data)
        {
            FieldInfo[] fields = this.GetType().GetFields();
            for (int index = 0; index < fields.Length && index < data.Length; ++index)
            {
                FieldInfo fieldInfo = fields[index];
                if (fieldInfo.FieldType == typeof(int))
                {
                    int result = 0;
                    int.TryParse(data[index], out result);
                    fieldInfo.SetValue((object)this, (object)result);
                }
                if (fieldInfo.FieldType == typeof(float))
                {
                    float result = 0.0f;
                    float.TryParse(data[index], out result);
                    fieldInfo.SetValue((object)this, (object)result);
                }
                else if (fieldInfo.FieldType == typeof(string))
                {
                    string str = data[index];
                    fieldInfo.SetValue((object)this, (object)str);
                }
            }
        }

        public static float ToRadians(float degrees)
        {
            return (float)((double)degrees * Math.PI / 180.0);
        }

        public static float ToDegrees(float radians)
        {
            return (float)((double)radians * 180.0 / Math.PI);
        }

        public static float Clamp(float value, float low, float high)
        {
            if ((double)value < (double)low)
                return low;
            if ((double)value > (double)high)
                return high;
            return value;
        }

        public override string ToString()
        {
            return this.TextualName;
        }
    }
}