﻿// Decompiled with JetBrains decompiler
// Type: MypLib.CSVGrid
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MypLib
{
    public class CSVGrid : UserControl
    {
        private CSV _csv;
        private int _skip;
        private Dictionary<int, ListViewItem> _lviCache;
        private bool _disableUpdates;
        private int _currentRow;
        private int _currentCol;
        private bool _changed;
        private MYP.AssetInfo _asset;
        private readonly TextBox txt;
        private string _csvOrig;
        private IContainer components;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private ListView listView1;
        private RichTextBox txtRaw;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem addRowToolStripMenuItem;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private ToolStripMenuItem saveToolStripMenuItem;

        public event CSVGrid.AssetDelegate OnSave;

        public bool Modified
        {
            get
            {
                return this._csvOrig != this._csv.ToText();
            }
        }

        public CSVGrid()
        {
            TextBox textBox = new TextBox();
            textBox.BorderStyle = BorderStyle.FixedSingle;
            textBox.Visible = false;
            this.txt = textBox;
            this._csvOrig = "";
            this.InitializeComponent();
            this.listView1.Controls.Add((Control)this.txt);
            this.listView1.FullRowSelect = true;
            this.txt.Leave += new EventHandler(this.txt_Leave);
        }

        private void txt_Leave(object sender, EventArgs e)
        {
            this.txt.Visible = false;
            this._csv.RowIndex = this._currentRow;
            this._csv.WriteCol(this._currentCol, this.txt.Text);
            this._disableUpdates = true;
            this.txtRaw.Text = this._csv.ToText();
            this.CheckChanged();
            if (this._lviCache.ContainsKey(this._currentRow))
                this._lviCache[this._currentRow].SubItems[this._currentCol].Text = this.txt.Text;
            this._disableUpdates = false;
        }

        private void CheckChanged()
        {
            this._changed = this._csvOrig != this._csv.ToText();
            if (!this._changed || this.OnChanged == null)
                return;
            this.OnChanged();
        }

        public event CSVGrid.ColumnInfoDelegate OnGetColumnWidth;

        public void LoadCSV(byte[] data)
        {
            this.listView1.VirtualMode = true;
            this._csv = new CSV(Encoding.ASCII.GetString(data), (string)null, false, -1);
            this._csvOrig = this._csv.ToText();
            this.LoadCSV(this._csv);
        }

        public CSV CSV
        {
            get
            {
                this.txtRaw_Leave((object)null, (EventArgs)null);
                return this._csv;
            }
        }

        public void LoadCSV(string text)
        {
            this._csv = new CSV(text, (string)null, false, -1);
            this.LoadCSV(this._csv);
        }

        public List<int> GetColWidths()
        {
            List<int> intList = new List<int>();
            for (int index = 0; index < this.listView1.Columns.Count; ++index)
                intList.Add(this.listView1.Columns[index].Width);
            return intList;
        }

        public void LoadCSV(CSV csv)
        {
            this.listView1.Items.Clear();
            this.listView1.Columns.Clear();
            this._lviCache = new Dictionary<int, ListViewItem>();
            this._csv = csv;
            this._disableUpdates = true;
            if (csv.Lines.Count == 0)
            {
                this.listView1.VirtualListSize = 0;
            }
            else
            {
                Dictionary<string, int> dictionary = new Dictionary<string, int>();
                int colIndex = 0;
                foreach (string key in this._csv.Row)
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = key;
                    if (!dictionary.ContainsKey(key))
                    {
                        dictionary[key] = 1;
                    }
                    else
                    {
                        dictionary[key]++;
                        string str = key + (object)dictionary[key];
                    }
                    int width = columnHeader.Width;
                    if (this.OnGetColumnWidth != null)
                        this.OnGetColumnWidth(this, colIndex, out width);
                    ++colIndex;
                    if ((uint)width > 0U)
                        columnHeader.Width = width;
                    this.listView1.Columns.Add(columnHeader);
                }
                this._csv.NextRow();
                this.listView1.VirtualMode = true;
                this.listView1.VirtualListSize = this._csv.Lines.Count;
                this.txtRaw.Text = this._csv.ToText();
                this._disableUpdates = false;
                this._changed = false;
            }
        }

        private void listView1_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            if (this._disableUpdates || this._csv == null)
                return;
            if (!this._lviCache.ContainsKey(e.ItemIndex + this._skip))
                this.listView1_CacheVirtualItems((object)null, new CacheVirtualItemsEventArgs(e.ItemIndex, e.ItemIndex));
            e.Item = this._lviCache[e.ItemIndex];
        }

        private void listView1_CacheVirtualItems(object sender, CacheVirtualItemsEventArgs e)
        {
            if (this._csv == null)
                return;
            for (int startIndex = e.StartIndex; startIndex <= e.EndIndex; ++startIndex)
            {
                this._csv.RowIndex = startIndex;
                if (!this._lviCache.ContainsKey(this._csv.RowIndex))
                {
                    List<string> row = this._csv.Row;
                    ListViewItem listViewItem = new ListViewItem();
                    listViewItem.Tag = (object)startIndex;
                    if (row.Count > 0)
                        listViewItem.Text = row[0];
                    for (int index = 1; index < this.listView1.Columns.Count; ++index)
                    {
                        if (index < row.Count)
                            listViewItem.SubItems.Add(row[index]);
                        else
                            listViewItem.SubItems.Add("");
                    }
                    if (startIndex % 2 == 0)
                        listViewItem.BackColor = Color.FromArgb((int)byte.MaxValue, 240, 240, 240);
                    this._lviCache[startIndex] = listViewItem;
                }
            }
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo listViewHitTestInfo = this.listView1.HitTest(e.Location);
            Rectangle bounds1 = listViewHitTestInfo.SubItem.Bounds;
            Rectangle bounds2 = listViewHitTestInfo.Item.GetBounds(ItemBoundsPortion.Label);
            this._currentRow = (int)listViewHitTestInfo.Item.Tag;
            this._currentCol = listViewHitTestInfo.Item.SubItems.IndexOf(listViewHitTestInfo.SubItem);
            int num = bounds2.Left - 1;
            this.txt.Bounds = new Rectangle(bounds1.Left + num, bounds1.Top, bounds1.Width - num - 1, bounds1.Height);
            this.txt.Text = listViewHitTestInfo.SubItem.Text;
            this.txt.SelectAll();
            this.txt.Visible = true;
            this.txt.Focus();
        }

        private void txtRaw_TextChanged(object sender, EventArgs e)
        {
            if (this._disableUpdates)
                return;
            this.CheckChanged();
        }

        public event CSVGrid.ChangeDelegate OnChanged;

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._disableUpdates = true;
            List<int> rows = new List<int>();
            foreach (int selectedIndex in this.listView1.SelectedIndices)
                rows.Add(selectedIndex);
            this._currentRow = 0;
            this._csv.Remove(rows);
            this.LoadCSV(this._csv.ToText());
            this._disableUpdates = false;
        }

        private void txtRaw_Leave(object sender, EventArgs e)
        {
            if (!this._changed)
                return;
            this._csv = new CSV(this.txtRaw.Text, (string)null, false, -1);
            this.LoadCSV(this.txtRaw.Text);
        }

        private void addRowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._currentRow = this.listView1.SelectedIndices.Count <= 0 ? this._csv.Lines.Count - 1 : this.listView1.Items[this.listView1.SelectedIndices[0]].Index;
            this._csv.RowIndex = this._currentRow;
            this._csv.NewRow();
            this.LoadCSV(this._csv);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listView1.SelectedIndices.Count > 0)
                this._currentRow = this.listView1.Items[this.listView1.SelectedIndices[0]].Index;
            this._csv.RowIndex = this._currentRow;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            this.tabControl1 = new TabControl();
            this.tabPage1 = new TabPage();
            this.listView1 = new ListView();
            this.tabPage2 = new TabPage();
            this.txtRaw = new RichTextBox();
            this.contextMenuStrip1 = new ContextMenuStrip(this.components);
            this.addRowToolStripMenuItem = new ToolStripMenuItem();
            this.deleteToolStripMenuItem = new ToolStripMenuItem();
            this.saveToolStripMenuItem = new ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            this.tabControl1.Controls.Add((Control)this.tabPage1);
            this.tabControl1.Controls.Add((Control)this.tabPage2);
            this.tabControl1.Dock = DockStyle.Fill;
            this.tabControl1.Location = new Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new Size(791, 622);
            this.tabControl1.TabIndex = 0;
            this.tabPage1.Controls.Add((Control)this.listView1);
            this.tabPage1.Location = new Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new Padding(3);
            this.tabPage1.Size = new Size(783, 596);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Table";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.listView1.Dock = DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new Point(3, 3);
            this.listView1.Name = "listView1";
            this.listView1.Size = new Size(777, 590);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = View.Details;
            this.listView1.VirtualListSize = 1000;
            this.listView1.CacheVirtualItems += new CacheVirtualItemsEventHandler(this.listView1_CacheVirtualItems);
            this.listView1.RetrieveVirtualItem += new RetrieveVirtualItemEventHandler(this.listView1_RetrieveVirtualItem);
            this.listView1.SelectedIndexChanged += new EventHandler(this.listView1_SelectedIndexChanged);
            this.listView1.MouseDoubleClick += new MouseEventHandler(this.listView1_MouseDoubleClick);
            this.tabPage2.Controls.Add((Control)this.txtRaw);
            this.tabPage2.Location = new Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new Padding(3);
            this.tabPage2.Size = new Size(783, 596);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Raw Text";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.txtRaw.Dock = DockStyle.Fill;
            this.txtRaw.Font = new Font("Consolas", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            this.txtRaw.Location = new Point(3, 3);
            this.txtRaw.Name = "txtRaw";
            this.txtRaw.Size = new Size(777, 590);
            this.txtRaw.TabIndex = 0;
            this.txtRaw.Text = "";
            this.txtRaw.WordWrap = false;
            this.txtRaw.TextChanged += new EventHandler(this.txtRaw_TextChanged);
            this.txtRaw.Leave += new EventHandler(this.txtRaw_Leave);
            this.contextMenuStrip1.Items.AddRange(new ToolStripItem[2]
            {
        (ToolStripItem) this.addRowToolStripMenuItem,
        (ToolStripItem) this.deleteToolStripMenuItem
            });
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new Size(153, 92);
            this.addRowToolStripMenuItem.Name = "addRowToolStripMenuItem";
            this.addRowToolStripMenuItem.Size = new Size(152, 22);
            this.addRowToolStripMenuItem.Text = "Add Row";
            this.addRowToolStripMenuItem.Click += new EventHandler(this.addRowToolStripMenuItem_Click);
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new Size(152, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new EventHandler(this.deleteToolStripMenuItem_Click);
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new EventHandler(this.saveToolStripMenuItem_Click);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add((Control)this.tabControl1);
            this.Name = "CSVViewer";
            this.Size = new Size(791, 622);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        public delegate void AssetDelegate(MYP.AssetInfo asset, byte[] data);

        public delegate void ColumnInfoDelegate(CSVGrid grid, int colIndex, out int width);

        public delegate void ChangeDelegate();
    }
}