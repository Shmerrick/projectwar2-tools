﻿// Decompiled with JetBrains decompiler
// Type: MypLib.MythicLoginConfigWriter
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace MypLib
{
    public class MythicLoginConfigWriter
    {
        public int ProductId { get; set; }

        public int MessageTimeoutSecs { get; set; }

        public List<MythicLoginConfigWriter.Region> Regions { get; set; }

        public byte[] Serialize()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (XmlTextWriter xmlTextWriter = new XmlTextWriter((TextWriter)new StreamWriter((Stream)memoryStream)))
                {
                    xmlTextWriter.Formatting = Formatting.Indented;
                    xmlTextWriter.WriteStartDocument(true);
                    xmlTextWriter.WriteStartElement("RootElementOfAnyName");
                    xmlTextWriter.WriteStartElement("MythLoginServiceConfig");
                    xmlTextWriter.WriteStartElement("Settings");
                    xmlTextWriter.WriteElementString("ProductId", this.ProductId.ToString());
                    xmlTextWriter.WriteElementString("MessageTimeoutSecs", this.MessageTimeoutSecs.ToString());
                    xmlTextWriter.WriteEndElement();
                    xmlTextWriter.WriteStartElement("RegionList");
                    foreach (MythicLoginConfigWriter.Region region in this.Regions)
                        region.Serialize((XmlWriter)xmlTextWriter);
                    xmlTextWriter.WriteEndElement();
                    xmlTextWriter.WriteEndElement();
                    xmlTextWriter.WriteEndElement();
                    xmlTextWriter.WriteEndDocument();
                    xmlTextWriter.Flush();
                    xmlTextWriter.WriteString("\r\n");
                }
                return memoryStream.ToArray();
            }
        }

        public class Server
        {
            public string Name { get; set; }

            public string Address { get; set; }

            public int Port { get; set; }

            public void Serialize(XmlWriter writer, string serverType)
            {
                writer.WriteStartElement(serverType);
                writer.WriteAttributeString("serverName", this.Name);
                writer.WriteElementString("Address", this.Address.ToString());
                writer.WriteElementString("Port", this.Port.ToString());
                writer.WriteEndElement();
            }
        }

        public class Region
        {
            public string Name { get; set; }

            public MythicLoginConfigWriter.Server PingServer { get; set; }

            public List<MythicLoginConfigWriter.Server> LoginServers { get; set; }

            public void Serialize(XmlWriter writer)
            {
                writer.WriteStartElement(nameof(Region));
                writer.WriteAttributeString("regionName", this.Name);
                this.PingServer.Serialize(writer, "PingServer");
                writer.WriteStartElement("LoginServerList");
                foreach (MythicLoginConfigWriter.Server loginServer in this.LoginServers)
                    loginServer.Serialize(writer, "LoginServer");
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }
    }
}