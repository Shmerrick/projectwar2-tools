﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.Config
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MYPLib
{
    public class Config
    {
        private string _configFile = "";
        private Dictionary<string, List<Config.KeyValue>> _settings = new Dictionary<string, List<Config.KeyValue>>();
        private Dictionary<string, List<Config.KeyValue>> _sections = new Dictionary<string, List<Config.KeyValue>>();
        private List<Config.KeyValue> _lines = new List<Config.KeyValue>();

        public List<Config.KeyValue> GetSectionValues(string section)
        {
            if (this._sections.ContainsKey(section))
                return this._sections[section].Where<Config.KeyValue>((Func<Config.KeyValue, bool>)(e => e.IsKeyVal)).ToList<Config.KeyValue>();
            return new List<Config.KeyValue>();
        }

        public List<Config.KeyValue> GetSectionValue(string section, string key)
        {
            if (this._sections.ContainsKey(section))
                return this._sections[section].Where<Config.KeyValue>((Func<Config.KeyValue, bool>)(e =>
               {
                   if (e.IsKeyVal)
                       return e.Key == key;
                   return false;
               })).ToList<Config.KeyValue>();
            else
            {
                Config.KeyValue keyValue = new Config.KeyValue()
                {
                    Key = key,
                    Value = "",
                    IsKeyVal = true
                };
                if (!this._sections.ContainsKey(section))
                    this._sections[section] = new List<Config.KeyValue>();
                this._sections[section].Add(keyValue);
            }

            return new List<Config.KeyValue>();
        }

        public void DeleteKey(string key, string section = "")
        {
            if (!this._settings.ContainsKey(key))
                return;
            foreach (Config.KeyValue keyValue in this._settings[key].ToList<Config.KeyValue>())
            {
                this._sections[section].Remove(keyValue);
                keyValue.Delete = true;
                this._settings[key].Remove(keyValue);
                if (this._settings[key].Count == 0)
                    this._settings.Remove(key);
            }
        }

        public object this[string key, string section = ""]
        {
            get
            {
                if (this._settings.ContainsKey(key))
                    return (object)this._settings[key].First<Config.KeyValue>().Value;
                return (object)null;
            }
            set
            {
                if (this._settings.ContainsKey(key))
                {
                    this._settings[key].First<Config.KeyValue>().Value = value.ToString();
                }
                else
                {
                    Config.KeyValue keyValue = new Config.KeyValue()
                    {
                        Key = key,
                        Value = value.ToString(),
                        IsKeyVal = true
                    };
                    if (!this._settings.ContainsKey(key))
                        this._settings[key] = new List<Config.KeyValue>();
                    this._settings[key].Add(keyValue);
                    this._lines.Add(keyValue);
                    if (!this._sections.ContainsKey(section))
                        this._sections[section] = new List<Config.KeyValue>();
                    this._sections[section].Add(keyValue);
                }
            }
        }

        public Config(string configFile)
        {
            this._configFile = configFile;
            this.LoadLocalSettings();
        }

        public void LoadLocalSettings()
        {
            string index = "";
            this._sections[index] = new List<Config.KeyValue>();
            if (!File.Exists(this._configFile))
                return;
            foreach (string readAllLine in File.ReadAllLines(this._configFile))
            {
                Config.KeyValue keyValue = new Config.KeyValue()
                {
                    Value = readAllLine
                };
                this._lines.Add(keyValue);
                this._sections[index].Add(keyValue);
                if (readAllLine.Trim().StartsWith("["))
                {
                    index = readAllLine.Replace("[", "").Replace("]", "").Trim();
                    this._sections[index] = new List<Config.KeyValue>();
                }
                if (!readAllLine.Trim().StartsWith(";"))
                {
                    int length = readAllLine.IndexOf("=");
                    if (length > 0)
                    {
                        string key = readAllLine.Substring(0, length).Trim();
                        if (!this._settings.ContainsKey(key))
                            this._settings[key] = new List<Config.KeyValue>();
                        this._settings[key].Add(keyValue);
                        keyValue.Key = key;
                        keyValue.IsKeyVal = true;
                        if (length + 1 < readAllLine.Length)
                            keyValue.Value = readAllLine.Substring(length + 1).Trim();
                    }
                }
            }
        }

        public void Save()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string key in this._sections.Keys)
            {
                if (key != "")
                    stringBuilder.AppendLine("[" + key + "]");
                foreach (Config.KeyValue keyValue in this._sections[key])
                {
                    if (!keyValue.Delete)
                    {
                        if (keyValue.Key != null)
                            stringBuilder.AppendLine(keyValue.Key + "=" + keyValue.Value);
                        else
                            stringBuilder.AppendLine(keyValue.Value);
                    }
                }
            }
            File.WriteAllText(this._configFile, stringBuilder.ToString());
        }

        public bool GetBool(string key)
        {
            if (!this._settings.ContainsKey(key))
                return false;
            string upper = this._settings[key].First<Config.KeyValue>().Value.ToUpper();
            if (!(upper == "TRUE") && !(upper == "1"))
                return upper == "ON";
            return true;
        }

        public int GetInt(string key, int defaultValue = 0)
        {
            int result = defaultValue;
            if (this._settings.ContainsKey(key))
                int.TryParse(this._settings[key].First<Config.KeyValue>().Value, out result);
            else
            {
                int value = result;
                Config.KeyValue keyValue = new Config.KeyValue()
                {
                    Key = key,
                    Value = value.ToString(),
                    IsKeyVal = true
                };
                if (!this._settings.ContainsKey(key))
                    this._settings[key] = new List<Config.KeyValue>();
                this._settings[key].Add(keyValue);
                this._lines.Add(keyValue);
            }
            return result;
        }

        public string GetString(string key)
        {
            key = key.ToUpper();
            if (this._settings.ContainsKey(key))
                return this._settings[key].First<Config.KeyValue>().Value;
            return "";
        }

        public string GetString(string key, string defaultvalue)
        {
            if (this._settings.ContainsKey(key))
                return this._settings[key].First<Config.KeyValue>().Value;
            return defaultvalue;
        }

        public class KeyValue
        {
            public string Key { get; set; }

            public string Value { get; set; }

            public bool IsKeyVal { get; set; }

            public bool Delete { get; set; }

            public override string ToString()
            {
                if (this.Key != null)
                    return this.Key + "=" + this.Value;
                return this.Value;
            }
        }
    }
}