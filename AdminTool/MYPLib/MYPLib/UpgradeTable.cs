﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.UpgradeTable
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Collections.Generic;
using System.IO;

namespace MYPLib
{
    public class UpgradeTable
    {
        public List<UpgradeTable.UpgradeItem> Items = new List<UpgradeTable.UpgradeItem>();

        public ushort ID { get; set; }

        public override string ToString()
        {
            return this.ID.ToString();
        }

        public UpgradeTable.UpgradeItem I1
        {
            get
            {
                return this.Items[0];
            }
        }

        public UpgradeTable.UpgradeItem I2
        {
            get
            {
                return this.Items[1];
            }
        }

        public UpgradeTable.UpgradeItem I3
        {
            get
            {
                return this.Items[2];
            }
        }

        public UpgradeTable.UpgradeItem I4
        {
            get
            {
                return this.Items[3];
            }
        }

        public UpgradeTable(Stream stream)
        {
            this.ID = PacketUtil.GetUint16R(stream);
            for (int index = 0; index < 20; ++index)
                this.Items.Add(new UpgradeTable.UpgradeItem(stream));
        }

        public void Save(Stream stream)
        {
            PacketUtil.WriteUInt16R(stream, this.ID);
            for (int index = 0; index < 20; ++index)
                this.Items[index].Save(stream);
        }

        public class UpgradeItem
        {
            public int A00 { get; set; }

            public int A01 { get; set; }

            public int A02 { get; set; }

            public byte A03 { get; set; }

            public byte A04 { get; set; }

            public byte A05 { get; set; }

            public byte A06 { get; set; }

            public override string ToString()
            {
                return string.Format("{0} {1} {2} {3} {4} {5} {6}", (object)this.A00.ToString().PadLeft(4, ' '), (object)this.A01.ToString().PadLeft(4, ' '), (object)this.A02.ToString().PadLeft(4, ' '), (object)this.A03.ToString().PadLeft(3, ' '), (object)this.A04.ToString().PadLeft(3, ' '), (object)this.A05.ToString().PadLeft(3, ' '), (object)this.A06.ToString().PadLeft(3, ' '));
            }

            public UpgradeItem(Stream stream)
            {
                this.A00 = (int)PacketUtil.GetUint32R(stream);
                this.A01 = (int)PacketUtil.GetUint32R(stream);
                this.A02 = (int)PacketUtil.GetUint32R(stream);
                this.A03 = PacketUtil.GetUint8(stream);
                this.A04 = PacketUtil.GetUint8(stream);
                this.A05 = PacketUtil.GetUint8(stream);
                this.A06 = PacketUtil.GetUint8(stream);
            }

            public void Save(Stream stream)
            {
                PacketUtil.WriteUInt32R(stream, (uint)this.A00);
                PacketUtil.WriteUInt32R(stream, (uint)this.A01);
                PacketUtil.WriteUInt32R(stream, (uint)this.A02);
                PacketUtil.WriteByte(stream, this.A03);
                PacketUtil.WriteByte(stream, this.A04);
                PacketUtil.WriteByte(stream, this.A05);
                PacketUtil.WriteByte(stream, this.A06);
            }
        }
    }
}