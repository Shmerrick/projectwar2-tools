﻿// Decompiled with JetBrains decompiler
// Type: MypLib.Crc32
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;

namespace MypLib
{
    public class Crc32
    {
        private uint[] table;

        public uint ComputeChecksum(byte[] bytes)
        {
            uint num1 = uint.MaxValue;
            for (int index = 0; index < bytes.Length; ++index)
            {
                byte num2 = (byte)(num1 & (uint)byte.MaxValue ^ (uint)bytes[index]);
                num1 = num1 >> 8 ^ this.table[(int)num2];
            }
            return ~num1;
        }

        public byte[] ComputeChecksumBytes(byte[] bytes)
        {
            return BitConverter.GetBytes(this.ComputeChecksum(bytes));
        }

        public Crc32()
        {
            uint num1 = 3988292384;
            this.table = new uint[256];
            for (uint index1 = 0; (long)index1 < (long)this.table.Length; ++index1)
            {
                uint num2 = index1;
                for (int index2 = 8; index2 > 0; --index2)
                {
                    if (((int)num2 & 1) == 1)
                        num2 = num2 >> 1 ^ num1;
                    else
                        num2 >>= 1;
                }
                this.table[(int)index1] = num2;
            }
        }
    }
}