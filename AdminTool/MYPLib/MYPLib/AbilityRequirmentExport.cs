﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.AbilityRequirmentExport
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Collections.Generic;
using System.IO;

namespace MYPLib
{
    public class AbilityRequirmentExport
    {
        public List<AbilityRequirmentBin> Requirments = new List<AbilityRequirmentBin>();
        public uint Header;
        public uint Size;

        public void Load(Stream stream)
        {
            this.Header = PacketUtil.GetUint32R(stream);
            this.Size = PacketUtil.GetUint32R(stream);
            while (stream.Position < stream.Length)
                this.Requirments.Add(new AbilityRequirmentBin(stream));
        }

        public void Save(Stream stream)
        {
            PacketUtil.WriteUInt32R(stream, 1U);
            PacketUtil.WriteUInt32R(stream, (uint)this.Requirments.Count);
            for (int index = 0; index < this.Requirments.Count; ++index)
                this.Requirments[index].Save(stream);
        }
    }
}