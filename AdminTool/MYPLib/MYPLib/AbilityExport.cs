﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.AbilityExport
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using MypLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MYPLib
{
    public class AbilityExport
    {
        public List<AbilityBin> Abilities = new List<AbilityBin>();
        public Dictionary<uint, AbilityBin> AbilitiesByID = new Dictionary<uint, AbilityBin>();
        public uint Header;
        public uint Size;

        public void Load(MYPManager manager, Stream stream)
        {
            long position = stream.Position;
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, (int)stream.Length);
            stream.Position = position;
            this.Header = PacketUtil.GetUint32R(stream);
            this.Size = PacketUtil.GetUint32R(stream);
            while (stream.Position < stream.Length)
            {
                AbilityBin abilityBin = new AbilityBin(stream);
                this.Abilities.Add(abilityBin);
                if (abilityBin.A40_AbilityID > (ushort)0)
                {
                    abilityBin.Name = manager.GetString(Language.english, "abilitynames.txt", (int)abilityBin.A40_AbilityID, MythicPackage.DEV).Replace("^n", "");
                    abilityBin.Description = manager.GetString(Language.english, "abilitydesc.txt", (int)abilityBin.A40_AbilityID, MythicPackage.DEV).Replace("^n", "");
                }
                this.AbilitiesByID[(uint)abilityBin.A40_AbilityID] = abilityBin;
            }
        }

        public void Save(Stream stream)
        {
            PacketUtil.WriteUInt32R(stream, 36U);
            PacketUtil.WriteUInt32R(stream, (uint)this.Abilities.Count);
            for (int index = 0; index < this.Abilities.Count; ++index)
                this.Abilities[index].Save(stream);
        }

        public AbilityBin NewAbility(
          MYPManager manager,
          MythicPackage package,
          AbilityBin cloneFrom,
          AbilityComponentExport compExpSource,
          AbilityComponentExport compExpDest)
        {
            ushort ID = 1;
            if (this.Abilities.Count > 0)
                ID = (ushort)((uint)this.Abilities.Select<AbilityBin, ushort>((Func<AbilityBin, ushort>)(e => e.A40_AbilityID)).Max<ushort>() + 1U);
            AbilityBin abilityBin = new AbilityBin(ID, cloneFrom, manager, package, compExpSource, compExpDest);
            this.Abilities.Add(abilityBin);
            return abilityBin;
        }
    }
}