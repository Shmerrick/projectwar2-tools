﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Program
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MYPLib;
using System;
using System.Windows.Forms;

namespace RorAdmin
{
    internal static class Program
    {
        public static Config UIConfig = new Config("uiconfig.ini");
        public static Config Config = new Config("config.ini");
        private static frmAdmin _admin = (frmAdmin)null;
        public static object _assetClipBoard = (object)null;
        public static object _abilityClipboard = (object)null;

        public static ILogger Log
        {
            get
            {
                return (ILogger)Program._admin;
            }
        }

        [STAThread]
        private static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Program._admin = new frmAdmin();
                Application.Run((Form)Program._admin);
            }
            finally
            {
                Program.UIConfig.Save();
                Program.Config.Save();
            }
        }
    }
}