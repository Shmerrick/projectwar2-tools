﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.frmAdmin
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using MYPLib;
using RorAdmin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RorAdmin
{
    public class frmAdmin : PersistentForm, ILogger
    {
        private string _logContext = "";
        private List<string> _log = new List<string>();
        private IContainer components = (IContainer)null;
        private MYPBrowseTree mypBrowseTree1;
        private StatusStrip statusStrip1;
        private ToolStrip toolStrip1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem helpToolStripMenuItem;
        private Splitter splitter1;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Splitter splitter2;
        private TextBox textBox1;
        private Timer timer1;

        public frmAdmin()
        {
            this.InitializeComponent();
        }

        public override async Task OnBeginLoad()
        {
            await Task.Run(() =>
            {
                mypBrowseTree1.LoadArchives();
            });
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.UnloadViewControl();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.LoadSettings(Program.UIConfig);
        }

        private void UnloadViewControl()
        {
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lock (this._log)
            {
                if (this._log.Count <= 0)
                    return;
                StringBuilder stringBuilder = new StringBuilder();
                foreach (string str in this._log)
                    stringBuilder.Append(str);
                this._log.Clear();
                this.textBox1.AppendText(stringBuilder.ToString());
                this.textBox1.SelectionStart = this.textBox1.Text.Length;
                this.textBox1.ScrollToCaret();
                File.AppendAllText("log.txt", stringBuilder.ToString());
            }
        }

        public void Log(string msg)
        {
            lock (this._log)
                this._log.Add(DateTime.Now.ToString() + ": " + msg + "\r\n");
        }

        private void mypBrowseTree1_OnMypNodeSelected(MYPManager manager, MYP.AssetInfo entry)
        {
            this.UnloadViewControl();
            this.groupBox1.Controls.Clear();
            entry.Name.ToUpper();
            BaseViewer viewer = this.GetViewer(entry);
            this.groupBox1.Text = entry.Name;
            viewer.LoadEntry(manager, entry);
            viewer.Dock = DockStyle.Fill;
            this.groupBox1.Controls.Add((Control)viewer);
            if (!(viewer is IPersistControl))
                return;
            ((IPersistControl)viewer).LoadSettings(Program.UIConfig);
        }

        private BaseViewer GetViewer(MYP.AssetInfo entry)
        {
            string upper = entry.Name.ToUpper();
            if (upper.EndsWith(".CSV"))
                return (BaseViewer)new frmCSVView();
            if (upper.StartsWith("DATA/STRINGS"))
                return (BaseViewer)new frmLocalizedTxtViewer();
            if (upper == "DATA/BIN/ABILITYEXPORT.BIN")
                return (BaseViewer)new frmAbilityBinViewer();
            if (upper == "DATA/BIN/ABILITYCOMPONENTEXPORT.BIN")
                return (BaseViewer)new frmAbilityComponentView();
            if (upper == "DATA/BIN/ABILITYREQUIREMENTEXPORT.BIN")
                return (BaseViewer)new frmAbilityRequirmentView();
            if (upper == "DATA/BIN/UPGRADETABLEEXPORT.BIN")
                return (BaseViewer)new frmAbilityUpgradewView();
            if (upper.EndsWith(".DDS") || upper.EndsWith(".PNG") || upper.EndsWith(".BMP") || upper.EndsWith(".JPG"))
                return (BaseViewer)new frmImageViewer();
            return (BaseViewer)new frmDocumentView();
        }

        private void frmAdmin_Load(object sender, EventArgs e)
        {
            Program.Log.Append("RoR Admin Patcher loaded");
        }

        public void SetContext(string name)
        {
        }

        public void Append(string msg)
        {
            this.Log(msg);
        }

        public void Error(string msg)
        {
            this.Log("ERROR " + msg);
        }

        public void Exception(string msg, Exception e)
        {
            this.Log("EXCEPTION " + msg + "\r\n" + e.ToString());
        }

        private void mypBrowseTree1_OnMypNodeNewWindow(MYPManager manager, MYP.AssetInfo entry)
        {
            PersistentForm persistentForm = new PersistentForm();
            persistentForm.Width = 1024;
            persistentForm.Height = 800;
            persistentForm.Text = entry.Name;
            persistentForm.StartPosition = FormStartPosition.CenterScreen;
            BaseViewer viewer = this.GetViewer(entry);
            viewer.LoadEntry(manager, entry);
            viewer.Dock = DockStyle.Fill;
            persistentForm.Controls.Add((Control)viewer);
            if (viewer is IPersistControl)
                ((IPersistControl)viewer).LoadSettings(Program.UIConfig);
            persistentForm.Show();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            this.mypBrowseTree1 = new MYPBrowseTree();
            this.statusStrip1 = new StatusStrip();
            this.toolStrip1 = new ToolStrip();
            this.menuStrip1 = new MenuStrip();
            this.fileToolStripMenuItem = new ToolStripMenuItem();
            this.helpToolStripMenuItem = new ToolStripMenuItem();
            this.splitter1 = new Splitter();
            this.groupBox1 = new GroupBox();
            this.groupBox2 = new GroupBox();
            this.textBox1 = new TextBox();
            this.splitter2 = new Splitter();
            this.timer1 = new Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            this.mypBrowseTree1.Dock = DockStyle.Left;
            this.mypBrowseTree1.Location = new Point(0, 49);
            this.mypBrowseTree1.Name = "mypBrowseTree1";
            this.mypBrowseTree1.Size = new Size(293, 373);
            this.mypBrowseTree1.TabIndex = 0;
            this.mypBrowseTree1.OnMypNodeSelected += new MYPBrowseTree.MypNodeDelegate(this.mypBrowseTree1_OnMypNodeSelected);
            this.mypBrowseTree1.OnMypNodeNewWindow += new MYPBrowseTree.MypNodeDelegate(this.mypBrowseTree1_OnMypNodeNewWindow);
            this.statusStrip1.Location = new Point(0, 567);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new Size(1079, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            this.toolStrip1.Location = new Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new Size(1079, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            this.menuStrip1.Items.AddRange(new ToolStripItem[2]
            {
        (ToolStripItem) this.fileToolStripMenuItem,
        (ToolStripItem) this.helpToolStripMenuItem
            });
            this.menuStrip1.Location = new Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new Size(1079, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.splitter1.Location = new Point(293, 49);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new Size(3, 373);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            this.groupBox1.Dock = DockStyle.Fill;
            this.groupBox1.Location = new Point(296, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(783, 373);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox2.Controls.Add((Control)this.textBox1);
            this.groupBox2.Dock = DockStyle.Bottom;
            this.groupBox2.Location = new Point(0, 425);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new Size(1079, 142);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Log";
            this.textBox1.BackColor = Color.DimGray;
            this.textBox1.Dock = DockStyle.Fill;
            this.textBox1.Font = new Font("Lucida Console", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            this.textBox1.ForeColor = Color.WhiteSmoke;
            this.textBox1.Location = new Point(3, 16);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = ScrollBars.Both;
            this.textBox1.Size = new Size(1073, 123);
            this.textBox1.TabIndex = 0;
            this.splitter2.Dock = DockStyle.Bottom;
            this.splitter2.Location = new Point(0, 422);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new Size(1079, 3);
            this.splitter2.TabIndex = 8;
            this.splitter2.TabStop = false;
            this.timer1.Enabled = true;
            this.timer1.Interval = 30;
            this.timer1.Tick += new EventHandler(this.timer1_Tick);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(1079, 589);
            this.Controls.Add((Control)this.groupBox1);
            this.Controls.Add((Control)this.splitter1);
            this.Controls.Add((Control)this.mypBrowseTree1);
            this.Controls.Add((Control)this.toolStrip1);
            this.Controls.Add((Control)this.menuStrip1);
            this.Controls.Add((Control)this.splitter2);
            this.Controls.Add((Control)this.groupBox2);
            this.Controls.Add((Control)this.statusStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = nameof(frmAdmin);
            this.Text = "RoR Admin Patcher";
            this.FormClosing += new FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new EventHandler(this.frmAdmin_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}