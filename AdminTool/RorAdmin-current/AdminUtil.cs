﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.AdminUtil
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RorAdmin
{
    public static class AdminUtil
    {
        public static TaskScheduler UIThread { get; set; }

        public static void RunAsync(this Task task)
        {
            task.ContinueWith((Action<Task>)(t => { }), TaskContinuationOptions.OnlyOnFaulted);
        }

        public static Task UITask(Action<Task> a)
        {
            return Task.Run((Action)(() => { })).ContinueWith(a, AdminUtil.UIThread);
        }

        public static List<int> CSVToIntList(string csv, char[] splitTokens = null)
        {
            List<int> intList = new List<int>();
            if (splitTokens == null)
                splitTokens = new char[3] { ',', ' ', '|' };
            foreach (string s in csv.Split(splitTokens, StringSplitOptions.RemoveEmptyEntries))
            {
                int result = 0;
                if (int.TryParse(s, out result))
                    intList.Add(result);
            }
            return intList;
        }

        public static List<string> CSVToStrList(string csv, char[] splitTokens = null)
        {
            List<string> stringList = new List<string>();
            if (splitTokens == null)
                splitTokens = new char[3] { ',', ' ', '|' };
            foreach (string str in csv.Split(splitTokens, StringSplitOptions.RemoveEmptyEntries))
                stringList.Add(str);
            return stringList;
        }
    }
}