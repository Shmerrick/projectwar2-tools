﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.frmLogin
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin
{
    public class frmLogin : Form
    {
        private IContainer components = (IContainer)null;
        private Label label1;
        private TextBox txtUsername;
        private TextBox txtPassword;
        private Label label2;
        private Button btnCancel;
        private Button btnLogin;
        private TextBox txtServer;
        private Label label3;

        public frmLogin()
        {
            this.InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.label1 = new Label();
            this.txtUsername = new TextBox();
            this.txtPassword = new TextBox();
            this.label2 = new Label();
            this.btnCancel = new Button();
            this.btnLogin = new Button();
            this.txtServer = new TextBox();
            this.label3 = new Label();
            this.SuspendLayout();
            this.label1.AutoSize = true;
            this.label1.Location = new Point(27, 18);
            this.label1.Name = "label1";
            this.label1.Size = new Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            this.txtUsername.Location = new Point(88, 15);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new Size(244, 20);
            this.txtUsername.TabIndex = 1;
            this.txtPassword.Location = new Point(88, 41);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new Size(244, 20);
            this.txtPassword.TabIndex = 3;
            this.label2.AutoSize = true;
            this.label2.Location = new Point(27, 44);
            this.label2.Name = "label2";
            this.label2.Size = new Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password";
            this.btnCancel.Location = new Point(108, 98);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "C&ancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnLogin.Location = new Point(189, 98);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new Size(75, 23);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "&OK";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.txtServer.Location = new Point(88, 67);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new Size(244, 20);
            this.txtServer.TabIndex = 7;
            this.label3.AutoSize = true;
            this.label3.Location = new Point(42, 70);
            this.label3.Name = "label3";
            this.label3.Size = new Size(38, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Server";
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(344, 134);
            this.Controls.Add((Control)this.txtServer);
            this.Controls.Add((Control)this.label3);
            this.Controls.Add((Control)this.btnLogin);
            this.Controls.Add((Control)this.btnCancel);
            this.Controls.Add((Control)this.txtPassword);
            this.Controls.Add((Control)this.label2);
            this.Controls.Add((Control)this.txtUsername);
            this.Controls.Add((Control)this.label1);
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.Name = nameof(frmLogin);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Ror Admin Patcher Login";
            this.Load += new EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}