﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.PersistentForm
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MYPLib;
using RorAdmin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace RorAdmin
{
    public class PersistentForm : Form, IPersistControl
    {
        private IContainer components = (IContainer)null;

        public PersistentForm()
        {
            AdminUtil.UIThread = TaskScheduler.FromCurrentSynchronizationContext();
            this.InitializeComponent();
        }

        public virtual async Task OnBeginLoad()
        {
            await Task.Run(() =>
            {
            });
        }

        private void LoadControls(string path, Control parent, Dictionary<string, Control> controls)
        {
            controls[path] = parent;
            foreach (Control control in (ArrangedElementCollection)parent.Controls)
                this.LoadControls(path + "." + control.Name, control, controls);
        }

        public void SaveSettings(Config config)
        {
            Dictionary<string, Control> controls = new Dictionary<string, Control>();
            this.LoadControls(this.Name, (Control)this, controls);
            foreach (string key in controls.Keys)
            {
                if (!(controls[key] is Form) || controls[key] is Form && ((Form)controls[key]).WindowState == FormWindowState.Normal)
                {
                    config[key + "_x", ""] = (object)controls[key].Left;
                    config[key + "_y", ""] = (object)controls[key].Top;
                    config[key + "_width", ""] = (object)controls[key].Width;
                    config[key + "_height", ""] = (object)controls[key].Height;
                }
                if (controls[key] is Splitter)
                {
                    Splitter splitter = (Splitter)controls[key];
                    config[key + "_SplitPosition", ""] = (object)splitter.SplitPosition;
                }
                if (controls[key] is IPersistControl && controls[key] != this)
                    ((IPersistControl)controls[key]).SaveSettings(config);
            }
            config.Save();
        }

        public void LoadSettings(Config config)
        {
            Dictionary<string, Control> controls = new Dictionary<string, Control>();
            this.LoadControls(this.Name, (Control)this, controls);
            foreach (string key in controls.Keys)
            {
                if (config[key + "_x", ""] != null && !(controls[key] is PropertyGrid))
                {
                    if (controls[key] is Splitter)
                        ((Splitter)controls[key]).SplitPosition = config.GetInt(key + "_SplitPosition", 0);
                    if (controls[key] is IPersistControl && controls[key] != this)
                        ((IPersistControl)controls[key]).LoadSettings(config);
                    if (controls[key] is Form)
                    {
                        controls[key].Left = config.GetInt(key + "_x", 0);
                        controls[key].Top = config.GetInt(key + "_y", 0);
                        controls[key].Width = config.GetInt(key + "_width", 0);
                        controls[key].Height = config.GetInt(key + "_height", 0);
                    }
                }
            }
        }

        private async void PersistentForm_Load(object sender, EventArgs e)
        {
            await this.OnBeginLoad();
            this.LoadSettings(Program.UIConfig);
        }

        private void PersistentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.SaveSettings(Program.UIConfig);
            Program.Config.Save();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(284, 262);
            this.Name = nameof(PersistentForm);
            this.Text = nameof(PersistentForm);
            this.FormClosing += new FormClosingEventHandler(this.PersistentForm_FormClosing);
            this.Load += new EventHandler(this.PersistentForm_Load);
            this.ResumeLayout(false);
        }
    }
}