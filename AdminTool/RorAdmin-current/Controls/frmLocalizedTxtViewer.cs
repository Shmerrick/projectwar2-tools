﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmLocalizedTxtViewer
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class frmLocalizedTxtViewer : BaseViewer
    {
        private Dictionary<int, ListViewItem> _lviCache = new Dictionary<int, ListViewItem>();
        private List<string> _lines = new List<string>();
        private Dictionary<Language, LocalizatedItem> _languages = new Dictionary<Language, LocalizatedItem>();
        private IContainer components = (IContainer)null;
        private bool _disableUpdates;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private GroupBox gpLocal;
        private Splitter splitter1;
        private GroupBox groupBox1;
        private ListView lvKeys;
        private TabPage tabPage2;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private Panel panel1;
        private TextBox txtRaw;

        public frmLocalizedTxtViewer()
        {
            this.InitializeComponent();
        }

        public override void LoadEntry(MYPManager manager, MYP.AssetInfo entry)
        {
            this._disableUpdates = true;
            base.LoadEntry(manager, entry);
            string[] strArray = this._entry.Name.Split(new char[1]
            {
        '/'
            }, StringSplitOptions.RemoveEmptyEntries);
            string str = "";
            for (int index = 3; index < strArray.Length; ++index)
                str = str + "/" + strArray[index];
            Language language = (Language)Enum.Parse(typeof(Language), strArray[2]);
            this._lines = manager.GetStringTable(language, entry.Name, MythicPackage.DEV);
            this.lvKeys.VirtualMode = true;
            this.lvKeys.VirtualListSize = this._lines.Count;
            this.txtRaw.Text = Encoding.Unicode.GetString(manager.GetAsset(entry));
            this._disableUpdates = false;
            this.LoadRow(0);
        }

        private void lvKeys_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            if (this._disableUpdates)
                return;
            if (!this._lviCache.ContainsKey(e.ItemIndex))
                this.lvKeys_CacheVirtualItems((object)null, new CacheVirtualItemsEventArgs(e.ItemIndex, e.ItemIndex));
            e.Item = this._lviCache[e.ItemIndex];
        }

        private void lvKeys_CacheVirtualItems(object sender, CacheVirtualItemsEventArgs e)
        {
            for (int startIndex = e.StartIndex; startIndex <= e.EndIndex; ++startIndex)
            {
                if (!this._lviCache.ContainsKey(startIndex))
                {
                    string line = this._lines[startIndex];
                    ListViewItem listViewItem = new ListViewItem(startIndex.ToString());
                    listViewItem.SubItems.Add(line);
                    listViewItem.Tag = (object)startIndex;
                    if (startIndex % 2 == 0)
                        listViewItem.BackColor = Color.FromArgb((int)byte.MaxValue, 240, 240, 240);
                    this._lviCache[startIndex] = listViewItem;
                }
            }
        }

        private void LoadRow(int rowIndex)
        {
            string[] strArray = this._entry.Name.Split(new char[1]
            {
        '/'
            }, StringSplitOptions.RemoveEmptyEntries);
            string str = "";
            for (int index = 3; index < strArray.Length; ++index)
                str = str + "/" + strArray[index];
            Language language1 = (Language)Enum.Parse(typeof(Language), strArray[2]);
            List<Language> list = Enum.GetValues(typeof(Language)).Cast<Language>().ToList<Language>();
            list.Reverse();
            foreach (Language index in list)
            {
                string tableName = "data/strings/" + index.ToString() + str;
                List<string> stringTable = this.Manager.GetStringTable(index, tableName, MythicPackage.DEV);
                if (!this._languages.ContainsKey(index))
                {
                    this._languages[index] = new LocalizatedItem();
                    this._languages[index].Dock = DockStyle.Top;
                    this._languages[index].Filename = tableName;
                    this._languages[index].RowID = rowIndex;
                    this._languages[index].Language = index;
                    this._languages[index].OnChange += new LocalizatedItem.ChangeDelegate(this.FrmLocalizedTxtViewer_OnChange);
                    this.panel1.Controls.Add((Control)this._languages[index]);
                }
                LocalizatedItem language2 = this._languages[index];
                if (rowIndex < stringTable.Count)
                {
                    language2.Value = stringTable[rowIndex];
                    language2.RowID = rowIndex;
                    if (language1 == index)
                        language2.BackColor = Color.LightBlue;
                    else
                        language2.BackColor = this.BackColor;
                }
            }
        }

        private void FrmLocalizedTxtViewer_OnChange(LocalizatedItem c)
        {
            this.Manager.SetString(c.Language, c.Filename, c.RowID, c.Value, MythicPackage.DEV);
            string s = this.Manager.GetString(c.Language, c.Filename);
            this.Manager.UpdateAsset(this._entry.Package, c.Filename, Encoding.Unicode.GetBytes(s), false);
            if (c.Language != Language.english)
                return;
            this.lvKeys.Items[c.RowID].SubItems[1].Text = c.Value;
            this.lvKeys.Invalidate();
        }

        private void lvKeys_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lvKeys.SelectedIndices.Count <= 0)
                return;
            this.LoadRow(this.lvKeys.SelectedIndices[0]);
        }

        private void txtRaw_TextChanged(object sender, EventArgs e)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.tabControl1 = new TabControl();
            this.tabPage1 = new TabPage();
            this.gpLocal = new GroupBox();
            this.splitter1 = new Splitter();
            this.groupBox1 = new GroupBox();
            this.lvKeys = new ListView();
            this.columnHeader1 = new ColumnHeader();
            this.columnHeader2 = new ColumnHeader();
            this.tabPage2 = new TabPage();
            this.panel1 = new Panel();
            this.txtRaw = new TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gpLocal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            this.tabControl1.Controls.Add((Control)this.tabPage1);
            this.tabControl1.Controls.Add((Control)this.tabPage2);
            this.tabControl1.Dock = DockStyle.Fill;
            this.tabControl1.Location = new Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new Size(997, 429);
            this.tabControl1.TabIndex = 2;
            this.tabPage1.Controls.Add((Control)this.gpLocal);
            this.tabPage1.Controls.Add((Control)this.splitter1);
            this.tabPage1.Controls.Add((Control)this.groupBox1);
            this.tabPage1.Location = new Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new Padding(3);
            this.tabPage1.Size = new Size(989, 403);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Localized";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.gpLocal.Controls.Add((Control)this.panel1);
            this.gpLocal.Dock = DockStyle.Fill;
            this.gpLocal.Location = new Point(470, 3);
            this.gpLocal.Name = "gpLocal";
            this.gpLocal.Size = new Size(516, 397);
            this.gpLocal.TabIndex = 3;
            this.gpLocal.TabStop = false;
            this.gpLocal.Text = "Localizations";
            this.splitter1.Location = new Point(467, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new Size(3, 397);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            this.groupBox1.Controls.Add((Control)this.lvKeys);
            this.groupBox1.Dock = DockStyle.Left;
            this.groupBox1.Location = new Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(464, 397);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Keys";
            this.lvKeys.Columns.AddRange(new ColumnHeader[2]
            {
        this.columnHeader1,
        this.columnHeader2
            });
            this.lvKeys.Dock = DockStyle.Fill;
            this.lvKeys.FullRowSelect = true;
            this.lvKeys.Location = new Point(3, 16);
            this.lvKeys.Name = "lvKeys";
            this.lvKeys.Size = new Size(458, 378);
            this.lvKeys.TabIndex = 1;
            this.lvKeys.UseCompatibleStateImageBehavior = false;
            this.lvKeys.View = View.Details;
            this.lvKeys.VirtualMode = true;
            this.lvKeys.CacheVirtualItems += new CacheVirtualItemsEventHandler(this.lvKeys_CacheVirtualItems);
            this.lvKeys.RetrieveVirtualItem += new RetrieveVirtualItemEventHandler(this.lvKeys_RetrieveVirtualItem);
            this.lvKeys.SelectedIndexChanged += new EventHandler(this.lvKeys_SelectedIndexChanged);
            this.columnHeader1.Text = "Line#";
            this.columnHeader1.Width = 76;
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 375;
            this.tabPage2.Controls.Add((Control)this.txtRaw);
            this.tabPage2.Location = new Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new Padding(3);
            this.tabPage2.Size = new Size(989, 403);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Raw";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.panel1.AutoScroll = true;
            this.panel1.Dock = DockStyle.Fill;
            this.panel1.Location = new Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(510, 378);
            this.panel1.TabIndex = 0;
            this.txtRaw.Dock = DockStyle.Fill;
            this.txtRaw.Location = new Point(3, 3);
            this.txtRaw.Multiline = true;
            this.txtRaw.Name = "txtRaw";
            this.txtRaw.ScrollBars = ScrollBars.Both;
            this.txtRaw.Size = new Size(983, 397);
            this.txtRaw.TabIndex = 0;
            this.txtRaw.WordWrap = false;
            this.txtRaw.TextChanged += new EventHandler(this.txtRaw_TextChanged);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add((Control)this.tabControl1);
            this.Name = nameof(frmLocalizedTxtViewer);
            this.Size = new Size(997, 453);
            this.Controls.SetChildIndex((Control)this.tabControl1, 0);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.gpLocal.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}