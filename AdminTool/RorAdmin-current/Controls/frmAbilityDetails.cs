﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmAbilityDetails
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using MYPLib;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class frmAbilityDetails : PersistentForm
    {
        private AbilityComponentExport _compExporter = new AbilityComponentExport();
        private IContainer components = (IContainer)null;
        private MYPManager _manager;
        private AbilityBin _ability;
        private MythicPackage _package;
        private MYP.AssetInfo _entry;
        private AbilityExport _abExport;
        public MYP.AssetInfo _compAsset;
        private AbilityComponent currentComponent;
        private Panel panel1;
        private Button btnCancel;
        private Button btnOK;
        private GroupBox groupBox1;
        private TextBox txtID;
        private Label label1;
        private TextBox txtName;
        private Label label2;
        private Button button1;
        private TextBox txtDescription;
        private Label label3;
        private Button btnEditName;
        private PropertyGrid propertyGrid1;
        private Splitter splitter1;
        private GroupBox groupBox4;
        private Splitter splitter2;
        private Splitter splitter3;
        private GroupBox groupBox3;
        private TabControl tabControl1;
        private TextBox textBox1;
        private Panel panel2;
        private Button btnCloneComponent;
        private Button btnNewComponent;
        private Button btnDelete;

        public frmAbilityDetails()
        {
            this.InitializeComponent();
        }

        private void LoadData()
        {
            this.txtID.Text = this._ability.A40_AbilityID.ToString();
            this.txtName.Text = this._ability.Name;
            this.txtDescription.Text = this._ability.Description;
            this.propertyGrid1.SelectedObject = (object)this._ability;
        }

        public void LoadAbility(
          MythicPackage package,
          MYPManager manager,
          AbilityBin ability,
          MYP.AssetInfo abEntry,
          AbilityExport abExport,
          AbilityComponentExport compExp,
          MYP.AssetInfo compAsset)
        {
            this._abExport = abExport;
            this._entry = abEntry;
            this._manager = manager;
            this._ability = ability;
            this.LoadData();
            this._package = package;
            this._compAsset = compAsset;
            this._compExporter = compExp;
            int index = 0;
            ability.Components.Clear();
            foreach (ushort componentId in ability.ComponentIDs)
            {
                if (componentId > (ushort)0)
                {
                    if (this._compExporter.CompHash.ContainsKey(componentId))
                    {
                        ability.Components.Add(this._compExporter.CompHash[componentId]);
                        string desc = "";
                        if (this._compExporter.CompHash[componentId].Description != "")
                            desc = " " + this._compExporter.CompHash[componentId].Description;
                        if (desc.Length > 20)
                            desc = desc.Substring(0, 20) + "...";
                        if (componentId > (ushort)0)
                            this.AddComponent(index, desc, this._compExporter.CompHash[componentId]);
                    }
                    ++index;
                }
            }
            if (this.tabControl1.TabCount > 0)
                this.tabControl1_SelectedIndexChanged((object)null, (EventArgs)null);
            CSV csv = new CSV(Encoding.ASCII.GetString(manager.GetAsset("data/gamedata/effects.csv")), (string)null, false, 0);
            this.Text = ability.Name + string.Format(" (ID:{0} Effect:{1} Components:{2})", (object)ability.A40_AbilityID, (object)csv.GetKeyedValue((object)ability.A42_EffectID, 1, ""), (object)ability.Components.Count);
        }

        private void AddComponent(int index, string desc, AbilityComponent component)
        {
            TabPage tabPage = new TabPage("Comp:" + index.ToString() + desc + " (" + (object)component.A09_Operation + ")");
            tabPage.Tag = (object)component;
            component.Ability = this._ability;
            this.tabControl1.TabPages.Add(tabPage);
            tabPage.SuspendLayout();
            LocalizedItemView localizedItemView = new LocalizedItemView();
            localizedItemView.LabelValue = "Description";
            localizedItemView.EditValue = component.Description;
            localizedItemView.OnEdit += (LocalizedItemView.ButtonDelegate)(sender =>
           {
               frmLocalizedStringEdit localizedStringEdit = new frmLocalizedStringEdit();
               localizedStringEdit.LoadIntEntry(this._package, this._manager, "data/strings/english/componenteffects.txt", (int)component.A11_ComponentID);
               if (localizedStringEdit.ShowDialog() == DialogResult.OK)
                   ;
           });
            localizedItemView.Dock = DockStyle.Top;
            tabPage.Controls.Add((Control)localizedItemView);
            PropertyGrid propertyGrid = new PropertyGrid();
            propertyGrid.HelpVisible = false;
            propertyGrid.PropertySort = PropertySort.Alphabetical;
            propertyGrid.ToolbarVisible = false;
            propertyGrid.Dock = DockStyle.Fill;
            propertyGrid.SelectedObject = (object)component;
            tabPage.Controls.Add((Control)propertyGrid);
            tabPage.ResumeLayout();
            tabPage.PerformLayout();
            propertyGrid.BringToFront();
            this.tabControl1.SelectedTab = tabPage;
        }

        private void btnEditName_Click(object sender, EventArgs e)
        {
            frmLocalizedStringEdit localizedStringEdit = new frmLocalizedStringEdit();
            localizedStringEdit.LoadIntEntry(this._package, this._manager, "data/strings/english/abilitynames.txt", (int)this._ability.A40_AbilityID);
            if (localizedStringEdit.ShowDialog() != DialogResult.OK)
                return;
            this._ability.Name = this._manager.GetString(Language.english, "abilitynames.txt", (int)this._ability.A40_AbilityID, this._package).Replace("^n", "");
            this.LoadData();
        }

        private void frmAbilityDetails_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmLocalizedStringEdit localizedStringEdit = new frmLocalizedStringEdit();
            localizedStringEdit.LoadIntEntry(this._package, this._manager, "data/strings/english/abilitydesc.txt", (int)this._ability.A40_AbilityID);
            if (localizedStringEdit.ShowDialog() != DialogResult.OK)
                return;
            this._ability.Description = this._manager.GetString(Language.english, "abilitydesc.txt", (int)this._ability.A40_AbilityID, this._package).Replace("^n", "");
            this.LoadData();
        }

        public int GetAbilityValueFromComp(
          AbilityBin ability,
          AbilityComponent component,
          int level,
          int effectiveLevel,
          int mastery,
          int compIndex,
          int valIndex,
          int multIndex,
          int scaleThreshold,
          int staticLevel)
        {
            int num1 = 1;
            if (component.A10_Interval > 0U)
                num1 = (int)(component.A06_Duration / component.A10_Interval);
            int num2 = level;
            if (level != effectiveLevel)
            {
                int num3 = effectiveLevel <= 25 ? effectiveLevel : 25;
                if (effectiveLevel > 40)
                    num3 += effectiveLevel - 40;
                else if (level < 25)
                    num3 = effectiveLevel;
                num2 = num3 + mastery;
            }
            level = num2;
            if ((uint)staticLevel > 0U)
            {
                num2 = staticLevel;
                level = staticLevel;
            }
            float num4 = (float)component.Values[valIndex];
            float multiplier = (float)component.Multipliers[multIndex];
            float num5 = 0.166667f;
            int num6 = 1;
            int num7 = 3;
            if (component.A09_Operation == ComponentOP.STAT_CHANGE)
                num5 = 1f;
            if (component.A06_Duration > 0U && component.A10_Interval > 0U)
                num6 = (int)(component.A06_Duration / component.A10_Interval);
            num7 = (int)component.A07;
            if (ability.Components[compIndex].A07 == 32U)
                return (int)num4;
            int num8 = ((int)((double)(num2 - 1) * (double)num5 * (double)num4) + (int)num4) * (int)multiplier / 100;
            if (num6 > 1 && ability.A52_ChannelInterval <= (ushort)0)
                num8 *= num6;
            return num8;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnCloneComponent.Enabled = false;
            this.btnDelete.Enabled = false;
            if (this.tabControl1.SelectedTab == null)
                return;
            this.btnCloneComponent.Enabled = true;
            this.btnDelete.Enabled = true;
            this.currentComponent = (AbilityComponent)this.tabControl1.SelectedTab.Tag;
            string str = "";
            try
            {
                for (int index = 1; index <= 45; ++index)
                {
                    if (this.currentComponent.A09_Operation == ComponentOP.DAMAGE || this.currentComponent.A09_Operation == ComponentOP.HEAL)
                        str = str + index.ToString() + "=" + (object)this.GetAbilityValueFromComp(this.currentComponent.Ability, this.currentComponent, index, index, 0, 0, 0, 0, 45, 0) + "\r\n";
                    else if (this.currentComponent.A09_Operation == ComponentOP.STAT_CHANGE || this.currentComponent.A09_Operation == ComponentOP.MOVEMENT_SPEED)
                        str = str + index.ToString() + "=" + (object)this.GetAbilityValueFromComp(this.currentComponent.Ability, this.currentComponent, index, index, 0, 0, 0, 0, 45, 0) + "\r\n";
                    else if (this.currentComponent.A09_Operation == ComponentOP.MOVEMENT_SPEED)
                        str = str + index.ToString() + "=" + (object)this.GetAbilityValueFromComp(this.currentComponent.Ability, this.currentComponent, index, index, 0, 0, 0, 0, 45, 0) + "%\r\n";
                    else if (this.currentComponent.A09_Operation == ComponentOP.BONUS_TYPE)
                        str = str + index.ToString() + "=" + (object)this.GetAbilityValueFromComp(this.currentComponent.Ability, this.currentComponent, index, index, 0, 0, 2, 0, 45, 0) + "\r\n";
                }
            }
            catch (Exception ex)
            {
            }
            this.textBox1.Text = str;
        }

        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this._ability.ComponentIDs.Length != 10)
            {
                int num1 = (int)MessageBox.Show("Ability: {10} componentsID are required");
            }
            else if (this._ability.Labels1.Length != 4)
            {
                int num2 = (int)MessageBox.Show("Ability: {4} Labels1 are required");
            }
            else if (this._ability.Labels2.Length != 5)
            {
                int num3 = (int)MessageBox.Show("Ability: {5} Labels2 are required");
            }
            else if (this._ability.A92.Length != 10)
            {
                int num4 = (int)MessageBox.Show("Ability: {10} A92 are required");
            }
            else
            {
                int num5 = 0;
                foreach (ushort componentId in this._ability.ComponentIDs)
                {
                    if (componentId > (ushort)0)
                    {
                        if (this._compExporter.CompHash.ContainsKey(componentId))
                        {
                            AbilityComponent abilityComponent = this._compExporter.CompHash[componentId];
                            if (abilityComponent.Values.Length != 16)
                            {
                                int num6 = (int)MessageBox.Show("Component<" + (object)num5 + ">: {16} Values are required");
                                return;
                            }
                            if (abilityComponent.Multipliers.Length != 8)
                            {
                                int num6 = (int)MessageBox.Show("Component<" + (object)num5 + ">: {8} Multipliers are required");
                                return;
                            }
                            ++num5;
                        }
                        else
                        {
                            int num6 = (int)MessageBox.Show("Ability: ComponentID " + (object)componentId + " not found");
                            return;
                        }
                    }
                }
                MemoryStream memoryStream1 = new MemoryStream();
                this._compExporter.Save((Stream)memoryStream1);
                this._manager.UpdateAsset(this._compAsset, memoryStream1.ToArray());
                MemoryStream memoryStream2 = new MemoryStream();
                this._abExport.Save((Stream)memoryStream2);
                this._manager.UpdateAsset(this._entry, memoryStream2.ToArray());
                this.Close();
            }
        }

        private void btnNewComponent_Click(object sender, EventArgs ed)
        {
            this.AddComponent(this._ability.Components.Count, "", this._compExporter.NewComponent(this._manager, this._package, this._ability, (AbilityComponent)null));
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this._ability.Components.Remove(this.currentComponent);
            this._compExporter.Components.Remove(this.currentComponent);
            for (int index = 0; index < 10; ++index)
            {
                if ((int)this._ability.ComponentIDs[index] == (int)this.currentComponent.A11_ComponentID)
                    this._ability.ComponentIDs[index] = (ushort)0;
            }
            this.tabControl1.TabPages.Remove(this.tabControl1.SelectedTab);
            MemoryStream memoryStream1 = new MemoryStream();
            this._compExporter.Save((Stream)memoryStream1);
            this._manager.UpdateAsset(this._compAsset, memoryStream1.ToArray());
            MemoryStream memoryStream2 = new MemoryStream();
            this._abExport.Save((Stream)memoryStream2);
            this._manager.UpdateAsset(this._entry, memoryStream2.ToArray());
            if (this.tabControl1.TabPages.Count > 0)
                this.tabControl1.SelectedTab = this.tabControl1.TabPages[0];
            this.tabControl1_SelectedIndexChanged((object)null, (EventArgs)null);
        }

        private void btnCloneComponent_Click(object sender, EventArgs e)
        {
            this.AddComponent(this._ability.Components.Count, "", this._compExporter.NewComponent(this._manager, this._package, this._ability, this.currentComponent));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.panel1 = new Panel();
            this.btnCancel = new Button();
            this.btnOK = new Button();
            this.groupBox1 = new GroupBox();
            this.propertyGrid1 = new PropertyGrid();
            this.button1 = new Button();
            this.txtDescription = new TextBox();
            this.label3 = new Label();
            this.btnEditName = new Button();
            this.txtName = new TextBox();
            this.label2 = new Label();
            this.txtID = new TextBox();
            this.label1 = new Label();
            this.splitter1 = new Splitter();
            this.groupBox4 = new GroupBox();
            this.textBox1 = new TextBox();
            this.splitter2 = new Splitter();
            this.splitter3 = new Splitter();
            this.groupBox3 = new GroupBox();
            this.tabControl1 = new TabControl();
            this.panel2 = new Panel();
            this.btnNewComponent = new Button();
            this.btnCloneComponent = new Button();
            this.btnDelete = new Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            this.panel1.Controls.Add((Control)this.btnCancel);
            this.panel1.Controls.Add((Control)this.btnOK);
            this.panel1.Dock = DockStyle.Bottom;
            this.panel1.Location = new Point(0, 628);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(978, 31);
            this.panel1.TabIndex = 0;
            this.btnCancel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.btnCancel.Location = new Point(819, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnOK.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.btnOK.Location = new Point(900, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "&Save";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new EventHandler(this.btnOK_Click);
            this.groupBox1.Controls.Add((Control)this.propertyGrid1);
            this.groupBox1.Controls.Add((Control)this.button1);
            this.groupBox1.Controls.Add((Control)this.txtDescription);
            this.groupBox1.Controls.Add((Control)this.label3);
            this.groupBox1.Controls.Add((Control)this.btnEditName);
            this.groupBox1.Controls.Add((Control)this.txtName);
            this.groupBox1.Controls.Add((Control)this.label2);
            this.groupBox1.Controls.Add((Control)this.txtID);
            this.groupBox1.Controls.Add((Control)this.label1);
            this.groupBox1.Dock = DockStyle.Left;
            this.groupBox1.Location = new Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(378, 628);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ability";
            this.propertyGrid1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this.propertyGrid1.HelpVisible = false;
            this.propertyGrid1.LineColor = SystemColors.ControlDark;
            this.propertyGrid1.Location = new Point(12, 123);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = PropertySort.Alphabetical;
            this.propertyGrid1.Size = new Size(353, 499);
            this.propertyGrid1.TabIndex = 8;
            this.propertyGrid1.ToolbarVisible = false;
            this.propertyGrid1.PropertyValueChanged += new PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            this.button1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.button1.Location = new Point(330, 74);
            this.button1.Name = "button1";
            this.button1.Size = new Size(38, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Edit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new EventHandler(this.button1_Click);
            this.txtDescription.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            this.txtDescription.Location = new Point(94, 74);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new Size(227, 43);
            this.txtDescription.TabIndex = 6;
            this.label3.Location = new Point(-39, 77);
            this.label3.Name = "label3";
            this.label3.Size = new Size((int)sbyte.MaxValue, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Description";
            this.label3.TextAlign = ContentAlignment.MiddleRight;
            this.btnEditName.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.btnEditName.Location = new Point(330, 45);
            this.btnEditName.Name = "btnEditName";
            this.btnEditName.Size = new Size(38, 23);
            this.btnEditName.TabIndex = 4;
            this.btnEditName.Text = "Edit";
            this.btnEditName.UseVisualStyleBackColor = true;
            this.btnEditName.Click += new EventHandler(this.btnEditName_Click);
            this.txtName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            this.txtName.Location = new Point(94, 48);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new Size(230, 20);
            this.txtName.TabIndex = 3;
            this.label2.Location = new Point(-39, 50);
            this.label2.Name = "label2";
            this.label2.Size = new Size((int)sbyte.MaxValue, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name";
            this.label2.TextAlign = ContentAlignment.MiddleRight;
            this.txtID.Location = new Point(94, 23);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new Size(73, 20);
            this.txtID.TabIndex = 1;
            this.label1.Location = new Point(-39, 25);
            this.label1.Name = "label1";
            this.label1.Size = new Size((int)sbyte.MaxValue, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            this.label1.TextAlign = ContentAlignment.MiddleRight;
            this.splitter1.Location = new Point(378, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new Size(3, 628);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            this.groupBox4.Controls.Add((Control)this.textBox1);
            this.groupBox4.Dock = DockStyle.Bottom;
            this.groupBox4.Location = new Point(381, 433);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new Size(597, 195);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Client Paperdoll";
            this.textBox1.Dock = DockStyle.Fill;
            this.textBox1.Location = new Point(3, 16);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = ScrollBars.Both;
            this.textBox1.Size = new Size(591, 176);
            this.textBox1.TabIndex = 0;
            this.splitter2.Dock = DockStyle.Top;
            this.splitter2.Location = new Point(381, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new Size(597, 3);
            this.splitter2.TabIndex = 8;
            this.splitter2.TabStop = false;
            this.splitter3.Dock = DockStyle.Bottom;
            this.splitter3.Location = new Point(381, 430);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new Size(597, 3);
            this.splitter3.TabIndex = 9;
            this.splitter3.TabStop = false;
            this.groupBox3.Controls.Add((Control)this.tabControl1);
            this.groupBox3.Controls.Add((Control)this.panel2);
            this.groupBox3.Dock = DockStyle.Fill;
            this.groupBox3.Location = new Point(381, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new Size(597, 427);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Components";
            this.tabControl1.Dock = DockStyle.Fill;
            this.tabControl1.Location = new Point(3, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new Size(591, 373);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new EventHandler(this.tabControl1_SelectedIndexChanged);
            this.panel2.Controls.Add((Control)this.btnDelete);
            this.panel2.Controls.Add((Control)this.btnCloneComponent);
            this.panel2.Controls.Add((Control)this.btnNewComponent);
            this.panel2.Dock = DockStyle.Top;
            this.panel2.Location = new Point(3, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new Size(591, 35);
            this.panel2.TabIndex = 1;
            this.btnNewComponent.Location = new Point(3, 6);
            this.btnNewComponent.Name = "btnNewComponent";
            this.btnNewComponent.Size = new Size(126, 23);
            this.btnNewComponent.TabIndex = 0;
            this.btnNewComponent.Text = "Add New Component";
            this.btnNewComponent.UseVisualStyleBackColor = true;
            this.btnNewComponent.Click += new EventHandler(this.btnNewComponent_Click);
            this.btnCloneComponent.Enabled = false;
            this.btnCloneComponent.Location = new Point(135, 6);
            this.btnCloneComponent.Name = "btnCloneComponent";
            this.btnCloneComponent.Size = new Size(54, 23);
            this.btnCloneComponent.TabIndex = 1;
            this.btnCloneComponent.Text = "Clone";
            this.btnCloneComponent.UseVisualStyleBackColor = true;
            this.btnCloneComponent.Click += new EventHandler(this.btnCloneComponent_Click);
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new Point(195, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new Size(54, 23);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new EventHandler(this.btnDelete_Click);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(978, 659);
            this.Controls.Add((Control)this.groupBox3);
            this.Controls.Add((Control)this.splitter3);
            this.Controls.Add((Control)this.splitter2);
            this.Controls.Add((Control)this.groupBox4);
            this.Controls.Add((Control)this.splitter1);
            this.Controls.Add((Control)this.groupBox1);
            this.Controls.Add((Control)this.panel1);
            this.Name = nameof(frmAbilityDetails);
            this.Text = nameof(frmAbilityDetails);
            this.Load += new EventHandler(this.frmAbilityDetails_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
        }
    }
}