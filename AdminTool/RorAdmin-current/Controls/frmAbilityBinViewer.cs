﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmAbilityBinViewer
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using MYPLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class frmAbilityBinViewer : BaseViewer, IPersistControl
    {
        private Dictionary<int, ListViewItem> _lviCache = new Dictionary<int, ListViewItem>();
        private AbilityExport _ab = new AbilityExport();
        private AbilityComponentExport _compExp = new AbilityComponentExport();
        private IContainer components = (IContainer)null;
        private MYPManager _manager;
        private MYP.AssetInfo _compAsset;
        private GroupBox groupBox1;
        private RORListView lvKeys;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader4;
        private Panel panel1;
        private TextBox txtEffectID;
        private Label label3;
        private TextBox txtIDFilter;
        private Label label2;
        private TextBox txtNameFilter;
        private Label label1;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private ToolStripMenuItem newToolStripMenuItem;
        private ToolStripMenuItem copyToolStripMenuItem;
        private ToolStripMenuItem pasteToolStripMenuItem;

        public frmAbilityBinViewer()
        {
            this.InitializeComponent();
        }

        public override void LoadEntry(MYPManager manager, MYP.AssetInfo entry)
        {
            base.LoadEntry(manager, entry);
            this.lvKeys.Columns.Clear();
            this._manager = manager;
            this._compAsset = manager.FindAsset("data/bin/abilitycomponentexport.bin", entry.Package, false);
            if (this._compAsset != null)
                this._compExp.Load(manager, (Stream)new MemoryStream(manager.GetAsset(this._compAsset)));
            this._ab.Load(manager, (Stream)new MemoryStream(manager.GetAsset(entry)));
            this.lvKeys.LoadList<AbilityBin>(entry.Name, (ListView)this.lvKeys, this._ab.Abilities);
            this.LoadSettings(Program.UIConfig);
        }

        public void SaveSettings(Config config)
        {
            foreach (ColumnHeader column in this.lvKeys.Columns)
            {
                config[this.lvKeys.Name + "_" + this._entry.Name + "_" + (object)column.Index + "_Width", ""] = (object)column.Width;
                config[this.lvKeys.Name + "_" + this._entry.Name + "_" + (object)column.Index + "_Index", ""] = (object)column.DisplayIndex;
            }
        }

        public void LoadSettings(Config config)
        {
            int defaultValue = 0;
            foreach (ColumnHeader column in this.lvKeys.Columns)
            {
                column.Width = config.GetInt(this.lvKeys.Name + "_" + this._entry.Name + "_" + (object)column.Index + "_Width", 100);
                column.DisplayIndex = config.GetInt(this.lvKeys.Name + "_" + this._entry.Name + "_" + (object)column.Index + "_Index", defaultValue);
                ++defaultValue;
            }
        }

        private void lvKeys_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void lvKeys_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.lvKeys.SelectedIndices.Count <= 0)
                return;
            frmAbilityDetails frmAbilityDetails = new frmAbilityDetails();
            frmAbilityDetails.LoadAbility(this._entry.Package, this.Manager, (AbilityBin)this.lvKeys.SelectedObject, this._entry, this._ab, this._compExp, this._compAsset);
            frmAbilityDetails.Show();
        }

        private void txtNameFilter_TextChanged(object sender, EventArgs ee)
        {
            this.lvKeys.ApplyFilter();
        }

        private List<object> lvKeys_OnFliter(RORListView sender, List<object> items)
        {
            List<object> objectList = new List<object>();
            List<int> intList1 = AdminUtil.CSVToIntList(this.txtIDFilter.Text, (char[])null);
            List<int> intList2 = AdminUtil.CSVToIntList(this.txtEffectID.Text, (char[])null);
            string lower = this.txtNameFilter.Text.ToLower();
            foreach (AbilityBin abilityBin in items)
            {
                if ((intList1.Count == 0 || intList1.Contains((int)abilityBin.A40_AbilityID)) && (intList2.Count == 0 || intList2.Contains((int)abilityBin.A42_EffectID)) && (this.txtNameFilter.Text == "" || abilityBin.Name.ToLower().StartsWith(lower)))
                    objectList.Add((object)abilityBin);
            }
            return objectList;
        }

        private void lvKeys_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.lvKeys.ColumnSort(sender, e);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            this.deleteToolStripMenuItem.Enabled = this.lvKeys.SelectedIndices.Count > 0;
            this.copyToolStripMenuItem.Visible = this.lvKeys.SelectedIndices.Count > 0;
            this.pasteToolStripMenuItem.Visible = Program._abilityClipboard != null;
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.lvKeys.SelectedIndices.Count <= 0 || MessageBox.Show("Delete " + (object)this.lvKeys.SelectedIndices.Count + " abilities?", "Question", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            foreach (int selectedIndex in this.lvKeys.SelectedIndices)
            {
                AbilityBin abilityBin = (AbilityBin)this.lvKeys._filtered[selectedIndex];
                this._ab.Abilities.Remove(abilityBin);
                this.lvKeys._items.Remove((object)abilityBin);
            }
            this.lvKeys._cache.Clear();
            this.lvKeys.VirtualListSize = 0;
            MemoryStream memoryStream = new MemoryStream();
            this._ab.Save((Stream)memoryStream);
            this._manager.UpdateAsset(this._entry, memoryStream.ToArray());
            this.lvKeys.LoadList<AbilityBin>(this._entry.Name, (ListView)this.lvKeys, this._ab.Abilities);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ab.NewAbility(this._manager, this._entry.Package, (AbilityBin)null, this._compExp, this._compExp);
            MemoryStream memoryStream = new MemoryStream();
            this._ab.Save((Stream)memoryStream);
            this._manager.UpdateAsset(this._entry, memoryStream.ToArray());
            this.lvKeys.LoadList<AbilityBin>(this._entry.Name, (ListView)this.lvKeys, this._ab.Abilities);
        }

        private void cloneToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<frmAbilityBinViewer.MYPNode> mypNodeList = new List<frmAbilityBinViewer.MYPNode>();
            foreach (int selectedIndex in this.lvKeys.SelectedIndices)
            {
                AbilityBin abilityBin = (AbilityBin)this.lvKeys._filtered[selectedIndex];
                mypNodeList.Add(new frmAbilityBinViewer.MYPNode()
                {
                    Ability = abilityBin,
                    Manager = this._manager,
                    AbilityExport = this._ab,
                    CompExport = this._compExp,
                    MYP = this._manager.Packages[this._entry.Package]
                });
            }
            Program._abilityClipboard = (object)mypNodeList;
            this.pasteToolStripMenuItem.Visible = true;
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (frmAbilityBinViewer.MYPNode mypNode in (List<frmAbilityBinViewer.MYPNode>)Program._abilityClipboard)
                this._ab.NewAbility(this._manager, this._entry.Package, mypNode.Ability, mypNode.CompExport, this._compExp);
            MemoryStream memoryStream1 = new MemoryStream();
            this._ab.Save((Stream)memoryStream1);
            byte[] array = memoryStream1.ToArray();
            this._entry.Compressed = false;
            this._manager.UpdateAsset(this._entry, array);
            MemoryStream memoryStream2 = new MemoryStream();
            this._compExp.Save((Stream)memoryStream2);
            this._manager.UpdateAsset(this._entry.Package, "data/bin/abilitycomponentexport.bin", memoryStream2.ToArray(), true);
            this.lvKeys.LoadList<AbilityBin>(this._entry.Name, (ListView)this.lvKeys, this._ab.Abilities);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            this.groupBox1 = new GroupBox();
            this.lvKeys = new RORListView();
            this.columnHeader1 = new ColumnHeader();
            this.columnHeader2 = new ColumnHeader();
            this.columnHeader4 = new ColumnHeader();
            this.contextMenuStrip1 = new ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new ToolStripMenuItem();
            this.newToolStripMenuItem = new ToolStripMenuItem();
            this.copyToolStripMenuItem = new ToolStripMenuItem();
            this.pasteToolStripMenuItem = new ToolStripMenuItem();
            this.panel1 = new Panel();
            this.txtEffectID = new TextBox();
            this.label3 = new Label();
            this.txtIDFilter = new TextBox();
            this.label2 = new Label();
            this.txtNameFilter = new TextBox();
            this.label1 = new Label();
            this.groupBox1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            this.groupBox1.Controls.Add((Control)this.lvKeys);
            this.groupBox1.Controls.Add((Control)this.panel1);
            this.groupBox1.Dock = DockStyle.Fill;
            this.groupBox1.Location = new Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(885, 352);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Abilities";
            this.lvKeys.AllowColumnReorder = true;
            this.lvKeys.Columns.AddRange(new ColumnHeader[3]
            {
        this.columnHeader1,
        this.columnHeader2,
        this.columnHeader4
            });
            this.lvKeys.ContextMenuStrip = this.contextMenuStrip1;
            this.lvKeys.Dock = DockStyle.Fill;
            this.lvKeys.FullRowSelect = true;
            this.lvKeys.Location = new Point(3, 93);
            this.lvKeys.Name = "lvKeys";
            this.lvKeys.Size = new Size(879, 256);
            this.lvKeys.TabIndex = 2;
            this.lvKeys.UseCompatibleStateImageBehavior = false;
            this.lvKeys.View = View.Details;
            this.lvKeys.OnFliter += new RORListView.FilterDelegate(this.lvKeys_OnFliter);
            this.lvKeys.ColumnClick += new ColumnClickEventHandler(this.lvKeys_ColumnClick);
            this.lvKeys.SelectedIndexChanged += new EventHandler(this.lvKeys_SelectedIndexChanged);
            this.lvKeys.MouseDoubleClick += new MouseEventHandler(this.lvKeys_MouseDoubleClick);
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 76;
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 100;
            this.columnHeader4.Text = "Description";
            this.columnHeader4.Width = 296;
            this.contextMenuStrip1.Items.AddRange(new ToolStripItem[4]
            {
        (ToolStripItem) this.deleteToolStripMenuItem,
        (ToolStripItem) this.newToolStripMenuItem,
        (ToolStripItem) this.copyToolStripMenuItem,
        (ToolStripItem) this.pasteToolStripMenuItem
            });
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new Size(108, 92);
            this.contextMenuStrip1.Opening += new CancelEventHandler(this.contextMenuStrip1_Opening);
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new Size(107, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new EventHandler(this.deleteToolStripMenuItem_Click);
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new Size(107, 22);
            this.newToolStripMenuItem.Text = "Ne&w";
            this.newToolStripMenuItem.Click += new EventHandler(this.newToolStripMenuItem_Click);
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new Size(107, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Visible = false;
            this.copyToolStripMenuItem.Click += new EventHandler(this.copyToolStripMenuItem_Click);
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new Size(107, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Visible = false;
            this.pasteToolStripMenuItem.Click += new EventHandler(this.pasteToolStripMenuItem_Click);
            this.panel1.Controls.Add((Control)this.txtEffectID);
            this.panel1.Controls.Add((Control)this.label3);
            this.panel1.Controls.Add((Control)this.txtIDFilter);
            this.panel1.Controls.Add((Control)this.label2);
            this.panel1.Controls.Add((Control)this.txtNameFilter);
            this.panel1.Controls.Add((Control)this.label1);
            this.panel1.Dock = DockStyle.Top;
            this.panel1.Location = new Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(879, 77);
            this.panel1.TabIndex = 0;
            this.txtEffectID.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            this.txtEffectID.Location = new Point(46, 53);
            this.txtEffectID.Name = "txtEffectID";
            this.txtEffectID.Size = new Size(830, 20);
            this.txtEffectID.TabIndex = 5;
            this.txtEffectID.TextChanged += new EventHandler(this.txtNameFilter_TextChanged);
            this.label3.AutoSize = true;
            this.label3.Location = new Point(-3, 56);
            this.label3.Name = "label3";
            this.label3.Size = new Size(46, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "EffectID";
            this.txtIDFilter.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            this.txtIDFilter.Location = new Point(46, 27);
            this.txtIDFilter.Name = "txtIDFilter";
            this.txtIDFilter.Size = new Size(830, 20);
            this.txtIDFilter.TabIndex = 3;
            this.txtIDFilter.TextChanged += new EventHandler(this.txtNameFilter_TextChanged);
            this.label2.AutoSize = true;
            this.label2.Location = new Point(23, 30);
            this.label2.Name = "label2";
            this.label2.Size = new Size(18, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "ID";
            this.txtNameFilter.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            this.txtNameFilter.Location = new Point(46, 3);
            this.txtNameFilter.Name = "txtNameFilter";
            this.txtNameFilter.Size = new Size(830, 20);
            this.txtNameFilter.TabIndex = 1;
            this.txtNameFilter.TextChanged += new EventHandler(this.txtNameFilter_TextChanged);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add((Control)this.groupBox1);
            this.Name = nameof(frmAbilityBinViewer);
            this.Size = new Size(885, 352);
            this.groupBox1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
        }

        public class MYPNode
        {
            public MYPManager Manager;
            public MYP MYP;
            public MYP.AssetInfo Asset;
            public AbilityBin Ability;
            public AbilityExport AbilityExport;
            public AbilityComponentExport CompExport;
        }
    }
}