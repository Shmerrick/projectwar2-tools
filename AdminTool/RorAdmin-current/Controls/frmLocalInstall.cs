﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmLocalInstall
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class frmLocalInstall : Form
    {
        private IContainer components = (IContainer)null;
        private Button btnLogin;
        private Button btnCancel;
        private TextBox txtPath;
        private Label label2;
        private TextBox txtName;
        private Label label1;
        private Button btnPickFolder;
        private FolderBrowserDialog folderBrowserDialog1;

        public string InstallName
        {
            get
            {
                return this.txtName.Text;
            }
            set
            {
                this.txtName.Text = value;
            }
        }

        public string Path
        {
            get
            {
                return this.txtPath.Text;
            }
            set
            {
                this.txtPath.Text = value;
            }
        }

        public frmLocalInstall()
        {
            this.InitializeComponent();
        }

        private void btnPickFolder_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() != DialogResult.OK)
                return;
            this.txtPath.Text = this.folderBrowserDialog1.SelectedPath;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (this.txtName.Text.Length == 0)
            {
                int num = (int)MessageBox.Show("Name is required");
                this.txtName.Focus();
            }
            if (this.txtPath.Text.Length == 0)
            {
                int num = (int)MessageBox.Show("Path is required");
                this.txtPath.Focus();
            }
            this.DialogResult = DialogResult.OK;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.btnLogin = new Button();
            this.btnCancel = new Button();
            this.txtPath = new TextBox();
            this.label2 = new Label();
            this.txtName = new TextBox();
            this.label1 = new Label();
            this.btnPickFolder = new Button();
            this.folderBrowserDialog1 = new FolderBrowserDialog();
            this.SuspendLayout();
            this.btnLogin.Anchor = AnchorStyles.Bottom;
            this.btnLogin.Location = new Point(220, 65);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new Size(75, 23);
            this.btnLogin.TabIndex = 13;
            this.btnLogin.Text = "&OK";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new EventHandler(this.btnLogin_Click);
            this.btnCancel.Anchor = AnchorStyles.Bottom;
            this.btnCancel.Location = new Point(139, 65);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(75, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "C&ancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.txtPath.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            this.txtPath.Location = new Point(68, 38);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new Size(289, 20);
            this.txtPath.TabIndex = 11;
            this.label2.AutoSize = true;
            this.label2.Location = new Point(7, 41);
            this.label2.Name = "label2";
            this.label2.Size = new Size(29, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Path";
            this.txtName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            this.txtName.Location = new Point(68, 12);
            this.txtName.Name = "txtName";
            this.txtName.Size = new Size(327, 20);
            this.txtName.TabIndex = 9;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(7, 15);
            this.label1.Name = "label1";
            this.label1.Size = new Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Name";
            this.btnPickFolder.Location = new Point(362, 38);
            this.btnPickFolder.Name = "btnPickFolder";
            this.btnPickFolder.Size = new Size(33, 23);
            this.btnPickFolder.TabIndex = 14;
            this.btnPickFolder.Text = "...";
            this.btnPickFolder.UseVisualStyleBackColor = true;
            this.btnPickFolder.Click += new EventHandler(this.btnPickFolder_Click);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(407, 100);
            this.Controls.Add((Control)this.btnPickFolder);
            this.Controls.Add((Control)this.btnLogin);
            this.Controls.Add((Control)this.btnCancel);
            this.Controls.Add((Control)this.txtPath);
            this.Controls.Add((Control)this.label2);
            this.Controls.Add((Control)this.txtName);
            this.Controls.Add((Control)this.label1);
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.Name = nameof(frmLocalInstall);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Pick Local Installation";
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}