﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmMYPDiff
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class frmMYPDiff : Form
    {
        private IContainer components = (IContainer)null;
        private MYP _a;
        private MYP _b;
        private SplitContainer splitContainer1;
        private GroupBox gpDiff;
        private SplitContainer splitContainer2;
        private ListView lvDiff;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader3;
        private ColumnHeader columnHeader4;
        private ColumnHeader columnHeader5;
        private ColumnHeader columnHeader6;
        private GroupBox gpAdded;
        private GroupBox gpMissing;
        private ListView lvMiss;
        private ColumnHeader columnHeader14;
        private ColumnHeader columnHeader15;
        private ColumnHeader columnHeader16;
        private ListView lvAdd;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader7;
        private ColumnHeader columnHeader8;
        private ColumnHeader columnHeader9;
        private ColumnHeader columnHeader10;
        private ColumnHeader columnHeader11;
        private ColumnHeader columnHeader12;

        public frmMYPDiff()
        {
            this.InitializeComponent();
        }

        private void frmMYPDiff_Load(object sender, EventArgs e)
        {
        }

        public void LoadCompare(MYP mypA, MYP mypB)
        {
            this.lvDiff.Items.Clear();
            this.lvMiss.Items.Clear();
            this.lvAdd.Items.Clear();
            List<ListViewItem> listViewItemList1 = new List<ListViewItem>();
            List<ListViewItem> listViewItemList2 = new List<ListViewItem>();
            List<ListViewItem> listViewItemList3 = new List<ListViewItem>();
            ulong hash;
            bool compressed;
            uint num;
            foreach (MYP.AssetInfo assetInfo in mypA.Assets.Values)
            {
                if (mypB.Assets.ContainsKey(assetInfo.Hash))
                {
                    MYP.AssetInfo asset = mypB.Assets[assetInfo.Hash];
                    if (assetInfo.Compressed != asset.Compressed || (int)assetInfo.UncompressedSize != (int)asset.UncompressedSize)
                    {
                        string name = assetInfo.Name;
                        if (name == null)
                        {
                            hash = assetInfo.Hash;
                            name = hash.ToString();
                        }
                        ListViewItem listViewItem = new ListViewItem(name);
                        ListViewItem.ListViewSubItemCollection subItems1 = listViewItem.SubItems;
                        compressed = assetInfo.Compressed;
                        string text1 = compressed.ToString();
                        subItems1.Add(text1);
                        ListViewItem.ListViewSubItemCollection subItems2 = listViewItem.SubItems;
                        compressed = asset.Compressed;
                        string text2 = compressed.ToString();
                        subItems2.Add(text2);
                        ListViewItem.ListViewSubItemCollection subItems3 = listViewItem.SubItems;
                        num = assetInfo.UncompressedSize;
                        string text3 = num.ToString();
                        subItems3.Add(text3);
                        ListViewItem.ListViewSubItemCollection subItems4 = listViewItem.SubItems;
                        num = asset.UncompressedSize;
                        string text4 = num.ToString();
                        subItems4.Add(text4);
                        ListViewItem.ListViewSubItemCollection subItems5 = listViewItem.SubItems;
                        num = assetInfo.CompressedSize;
                        string text5 = num.ToString();
                        subItems5.Add(text5);
                        ListViewItem.ListViewSubItemCollection subItems6 = listViewItem.SubItems;
                        num = asset.CompressedSize;
                        string text6 = num.ToString();
                        subItems6.Add(text6);
                        ListViewItem.ListViewSubItemCollection subItems7 = listViewItem.SubItems;
                        num = assetInfo.CRC;
                        string text7 = num.ToString();
                        subItems7.Add(text7);
                        ListViewItem.ListViewSubItemCollection subItems8 = listViewItem.SubItems;
                        num = asset.CRC;
                        string text8 = num.ToString();
                        subItems8.Add(text8);
                        listViewItemList1.Add(listViewItem);
                    }
                }
                else
                {
                    string name = assetInfo.Name;
                    if (name == null)
                    {
                        hash = assetInfo.Hash;
                        name = hash.ToString();
                    }
                    ListViewItem listViewItem = new ListViewItem(name);
                    ListViewItem.ListViewSubItemCollection subItems1 = listViewItem.SubItems;
                    compressed = assetInfo.Compressed;
                    string text1 = compressed.ToString();
                    subItems1.Add(text1);
                    ListViewItem.ListViewSubItemCollection subItems2 = listViewItem.SubItems;
                    num = assetInfo.UncompressedSize;
                    string text2 = num.ToString();
                    subItems2.Add(text2);
                    listViewItemList2.Add(listViewItem);
                }
            }
            foreach (MYP.AssetInfo assetInfo in mypB.Assets.Values)
            {
                if (!mypA.Assets.ContainsKey(assetInfo.Hash))
                    listViewItemList3.Add(new ListViewItem(assetInfo.Name ?? assetInfo.Hash.ToString())
                    {
                        SubItems = {
              assetInfo.Compressed.ToString(),
              assetInfo.UncompressedSize.ToString()
            }
                    });
            }
            this.lvDiff.Items.AddRange(listViewItemList1.ToArray());
            this.lvMiss.Items.AddRange(listViewItemList2.ToArray());
            this.lvAdd.Items.AddRange(listViewItemList3.ToArray());
            this.gpDiff.Text = "Different (" + (object)listViewItemList1.Count + ")";
            this.gpMissing.Text = "Missing in " + mypB.Package.ToString() + " (" + (object)listViewItemList2.Count + ")";
            this.gpAdded.Text = "Missing in " + mypA.Package.ToString() + " (" + (object)listViewItemList3.Count + ")";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.splitContainer1 = new SplitContainer();
            this.splitContainer2 = new SplitContainer();
            this.gpDiff = new GroupBox();
            this.gpAdded = new GroupBox();
            this.gpMissing = new GroupBox();
            this.lvDiff = new ListView();
            this.columnHeader2 = new ColumnHeader();
            this.columnHeader3 = new ColumnHeader();
            this.columnHeader4 = new ColumnHeader();
            this.columnHeader5 = new ColumnHeader();
            this.columnHeader6 = new ColumnHeader();
            this.lvMiss = new ListView();
            this.columnHeader14 = new ColumnHeader();
            this.columnHeader15 = new ColumnHeader();
            this.columnHeader16 = new ColumnHeader();
            this.lvAdd = new ListView();
            this.columnHeader1 = new ColumnHeader();
            this.columnHeader7 = new ColumnHeader();
            this.columnHeader8 = new ColumnHeader();
            this.columnHeader9 = new ColumnHeader();
            this.columnHeader10 = new ColumnHeader();
            this.columnHeader11 = new ColumnHeader();
            this.columnHeader12 = new ColumnHeader();
            this.splitContainer1.BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.gpDiff.SuspendLayout();
            this.gpAdded.SuspendLayout();
            this.gpMissing.SuspendLayout();
            this.SuspendLayout();
            this.splitContainer1.Dock = DockStyle.Fill;
            this.splitContainer1.Location = new Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = Orientation.Horizontal;
            this.splitContainer1.Panel1.Controls.Add((Control)this.gpDiff);
            this.splitContainer1.Panel2.Controls.Add((Control)this.splitContainer2);
            this.splitContainer1.Size = new Size(936, 550);
            this.splitContainer1.SplitterDistance = 178;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer2.Dock = DockStyle.Fill;
            this.splitContainer2.Location = new Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = Orientation.Horizontal;
            this.splitContainer2.Panel1.Controls.Add((Control)this.gpAdded);
            this.splitContainer2.Panel2.Controls.Add((Control)this.gpMissing);
            this.splitContainer2.Size = new Size(936, 368);
            this.splitContainer2.SplitterDistance = 171;
            this.splitContainer2.TabIndex = 1;
            this.gpDiff.Controls.Add((Control)this.lvDiff);
            this.gpDiff.Dock = DockStyle.Fill;
            this.gpDiff.Location = new Point(0, 0);
            this.gpDiff.Name = "gpDiff";
            this.gpDiff.Size = new Size(936, 178);
            this.gpDiff.TabIndex = 0;
            this.gpDiff.TabStop = false;
            this.gpDiff.Text = "Different";
            this.gpAdded.Controls.Add((Control)this.lvAdd);
            this.gpAdded.Dock = DockStyle.Fill;
            this.gpAdded.Location = new Point(0, 0);
            this.gpAdded.Name = "gpAdded";
            this.gpAdded.Size = new Size(936, 171);
            this.gpAdded.TabIndex = 0;
            this.gpAdded.TabStop = false;
            this.gpAdded.Text = "Added";
            this.gpMissing.Controls.Add((Control)this.lvMiss);
            this.gpMissing.Dock = DockStyle.Fill;
            this.gpMissing.Location = new Point(0, 0);
            this.gpMissing.Name = "gpMissing";
            this.gpMissing.Size = new Size(936, 193);
            this.gpMissing.TabIndex = 1;
            this.gpMissing.TabStop = false;
            this.gpMissing.Text = "Missing";
            this.lvDiff.Columns.AddRange(new ColumnHeader[9]
            {
        this.columnHeader2,
        this.columnHeader3,
        this.columnHeader4,
        this.columnHeader5,
        this.columnHeader6,
        this.columnHeader9,
        this.columnHeader10,
        this.columnHeader11,
        this.columnHeader12
            });
            this.lvDiff.Dock = DockStyle.Fill;
            this.lvDiff.FullRowSelect = true;
            this.lvDiff.Location = new Point(3, 16);
            this.lvDiff.Name = "lvDiff";
            this.lvDiff.Size = new Size(930, 159);
            this.lvDiff.TabIndex = 0;
            this.lvDiff.UseCompatibleStateImageBehavior = false;
            this.lvDiff.View = View.Details;
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 195;
            this.columnHeader3.Text = "A_Compressed";
            this.columnHeader3.Width = 110;
            this.columnHeader4.Text = "B_Compressed";
            this.columnHeader4.Width = 128;
            this.columnHeader5.Text = "A_Size";
            this.columnHeader5.Width = 113;
            this.columnHeader6.Text = "B_Size";
            this.columnHeader6.Width = 96;
            this.lvMiss.Columns.AddRange(new ColumnHeader[3]
            {
        this.columnHeader14,
        this.columnHeader15,
        this.columnHeader16
            });
            this.lvMiss.Dock = DockStyle.Fill;
            this.lvMiss.FullRowSelect = true;
            this.lvMiss.Location = new Point(3, 16);
            this.lvMiss.Name = "lvMiss";
            this.lvMiss.Size = new Size(930, 174);
            this.lvMiss.TabIndex = 1;
            this.lvMiss.UseCompatibleStateImageBehavior = false;
            this.lvMiss.View = View.Details;
            this.columnHeader14.Text = "Name";
            this.columnHeader14.Width = 195;
            this.columnHeader15.Text = "Compressed";
            this.columnHeader15.Width = 110;
            this.columnHeader16.Text = "Size";
            this.columnHeader16.Width = 128;
            this.lvAdd.Columns.AddRange(new ColumnHeader[3]
            {
        this.columnHeader1,
        this.columnHeader7,
        this.columnHeader8
            });
            this.lvAdd.Dock = DockStyle.Fill;
            this.lvAdd.FullRowSelect = true;
            this.lvAdd.Location = new Point(3, 16);
            this.lvAdd.Name = "lvAdd";
            this.lvAdd.Size = new Size(930, 152);
            this.lvAdd.TabIndex = 2;
            this.lvAdd.UseCompatibleStateImageBehavior = false;
            this.lvAdd.View = View.Details;
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 195;
            this.columnHeader7.Text = "Compressed";
            this.columnHeader7.Width = 110;
            this.columnHeader8.Text = "Size";
            this.columnHeader8.Width = 128;
            this.columnHeader9.Text = "A_CompressedSize";
            this.columnHeader9.Width = 123;
            this.columnHeader10.Text = "B_CompressedSize";
            this.columnHeader10.Width = (int)sbyte.MaxValue;
            this.columnHeader11.Text = "ACRC";
            this.columnHeader12.Text = "BCRC";
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(936, 550);
            this.Controls.Add((Control)this.splitContainer1);
            this.Name = nameof(frmMYPDiff);
            this.Text = nameof(frmMYPDiff);
            this.Load += new EventHandler(this.frmMYPDiff_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.gpDiff.ResumeLayout(false);
            this.gpAdded.ResumeLayout(false);
            this.gpMissing.ResumeLayout(false);
            this.ResumeLayout(false);
        }
    }
}