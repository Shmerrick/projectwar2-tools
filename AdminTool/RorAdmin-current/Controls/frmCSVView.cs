﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmCSVView
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class frmCSVView : BaseViewer
    {
        private IContainer components = (IContainer)null;
        private CSVGrid csvGrid1;

        public frmCSVView()
        {
            this.InitializeComponent();
        }

        public override bool Changed
        {
            get
            {
                return this.csvGrid1.Modified;
            }
        }

        public override void LoadEntry(MYPManager manager, MYP.AssetInfo entry)
        {
            base.LoadEntry(manager, entry);
            this.csvGrid1.LoadCSV(manager.GetAsset(entry));
        }

        public override void Save()
        {
            this.Manager.UpdateAsset(this._entry, Encoding.ASCII.GetBytes(this.csvGrid1.CSV.ToText()));
        }

        public override void BeforeUnload()
        {
            List<int> colWidths = this.csvGrid1.GetColWidths();
            for (int index = 0; index < colWidths.Count; ++index)
                Program.UIConfig[this.Name + this._entry.Name + "_ColumnWidth_" + (object)index, ""] = (object)colWidths[index];
        }

        private void csvGrid1_OnGetColumnWidth(CSVGrid grid, int colIndex, out int width)
        {
            width = Program.UIConfig.GetInt(this.Name + this._entry.Name + "_ColumnWidth_" + (object)colIndex, 0);
        }

        private void csvGrid1_OnChanged()
        {
            this.Save();
        }

        private void InitializeComponent()
        {
            this.csvGrid1 = new CSVGrid();
            this.SuspendLayout();
            this.csvGrid1.Dock = DockStyle.Fill;
            this.csvGrid1.Location = new Point(0, 0);
            this.csvGrid1.Name = "csvGrid1";
            this.csvGrid1.Size = new Size(820, 497);
            this.csvGrid1.TabIndex = 2;
            this.csvGrid1.OnGetColumnWidth += new CSVGrid.ColumnInfoDelegate(this.csvGrid1_OnGetColumnWidth);
            this.csvGrid1.OnChanged += new CSVGrid.ChangeDelegate(this.csvGrid1_OnChanged);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add((Control)this.csvGrid1);
            this.Name = nameof(frmCSVView);
            this.ResumeLayout(false);
        }
    }
}