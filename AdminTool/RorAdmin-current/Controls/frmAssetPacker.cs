﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmAssetPacker
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class frmAssetPacker : Form
    {
        private IContainer components = (IContainer)null;
        private Label label1;
        private TextBox txtPath;
        private Button btnPick;
        private Button btnAdd;
        private Panel panel1;
        private Button btnCancel;
        private Button btnOK;
        private ListView lvAssets;

        public frmAssetPacker()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.label1 = new Label();
            this.txtPath = new TextBox();
            this.btnPick = new Button();
            this.btnAdd = new Button();
            this.panel1 = new Panel();
            this.btnCancel = new Button();
            this.btnOK = new Button();
            this.lvAssets = new ListView();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            this.label1.AutoSize = true;
            this.label1.Location = new Point(17, 19);
            this.label1.Name = "label1";
            this.label1.Size = new Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Path";
            this.txtPath.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            this.txtPath.Location = new Point(52, 15);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new Size(508, 20);
            this.txtPath.TabIndex = 1;
            this.btnPick.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.btnPick.Location = new Point(566, 14);
            this.btnPick.Name = "btnPick";
            this.btnPick.Size = new Size(32, 23);
            this.btnPick.TabIndex = 2;
            this.btnPick.Text = "...";
            this.btnPick.UseVisualStyleBackColor = true;
            this.btnAdd.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.btnAdd.Location = new Point(602, 14);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new Size(42, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.panel1.Controls.Add((Control)this.btnCancel);
            this.panel1.Controls.Add((Control)this.btnOK);
            this.panel1.Dock = DockStyle.Bottom;
            this.panel1.Location = new Point(0, 318);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(658, 31);
            this.panel1.TabIndex = 4;
            this.btnCancel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.btnCancel.Location = new Point(499, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnOK.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.btnOK.Location = new Point(580, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "&Done";
            this.btnOK.UseVisualStyleBackColor = true;
            this.lvAssets.Location = new Point(0, 35);
            this.lvAssets.Name = "lvAssets";
            this.lvAssets.Size = new Size(658, 280);
            this.lvAssets.TabIndex = 5;
            this.lvAssets.UseCompatibleStateImageBehavior = false;
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(658, 349);
            this.Controls.Add((Control)this.lvAssets);
            this.Controls.Add((Control)this.panel1);
            this.Controls.Add((Control)this.btnAdd);
            this.Controls.Add((Control)this.btnPick);
            this.Controls.Add((Control)this.txtPath);
            this.Controls.Add((Control)this.label1);
            this.Name = nameof(frmAssetPacker);
            this.Text = nameof(frmAssetPacker);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}