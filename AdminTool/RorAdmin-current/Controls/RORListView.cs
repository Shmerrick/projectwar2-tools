﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.RORListView
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class RORListView : ListView
    {
        public Dictionary<int, ListViewItem> _cache = new Dictionary<int, ListViewItem>();
        public List<object> _items = (List<object>)null;
        public List<object> _filtered = (List<object>)null;
        private Dictionary<object, List<object>> _itemValues = new Dictionary<object, List<object>>();
        private PropertyInfo[] _props = (PropertyInfo[])null;
        private RORListView.ListViewColumnSorter componentSorter = new RORListView.ListViewColumnSorter();
        private RORListView.ListViewColumnSorter abilitySorter = new RORListView.ListViewColumnSorter();

        public event RORListView.FilterDelegate OnFliter;

        public object SelectedObject
        {
            get
            {
                if (this.SelectedIndices.Count > 0)
                    return this._filtered[this.SelectedIndices[0]];
                return (object)null;
            }
        }

        public void ColumnSort(object sender, ColumnClickEventArgs e)
        {
            if (this.Columns[e.Column].Index == 0)
                return;
            this.componentSorter.SortType = (System.Type)this.Columns[e.Column].Tag;
            if (e.Column == this.componentSorter.SortColumn + 1)
            {
                this.componentSorter.Order = this.componentSorter.Order != SortOrder.Ascending ? SortOrder.Ascending : SortOrder.Descending;
            }
            else
            {
                this.componentSorter.SortColumn = e.Column - 1;
                this.componentSorter.Order = SortOrder.Ascending;
            }
            this.ApplyFilter();
        }

        public void LoadList<T>(string assetName, ListView list, List<T> items)
        {
            this.ListViewItemSorter = (IComparer)this.abilitySorter;
            this._items = items.Select<T, object>((Func<T, object>)(e => (object)e)).ToList<object>();
            this._filtered = this._items;
            this._cache.Clear();
            List<int> intList = new List<int>();
            foreach (ColumnHeader column in list.Columns)
                intList.Add(column.Width);
            list.Items.Clear();
            this._itemValues.Clear();
            if (items.Count == 0)
                return;
            list.VirtualMode = true;
            this._props = items[0].GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            foreach (object index in this._items)
            {
                this._itemValues[index] = new List<object>();
                foreach (PropertyInfo prop in this._props)
                    this._itemValues[index].Add(prop.GetValue(index));
            }
            if (list.Columns.Count == 0)
            {
                list.Columns.Add("Index");
                foreach (PropertyInfo prop in this._props)
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = prop.Name;
                    list.Columns.Add(columnHeader);
                    columnHeader.Tag = (object)prop.PropertyType;
                }
            }
            this.ApplyFilter();
        }

        protected override void OnCacheVirtualItems(CacheVirtualItemsEventArgs e)
        {
            base.OnCacheVirtualItems(e);
        }

        private int Compare(object a, object b)
        {
            int num1;
            if (this._itemValues[a][this.componentSorter.SortColumn].GetType() == typeof(string))
            {
                num1 = string.Compare(this._itemValues[a][this.componentSorter.SortColumn]?.ToString() ?? "", this._itemValues[b][this.componentSorter.SortColumn]?.ToString() ?? "", true);
            }
            else
            {
                double num2 = (double)Convert.ChangeType(this._itemValues[a][this.componentSorter.SortColumn], typeof(double));
                double num3 = (double)Convert.ChangeType(this._itemValues[b][this.componentSorter.SortColumn], typeof(double));
                num1 = num2 <= num3 ? (num2 >= num3 ? 0 : -1) : 1;
            }
            if (this.componentSorter.Order == SortOrder.Descending)
                num1 = -num1;
            return num1;
        }

        public void ApplyFilter()
        {
            this._cache.Clear();
            this.Items.Clear();
            if (this.OnFliter != null)
            {
                this._filtered = this.OnFliter(this, this._items);
                this._filtered.ToArray();
                this._filtered.Sort(new Comparison<object>(this.Compare));
            }
            this.VirtualListSize = this._filtered.Count;
            this.Invalidate();
        }

        private static ListViewItem GetLVI(int index, object item, PropertyInfo[] props)
        {
            Color color = Color.FromArgb(240, 240, 240);
            object obj1 = props[0].GetValue(item);
            ListViewItem listViewItem = new ListViewItem(index.ToString());
            listViewItem.Tag = item;
            listViewItem.SubItems[0].Tag = obj1;
            if (props[0].PropertyType.IsArray)
                listViewItem.SubItems[listViewItem.SubItems.Count - 1].Tag = (object)obj1.ToString();
            for (int index1 = 0; index1 < props.Length; ++index1)
            {
                object obj2 = props[index1].GetValue(item);
                if (obj2 != null)
                {
                    string text = obj2.ToString();
                    if (props[index1].PropertyType == typeof(byte[]))
                    {
                        text = "";
                        foreach (byte num in (byte[])props[index1].GetValue(item))
                            text = text + num.ToString().PadLeft(3, ' ') + " ";
                    }
                    else if (props[index1].PropertyType == typeof(ushort[]))
                    {
                        text = "";
                        foreach (ushort num in (ushort[])props[index1].GetValue(item))
                            text = text + num.ToString() + " ";
                    }
                    else if (props[index1].PropertyType == typeof(short[]))
                    {
                        text = "";
                        foreach (short num in (short[])props[index1].GetValue(item))
                            text = text + num.ToString() + " ";
                    }
                    else if (props[index1].PropertyType == typeof(uint[]))
                    {
                        text = "";
                        foreach (uint num in (uint[])props[index1].GetValue(item))
                            text = text + num.ToString() + " ";
                    }
                    else if (props[index1].PropertyType == typeof(float[]))
                    {
                        text = "";
                        foreach (float num in (float[])props[index1].GetValue(item))
                            text = text + num.ToString() + " ";
                    }
                    if (props[index1].PropertyType == typeof(List<object>))
                    {
                        text = "";
                        foreach (object obj3 in (List<object>)props[index1].GetValue(item))
                        {
                            if (obj3 is uint)
                            {
                                uint num = (uint)obj3;
                                text = num == 2863311530U ? text + "| " : text + num.ToString() + " ";
                            }
                            else if (obj3 is byte)
                            {
                                byte num = (byte)obj3;
                                text = text + num.ToString() + " ";
                            }
                        }
                    }
                    listViewItem.SubItems.Add(text);
                    listViewItem.SubItems[listViewItem.SubItems.Count - 1].Tag = obj2;
                    if (props[index1].PropertyType.IsArray)
                        listViewItem.SubItems[listViewItem.SubItems.Count - 1].Tag = (object)text;
                }
            }
            if (index % 2 == 0)
                listViewItem.BackColor = color;
            return listViewItem;
        }

        protected override void OnRetrieveVirtualItem(RetrieveVirtualItemEventArgs e)
        {
            base.OnRetrieveVirtualItem(e);
            if (!this._cache.ContainsKey(e.ItemIndex))
                this._cache[e.ItemIndex] = RORListView.GetLVI(e.ItemIndex, this._filtered[e.ItemIndex], this._props);
            e.Item = this._cache[e.ItemIndex];
        }

        public delegate List<object> FilterDelegate(RORListView sender, List<object> items);

        public class ListViewColumnSorter : IComparer
        {
            private int ColumnToSort;
            private SortOrder OrderOfSort;
            private CaseInsensitiveComparer ObjectCompare;

            public ListViewColumnSorter()
            {
                this.ColumnToSort = 0;
                this.OrderOfSort = SortOrder.None;
                this.ObjectCompare = new CaseInsensitiveComparer();
            }

            public int Compare(object x, object y)
            {
                ListViewItem listViewItem1 = (ListViewItem)x;
                ListViewItem listViewItem2 = (ListViewItem)y;
                int num = !(this.SortType == typeof(ushort)) ? this.ObjectCompare.Compare(listViewItem1.SubItems[this.ColumnToSort].Tag, listViewItem2.SubItems[this.ColumnToSort].Tag) : this.ObjectCompare.Compare(listViewItem1.SubItems[this.ColumnToSort].Tag, listViewItem2.SubItems[this.ColumnToSort].Tag);
                if (this.OrderOfSort == SortOrder.Ascending)
                    return num;
                if (this.OrderOfSort == SortOrder.Descending)
                    return -num;
                return 0;
            }

            public System.Type SortType { get; set; }

            public int SortColumn
            {
                set
                {
                    this.ColumnToSort = value;
                }
                get
                {
                    return this.ColumnToSort;
                }
            }

            public SortOrder Order
            {
                set
                {
                    this.OrderOfSort = value;
                }
                get
                {
                    return this.OrderOfSort;
                }
            }
        }
    }
}