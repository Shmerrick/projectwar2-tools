﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.BaseViewer
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class BaseViewer : UserControl, IViewerControl
    {
        private IContainer components = (IContainer)null;
        protected MYP.AssetInfo _entry;
        public MYPManager Manager;

        public BaseViewer()
        {
            this.InitializeComponent();
        }

        public virtual bool Changed
        {
            get
            {
                return false;
            }
        }

        public virtual void LoadEntry(MYPManager manager, MYP.AssetInfo entry)
        {
            this.Manager = manager;
            this._entry = entry;
        }

        public virtual void Save()
        {
            throw new Exception("not implemented");
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Save();
        }

        public virtual void BeforeUnload()
        {
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Name = nameof(BaseViewer);
            this.Size = new Size(820, 497);
            this.ResumeLayout(false);
        }
    }
}