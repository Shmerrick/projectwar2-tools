﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.LocalizatedItem
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class LocalizatedItem : UserControl
    {
        private bool _disableUpdates = false;
        private IContainer components = (IContainer)null;
        private Language _language;
        private GroupBox groupBox1;
        private TextBox txtLine;

        public bool Changed { get; set; }

        public Language Language
        {
            set
            {
                this._language = value;
                this.groupBox1.Text = value.ToString();
            }
            get
            {
                return this._language;
            }
        }

        public string Value
        {
            get
            {
                return this.txtLine.Text;
            }
            set
            {
                this._disableUpdates = true;
                this.txtLine.Text = value;
                this._disableUpdates = false;
            }
        }

        public string Filename { get; set; }

        public int RowID { get; set; }

        public LocalizatedItem()
        {
            this.InitializeComponent();
        }

        public event LocalizatedItem.ChangeDelegate OnChange;

        private void txtLine_TextChanged(object sender, EventArgs e)
        {
            if (this._disableUpdates)
                return;
            this.Changed = true;
            if (this.OnChange == null)
                return;
            this.OnChange(this);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.groupBox1 = new GroupBox();
            this.txtLine = new TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            this.groupBox1.Controls.Add((Control)this.txtLine);
            this.groupBox1.Dock = DockStyle.Fill;
            this.groupBox1.Location = new Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(349, 70);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            this.txtLine.Dock = DockStyle.Fill;
            this.txtLine.Location = new Point(3, 16);
            this.txtLine.Multiline = true;
            this.txtLine.Name = "txtLine";
            this.txtLine.Size = new Size(343, 51);
            this.txtLine.TabIndex = 0;
            this.txtLine.TextChanged += new EventHandler(this.txtLine_TextChanged);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add((Control)this.groupBox1);
            this.Name = nameof(LocalizatedItem);
            this.Size = new Size(349, 70);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
        }

        public delegate void ChangeDelegate(LocalizatedItem item);
    }
}