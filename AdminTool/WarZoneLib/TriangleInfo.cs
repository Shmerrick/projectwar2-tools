﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.TriangleInfo
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

namespace WarZoneLib
{
    public struct TriangleInfo
    {
        public uint i0;
        public uint i1;
        public uint i2;
        public uint fixture;
    }
}