﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.CollisionInfo
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

using System.Collections.Generic;

namespace WarZoneLib
{
    public class CollisionInfo
    {
        public Dictionary<uint, FixtureInfo> Fixtures = new Dictionary<uint, FixtureInfo>();
        public Vector3[] Vertices;
        public TriangleInfo[] Triangles;
        public BSPNodeInfo BSP;
    }
}