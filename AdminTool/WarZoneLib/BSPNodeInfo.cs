﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.BSPNodeInfo
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

namespace WarZoneLib
{
    public class BSPNodeInfo
    {
        public Plane P;
        public BSPNodeInfo Back;
        public BSPNodeInfo Front;
        public int[] Triangles;

        public bool IsLeaf
        {
            get
            {
                return this.Back == null;
            }
        }
    }
}