﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.ZoneInfo
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

namespace WarZoneLib
{
    public class ZoneInfo
    {
        public uint ID;
        public uint OffsetX;
        public uint OffsetY;
        public CollisionInfo Collision;
        public string Name;
        public bool Enabled;
        public TerrainInfo Terrain;
        public RegionInfo Region;

        public ZoneInfo()
        {
            this.Enabled = true;
        }
    }
}