﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.Timer
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

using System.Runtime.InteropServices;

namespace WarZoneLib
{
    internal class Timer
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool QueryPerformanceCounter(out long lpPerfCounter);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool QueryPerformanceFrequency(out long lpPerfFrequency);

        public static long Value
        {
            get
            {
                long lpPerfCounter;
                if (!Timer.QueryPerformanceCounter(out lpPerfCounter))
                    return 0;
                return lpPerfCounter;
            }
        }

        public static long Frequency
        {
            get
            {
                long lpPerfFrequency;
                if (!Timer.QueryPerformanceFrequency(out lpPerfFrequency))
                    return 0;
                return lpPerfFrequency;
            }
        }
    }
}