﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.MYPManager
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

using System;
using System.Collections.Generic;
using System.IO;

namespace WarZoneLib
{
    public class MYPManager
    {
        public Dictionary<MythicPackage, MYP> Archives = new Dictionary<MythicPackage, MYP>();
        public static MYPManager Instance;

        public MYPManager(string folder)
        {
            foreach (string file in Directory.GetFiles(folder, "*.myp"))
            {
                string upper = Path.GetFileNameWithoutExtension(file).ToUpper();
                MythicPackage package = MythicPackage.ART;
                ref MythicPackage local = ref package;
                if (Enum.TryParse<MythicPackage>(upper, out local))
                {
                    FileStream fileStream = File.Open(file, FileMode.Open, FileAccess.Read);
                    this.Archives[package] = new MYP(package, (Stream)fileStream);
                }
            }
        }

        public bool HasAsset(string assetName)
        {
            long key = MYP.HashWAR(assetName);
            foreach (MYP myp in this.Archives.Values)
            {
                if (myp.Enteries.ContainsKey(key))
                    return true;
            }
            return false;
        }

        public byte[] GetAsset(string assetName)
        {
            long key = MYP.HashWAR(assetName);
            foreach (MYP myp in this.Archives.Values)
            {
                if (myp.Enteries.ContainsKey(key))
                    return this.GetAsset(myp.Enteries[key]);
            }
            return (byte[])null;
        }

        public Stream GetAssetStream(string assetName)
        {
            long key = MYP.HashWAR(assetName);
            foreach (MYP myp in this.Archives.Values)
            {
                if (myp.Enteries.ContainsKey(key))
                    return (Stream)new MemoryStream(this.GetAsset(myp.Enteries[key]));
            }
            return (Stream)null;
        }

        public byte[] GetAsset(MYP.MFTEntry entry)
        {
            return entry.Archive.ReadFile(entry);
        }
    }
}