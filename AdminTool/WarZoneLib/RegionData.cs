﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.RegionData
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WarZoneLib
{
    public class RegionData
    {
        public static RegionInfo[] _regions = new RegionInfo[512];
        public static ZoneInfo[] _zones = new ZoneInfo[512];
        public static bool DebugEnable = false;
        private static bool _enabled = true;
        private const float Epsilon = 1E-05f;
        private const int FileFormatVersion = 2;

        public static RegionData.LoadResult LoadDataFile(
          uint regionID,
          string pathToFile,
          bool includeTerrain = false)
        {
            try
            {
                using (FileStream fileStream = File.OpenRead(pathToFile))
                    return RegionData.LoadDataStream((Stream)fileStream, includeTerrain);
            }
            catch (FileNotFoundException ex)
            {
                return RegionData.LoadResult.NotFound;
            }
            catch (NotImplementedException ex)
            {
                return RegionData.LoadResult.NotImplemented;
            }
            catch
            {
                return RegionData.LoadResult.InternalError;
            }
        }

        public static bool HasRegion(ushort regionID)
        {
            if (RegionData._regions[(int)regionID] != null)
                return RegionData._regions[(int)regionID].ID > 0U;
            return false;
        }

        public static RegionData.LoadResult LoadDataStream(Stream stream, bool includeTerrain = false)
        {
            try
            {
                BinaryReader reader = new BinaryReader(stream);
                byte num1 = reader.ReadByte();
                byte num2 = reader.ReadByte();
                byte num3 = reader.ReadByte();
                if (num1 != (byte)82 || num2 != (byte)79 || num3 != (byte)82)
                    return RegionData.LoadResult.Invalid;
                if (reader.ReadByte() != (byte)2)
                    return RegionData.LoadResult.VersionMismatch;
                byte num4 = reader.ReadByte();
                stream.Seek((long)num4, SeekOrigin.Begin);
                RegionInfo regionInfo = (RegionInfo)null;
                while (stream.Position < stream.Length)
                {
                    ChunkType chunkType = (ChunkType)reader.ReadUInt32();
                    uint num5 = reader.ReadUInt32();
                    long offset = stream.Position + (long)num5;
                    switch (chunkType)
                    {
                        case ChunkType.Terrain:
                            if (includeTerrain)
                            {
                                RegionData.LoadTerrainChunk(reader);
                                break;
                            }
                            break;

                        case ChunkType.Collision:
                            RegionData.LoadCollisionChunk(reader);
                            break;

                        case ChunkType.BSP:
                            RegionData.LoadBSPChunk(reader);
                            break;

                        case ChunkType.Region:
                            regionInfo = RegionData.LoadRegionChunk(reader);
                            break;
                    }
                    stream.Seek(offset, SeekOrigin.Begin);
                }
                RegionData._regions[(int)regionInfo.ID] = regionInfo;
                return RegionData.LoadResult.OK;
            }
            catch (Exception ex)
            {
                return RegionData.LoadResult.InternalError;
            }
        }

        private static void LoadTerrainChunk(BinaryReader reader)
        {
            uint num1 = reader.ReadUInt32();
            if (RegionData._zones[(int)num1] == null)
                RegionData._zones[(int)num1] = new ZoneInfo();
            uint num2 = reader.ReadUInt32();
            uint num3 = reader.ReadUInt32();
            TerrainInfo terrainInfo = new TerrainInfo()
            {
                Height = num3,
                Width = num2,
                HeightMap = new ushort[(int)num2, (int)num3]
            };
            for (int index1 = 0; (long)index1 < (long)num3; ++index1)
            {
                for (int index2 = 0; (long)index2 < (long)num2; ++index2)
                    terrainInfo.HeightMap[index2, index1] = (ushort)reader.ReadUInt32();
            }
            RegionData._zones[(int)num1].Terrain = terrainInfo;
        }

        public static bool SetZoneEnabled(int zoneId, bool enabled)
        {
            return true;
        }

        private static RegionInfo LoadRegionChunk(BinaryReader reader)
        {
            RegionInfo regionInfo = new RegionInfo();
            regionInfo.ID = reader.ReadUInt32();
            uint num = reader.ReadUInt32();
            for (int index = 0; (long)index < (long)num; ++index)
            {
                ZoneInfo zoneInfo = RegionData.LoadZone(reader);
                zoneInfo.Region = regionInfo;
                regionInfo.Zones[zoneInfo.ID] = zoneInfo;
                RegionData._zones[(int)zoneInfo.ID] = zoneInfo;
            }
            return regionInfo;
        }

        private static ZoneInfo LoadZone(BinaryReader reader)
        {
            uint num1 = reader.ReadUInt32();
            if (RegionData._zones[(int)num1] == null)
                RegionData._zones[(int)num1] = new ZoneInfo();
            ZoneInfo zone = RegionData._zones[(int)num1];
            zone.ID = num1;
            zone.OffsetX = reader.ReadUInt32();
            zone.OffsetY = reader.ReadUInt32();
            int num2 = (int)reader.ReadUInt32();
            int num3 = (int)reader.ReadUInt32();
            return zone;
        }

        private static CollisionInfo LoadCollisionChunk(BinaryReader reader)
        {
            CollisionInfo collisionInfo = new CollisionInfo();
            uint num1 = reader.ReadUInt32();
            uint num2 = reader.ReadUInt32();
            collisionInfo.Vertices = new Vector3[(int)num2];
            for (uint index = 0; index < num2; ++index)
                collisionInfo.Vertices[(int)index] = RegionData.ReadVector3(reader);
            uint num3 = reader.ReadUInt32();
            uint num4 = reader.ReadUInt32();
            collisionInfo.Triangles = new TriangleInfo[(int)num3];
            if (num4 == 4U)
            {
                for (uint index = 0; index < num3; ++index)
                {
                    collisionInfo.Triangles[(int)index] = new TriangleInfo()
                    {
                        i0 = reader.ReadUInt32(),
                        i1 = reader.ReadUInt32(),
                        i2 = reader.ReadUInt32(),
                        fixture = reader.ReadUInt32()
                    };
                    if (!collisionInfo.Fixtures.ContainsKey(collisionInfo.Triangles[(int)index].fixture))
                        collisionInfo.Fixtures[collisionInfo.Triangles[(int)index].fixture] = new FixtureInfo();
                    collisionInfo.Fixtures[collisionInfo.Triangles[(int)index].fixture].Triangles.Add(index);
                }
                if (RegionData._zones[(int)num1] == null)
                    RegionData._zones[(int)num1] = new ZoneInfo();
                RegionData._zones[(int)num1].Collision = collisionInfo;
                return collisionInfo;
            }
            if (num4 == 2U)
                throw new NotImplementedException();
            throw new Exception();
        }

        private static void LoadBSPChunk(BinaryReader reader)
        {
            uint num1 = reader.ReadUInt32();
            int num2 = (int)reader.ReadUInt32();
            int num3 = (int)reader.ReadUInt32();
            RegionData._zones[(int)num1].Collision.BSP = RegionData.ReadBSPNode(reader);
        }

        private static BSPNodeInfo ReadBSPNode(BinaryReader reader)
        {
            BSPNodeInfo bspNodeInfo = new BSPNodeInfo();
            if (reader.ReadByte() > (byte)0)
            {
                uint num = reader.ReadUInt32();
                bspNodeInfo.Triangles = new int[(int)num];
                for (uint index = 0; index < num; ++index)
                    bspNodeInfo.Triangles[(int)index] = reader.ReadInt32();
            }
            else
            {
                bspNodeInfo.P.N = RegionData.ReadVector3(reader);
                bspNodeInfo.P.D = reader.ReadSingle();
                bspNodeInfo.Back = RegionData.ReadBSPNode(reader);
                bspNodeInfo.Front = RegionData.ReadBSPNode(reader);
            }
            return bspNodeInfo;
        }

        private static Vector3 ReadVector3(BinaryReader reader)
        {
            Vector3 vector3;
            vector3.X = reader.ReadSingle();
            vector3.Y = reader.ReadSingle();
            vector3.Z = reader.ReadSingle();
            return vector3;
        }

        public static bool Enabled
        {
            get
            {
                return RegionData._enabled;
            }
            set
            {
                RegionData._enabled = value;
            }
        }

        public static void HideDoor(bool hide, ushort zoneID, uint uniqueID, uint instance = 0)
        {
            uint key = (uint)(((int)uniqueID & 49152) << 16 | ((int)zoneID & 1023) << 20 | ((int)uniqueID & 16383) << 6 | 40 + (int)instance);
            if (RegionData._zones[(int)zoneID] == null || RegionData._zones[(int)zoneID].ID == 0U || !RegionData._zones[(int)zoneID].Collision.Fixtures.ContainsKey(key))
                return;
            RegionData._zones[(int)zoneID].Collision.Fixtures[key].Hidden = hide;
        }

        public static void HideDoor(bool hide, ushort zoneID, uint doorID)
        {
            if (RegionData._zones[(int)zoneID] == null || RegionData._zones[(int)zoneID].ID == 0U)
                return;
            ZoneInfo zone = RegionData._zones[(int)zoneID];
            if (zone == null || !zone.Collision.Fixtures.ContainsKey(doorID))
                return;
            zone.Collision.Fixtures[doorID].Hidden = hide;
        }

        public static ZoneInfo PinZone(uint regionID, int x, int y)
        {
            foreach (ZoneInfo zoneInfo in ((IEnumerable<ZoneInfo>)RegionData._zones).Where<ZoneInfo>((Func<ZoneInfo, bool>)(e =>
          {
              if (e != null)
                  return (int)e.Region.ID == (int)regionID;
              return false;
          })).ToList<ZoneInfo>())
            {
                if ((long)x >= (long)zoneInfo.OffsetX && (long)x <= (long)(zoneInfo.OffsetX + (uint)ushort.MaxValue) && ((long)y >= (long)zoneInfo.OffsetY && (long)y <= (long)(zoneInfo.OffsetY + (uint)ushort.MaxValue)))
                    return zoneInfo;
            }
            return (ZoneInfo)null;
        }

        public static RegionData.OcclusionResult OcclusionQuery(
          ushort zoneId0,
          float x0,
          float y0,
          float z0,
          ushort zoneId1,
          float x1,
          float y1,
          float z1)
        {
            return RegionData.OcclusionQuery(zoneId0, x0, y0, z0, zoneId1, x1, y1, z1);
        }

        public static Vector3 Lerp(Vector3 a, Vector3 b, double t)
        {
            return new Vector3(a.X + (float)(((double)b.X - (double)a.X) * t), a.Y + (float)(((double)b.Y - (double)a.Y) * t), a.Z + (float)(((double)b.Z - (double)a.Z) * t));
        }

        public static double GetDistance(Vector3 locA, Vector3 locB)
        {
            double num1 = (double)((int)locB.X - (int)locA.X);
            double num2 = (double)((int)locB.Y - (int)locA.Y);
            double num3 = (double)((int)locB.Z - (int)locA.Z);
            return Math.Sqrt(num1 * num1 + num2 * num2 + num3 * num3);
        }

        public static RegionData.OcclusionResult OcclusionQuery(
          ushort zoneId0,
          float x0,
          float y0,
          float z0,
          ushort zoneId1,
          float x1,
          float y1,
          float z1,
          ref Vector3 hitpoint)
        {
            RegionData.SegmentTestInfo info = new RegionData.SegmentTestInfo();
            long num1 = RegionData.DebugEnable ? Timer.Value : 0L;
            RegionData.OcclusionResult occlusionResult;
            try
            {
                ZoneInfo zone = RegionData._zones[(int)zoneId0];
                if (zone == null || zone.Collision == null)
                {
                    occlusionResult = RegionData.OcclusionResult.NotLoaded;
                }
                else
                {
                    if (!zone.Enabled)
                        return RegionData.OcclusionResult.NotEnabled;
                    if ((int)zoneId0 != (int)zoneId1)
                    {
                        occlusionResult = RegionData.OcclusionResult.NotImplemented;
                    }
                    else
                    {
                        x0 = (float)ushort.MaxValue - x0;
                        x1 = (float)ushort.MaxValue - x1;
                        Vector3 vector3_1 = new Vector3(x0, y0, z0);
                        Vector3 p1_1 = new Vector3(x1, y1, z1);
                        if (RegionData.SegmentTest(vector3_1, p1_1, zone.Collision, zone.Collision.BSP, 0, ref info))
                        {
                            bool flag = true;
                            double distance = RegionData.GetDistance(vector3_1, info.HitPoint);
                            Vector3 vector3_2 = new Vector3(info.HitPoint.X, info.HitPoint.Y, info.HitPoint.Z);
                            hitpoint = info.HitPoint;
                            int num2 = 0;
                            while (flag)
                            {
                                Vector3 p1_2 = RegionData.Lerp(info.HitPoint, vector3_1, 0.01);
                                flag = RegionData.SegmentTest(vector3_1, p1_2, zone.Collision, zone.Collision.BSP, 0, ref info);
                                if (flag && ((double)info.HitPoint.X != (double)vector3_2.X || (double)info.HitPoint.Y != (double)vector3_2.Y || (double)info.HitPoint.Z != (double)vector3_2.Z) && RegionData.GetDistance(vector3_1, info.HitPoint) < distance)
                                    hitpoint = info.HitPoint;
                                vector3_2 = new Vector3(info.HitPoint.X, info.HitPoint.Y, info.HitPoint.Z);
                                if (num2 <= 100)
                                    ++num2;
                                else
                                    break;
                            }
                            occlusionResult = RegionData.OcclusionResult.Occluded;
                            hitpoint.X = (float)ushort.MaxValue - hitpoint.X;
                        }
                        else
                            occlusionResult = RegionData.OcclusionResult.NotOccluded;
                    }
                }
            }
            catch
            {
                occlusionResult = RegionData.OcclusionResult.InternalError;
            }
            if (RegionData.DebugEnable)
            {
                long num2 = (Timer.Value - num1) * 1000000L / Timer.Frequency;
                float num3 = (float)num2 / (float)info.NumTrianglesTested;
                Console.Write(string.Format("LOS Check: ({0}, {1}, {2}, {3}) -> ({4}, {5}, {6}, {7}) -> ", (object)zoneId0, (object)x0, (object)y0, (object)z0, (object)zoneId1, (object)x1, (object)y1, (object)z1));
                Console.WriteLine(occlusionResult.ToString() + " [nodes=" + (object)info.NumNodesTested + ", triangles=" + (object)info.NumTrianglesTested + ", μs=" + (object)num2 + ", μs/tri=" + (object)num3 + "]");
            }
            return occlusionResult;
        }

        private static bool SegmentTest(
          Vector3 p0,
          Vector3 p1,
          CollisionInfo collision,
          BSPNodeInfo node,
          int depth,
          ref RegionData.SegmentTestInfo info)
        {
            if (node == null)
                return false;
            ++info.NumNodesTested;
            info.MaxDepthReached = Math.Max(info.MaxDepthReached, depth);
            if (node.IsLeaf)
            {
                TriangleInfo triangleInfo = new TriangleInfo();
                int num = RegionData.NodeTriangleTest(p0, p1, collision, node, ref info.HitPoint);
                info.NumTrianglesTested += num != -1 ? num + 1 : node.Triangles.Length;
                info.HitTriangle = triangleInfo;
                return num != -1;
            }
            float num1 = Vector3.Dot(node.P.N, p0) - node.P.D;
            float num2 = Vector3.Dot(node.P.N, p1) - node.P.D;
            if ((double)num1 < -9.99999974737875E-06 && (double)num2 < -9.99999974737875E-06)
                return RegionData.SegmentTest(p0, p1, collision, node.Back, depth + 1, ref info);
            if ((double)num1 > 9.99999974737875E-06 && (double)num2 > 9.99999974737875E-06)
                return RegionData.SegmentTest(p0, p1, collision, node.Front, depth + 1, ref info);
            if (!RegionData.SegmentTest(p0, p1, collision, node.Front, depth + 1, ref info))
                return RegionData.SegmentTest(p0, p1, collision, node.Back, depth + 1, ref info);
            return true;
        }

        private static int NodeTriangleTest(
          Vector3 p0,
          Vector3 p1,
          CollisionInfo collision,
          BSPNodeInfo node,
          ref Vector3 hitPoint)
        {
            if (node.Triangles.Length == 0)
                return -1;
            Vector3 d = p1 - p0;
            float length = d.Length;
            if ((double)d.Length < 9.99999974737875E-06)
                return -1;
            d = d.Normalize();
            for (int index = 0; index < node.Triangles.Length; ++index)
            {
                int triangle1 = node.Triangles[index];
                TriangleInfo triangle2 = collision.Triangles[triangle1];
                if (collision.Fixtures[triangle2.fixture].Hidden)
                    return -1;
                Vector3 vertex1 = collision.Vertices[(int)triangle2.i0];
                Vector3 vertex2 = collision.Vertices[(int)triangle2.i1];
                Vector3 vertex3 = collision.Vertices[(int)triangle2.i2];
                float distance;
                if (RegionData.RayTestTriangle(out distance, p0, d, vertex1, vertex2, vertex3, ref hitPoint) && (double)distance < (double)length)
                    return index;
            }
            return -1;
        }

        private static bool RayTestTriangle(
          out float distance,
          Vector3 o,
          Vector3 d,
          Vector3 v1,
          Vector3 v2,
          Vector3 v3,
          ref Vector3 hitPoint)
        {
            distance = 0.0f;
            Vector3 vector3_1 = v2 - v1;
            Vector3 vector3_2 = v3 - v1;
            Vector3 v4 = Vector3.Cross(d, vector3_2);
            float num1 = Vector3.Dot(vector3_1, v4);
            if ((double)num1 > -9.99999974737875E-06 && (double)num1 < 9.99999974737875E-06)
                return false;
            float num2 = 1f / num1;
            Vector3 u = o - v1;
            float num3 = Vector3.Dot(u, v4) * num2;
            if ((double)num3 < 0.0 || (double)num3 > 1.0)
                return false;
            Vector3 v5 = Vector3.Cross(u, vector3_1);
            float num4 = Vector3.Dot(d, v5) * num2;
            if ((double)num4 < 0.0 || (double)num3 + (double)num4 > 1.0)
                return false;
            float num5 = Vector3.Dot(vector3_2, v5) * num2;
            if ((double)num5 <= 9.99999974737875E-06)
                return false;
            distance = num5;
            hitPoint = new Vector3(o.X + d.X * num5, o.Y + d.Y * num5, o.Z + d.Z * num5);
            return true;
        }

        public enum LoadResult
        {
            OK,
            NotFound,
            Invalid,
            VersionMismatch,
            NotImplemented,
            InternalError,
        }

        public enum OcclusionResult
        {
            Occluded,
            NotOccluded,
            NotEnabled,
            NotLoaded,
            NotImplemented,
            InternalError,
        }

        private struct SegmentTestInfo
        {
            public int NumNodesTested;
            public int NumTrianglesTested;
            public int MaxDepthReached;
            public TriangleInfo HitTriangle;
            public Vector3 HitPoint;
        }
    }
}