﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class RegionTable : FigTable
    {
        public List< RegionView> Regions;
        public override List<object> Records => Regions.Select(e => (object)e).ToList();
        public override int RecordSize =>Marshal.SizeOf(typeof(Region));

        public RegionTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.Regions, db, data, reader)
        {

        }

        protected override int LoadInternal()
        {
            Regions = ReadStructs<Region>(_fileData, Offset, (int)EntryCount).Select(e => new RegionView(e)).ToList();
            return RecordSize * (int)EntryCount;
        }

        protected override bool LoadExtData()
        {
            var recSize = Marshal.SizeOf(typeof(Region));
            int zoneSize = Marshal.SizeOf(typeof(Zone));
            int sliceSize = Marshal.SizeOf(typeof(ZoneSlice));

            for (int i = 0; i < Regions.Count; i++)
            {
                var region = Regions[i];

                if (region.ZoneStartOffset == 0)
                    continue;

                var offset = Offset + region.ZoneStartOffset + (i * recSize);

                if (region.ZoneStartOffset > 0)
                {

                    region.Slices = ReadStructs<ZoneSlice>(_fileData, (uint)offset, (int)region.SliceCount).Select(e => new ZoneSliceView(e)).ToList();
                    region.Zones = ReadStructs<Zone>(_fileData, (uint)(offset + region.SliceCount * sliceSize), (int)region.ZoneCount).Select(e => new ZoneView(e)).ToList();
                }
            }

            return true;
        }

        public override void SaveData(BinaryWriter w)
        {

            Offset = (uint)w.BaseStream.Position;
            EntryCount = (uint)Regions.Count;

            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);

            long startPos2 = 0;
            long startPos = 0;
            if (EntryCount > 0)
            {

                writer.Write(new byte[32 * Regions.Count], 0, 32 * Regions.Count);

                for(int i=0; i<Regions.Count; i++)
                {
                    var region = Regions[i];
                     startPos = writer.BaseStream.Position - (i * 32);

                    foreach(var slice in region.Slices)
                    {
                        slice.Save(writer);
                    }

                     startPos2 = writer.BaseStream.Position - (i * 32);

                    foreach (ZoneView zone in region.Zones)
                        zone.Save(writer);

                    region.ZoneStartOffset = (int)startPos;
                    region.ZoneEndOffse = (int)startPos2;
                }

                writer.Write((byte)0);
                writer.Write((byte)0);


                writer.BaseStream.Position = 0;

                foreach (var region in Regions)
                {
                    region.ZoneCount = (uint)region.Zones.Count;
                    region.SliceCount = (uint)region.Slices.Count;

                    var data = WriteStructs(region.Region);
                    writer.Write(data, 0, data.Length);
                }


                w.BaseStream.Position = TocOffset;
                w.Write((uint)EntryCount);
                w.Write((uint)Offset);
                w.Write((uint)ms.Length);

                w.BaseStream.Position = Offset;
                w.Write(ms.ToArray(), 0,(int)ms.Length);
            }
        }


    }

}
