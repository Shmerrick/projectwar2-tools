﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class Unk8Table : FigTable
    {
        public List<Unk8View> Unks;
        public override int RecordSize => Marshal.SizeOf(typeof(Unk8));
        public override List<object> Records => Unks.Select(e => (object)e).ToList();

        public Unk8Table(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.Unk8, db, data, reader)
        {
        }

        protected override int LoadInternal()
        {
            Unks = ReadStructs<Unk8>(_fileData, Offset, (int)EntryCount).Select(e => new Unk8View(e)).ToList();

            int i = 0;
            Unks.ForEach(e =>
            {
                e.Index = i;

                if (e.DiffuseSourceIndex < _db.TableStrings1.Strings.Count)
                    e.NameDiffuse = _db.TableStrings1.Strings[(int)e.DiffuseSourceIndex].Value;
                else
                    e.NameDiffuse = "!INVALID_INDEX!";

                if (e.SpecularSourceIndex < _db.TableStrings1.Strings.Count)
                    e.NameSpecular = _db.TableStrings1.Strings[(int)e.SpecularSourceIndex].Value;
                else
                    e.NameSpecular = "!INVALID_INDEX!";

                if (e.TintSourceIndex < _db.TableStrings1.Strings.Count)
                    e.NameTint = _db.TableStrings1.Strings[(int)e.TintSourceIndex].Value;
                else
                    e.NameTint = "!INVALID_INDEX!";

                i++;
            });

            return RecordSize * (int)EntryCount;
        }



    }

}

