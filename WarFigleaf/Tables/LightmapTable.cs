﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class LightmapTable : FigTable
    {
        public List<LightmapView> Lightmaps;
        public override int RecordSize => Marshal.SizeOf(typeof(Lightmap));

        public override List<object> Records => Lightmaps.Select(e => (object)e).ToList();
        public LightmapTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.Lightmaps, db, data, reader)
        {
            if (Marshal.SizeOf(typeof(Lightmap)) != recordSize)
                throw new Exception("Invalid size for " + nameof(Lightmap));
        }

        protected override int LoadInternal()
        {
            Lightmaps = ReadStructs<Lightmap>(_fileData, Offset, (int)EntryCount).Select(e => new LightmapView(e)).ToList();

            int i = 0;
            Lightmaps.ForEach(e =>
            {
                e.Index = i;
                if (e.SourceIndex < _db.TableStrings1.Strings.Count)
                    e.Name = _db.TableStrings1.Strings[(int)e.SourceIndex].Value;
                else
                    e.Name = "!INVALID_INDEX!";
                i++;
            });

            return RecordSize * (int)EntryCount;
        }
    }



}

