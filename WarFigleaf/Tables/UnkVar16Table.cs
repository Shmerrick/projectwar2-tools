﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WarFigleaf.Tables
{
    public class UnkVar16Table : FigTable
    {
        public List<UnkVarRecord> UnkVarRecord = new List<UnkVarRecord>();
        public override List<object> Records => UnkVarRecord.Select(e => (object)e).ToList();


        public UnkVar16Table(FigleafDB db, BinaryReader reader, byte[] data, TableType type) : base(type, db,data, reader)
        {
        }

        public override int RecordSize
        {
            get
            {
                return 0;
            }
        }
        protected override void ReadToc()
        {
            base.ReadToc();
        }

        protected override int LoadInternal()
        {
            _reader.BaseStream.Position = Offset;

            long total = 0;
            for (int i = 0; i < EntryCount; i++)
            {
                var pos = _reader.BaseStream.Position;
                _reader.BaseStream.Position += 0x10;
                var size = _reader.ReadUInt32();
                _reader.BaseStream.Position = pos;
                var d = new byte[size];
                _reader.Read(d, 0, d.Length);
                var record = new UnkVarRecord()
                {
                    Data = d
                };
                UnkVarRecord.Add(record);
                total += _reader.BaseStream.Position - pos;
            }
            return (int)total;
        }
    }

    
    public struct UnkVarRecord
    {
        public byte[] Data;
    }
}

