﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class FigStringTable : FigTable
    {
        public List<FigString> Strings { get; private set; } = new List<FigString>();

        public override List<object> Records => Strings.Select(e => (object)e).ToList();

        public FigStringTable(FigleafDB db, byte[] data, BinaryReader reader) : base(TableType.Strings1, db, data, reader)
        {
          
        }

        protected override void ReadToc()
        {
            TocOffset = (uint)_reader.BaseStream.Position;
            EntryCount = _reader.ReadUInt32();
            Offset = _reader.ReadUInt32();
        }

        public override void SaveToc(BinaryWriter writer)
        {
            writer.BaseStream.Position = TocOffset;
            writer.Write((uint)0);
            writer.Write((uint)0); //offset
        }

        public override void SaveData(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for(int i=0; i<Strings.Count; i++)
            {
                var starPos = writer.BaseStream.Position;
                var entry = Strings[i];
                
                writer.Write((uint)entry.Unk1);
                writer.Write((byte)entry.Type);

                if (!String.IsNullOrEmpty(entry.Value))
                    writer.Write(Encoding.GetEncoding(437).GetBytes(entry.Value), 0, entry.Value.Length);

                writer.Write((byte)0);
    
                writer.Write((byte)entry.Unk3);
            }

            writer.Write((byte)0);
            var endPos = writer.BaseStream.Position;
            writer.BaseStream.Position = TocOffset;
            writer.Write((uint)(Strings.Count));
            writer.Write((uint)(pos));
            writer.BaseStream.Position = endPos;
        }

        public override int RecordSize
        {
            get
            {
                return 0;
            }
        }

        protected override int LoadInternal()
        {
            _reader.BaseStream.Position = Offset;
            var figData = _db.FigData;

            for (int i = 0; i < EntryCount; i++)
            {
                var Unk1 = _reader.ReadUInt32();
                var type = _reader.ReadByte();
                var offset = _reader.BaseStream.Position;

                while (figData[offset] != 0)
                    offset++;

                int size = (int)(offset - _reader.BaseStream.Position);

               
                string value = Encoding.GetEncoding(437).GetString(figData, (int)_reader.BaseStream.Position, size);

                _reader.BaseStream.Position += size + 1;
  
                Strings.Add(new FigString()
                {
                    Index = i,
                    Unk1 = Unk1,
                    Type = type,
                    Value = value,
                    Unk3 = _reader.ReadByte(),
                });

            }

            return 0;
        }
    }

}
