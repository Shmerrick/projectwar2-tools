﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class PatchTable : FigTable
    {
        public List<PatchView> Patches;
        public override List<object> Records => Patches.Select(e => (object)e).ToList();
        public override int RecordSize =>Marshal.SizeOf(typeof(Patch));

        public PatchTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.Patches, db, data,  reader)
        {
            if (Marshal.SizeOf(typeof(Patch)) != recordSize)
                throw new Exception("Invalid size for " + nameof(Patch));
        }

        protected override bool LoadExtData()
        {
            var recSize = Marshal.SizeOf(typeof(Patch));

            for (int i = 0; i < Patches.Count; i++)
            {
                var ca = Patches[i];
                var offset = Offset + ca.DataStart + (i * recSize);
                ca.PatchData = ReadStructs<PatchData>(_fileData, (uint)offset, (int)ca.Count).Select(e => new PatchDataView(e)).ToList();
            }

            return true;
        }

        public override void SaveData(BinaryWriter w)
        {

            Offset = (uint)w.BaseStream.Position;
            EntryCount = (uint)Patches.Count;

            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);

            long startPos = 0;
            if (EntryCount > 0)
            {

                writer.Write(new byte[RecordSize * Patches.Count], 0, RecordSize * Patches.Count);

                for (int i = 0; i < Patches.Count; i++)
                {
                    var region = Patches[i];
                    startPos = writer.BaseStream.Position - (i * RecordSize);

                    foreach (PatchDataView view in region.PatchData)
                        view.Save(writer);

                    region.DataStart = (uint)startPos;
                }

                writer.BaseStream.Position = 0;

                foreach (var region in Patches)
                {
                    region.Count = (ushort)region.PatchData.Count;

                    var data = WriteStructs(region.Patch);
                    writer.Write(data, 0, data.Length);
                }

                w.BaseStream.Position = TocOffset;
                w.Write((uint)EntryCount);
                w.Write((uint)Offset);
                w.Write((uint)ms.Length);

                w.BaseStream.Position = Offset;
                w.Write(ms.ToArray(), 0, (int)ms.Length);
            }
        }

        protected override int LoadInternal()
        {
            Patches = ReadStructs<Patch>(_fileData, (uint)Offset, (int)EntryCount).Select(e => new PatchView(e))
               // .OrderBy(e => e.ZoneID)
                .ToList();
            return RecordSize * (int)EntryCount;
        }

    }

}

