﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Unk8
    {
        public uint DiffuseSourceIndex;
        public uint A02;
        public uint A03;
        public uint A04;
        public uint SpecularSourceIndex;
        public uint A06;
        public uint A07;
        public uint A08;
        public uint TintSourceIndex;
        public uint A10;
        public uint A11;
        public uint A12;
        public uint A13;
        public uint A14;
        public uint A15;
    }

    public class Unk8View
    {
        public int Index { get; set; }
        public Unk8 Unk8;

        public Unk8View(Unk8 unk8)
        {
            Unk8 = unk8;
        }
        public uint DiffuseSourceIndex { get => Unk8.DiffuseSourceIndex; set => Unk8.DiffuseSourceIndex = value; }
        public string NameDiffuse { get; set; }
        public uint A02 { get => Unk8.A02; set => Unk8.A02 = value; }
        public uint A03 { get => Unk8.A03; set => Unk8.A03 = value; }
        public uint A04 { get => Unk8.A04; set => Unk8.A04 = value; }
        public uint SpecularSourceIndex { get => Unk8.SpecularSourceIndex; set => Unk8.SpecularSourceIndex = value; }
        public string NameSpecular { get; set; }
        public uint A06 { get => Unk8.A06; set => Unk8.A06 = value; }
        public uint A07 { get => Unk8.A07; set => Unk8.A07 = value; }
        public uint A08 { get => Unk8.A08; set => Unk8.A08 = value; }
        public uint TintSourceIndex { get => Unk8.TintSourceIndex; set => Unk8.TintSourceIndex = value; }
        public string NameTint { get; set; }
        public uint A10 { get => Unk8.A10; set => Unk8.A10 = value; }
        public uint A11 { get => Unk8.A11; set => Unk8.A11 = value; }
        public uint A12 { get => Unk8.A12; set => Unk8.A12 = value; }

        public uint A13 { get => Unk8.A13; set => Unk8.A13 = value; }
        public uint A14 { get => Unk8.A14; set => Unk8.A14 = value; }
        public uint A15 { get => Unk8.A15; set => Unk8.A15 = value; }
    }
}
