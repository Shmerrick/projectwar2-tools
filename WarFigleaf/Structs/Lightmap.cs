﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Lightmap
    {

        public ushort B01;
        public ushort B01b;

        public ushort B02;
        public ushort B02b;

        public ushort B03;
        public ushort B03b;

        public ushort B04;
        public ushort B04b;
        //5
        public uint B05_Pages;
        //6
        public uint SourceIndex;
        //7
        public uint B07;
        public uint B08;

        public short B09a;
        public short B09b;

        public short B10a;
        public short B10b;
    }

    public class LightmapView
    {
        public Lightmap Lightmap;
        public int Index { get; set; }
        public string Name { get; set; }
        public LightmapView(Lightmap map)
        {
            Lightmap = map;
        }

        public ushort B01 { get => Lightmap.B01; set => Lightmap.B01 = value; }
        public ushort B01b { get => Lightmap.B01b; set => Lightmap.B01b = value; }

        public ushort B02 { get => Lightmap.B02; set => Lightmap.B02 = value; }
        public ushort B02b { get => Lightmap.B02b; set => Lightmap.B02b = value; }

        public ushort B03 { get => Lightmap.B03; set => Lightmap.B03 = value; }
        public ushort B03b { get => Lightmap.B03b; set => Lightmap.B03b = value; }

        public ushort B04 { get => Lightmap.B04; set => Lightmap.B04 = value; }
        public ushort B04b { get => Lightmap.B04b; set => Lightmap.B04b = value; }
        //5
        public uint B05_Pages { get => Lightmap.B05_Pages; set => Lightmap.B05_Pages = value; }
        //6
        public uint SourceIndex { get => Lightmap.SourceIndex; set => Lightmap.SourceIndex = value; }
        //7
        public uint B07 { get => Lightmap.B07; set => Lightmap.B07 = value; }
        public uint B08 { get => Lightmap.B08; set => Lightmap.B08 = value; }

        public short B09a { get => Lightmap.B09a; set => Lightmap.B09a = value; }
        public short B09b { get => Lightmap.B09b; set => Lightmap.B09b = value; }

        public short B10a { get => Lightmap.B10a; set => Lightmap.B10a = value; }
        public short B10b { get => Lightmap.B10b; set => Lightmap.B10b = value; }
    }
}
