﻿using System.Runtime.InteropServices;

namespace WarFigleaf.Structs
{
    public enum SwitchType : uint
    {
        Mesh = 0,
        Decal = 1,
        Color = 2
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ArtSwitches
    {
        public SwitchType SwitchType;
        public uint SourceIndex;
        public uint Unk2;
        public uint Unk3;
        public uint Unk4;
        public uint Other;
        public uint Male;
        public uint Female;
        public uint DwarfOther;
        public uint DwarfMale;
        public uint DwarfFemale;
        public uint HumanOther;
        public uint HumanMale;
        public uint HumanFemale;
        public uint ChaosHumanOther;
        public uint ChaosHumanMale;
        public uint ChaosHumanFemale;
        public uint ElfOther;
        public uint ElfMale;
        public uint ElfFemale;
        public uint DarkElfOther;
        public uint DarkElfMale;
        public uint DarkElfFemale;
        public uint OrcOther;
        public uint OrcMale;
        public uint OrcFemale;
        public uint GoblinOther;
        public uint GoblinMale;
        public uint GoblinFemale;
        public uint BeastmanOther;
        public uint BeastmanMale;
        public uint BeastmanFemale;
        public uint SkavenOther;
        public uint SkavenMale;
        public uint SkavenFemale;
        public uint OgreOther;
        public uint OgreMale;
        public uint OgreFemale;
        public uint ChaosWarriorOther;
        public uint ChaosWarriorMale;
        public uint ChaosWarriorFemale;
    }
}
