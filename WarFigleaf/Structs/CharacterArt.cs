﻿using System.Runtime.InteropServices;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CharacterArtData
    {
        public int A1;
        public int A2;
        public int A3;
        public int A4;
        public int A5;
        public int A6;
        public int A7;
        public int A8;
        public int A9;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CharacterArt
    {
        public uint SourceIndex;
        public uint Unk1;
        public uint Unk2;
        public uint Unk3;
        public uint Race;
        public uint Gender;
        public uint Unk6;
        public uint Unk7;
        public uint Animations;
        public uint Unk9;
        public uint PartCount;
        public uint DataStart;
        public uint Unk12;
        public uint DataStart2;
        public uint Unk14;
        public uint DataEnd;
        public uint Unk16;
        public uint DataStart4;
    }

    public class CharacterArtView
    {
        public int Index { get; set; }
        public CharacterArt CharacterArt;

        public CharacterArtView(CharacterArt art)
        {
            CharacterArt = art;
        }
        public string Name { get; set; }
        public uint SourceIndex { get => CharacterArt.SourceIndex; set => CharacterArt.SourceIndex = value; }
        public uint Unk1 { get => CharacterArt.Unk1; set => CharacterArt.Unk1 = value; }
        public uint Unk2 { get => CharacterArt.Unk2; set => CharacterArt.Unk2 = value; }
        public uint Unk3 { get => CharacterArt.Unk3; set => CharacterArt.Unk3 = value; }
        public uint Race { get => CharacterArt.Race; set => CharacterArt.Race = value; }
        public uint Gender { get => CharacterArt.Gender; set => CharacterArt.Gender = value; }
        public uint Unk6 { get => CharacterArt.Unk6; set => CharacterArt.Unk6 = value; }
        public uint Unk7 { get => CharacterArt.Unk7; set => CharacterArt.Unk7 = value; }
        public uint Animations { get => CharacterArt.Animations; set => CharacterArt.Animations = value; }
        public uint Unk9 { get => CharacterArt.Unk9; set => CharacterArt.Unk9 = value; }
        public uint PartCount { get => CharacterArt.PartCount; set => CharacterArt.PartCount = value; }
        public uint DataStart { get => CharacterArt.DataStart; set => CharacterArt.DataStart = value; }
        public uint Unk12 { get => CharacterArt.Unk12; set => CharacterArt.Unk12 = value; }
        public uint DataStart2 { get => CharacterArt.DataStart2; set => CharacterArt.DataStart2 = value; }
        public uint Unk14 { get => CharacterArt.Unk14; set => CharacterArt.Unk14 = value; }
        public uint DataEnd { get => CharacterArt.DataEnd; set => CharacterArt.DataEnd = value; }
        public uint Unk16 { get => CharacterArt.Unk16; set => CharacterArt.Unk16 = value; }
        public uint DataStart4 { get => CharacterArt.DataStart4; set => CharacterArt.DataStart4 = value; }
    }
}
