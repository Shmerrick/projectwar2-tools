#include <SDKDDKVer.h>

#include <WinSock2.h>
#include <MSWSock.h>
#include <Windows.h>
#include <stdint.h>

#include "Myp.h"

using namespace WarhammerOnline::DataMining;

extern "C"
{

    LIBRARY_API void *Create(const char* path) {
        Myp* mypPtr = new Myp();
        if (nullptr == mypPtr)
            return nullptr;
        if (!mypPtr->Create())
            return nullptr;
        return mypPtr;
    }

    LIBRARY_API void *Load(const char* path) {
        Myp* myp = new Myp();
        if (nullptr == myp)
            return nullptr;
        if (myp->Open(path))
            if (myp->IsOpened() && myp->Load())
                if (myp->IsLoaded())
                    return (void*)myp;
        delete myp;
        return nullptr;
    }

    LIBRARY_API void Unload(void *mypPtr) {
        if (nullptr == mypPtr)
            return;
        Myp* myp = static_cast<Myp*>(mypPtr);
        myp->Close();
        myp->Destroy();
        delete myp;
    }

    LIBRARY_API bool Save(void *mypPtr) {
        if (nullptr == mypPtr)
            return false;
        Myp* myp = static_cast<Myp*>(mypPtr);
        return myp->Save();
    }
    LIBRARY_API const char* GetFilenameFromHash(void *mypPtr, const uint64_t hash) {
        if (nullptr == mypPtr)
            return nullptr;
        Myp* myp = static_cast<Myp*>(mypPtr);
        std::string str = myp->GetFilenameFromAssetHash(hash);
        if (str.empty())
            return nullptr;
        return str.c_str();
    }

    LIBRARY_API int32_t GetAssetSize(void *mypPtr, const char* assetPath) {
        if (nullptr == mypPtr)
            return 0;
        Myp* myp = static_cast<Myp*>(mypPtr);
        Myp::FileTableEntry *asset = myp->GetAsset(assetPath);
        if (nullptr != asset)
            return asset->DecompressedSize;
        return 0;
    }

    LIBRARY_API int32_t GetAssetSizeByHash(void *mypPtr, int64_t hash) {
        if (nullptr == mypPtr)
            return 0;
        Myp* myp = static_cast<Myp*>(mypPtr);
        Myp::FileTableEntry *asset = myp->GetAsset(hash);
        if (nullptr != asset)
            return asset->DecompressedSize;
        return 0;
    }

    LIBRARY_API int32_t GetAssetSizeCompressedByHash(void *mypPtr, int64_t hash) {
        if (nullptr == mypPtr)
            return 0;
        Myp* myp = static_cast<Myp*>(mypPtr);
        Myp::FileTableEntry *asset = myp->GetAsset(hash);
        if (nullptr != asset)
            return asset->CompressedSize;
        return 0;
    }

    LIBRARY_API bool GetDecompressedAssetDataByPath(void *mypPtr, const char* assetPath, char* data) {
        if (nullptr == mypPtr)
            return false;
        Myp* myp = static_cast<Myp*>(mypPtr);
        Myp::FileTableEntry *asset = myp->GetAsset(assetPath);
        if (nullptr != asset)
            return myp->GetAssetData(asset, (uint8_t*)data);
        return false;
    }

    LIBRARY_API bool GetDecompressedAssetDataByHash(void *mypPtr, int64_t hash, char* data) {
        if (nullptr == mypPtr)
            return false;
        Myp* myp = static_cast<Myp*>(mypPtr);
        if (!myp->IsLoaded())
            return false;
        if (0 == hash)
            return false;
        Myp::FileTableEntry *asset = myp->GetAsset(hash);
        if (nullptr != asset) {
            return myp->GetAssetData(asset, reinterpret_cast<uint8_t*>(data));
        }
        return false;
    }

    LIBRARY_API bool GetCompressedAssetDataByHash(void *mypPtr, int64_t hash, char* data) {
        if (nullptr == mypPtr)
            return false;
        Myp* myp = static_cast<Myp*>(mypPtr);
        if (!myp->IsLoaded())
            return false;
        if (0 == hash)
            return false;
        Myp::FileTableEntry *asset = myp->GetAsset(hash);
        if (nullptr != asset) {
            return myp->GetAssetData(asset, reinterpret_cast<uint8_t*>(data), 0, false);
        }
        return false;
    }

    LIBRARY_API void UpdateAssetByHash(void *mypPtr, int64_t hash, char* data, int32_t size, bool compress) {
        Myp* myp = static_cast<Myp*>(mypPtr);
        myp->UpdateAsset(hash, (uint8_t*)data, size, (uint8_t)(compress ? 1 : 0));
    }

    LIBRARY_API void UpdateAsset(void *mypPtr, const char* assetPath, char* data, int32_t size, bool compress) {
        Myp* myp = static_cast<Myp*>(mypPtr);
        myp->UpdateAsset(assetPath, (uint8_t*)data, size, (uint8_t)(compress ? 1 : 0));
    }

    LIBRARY_API bool DeleteAssetByHash(void *mypPtr, int64_t hash) {
        if (nullptr == mypPtr)
            return false;
        Myp* myp = static_cast<Myp*>(mypPtr);
        Myp::FileTableEntry *asset = myp->GetAsset(hash);
        if (nullptr != asset) {
            myp->DeleteAsset(asset);
            return true;
        }
        return false;
    }

    LIBRARY_API bool DeleteAssetByPath(void *mypPtr, const char* assetPath) {
        if (nullptr == mypPtr)
            return false;
        Myp* myp = static_cast<Myp*>(mypPtr);
        Myp::FileTableEntry *asset = myp->GetAsset(assetPath);
        if (nullptr != asset) {
            myp->DeleteAsset(asset);
            return true;
        }
        return false;
    }



    LIBRARY_API size_t GetAssetCount(void *mypPtr) {
        if (nullptr == mypPtr)
            return 0;
        Myp* myp = static_cast<Myp*>(mypPtr);
        return myp->GetTotalFileTableEntries();
    }

    LIBRARY_API void GetAssetsInfo(void *mypPtr, uint64_t* hashes, int32_t* compressedSizes, int32_t* uncompressedSizes, int32_t* compressed, int32_t* crc) {
        if (nullptr == mypPtr)
            return;
        Myp* myp = static_cast<Myp*>(mypPtr);
        uint32_t i = 0;
        for (auto &v : myp->GetFileTableEntries()) {
            hashes[i] = v.Hash;
            compressedSizes[i] = v.CompressedSize;
            uncompressedSizes[i] = v.DecompressedSize;
            compressed[i] = v.IsCompressed;
            crc[i] = v.CRC32;
            ++i;
        }
    }

    LIBRARY_API void Log(std::string msg) {
        LogDebugInfo(msg);
    }

    BOOL WINAPI DllMain(HANDLE hInst, ULONG ul_reason, LPVOID lpReserved) {
        UNREFERENCED_PARAMETER(lpReserved);

        switch (ul_reason) {
            case DLL_THREAD_ATTACH:
                break;
            case DLL_THREAD_DETACH:
                break;
            case DLL_PROCESS_ATTACH:
                Myp::SetInstanceHandle((HMODULE)hInst);
                break;
            case DLL_PROCESS_DETACH:
                break;
        }
        return TRUE;
    }
}
