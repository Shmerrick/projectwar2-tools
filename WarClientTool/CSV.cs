﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WarClientTool
{
    public class CSV
    {
        public List<List<string>> Lines = new List<List<string>>();
        public int RowIndex { get; set; }
        private string _commentSection = null;
        public Dictionary<string, List<string>> _keyedRows = new Dictionary<string, List<string>>();

        public List<string> Row
        {
            get
            {
                return ReadRow();
            }
        }

        public bool EOF
        {
            get
            {
                return RowIndex == Lines.Count - 1;
            }
        }

        public CSV()
        {
            RowIndex = 0;
        }

        public string GetKeyedValue(object rowID, int colIndex, string defaultValue = "")
        {
            var key = rowID.ToString();
            if (_keyedRows.ContainsKey(key) && colIndex < _keyedRows[key].Count)
                return _keyedRows[key][colIndex];
            return defaultValue;
        }

        public static List<string> QuotedCSVToList(string line, char quote = '"', char splitter = ',')
        {
            var list = new List<String>();

            char[] chars = line.ToCharArray();
            bool inQuote = false;
            string current = "";
            for (int i = 0; i < chars.Length; i++)
            {
                if (chars[i] == quote && !inQuote)
                {
                    inQuote = true;
                    //    continue;
                }
                else if (chars[i] == quote && inQuote)
                {
                    inQuote = false;
                    // continue;
                }
                else if (chars[i] == splitter && !inQuote)
                {
                    list.Add(current);
                    current = "";
                }
                else if (inQuote)
                {
                    current += chars[i];
                }
                else
                {
                    current += chars[i];
                }
            }
            if (current.Length > 0)
                list.Add(current);

            return list;
        }

        public CSV(string text, string commentSection = null, bool quoted = false, int idCol = -1)
        {
            _commentSection = commentSection;
            string comment = "";
            foreach (string row in text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList())
            {
                if (_commentSection != null && row.StartsWith(_commentSection))
                {
                    comment = row.Replace(",", "").Replace(";", "");
                }

                if (row.StartsWith(";"))
                    continue;

                List<string> cols = new List<string>();
                if (quoted)
                {
                    cols = QuotedCSVToList(row);
                }
                else
                {
                    foreach (string col in row.Split(new char[] { ',' }))
                    {
                        cols.Add(col);
                    }
                }

                if (_commentSection != null && comment != "")
                    cols.Add(comment);
                Lines.Add(cols);
                if (idCol > -1 && idCol < cols.Count)
                    _keyedRows[cols[idCol]] = cols;
            }
            RowIndex = 0;
        }

        public Dictionary<int, int> ToDictionary(int IDCol, int valueCol)
        {
            var results = new Dictionary<int, int>();
            while (!EOF)
            {
                var row = ReadRow();
                results[ReadInt32(IDCol)] = ReadInt32(valueCol);
                NextRow();
            }
            return results;
        }

        public void MoveToEnd()
        {
            RowIndex = Lines.Count - 1;
        }

        public void NewRow()
        {
            Lines.Insert(RowIndex + 1, new List<string>());
        }

        public void RemoveRow()
        {
            Lines.RemoveAt(RowIndex);
        }

        public void Remove(List<int> rows)
        {
            List<List<string>> lines = new List<List<string>>();
            for (int i = 0; i < Lines.Count; i++)
            {
                if (!rows.Contains(i))
                    lines.Add(Lines[i]);
            }
            Lines = lines;
            if (RowIndex > Lines.Count - 1)
                RowIndex = lines.Count - 1;
        }

        public void WriteCol(int col, string value)
        {
            List<string> line = Lines[RowIndex];
            if (line.Count - 1 < col)
            {
                int count = col - line.Count;
                for (int i = 0; i <= count; i++)
                    line.Add("");
            }
            line[col] = value;
        }

        public List<string> ReadRow()
        {
            return Lines[RowIndex];
        }

        public void NextRow()
        {
            RowIndex++;
        }

        public int ReadInt32(int col, int defaultValue = 0)
        {
            int result = 0;
            if (col < Lines[RowIndex].Count && int.TryParse(Lines[RowIndex][col], out result))
            {
                return result;
            }
            return defaultValue;
        }

        public int PeakInt32(int row, int col, int defaultValue = -1)
        {
            if (row < Lines.Count)
            {
                int result = 0;
                int.TryParse(Lines[row][col], out result);
                return result;
            }
            return defaultValue;
        }

        public float ReadFloat(int col)
        {
            float result = 0;

            if (col < Lines[RowIndex].Count && float.TryParse(Lines[RowIndex][col], out result))
                return result;
            return 0;
        }

        public string ReadString(int col)
        {
            return Lines[RowIndex][col];
        }

        public string ToText()
        {
            StringBuilder builder = new StringBuilder();
            foreach (List<string> row in Lines)
            {
                for (int i = 0; i < row.Count; i++)
                {
                    if (i < row.Count - 1)
                        builder.Append(row[i] + ",");
                    else
                        builder.Append(row[i]);
                }
                builder.Append("\n");
            }
            return builder.ToString();
        }
    }
}