﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;

namespace WarClientTool
{
    internal class Program
    {
        //public static Dictionary<long, string> Dehashes = new Dictionary<long, string>();

        [STAThread]
        private static void Main(string[] args)
        {
            //Console.WriteLine($"{UIntPtr.Size}");
            Console.WriteLine("War Client Tools 1.25");

            string cmd = (args == null || args.Length == 0 || args[0] == null) ? "" : args[0].ToUpper();
            //LoadDehashes();

            switch (cmd)
            {
                case "-X":
                    Extract(args);
                    break;

                case "EXTRACT":
                    Extract(args);
                    break;

                case "-E":
                    Extract(args);
                    break;

                case "PACKFOLDER":
                    PACKFOLDER(args);
                    break;

                case "PACKFILE":
                    PACKFILE(args);
                    break;

                case "EDIT":
                    EDIT(args);
                    break;

                default:
                    Help();
                    break;
            }
        }

        private static void EDIT(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Usage: EDIT [myp file] [assetname]");
                return;
            }
            if (!File.Exists(args[1]))
            {
                Console.WriteLine("Myp not found");
                return;
            }

            MythicPackage p = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(args[1].ToUpper()));

            using (MYP myp = new MYP(p, args[1], true))
            {
                string assetName = args[2].ToLower().Replace("\\", "/").ToLower();
                byte[] asset = myp.GetAssetData(MYP.HashWAR(assetName));
                if (asset == null)
                {
                    Console.WriteLine("Asset not found");
                    return;
                }
                frmCSVViewer viewer = new frmCSVViewer();
                viewer.LoadAsset(myp, assetName, asset);
                viewer.ShowDialog();
                if (viewer.Changed)
                {
                    myp.Save();
                }
            }
        }

        private static void Extract(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Usage: EXTRACT [myps folder/myp file] [dest folder/asset path]");
                return;
            }

            if (Directory.Exists(args[1]))
            {
                Console.WriteLine($"Extracting all .myp to {args[1]}");
                foreach (string mypName in Enum.GetNames(typeof(MythicPackage)))
                {
                    string name = Path.Combine(args[1], mypName + ".myp");
                    MythicPackage p = (MythicPackage)Enum.Parse(typeof(MythicPackage), mypName);
                    ExtractMyp(p, name, args[2]);
                }
            }
            else if (File.Exists(args[1]))
            {
                string name = args[1].ToUpper();
                MythicPackage p = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(name));
                ExtractMyp(p, name, args[2]);
            }
            else
            {
                Console.WriteLine($"Nothing to extract {args[1]}");
            }
        }

        private static void PACKFOLDER(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: PACKFOLDER [myps folder] [dest myp file]");
                return;
            }

            if (!Directory.Exists(args[1]))
            {
                Console.WriteLine($"Myps folder not found {args[1]}");
                return;
            }

            string mypName = Path.GetFileName(args[2]);

            MythicPackage p = MythicPackage.NONE;
            if (Enum.TryParse<MythicPackage>(mypName, out p))
            {
                Console.WriteLine($"Destination myp name is limited to existing names");
                return;
            }

            if (!File.Exists(args[2]))
            {
                Console.WriteLine($"Creating myp ({mypName})");
                using (MYP myp = new MYP(p, args[2], true)) { }
            }

            using (MYP myp = new MYP(p, args[2], true))
            {
                foreach (string file in Directory.GetFiles(args[1]))
                {
                    string assetName = file.Replace(args[1], "").Replace("\\", "/").Substring(1).ToLower();
                    Console.WriteLine("Packing " + Path.GetFileName(file));
                    myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                }

                foreach (string folder in Directory.GetDirectories(args[1]))
                {
                    PackFolder(args[1], folder, myp);
                }
                Console.WriteLine("Writing " + args[2]);
                myp.Save();
                Console.WriteLine("Done");
            }
        }

        private static void PACKFILE(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Usage: PACKFILE [file asset path] [asset name] [dest myp file]");
                return;
            }

            if (!File.Exists(args[1]))
            {
                Console.WriteLine($"Asset to pack not found {args[1]}");
                return;
            }

            string mypName = Path.GetFileName(args[3]);

            MythicPackage p = MythicPackage.NONE;
            if (Enum.TryParse<MythicPackage>(mypName, out p))
            {
                Console.WriteLine($"Destination myp name is limited to existing MYP archive names");
                return;
            }

            if (!File.Exists(args[3]))
            {
                Console.WriteLine($"Creating myp ({mypName})");
                using (MYP myp = new MYP(p, args[3], true)) { }
            }

            using (MYP myp = new MYP(p, args[3], true))
            {
                string assetName = args[2].Replace("\\", "/").ToLower();
                Console.WriteLine("Packing [" + assetName + "]");
                myp.UpdateAsset(assetName, File.ReadAllBytes(args[1]), CanCompress(args[1]));

                Console.WriteLine("Writing " + args[2]);
                myp.Save();
                Console.WriteLine("Done");
            }
        }

        private static void PackFolder(string root, string currentFolder, MYP myp)
        {
            foreach (string folder in Directory.GetDirectories(currentFolder))
            {
                PackFolder(root, folder, myp);
            }

            foreach (string file in Directory.GetFiles(currentFolder))
            {
                Console.WriteLine("Packing " + Path.GetFileName(file));
                string assetName = file.Replace(root, "").Replace("\\", "/").Substring(1).ToLower();

                if (assetName.Contains("no_dehash"))
                {
                    long hash = (long)ulong.Parse(Path.GetFileNameWithoutExtension(file));
                    myp.UpdateAsset(hash, File.ReadAllBytes(file), false);
                }
                else
                {
                    myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                }
            }
        }

        private static bool CanCompress(string filename, bool defaultValue = true)
        {
            string ext = Path.GetExtension(filename).ToLower().Replace(".", "");

            if (ext == "bik")
            {
                return false;
            }

            if (ext == "bin")
            {
                return true;
            }

            if (ext == "dds")
            {
                return false;
            }

            if (ext == "h")
            {
                return false;
            }

            if (ext == "inc")
            {
                return false;
            }

            if (ext == "lmp")
            {
                return false;
            }

            if (ext == "mp3")
            {
                return false;
            }

            if (ext == "psh")
            {
                return false;
            }

            if (ext == "stx")
            {
                return false;
            }

            if (ext == "txt")
            {
                return true;
            }

            if (ext == "vsh")
            {
                return false;
            }

            if (ext == "wav")
            {
                return true;
            }

            if (ext == "csv")
            {
                return true;
            }

            return defaultValue;
        }

        private static void ExtractMyp(MythicPackage p, string name, string dest)
        {
            if (File.Exists(name))
            {
                Console.Write($"Processing {name}\n");
                try
                {
                    using (MYP myp = new MYP(p, name, false))
                    {
                        Console.WriteLine($"{myp.Assets.Count} assets");
                        int count = myp.Assets.Count;
                        int lastPercent = 0;
                        foreach (long hash in myp.Assets.Keys)
                        {
                            byte[] data = myp.GetAssetData(hash);
                            string filepath = myp.GetFilenameFromHash(hash);

                            string path = "";

                            if (!string.IsNullOrEmpty(filepath))
                            {
                                path = Path.Combine(dest, filepath
                                    .Replace("fi.0.0.", "")
                                    .Replace("sk.0.0.", "")
                                    .Replace("it.0.0.", "")
                                    .Replace(".stx", ".dds")
                                    .Replace("/", "\\"));

                                string folder = Path.GetDirectoryName(path);

                                if (!Directory.Exists(folder))
                                {
                                    Directory.CreateDirectory(folder);
                                }
                            }
                            else
                            {
                                string type = MYP.GetExtension(data);

                                string folder = Path.Combine(dest, "NO_DEHASH", type);
                                if (!Directory.Exists(folder))
                                {
                                    Directory.CreateDirectory(folder);
                                }

                                path = Path.Combine(folder, string.Format("{0:X}", hash) + "." + type);
                            }
                            File.WriteAllBytes(path, data);
                            count--;

                            int percent = 100 - (count * 100 / myp.Assets.Count);

                            if (percent % 10 == 0 && lastPercent != percent)
                            {
                                Console.Write(".." + percent + "%");
                            }
                            lastPercent = percent;
                        }
                        Console.WriteLine(" Done");
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }
        }

        public static byte[] Compress(byte[] data)
        {
            MemoryStream outB = new MemoryStream();

            using (DeflateStream archive = new DeflateStream(outB, CompressionLevel.Optimal, true))
            {
                archive.Write(data, 0, data.Length);
            }
            outB.Position = 0;
            return outB.ToArray();
        }

        public static byte[] DeCompress(byte[] data, bool gzip = false)
        {
            MemoryStream outB = new MemoryStream(data);
            if (gzip)
            {
                outB.Position += 2;
            }

            using (DeflateStream archive = new DeflateStream(outB, CompressionMode.Decompress, true))
            {
                MemoryStream dest = new MemoryStream();
                archive.CopyTo(dest);
                return dest.ToArray();
            }
        }

        private static void Help()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("");
            Console.WriteLine(" -x      source.myp [out/folder/]");
            Console.WriteLine(" -c     [myps folder] [dest myp file]");
            Console.WriteLine(" -p [file asset path] [asset name] [dest myp file]");
            Console.WriteLine("  EDIT [myp file] [asset name]");
            //Console.WriteLine("  DELETE [dest myp file] [asset name]");
        }
    }
}