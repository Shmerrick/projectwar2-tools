﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;

namespace WarClientTool
{
    public enum MythicPackage : int
    {
        NONE = 0,
        MFT = 1,
        ART = 2,
        ART2 = 3,
        ART3 = 4,
        AUDIO = 5,
        DATA = 6,
        WORLD = 7,
        INTERFACE = 8,
        VIDEO = 9,
        BLOODHUNT = 10,
        PATCH = 11,
        VO_ENGLISH = 12,
        VO_FRENCH = 13,
        VIDEO_FRENCH = 14,
        VO_GERMAN = 15,
        VIDEO_GERMAN = 16,
        VO_ITALIAN = 17,
        VIDEO_ITALIAN = 18,
        VO_SPANISH = 19,
        VIDEO_SPANISH = 20,
        VO_KOREAN = 21,
        VIDEO_KOREAN = 22,
        VO_CHINESE = 23,
        VIDEO_CHINESE = 24,
        VO_JAPANESE = 25,
        VIDEO_JAPANESE = 26,
        VO_RUSSIAN = 27,
        VIDEO_RUSSIAN = 28,
        WARTEST = 29,
        DEV = 30,
    }

    //public interface ILog
    //{
    //    void Info(string msg, string context = "");
    //    void Error(string msg, string context = "");
    //    void Exception(string msg, Exception e, string context = "");
    //    void Warning(string msg, string context = "");
    //}

    public class AssetInfo
    {
        public MythicPackage Package { get; set; }
        public long Hash { get; set; }
        public uint CompressedSize { get; set; }
        public uint UncompressedSize { get; set; }
        public bool Compressed { get; set; }
        public string Name { get; set; }
        public uint CRC { get; set; }
        public long ID { get; set; }
        public bool Changed { get; set; }
        public byte[] Data { get; set; }
    }

    public class MYP : IDisposable
    {
        public Dictionary<long, PatcherAsset> Assets { private set; get; } = new Dictionary<long, PatcherAsset>();

        private UIntPtr _mypPtr = UIntPtr.Zero;

        public MythicPackage Package { get; private set; }

        public MYPManager Manager { get; set; }
        private Dictionary<long, Tuple<bool, byte[]>> _dataChanges = new Dictionary<long, Tuple<bool, byte[]>>();
        private bool _changed = false;

        public bool Changed { get => _changed; set => _changed = value; }

        #region IMPORTS

        //[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void LogDelegate(int type, int level, string msg);

#if WIN64
        [SuppressUnmanagedCodeSecurity, DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else

        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern string GetFilenameFromHash(UIntPtr ptr, ulong hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern UIntPtr LoadMyp(string path);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern UIntPtr CreateMyp(string path);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern void UnloadMyp(UIntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern uint GetAssetSize(UIntPtr ptr, string assetPath);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern uint GetAssetSizeByHash(UIntPtr ptr, long hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern uint GetAssetSizeCompressedByHash(UIntPtr ptr, long hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern int GetAssetCount(UIntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern int GetDecompressedAssetDataByPath(UIntPtr ptr, string assetPath, [In, Out] byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern int GetDecompressedAssetDataByHash(UIntPtr ptr, long hash, [In, Out] byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern int GetCompressedAssetDataByHash(UIntPtr ptr, long hash, [In, Out] byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern int UpdateAssetByHash(UIntPtr ptr, long hash, [In, Out] byte[] data, uint size, bool compressed);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern bool Save(UIntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern bool DeleteAssetByHash(UIntPtr ptr, long hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"myp_x64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp_32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern int GetAssetsInfo(UIntPtr ptr, [In, Out] long[] hashes, [In, Out] uint[] CompressedSize, [In, Out] uint[] UnCompressedSize, [In, Out] uint[] compressed, [In, Out] uint[] crc);

        #endregion IMPORTS

        public void Dispose()
        {
            if (_mypPtr != UIntPtr.Zero)
            {
                UnloadMyp(_mypPtr);
                _mypPtr = UIntPtr.Zero;
            }
        }

        public string Filename { private set; get; }

        //private readonly UIntPtr _logDelegate;
        //private bool _loaded = false;
        //private bool _closed = true;
        //private readonly LogDelegate _callback = new LogDelegate(MypLog);
        //private readonly UIntPtr _callbackhandle;

        public MYP(MythicPackage package)
        {
            try
            {
                // _callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                //GC.KeepAlive(_callback);
                Package = package;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        public MYP(MythicPackage package, string path, bool create)
        {
            try
            {
                //_callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                // GC.KeepAlive(_callback);
                Package = package;
                LoadFromFile(path, create);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        public string GetFilenameFromHash(long hash)
        {
            if (null != _mypPtr)
            {
                return GetFilenameFromHash(_mypPtr, (ulong)hash);
            }

            return string.Empty;
        }

        public void LoadFromFile(string filename, bool create = true)
        {
            lock (Assets)
            {
                Filename = filename;
                if (create && !File.Exists(filename))
                {
                    CreateMyp(filename);
                }
                _mypPtr = LoadMyp(filename);

                if (_mypPtr == UIntPtr.Zero)
                {
                    throw new Exception($"Error loading '{filename}'");
                }

                int count = GetAssetCount(_mypPtr);
                long[] hashes = new long[count];
                uint[] csize = new uint[count];
                uint[] uscise = new uint[count];
                uint[] crc = new uint[count];
                uint[] c = new uint[count];

                GetAssetsInfo(_mypPtr, hashes, csize, uscise, c, crc);
                for (int i = 0; i < count; i++)
                {
                    PatcherAsset asset = new PatcherAsset()
                    {
                        Package = Package,
                        Hash = hashes[i],
                        CompressedSize = csize[i],
                        Size = uscise[i],
                        Compressed = c[i] > 0,
                        CRC32 = crc[i],
                    };
                    Assets[asset.Hash] = asset;
                }
                //_loaded = true;
            }
        }

        public void UpdateAsset(PatcherAsset asset)
        {
            lock (Assets)
            {
                _changed = true;
                Assets[asset.Hash] = asset;
                UpdateAssetByHash(_mypPtr, asset.Hash, asset.Data, (uint)asset.Data.Length, asset.Compressed);
            }
        }

        public void UpdateAsset(long hash, byte[] data, bool compress)
        {
            lock (Assets)
            {
                _changed = true;
                UpdateAssetByHash(_mypPtr, hash, data, (uint)data.Length, compress);
            }
        }

        public void UpdateAsset(string path, byte[] data, bool compress)
        {
            lock (Assets)
            {
                long hash = HashWAR(path);
                _changed = true;
                UpdateAssetByHash(_mypPtr, hash, data, (uint)data.Length, compress);
            }
        }

        public bool Save()
        {
            _dataChanges.Clear();

            lock (Assets)
            {
                if (Save(_mypPtr))
                {
                    int count = GetAssetCount(_mypPtr);
                    long[] hashes = new long[count];
                    uint[] csize = new uint[count];
                    uint[] uscise = new uint[count];
                    uint[] crc = new uint[count];
                    uint[] c = new uint[count];

                    GetAssetsInfo(_mypPtr, hashes, csize, uscise, c, crc);
                    for (int i = 0; i < count; i++)
                    {
                        if (Assets.ContainsKey(hashes[i]))
                        {
                            Assets[hashes[i]].CompressedSize = csize[i];
                            Assets[hashes[i]].CRC32 = crc[i];
                            Assets[hashes[i]].Compressed = c[i] > 0;
                        }
                    }

                    _changed = false;
                    return true;
                }
            }
            return false;
        }

        public bool Delete(PatcherAsset asset)
        {
            lock (Assets)
            {
                if (Assets.ContainsKey(asset.Hash) && DeleteAssetByHash(_mypPtr, asset.Hash))
                {
                    Assets.Remove(asset.Hash);
                    _changed = true;
                    return true;
                }
            }
            return false;
        }

        public byte[] GetAssetData(string name)
        {
            return GetAssetData(HashWAR(name));
        }

        public byte[] GetAssetData(long hash)
        {
            if (_dataChanges.ContainsKey(hash))
            {
                return _dataChanges[hash].Item2;
            }

            lock (Assets)
            {
                uint assetSize = GetAssetSizeByHash(_mypPtr, hash);
                if (assetSize > 0)
                {
                    byte[] data = new byte[(int)assetSize];
                    GetDecompressedAssetDataByHash(_mypPtr, hash, data);
                    return data;
                }
                return null;
            }
        }

        //public byte[] GetAssetDataRaw(long hash)
        //{
        //    lock (Assets)
        //    {
        //        var assetSize = GetAssetSizeCompressedByHash(_mypPtr, hash);
        //        if (assetSize > 0)
        //        {
        //            var data = new byte[(int)assetSize];
        //            GetAssetDataByHashRaw(_mypPtr, hash, data);
        //            return data;
        //        }
        //    }
        //    return null;
        //}

        public static long HashWAR(string s)
        {
            HashWAR(s, 0xDEADBEEF, out uint ph, out uint sh);
            return ((long)ph << 32) + sh;
        }

        public static void HashWAR(string s, uint seed, out uint ph, out uint sh)
        {
            uint edx = 0, eax, esi, ebx = 0;
            uint edi, ecx;

            eax = ecx = edx = ebx = esi = edi = 0;
            ebx = edi = esi = (uint)s.Length + seed;

            int i = 0;

            for (i = 0; i + 12 < s.Length; i += 12)
            {
                edi = (uint)((s[i + 7] << 24) | (s[i + 6] << 16) | (s[i + 5] << 8) | s[i + 4]) + edi;
                esi = (uint)((s[i + 11] << 24) | (s[i + 10] << 16) | (s[i + 9] << 8) | s[i + 8]) + esi;
                edx = (uint)((s[i + 3] << 24) | (s[i + 2] << 16) | (s[i + 1] << 8) | s[i]) - esi;

                edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
                esi += edi;
                edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
                edx += esi;
                esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
                edi += edx;
                ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
                esi += edi;
                edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
                ebx += esi;
                esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
                edi += ebx;
            }

            if (s.Length - i > 0)
            {
                switch (s.Length - i)
                {
                    case 12:
                        esi += (uint)s[i + 11] << 24;
                        goto case 11;
                    case 11:
                        esi += (uint)s[i + 10] << 16;
                        goto case 10;
                    case 10:
                        esi += (uint)s[i + 9] << 8;
                        goto case 9;
                    case 9:
                        esi += s[i + 8];
                        goto case 8;
                    case 8:
                        edi += (uint)s[i + 7] << 24;
                        goto case 7;
                    case 7:
                        edi += (uint)s[i + 6] << 16;
                        goto case 6;
                    case 6:
                        edi += (uint)s[i + 5] << 8;
                        goto case 5;
                    case 5:
                        edi += s[i + 4];
                        goto case 4;
                    case 4:
                        ebx += (uint)s[i + 3] << 24;
                        goto case 3;
                    case 3:
                        ebx += (uint)s[i + 2] << 16;
                        goto case 2;
                    case 2:
                        ebx += (uint)s[i + 1] << 8;
                        goto case 1;
                    case 1:
                        ebx += s[i];
                        break;
                }

                esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
                ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
                edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
                esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
                edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
                edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
                eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

                ph = edi;
                sh = eax;
                return;
            }
            ph = esi;
            sh = eax;
            return;
        }

        public static string GetExtension(byte[] buffer)
        {
            byte[] outbuffer = buffer;

            string file = "";

            if (buffer.Length > 0xFFFFFF)
            {
                return "";
            }

            int size = buffer.Length;
            if (size > 200)
            {
                size = 200;
            }

            file = System.Text.Encoding.ASCII.GetString(outbuffer, 0, size);
            char[] separators = { ',' };
            int commaNum = file.Split(separators, 10).Length;
            string header = System.Text.Encoding.ASCII.GetString(outbuffer, 0, 4);
            string ext = "txt";

            if (outbuffer[0] == 0 && outbuffer[1] == 1 && outbuffer[2] == 0)
            {
                ext = "ttf";
            }
            else if (outbuffer[0] == 0x0a && outbuffer[1] == 0x05 && outbuffer[2] == 0x01 && outbuffer[3] == 0x08)
            {
                ext = "pcx";
            }
            else if (header.IndexOf("PK") >= 0)
            {
                ext = "zip";
            }
            else if (header.IndexOf("SCPT") >= 0)
            {
                ext = "scpt";
            }
            else if (header[0] == '<')
            {
                ext = "xml";
            }
            else if (file.IndexOf("lua") >= 0 && file.IndexOf("lua") < 50)
            {
                ext = "lua";
            }
            else if (header.IndexOf("DDS") >= 0)
            {
                ext = "dds";
            }
            else if (header.IndexOf("XSM") >= 0)
            {
                ext = "xsm";
            }
            else if (header.IndexOf("XAC") >= 0)
            {
                ext = "xac";
            }
            else if (header.IndexOf("8BPS") >= 0)
            {
                ext = "8bps";
            }
            else if (header.IndexOf("bdLF") >= 0)
            {
                ext = "db";
            }
            else if (header.IndexOf("gsLF") >= 0)
            {
                ext = "geom";
            }
            else if (header.IndexOf("ID3") >= 0)
            {
                ext = "mp3";
            }
            else if (header.IndexOf("idLF") >= 0)
            {
                ext = "diffuse";
            }
            else if (header.IndexOf("psLF") >= 0)
            {
                ext = "specular";
            }
            else if (header.IndexOf("amLF") >= 0)
            {
                ext = "mask";
            }
            else if (header.IndexOf("ntLF") >= 0)
            {
                ext = "tint";
            }
            else if (header.IndexOf("lgLF") >= 0)
            {
                ext = "glow";
            }
            else if (file.IndexOf("Gamebry") >= 0)
            {
                ext = "nif";
            }
            else if (file.IndexOf("WMPHOTO") >= 0)
            {
                ext = "lmp";
            }
            else if (header.IndexOf("RIFF") >= 0)
            {
                string data = System.Text.Encoding.ASCII.GetString(outbuffer, 8, 4);
                if (data.IndexOf("WAVE") >= 0)
                {
                    ext = "wav";
                }
                else
                {
                    ext = "riff";
                }
            }
            else if (header.IndexOf("; Zo") >= 0)
            {
                ext = "zone.txt";
            }
            else if (header.IndexOf("\0\0\0\0") >= 0)
            {
                ext = "zero.txt";
            }
            else if (header.IndexOf("PNG") >= 0)
            {
                ext = "png";
            }
            else if (header.IndexOf("AMX") >= 0)
            {
                ext = "amx";
            }
            else if (header.IndexOf("SIDS") >= 0)
            {
                ext = "sids";
            } //SIDS
            else if (commaNum >= 10)
            {
                ext = "csv";
            }

            return ext;
        }
    }
}