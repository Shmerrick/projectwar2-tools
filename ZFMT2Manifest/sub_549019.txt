_DWORD *__thiscall sub_549019(char *this, void (__stdcall ***a2)(signed int))
{
  char *v2; // esi
  int v3; // edi
  void (__stdcall ***v4)(signed int); // ebx
  int *v5; // eax
  int v6; // esi
  _DWORD *result; // eax
  char DstBuf; // [esp+Ch] [ebp-214h]
  void (__stdcall ***v9)(signed int); // [esp+200h] [ebp-20h]
  int v10; // [esp+204h] [ebp-1Ch]
  char v11; // [esp+208h] [ebp-18h]
  char *v12; // [esp+20Ch] [ebp-14h]
  int v13; // [esp+210h] [ebp-10h]
  int v14; // [esp+21Ch] [ebp-4h]

  v2 = this;
  v12 = this + 4;
  sub_549323(&v13, (this + 4), &a2);
  if ( v13 == *(v2 + 2) )
  {
    v4 = a2;
    sprintf(&DstBuf, 0x1F4u, "%s/zone%03d.zmft", "data/manifests", a2);
    v5 = ReadMyp3(&a2, &DstBuf);
    v6 = *v5;
    *v5 = 0;
    v13 = v6;
    v14 = 0;
    if ( a2 )
      (**a2)(1);
    if ( !v6 )
      return 0;
    v13 = 0;
    v10 = v6;
    v3 = v6;
    v9 = v4;
    sub_54926E(&v11, v12, &v9);
    v14 = -1;
  }
  else
  {
    v3 = *(v13 + 16);
  }
  result = (*(*v3 + 52))(v3);
  if ( result )
  {
    *result = &ZoneManifest::`vftable';
    result[4] = result + 5;
  }
  return result;
}