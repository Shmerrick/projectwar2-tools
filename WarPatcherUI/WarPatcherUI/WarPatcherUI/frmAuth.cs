﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarPatcherUI
{
    public partial class frmAuth : MythicForm 
    {
        public override string _bgName { get; set; } = "authbackground";
        public override string _windowName { get; set; } = "authwindow";
        public bool LoggedIn { get; set; }
        public frmAuth()
        {
            InitializeComponent();

            InitResource(
                "WarPatcherUI.patch.txt",
                "WarPatcherUI.9420659275846915044.xml",
                "WarPatcherUI.patcher.png",
                "WarPatcherUI.ageofreckoning.ttf");
        }



        private void frmAuth_Load(object sender, EventArgs e)
        {
            alphaFormTransformer1.TransformForm2(255);
            txtName.TextBox.Focus();
            txtName.TextBox.Select();
            txtName.BackColor = Color.FromArgb(25, 25, 25);
            mTextBox1.BackColor = Color.FromArgb(25, 25, 25);

            txtName.ForeColor = Color.White;
            mTextBox1.ForeColor = Color.White;
            ActiveControl = txtName.TextBox;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void mButton1_Click(object sender, EventArgs e)
        {

        }

        private void frmAuth_Shown(object sender, EventArgs e)
        {
    
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            LoggedIn = true;
            Close();
        }

        private void frmAuth_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnPlay_Click(null, null);
            }
        }
    }
}
