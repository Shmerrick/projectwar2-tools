﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarPatcherUI.Types;

namespace WarPatcherUI
{
    public partial class frmAuth : MythicForm 
    {
        #region members
        private readonly Config Config;
        public override string _bgName { get; set; } = "authbackground";
        public override string _windowName { get; set; } = "authwindow";
        public bool LoggedIn { get; set; } = true;
        public LayeredWindowForm ContainerForm = null;
        public string Username { get; set; }
        public LOGIN_RESPONSE Response { get; private set; }
        #endregion

        public frmAuth(Config config = null)
        {
            InitializeComponent();
            Config = config;
        }

        #region methods
        private void frmAuth_Load(object sender, EventArgs e)
        {
            if (DesignMode)
                return;

              InitResource(
                @"WarPatcherUI.Resources.patch.txt",
                @"WarPatcherUI.Resources.layout.xml",
                @"WarPatcherUI.Resources.patcher.png",
                @"WarPatcherUI.Resources.ageofreckoning.ttf");

            ContainerForm = alphaFormTransformer1.TransformForm2(255);
            txtName.TextBox.Focus();
            txtName.TextBox.Select();
            txtName.BackColor = Color.FromArgb(25, 25, 25);
            txtPassword.BackColor = Color.FromArgb(25, 25, 25);

            txtName.ForeColor = Color.White;
            txtPassword.ForeColor = Color.White;
            ActiveControl = txtName.TextBox;
            Util.UIThread = TaskScheduler.FromCurrentSynchronizationContext();

            txtName.Text = Config.User;
            txtPassword.Text = Config.Password;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private async void btnCreate_Click(object sender, EventArgs e)
        {
            var acc = new frmNewAccount();

            if (acc.ShowDialog() == DialogResult.OK)
            {
                var response = await HttpUtil.RequestAsync<LOGIN_RESPONSE, LOGIN_CREATE>(Config.PatcherServer, new LOGIN_CREATE() {
                    Username = acc.Username,
                    Email = acc.Email,
                    Password = acc.Password,
                });

                Log.Info($"LOGIN_CREATE_RESPONSE:{response.Status}");

                if (response.Status != LoginStatus.OK)
                {
                    MessageBox.Show(response.Status.ToString(), "Error");
                }
            }
        }

        private void btnLogin_Click(object sender, EventArgs ee)
        {
            lblError.Text = "";

            if (txtName.Text.Trim().Length < 4)
            {
                lblError.Text = "Invalid user";
                Invalidate();
                txtName.Focus();
                return;
            }
            if (txtPassword.Text.Trim().Length < 4)
            {
                lblError.Text = "Invalid password";
                Invalidate();
                txtName.Focus();
                return;
            }

            btnLogin.Enabled = false;
            txtName.Enabled = false;
            txtPassword.Enabled = false;
            Username = txtName.Text;

            Task.Run(async () =>
            {
                try
                {
                    Response = await HttpUtil.RequestAsync<LOGIN_RESPONSE, LOGIN>(Config.PatcherServer, new LOGIN() { Username = txtName.Text, Password = txtPassword.Text });

                    Log.Info($"LOGIN_RESPONSE:{Response.Status}");

                    await Util.UITask((task) =>
                    {
                        if (Response.Status == LoginStatus.OK)
                        {
                            DialogResult = DialogResult.OK;
                            LoggedIn = true;
                            Close();
                        }
                        else
                        {
                            lblError.Text = Response.Status.ToString();
                            Invalidate();
                        }

                        btnLogin.Enabled = true;
                        txtName.Enabled = true;
                        txtPassword.Enabled = true;
                    });
                }
                catch (Exception e)
                {
                    Log.Exception(e.Message, e);
                    if (MessageBox.Show($"Unabled to connect to {Config.PatcherServer} Try Again?", "Error", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        btnLogin_Click(null, null);
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
            }).RunInBackground();
        }

        private void frmAuth_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(null, null);
            }
        }
        #endregion
    }
}
