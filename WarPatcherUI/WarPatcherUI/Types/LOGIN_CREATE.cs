﻿namespace WarPatcherUI.Types
{
    public class LOGIN_CREATE
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
