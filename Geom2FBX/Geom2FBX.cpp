// Geom2FBX.cpp : Defines the exported functions for the DLL.
//

#include "pch.h"
#include "framework.h"
#include "Geom2FBX.h"


// This is an example of an exported variable
GEOM2FBX_API int nGeom2FBX=0;

// This is an example of an exported function.
GEOM2FBX_API int fnGeom2FBX(void)
{
    return 0;
}

// This is the constructor of a class that has been exported.
CGeom2FBX::CGeom2FBX()
{
    return;
}
