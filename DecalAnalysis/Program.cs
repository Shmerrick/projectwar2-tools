﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using WindowsFormsApp1;

internal static class Program
{

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    public static void Main(string[] args)
    {
        Application.Run(new Form1());
    }
}

class Program2
{

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    //private static int Main(string[] args)
    //{
    //    //Program2.Main2(args);
    //    return 0;

    //    System.Console.WriteLine("[!] Warhammer Online Diffuse to Png conversion tool v0.0.1");
    //    System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com>");
    //    System.Console.WriteLine("[.]");

    //    //args = new string[2];
    //    //args[0] = @"D:\Games\Warhammer Online - Age of Reckoning\myp\assetdb\charmesh\fg.0.0.cre_bat_01.geom";
    //    //args[1] = @"D:\Games\Warhammer Online - Age of Reckoning\myp\assetdb\charmesh\fg.0.0.cre_bat_01.obj";

    //    if (args.Length <= 0 || args.Length > 2)
    //    {
    //        System.Console.WriteLine("[*] Error: Invalid number of parameters.");
    //        System.Console.WriteLine("[.]");

    //        return 1;
    //    }

    //    string destination = "";

    //    if (args.Length == 1)
    //    {
    //        if (args[0] == "/?" || args[0] == "/h" || args[0] == "/help" || args[0] == "-h" || args[0] == "-help" || args[0] == "--h" || args[0] == "--help" || args[0] == "help")
    //        {
    //            System.Console.WriteLine("[!] Converts Warhammer Online .diffuse to Wavefront .png");
    //            System.Console.WriteLine("[ ]");
    //            System.Console.WriteLine("[!] DIFFUSE2PNG source [destination]");
    //            System.Console.WriteLine("[ ]");
    //            System.Console.WriteLine("[!] source           Specifies the file to convert.");
    //            System.Console.WriteLine("[!] destination      Specifies the name of the new file.");
    //            System.Console.WriteLine("[ ]");
    //            System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com>");
    //            System.Console.WriteLine("[.]");
    //            return 0;
    //        }
    //        destination = Path.Combine(Path.GetPathRoot(args[0]), Path.GetFileNameWithoutExtension(args[0]) + ".png");
    //    }
    //    else
    //    {
    //        destination = args[1];
    //    }


    //    FileInfo fi = new FileInfo(args[0]);
    //    if (!fi.Exists)
    //    {
    //        System.Console.WriteLine("[*] Error: source does not exist.");
    //        System.Console.WriteLine("[.]");
    //        return 1;
    //    }
    //    else if (fi.Length > 8388608)
    //    {
    //        System.Console.WriteLine("[*] Error: source is larger than the 8388608 byte limit.");
    //        System.Console.WriteLine("[.]");
    //        return 1;
    //    }
    //    else if (fi.Length < 512)
    //    {
    //        System.Console.WriteLine("[*] Error: source is smaller than the 512 byte limit.");
    //        System.Console.WriteLine("[.]");
    //        return 1;
    //    }
    //    if (args[0] == destination)
    //    {
    //        System.Console.WriteLine("[*] Error: source and destination are the same.");
    //        System.Console.WriteLine("[.]");
    //        return 1;
    //    }

    //    using (FileStream fs = new FileStream(args[0], FileMode.Open, FileAccess.Read))
    //    {
    //        using (BinaryReader br = new BinaryReader(fs))
    //        {
    //            uint magic = br.ReadUInt32();
    //            if (magic != 0x464c6469 && magic != 0x69644c46)
    //            {
    //                System.Console.WriteLine("[*] Error: source is not a Warhammer Online .diffuse file.");
    //                System.Console.WriteLine("[.]");
    //                return 1;
    //            }
    //            uint version = br.ReadUInt32();
    //            if (version != 0x01)
    //            {
    //                System.Console.WriteLine("[*] Error: source is not a supported version.");
    //                System.Console.WriteLine("[.]");
    //                return 1;
    //            }
    //        }
    //    }

    //    MythicTexture texture = new MythicTexture();
    //    texture.Load(args[0]);

    //    bool done = false;
    //    int attempt = 0;
    //    while (!Directory.Exists(Path.GetPathRoot(destination)) && !done)
    //    {
    //        Directory.CreateDirectory(Path.GetPathRoot(destination));
    //        if (attempt++ > 8)
    //        {
    //            done = true;
    //        }
    //    }

    //    if (!Directory.Exists(Path.GetPathRoot(destination)))
    //    {
    //        System.Console.WriteLine("[*] Error: could not create the output folder.");
    //        System.Console.WriteLine("[.]");
    //        return 1;
    //    }

    //    texture.ColorTextureObject.MipMaps[(int)(texture.ColorTextureObject.ImageCount - 1)].Image.Save(destination, ImageFormat.Png);

    //    System.Console.WriteLine("[!] Export complete.");
    //    System.Console.WriteLine("[.]");

    //    return 0;
    //}
}
